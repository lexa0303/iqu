<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 28.02.17
 * Time: 12:10
 */
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use Bitrix\Main\Localization\Loc;

global $arUser;
if ($arUser["IS_AUTHOR"] != "Y") : ?>
    <section class="main-bot-btns__section">
        <div class="container">
            <div class="main-bot-btns__wrap">
                <a class="ripple button button--rounded main-bot-btns__link bot-btn-one js-auth <?/*link-disabled*/?>" href="<?=getLink("SHOP");?>">
                    <?=Loc::getMessage("MAIN_PAGE_SHOP");?>
                </a>
                <a class="ripple button button--rounded main-bot-btns__link bot-btn-two" href="<?=getLink("LIBRARY");?>">
                    <?=Loc::getMessage("MAIN_PAGE_FREE_LIBRARY");?>
                </a>
            </div>
        </div>
    </section>
<? else : ?>
    <section class="main-bot-btns__section">
        <div class="container">
            <div class="main-bot-btns__wrap">
                <a class="ripple button button--rounded main-bot-btns__link bot-btn-one" href="/personal/#stat">Статистика продаж</a>
            </div>
        </div>
    </section>
<? endif; ?>
