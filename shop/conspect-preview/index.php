<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
use Bitrix\Main\Localization\Loc;
$APPLICATION->SetTitle("Превью конспекта");
?>

    <section class="conspect-preview-wrap">
        <div class="container">
            <div class="exam__wrapper">
                <div class="row">
                    <div class="col-xs-12 col-lg-3 col-lg-push-9">
                        <section class="exam__navigation">
                            <div class="examNav-burger">
                                <img class="examNav-burger__icon" src="<?=SITE_TEMPLATE_PATH;?>/images/settings.svg" alt="">
                                <span class="examNav-burger__item"></span>
                                <span class="examNav-burger__item"></span>
                            </div>
                            <nav class="studying-nav">
                                <div class="studying-nav__conspect-info">
                                    <div class="studying-nav__user-photo"
                                         style="background-image: url(/local/templates/iq/images/кл.png)"></div>
                                    <div class="studying-nav__user-info">
                                        <span class="studying-nav__username">Валентин</span>
                                        <div class="studying-nav__user-info">
                                            <div class="studying-nav__user-watches">
                                                <span class="icon icon-views"></span>
                                                <span>12124</span>
                                            </div>
                                            <div class="studying-nav__user-loads">
                                                <span class="icon icon-download2"></span>
                                                <span>6 345</span>
                                            </div>
                                        </div>
                                        <?if ($arResult["MASTER"] != "Y") :?>
                                            <a class="studying-nav__btn-subscr js-unsubscribe js-sub" data-user_id="<?=$arResult["USER"]["ID"];?>" href="javascript:void(0);">
                                                <span class="icon icon-unsubscribe"></span><?=Loc::getMessage("UNSUBSCRIBE");?>
                                            </a>
                                            <a class="studying-nav__btn-subscr js-subscribe js-sub" data-user_id="<?=$arResult["USER"]["ID"];?>" href="javascript:void(0);">
                                                <span class="icon icon-subscribe"></span><?=Loc::getMessage("SUBSCRIBE");?>
                                            </a>
                                        <?endif;?>
                                    </div>
                                </div>

                                <div class="conspect-preview__buy-wrap">
                                    <div class="conspect-preview__buy-title">Стоимость конспекта:</div>
                                    <div class="conspect-preview__buy-price">9 999 $</div>
                                    <a href="#" class="conspect-preview__buy-btn button">Купить конспект</a>
                                </div>

                                <div class="studying-nav__share-wrap">
                                    <span class="studying-nav__share-title">Поделиться: </span>
                                    <a class="studying-nav__share-item js-share-fb" href="javascript:void(0);">
                                        <span class="icon icon-fb"></span>
                                    </a>
                                    <a class="studying-nav__share-item js-share-vk" href="javascript:void(0);">
                                        <span class="icon icon-vk"></span>
                                    </a>
                                    <a class="studying-nav__share-item js-share-gp" href="javascript:void(0);">
                                        <span class="icon icon-gp"></span>
                                    </a>
                                </div>
                            </nav>
                        </section>
                    </div>
                    <div class="col-xs-12 col-lg-9 col-lg-pull-3">
                        <div class="conspect-cards__wrapper">
                            <div class="section__heading section__heading--with-controls">
                                <span class="section__heading-txt">Конспект начинающего Лехи</span>
                            </div>
                            <div class="conspect-preview__heading">
                                <img src="../../local/templates/iq/images/main-fcard4.jpg" alt="" class="conspect-preview__img">
                                <div class="conspect-preview__info">
                                    <div class="conspect-preview__title">Аннотация автора</div>
                                    <div class="conspect-preview__text">
                                        Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus
                                    </div>
                                </div>
                            </div>
                            <article class="conspect-cards__content">
                                <?foreach ($arResult["CARDS"] as $arCard) : ?>
                                    <div class="conspect-cards__item">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="conspect-card__wrapper">
                                                    <div class="conspect-card__title"><?=Loc::getMessage("QUESTION");?></div>
                                                    <div class="conspect-card__body">
                                                        <div class="conspect-card__img">
                                                            <img src="<?=$arCard["UF_QUESTION_IMAGE"]["RESIZED"];?>" alt="">
                                                        </div>
                                                        <div class="conspect-card__text">
                                                            <span><?=$arCard["UF_QUESTION"];?></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="conspect-card__wrapper">
                                                    <div class="conspect-card__title"><?=Loc::getMessage("ANSWER");?></div>
                                                    <div class="conspect-card__body">
                                                        <div class="conspect-card__img">
                                                            <img src="<?=$arCard["UF_ANSWER_IMAGE"]["RESIZED"];?>" alt="">
                                                        </div>
                                                        <div class="conspect-card__text">
                                                            <span><?=$arCard["UF_ANSWER"];?></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?endforeach;?>
                            </article>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>