<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Магазин");
?>
<?php
$arLibraryFilter = array(

);
$arLibrarySort = array(

);
?>
<? $APPLICATION->IncludeComponent(
	"custom:shop", 
	".default", 
	array(
		"CACHE_TIME" => "36000",
		"CACHE_TYPE" => "N",
		"IBLOCK_ID" => "",
		"IBLOCK_TYPE" => "news",
		"COMPONENT_TEMPLATE" => ".default",
		"SEF_MODE" => "Y",
		"SEF_FOLDER" => "/shop/",
		"USER_TITLE" => $arUser["TITLE"],
		"CONSPECTS_HLBLOCK" => "6",
		"CATEGORIES_HLBLOCK" => "4",
		"CARDS_HLBLOCK" => "3",
		"CONSPECTS_HLIBLOCK" => "",
		"CATEGORY_CATEGORY_LIST_TITLE" => "Y",
		"CATEGORY_PAGEN_COUNT" => "5",
		"LIBRARY_PAGEN_COUNT" => "8",
		"CATEGORY_PAGEN_TEMPLATE" => "",
		"LIBRARY_PAGEN_TEMPLATE" => "infinity",
		"CATEGORY_SET_LIST_TITLE" => "Y",
		"MASTER_NEEDED" => array(
			0 => "",
		),
		"NO_AUTH_PAGES" => array(
			0 => "",
		),
		"SEF_URL_TEMPLATES" => array(
			"list" => "",
			"detail" => "#CONSPECT_CODE#/",
			"user" => "user/#USER_TITLE#/",
			"cart" => "cart/",
			"history" => "history/",
		)
	),
	false
); ?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>