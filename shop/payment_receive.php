<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 24.07.17
 * Time: 11:23
 */

if ($_SERVER["REQUEST_METHOD"] != "POST") die();
if (!$_REQUEST["signature"] || !$_REQUEST["data"]) die();

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
use Bitrix\Main\Loader;
Loader::includeModule("sale");
Loader::includeModule("catalog");

$cOrder = new CSaleOrder;

$file = $_SERVER["DOCUMENT_ROOT"] . "/payment_logs.txt";
AddMessage2File($_REQUEST, $file);

$private_key = "fDqc9fdC6C5PfmeAkgvxpkMmeKF3t42CFujSZQYK";

$sign = base64_encode( sha1(
    $private_key .
    $_REQUEST["data"] .
    $private_key
    , 1 ));

if ($sign != $_REQUEST["signature"]) die();

$data = json_decode(base64_decode($_REQUEST["data"]), true);

$order_id = $data["order_id"];

if (!$data["liqpay_order_id"]) die();
if ($order_id <= 0) { die(); }
if (!($arOrder = $cOrder->GetByID($order_id))) { die(); }
if ($arOrder['PAYED'] == 'Y') { die(); }

$data["status"] = "success";

if ($data["status"] == 'success') {
    $sDescription = '';
    $sStatusMessage = '';

    $sDescription .= 'sender phone: '.$data["sender_phone"].'; ';
    $sDescription .= 'amount: '.$data["amount"].'; ';
    $sDescription .= 'currency: '.$data["currency"].'; ';

    $sStatusMessage .= 'status: '.$data["status"].'; ';
    $sStatusMessage .= 'transaction_id: '.$data["transaction_id"].'; ';
    $sStatusMessage .= 'order_id: '.$data["order_id"].'; ';
    $sStatusMessage .= 'liqpay_order_id: '.$data["liqpay_order_id"].'; ';

    $arFields = array(
        'STATUS_ID' => 'F',
        'PS_STATUS' => 'Y',
        'PS_STATUS_CODE' => $status,
        'PS_STATUS_DESCRIPTION' => $sDescription,
        'PS_STATUS_MESSAGE' => $sStatusMessage,
        'PS_SUM' => $data["amount"],
        'PS_CURRENCY' => $data["currency"],
        'PS_RESPONSE_DATE' => date(CDatabase::DateFormatToPHP(CLang::GetDateFormat('FULL', LANG))),
    );

    $cOrder->PayOrder($arOrder['ID'], 'Y');
    $cOrder->Update($arOrder['ID'], $arFields);
    echo "Y";
}