<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "IQU");
$APPLICATION->SetPageProperty("keywords", "IQU");?>
<?use \Bitrix\Main\Localization\Loc;?>
<?$APPLICATION->SetTitle("IQU");?>
<?global $USER;?>
<?global $arUser;?>
<div class="container">

</div>
    <main>
        <section class="main-slider__section">
            <div class="main-slider__box">
                <div class="main-slider">
                    <?$APPLICATION->IncludeComponent("bitrix:advertising.banner", "main_page", Array(
                        "CACHE_TIME" => "0",	// Время кеширования (сек.)
                        "CACHE_TYPE" => "A",	// Тип кеширования
                        "NOINDEX" => "N",	// Добавлять в ссылки noindex/nofollow
                        "QUANTITY" => "5",	// Количество баннеров для показа
                        "TYPE" => "ADVERTISE_BANNERS",	// Тип баннера
                        "COMPONENT_TEMPLATE" => ".default"
                    ),
                        false
                    );?>
                </div>
            </div>
        </section>
        <section class="big-search__section">
            <div class="container">
                <form class="big-search__input-wrap" action="<?=SITE_DIR;?>search/">
                    <input name="q" class="big-search__input js-index_search" type="text" placeholder="<?=Loc::getMessage("MAIN_PAGE_SEARCH");?>">
                    <label class="big-search__submit-box" for="index-big-search">
                        <input class="big-search__submit" type="submit" value="" id="index-big-search">
                        <span class="icon icon-search big-search__submit-icon"></span>
                    </label>
                </form>
            </div>
        </section>
        <section class="big-square-btns__section">
            <div class="container">
                <div class="big-square-btns-wrap">
                    <? if ($USER->IsAuthorized()) : ?>
                        <a class="big-square-btn__link ripple" href="<?=getLink("MY_CONSPECTS");?>">
                            <span class="big-square-btn__txt"><?=Loc::getMessage("MAIN_PAGE_MY_CONSPECTS");?></span>
                        </a>
                        <a class="big-square-btn__link ripple" href="<?=getLink("CREATE_CONSPECT");?>">
                            <span class="big-square-btn__txt"><?=Loc::getMessage("MAIN_PAGE_CREATE_CONSPECT");?></span>
                        </a>
                        <a class="big-square-btn__link ripple" href="<?=getLink("LIBRARY");?>">
                            <span class="big-square-btn__txt"><?=Loc::getMessage("MAIN_PAGE_LIBRARY");?></span>
                        </a>
                        <a class="big-square-btn__link ripple js-auth <?/*link-disabled*/?>" href="<?=getLink("SHOP");?>">
                            <span class="big-square-btn__txt"><?=Loc::getMessage("MAIN_PAGE_SHOP");?></span>
                        </a>
                    <? else : ?>
                        <a class="big-square-btn__link ripple" href="<?=getLink("LIBRARY");?>">
                            <span class="big-square-btn__txt"><?=Loc::getMessage("MAIN_PAGE_LIBRARY");?></span>
                        </a>
                        <a class="big-square-btn__link ripple js-auth <?/*link-disabled*/?>" href="<?=getLink("SHOP");?>">
                            <span class="big-square-btn__txt"><?=Loc::getMessage("MAIN_PAGE_SHOP");?></span>
                        </a>
                        <a class="big-square-btn__link ripple js-auth" href="<?=getLink("CREATE_CONSPECT");?>">
                            <span class="big-square-btn__txt"><?=Loc::getMessage("MAIN_PAGE_CREATE_CONSPECT");?></span>
                        </a>
                    <? endif ;?>
                </div>
            </div>
        </section>
        <section class="main-section popular-shop__section">
            <div class="container">
                <h2 class="section__heading">
                    <span class="section__heading-txt">Популярное в библиотеке</span>
                </h2>
                <?php
                $arPopularFilter = array(
                    "UF_SHOW_IN_LIBRARY" => 1,
                );
                $arPopularSort = array(
                    "UF_VIEW_COUNT_MONTH" => "DESC"
                );
                ?>
                <?$APPLICATION->IncludeComponent(
                    "custom:library",
                    "index",
                    array(
                        "USER_TITLE" => $arUser["TITLE"],
                        "SEF_FOLDER" => "/",
                        "SEF_URL_TEMPLATES" => array(
                            "main_page" => "",
                            "conspect_add" => "conspects/add/",
                            "conspect_detail" => "conspects/#USER_TITLE#/#CONSPECT_ID#/",
                            "conspect_edit" => "conspects/#USER_TITLE#/#CONSPECT_ID#/edit/",
                            "conspect_list" => "conspects/#USER_TITLE#/",
                            "category_detail" => "category/#USER_TITLE#/#CATEGORY_ID#/",
                            "categories_list" => "categories/#USER_TITLE#/",
                            "exam" => "conspects/#USER_TITLE#/#CONSPECT_ID#/exam/",
                            "lesson" => "conspects/#USER_TITLE#/#CONSPECT_ID#/lesson/",
                            "search" => "search/",
                            "new" => "new/",
                            "exam_result" => "conspects/#USER_TITLE#/#CONSPECT_ID#/exam/#RESULT_ID#/",
                        ),
                        "COMPONENT_TEMPLATE" => ".default",
                        "CACHE_TYPE" => "N",
                        "CACHE_TIME" => "36000",
                        "MASTER" => "N",
                        "SET_TITLE" => "N",
                        "PAGEN_COUNT" => "4",
                        "PAGEN_TEMPLATE" => "",
                        "FILTER" => $arPopularFilter,
                        "SORT" => $arPopularSort
                    ),
                    false
                );?>
                <div class="section__more-btn-wrap">
                    <a class="section__more-btn" href="/library/?sort=popular"><?=Loc::getMessage("MORE_BUTTON");?></a>
                </div>
            </div>
        </section>
        <section class="main-section popular-shop__section">
            <div class="container">
                <h2 class="section__heading">
                    <span class="section__heading-txt">Новое в библиотеке</span>
                </h2>
                <?php
                $arNewFilter = array(
                    "UF_SHOW_IN_LIBRARY" => 1,
                );
                $arNewSort = array(
                    "UF_UPDATE_TIME" => "desc"
                );
                ?>
                <?$APPLICATION->IncludeComponent(
                    "custom:library",
                    "index",
                    array(
                        "USER_TITLE" => $arUser["TITLE"],
                        "SEF_FOLDER" => "/",
                        "SEF_URL_TEMPLATES" => array(
                            "main_page" => "",
                            "conspect_add" => "conspects/add/",
                            "conspect_detail" => "conspects/#USER_TITLE#/#CONSPECT_ID#/",
                            "conspect_edit" => "conspects/#USER_TITLE#/#CONSPECT_ID#/edit/",
                            "conspect_list" => "conspects/#USER_TITLE#/",
                            "category_detail" => "category/#USER_TITLE#/#CATEGORY_ID#/",
                            "categories_list" => "categories/#USER_TITLE#/",
                            "exam" => "conspects/#USER_TITLE#/#CONSPECT_ID#/exam/",
                            "lesson" => "conspects/#USER_TITLE#/#CONSPECT_ID#/lesson/",
                            "search" => "search/",
                            "new" => "new/",
                            "exam_result" => "conspects/#USER_TITLE#/#CONSPECT_ID#/exam/#RESULT_ID#/",
                        ),
                        "COMPONENT_TEMPLATE" => ".default",
                        "CACHE_TYPE" => "N",
                        "CACHE_TIME" => "36000",
                        "MASTER" => "N",
                        "SET_TITLE" => "N",
                        "PAGEN_COUNT" => "4",
                        "PAGEN_TEMPLATE" => "",
                        "FILTER" => $arNewFilter,
                        "SORT" => $arNewSort
                    ),
                    false
                );?>
                <div class="section__more-btn-wrap">
                    <a class="section__more-btn" href="/library/?sort=new"><?=Loc::getMessage("MORE_BUTTON");?></a>
                </div>
            </div>
        </section>
        <section class="main-section popular-shop__section">
            <div class="container">
                <h2 class="section__heading">
                    <span class="section__heading-txt">Популярное в магазине</span>
                </h2>
                <?php
                $arPopularShopFilter = array(
                    "UF_SHOP" => 1,
                );
                $arPopularShopSort = array(
                    "UF_VIEW_COUNT_MONTH" => "desc"
                );
                ?>
                <?$APPLICATION->IncludeComponent(
                    "custom:shop.list",
                    "index",
                    array(
                        "USER_TITLE" => $arUser["TITLE"],
                        "SEF_FOLDER" => "/shop/",
                        "SEF_URL_TEMPLATES" => array(
                            "list" => "",
                            "detail" => "#CONSPECT_CODE#/",
                            "user" => "user/#USER_TITLE#/",
                            "cart" => "cart/",
                            "history" => "history/",
                        ),
                        "COMPONENT_TEMPLATE" => ".default",
                        "CACHE_TYPE" => "N",
                        "CACHE_TIME" => "36000",
                        "MASTER" => "N",
                        "SET_TITLE" => "N",
                        "PAGEN_COUNT" => "4",
                        "PAGEN_TEMPLATE" => "",
                        "FILTER" => $arPopularShopFilter,
                        "SORT" => $arPopularShopSort
                    ),
                    false
                );?>
                <div class="section__more-btn-wrap">
                    <a class="section__more-btn" href="/shop/?sort=popular"><?=Loc::getMessage("MORE_BUTTON");?></a>
                </div>
            </div>
        </section>
        <section class="main-section popular-shop__section">
            <div class="container">
                <h2 class="section__heading">
                    <span class="section__heading-txt">Новое в магазине</span>
                </h2>
                <?php
                $arNewShopFilter = array(
                    "UF_SHOP" => 1,
                );
                $arNewShopSort = array(
                    "UF_UPDATE_TIME" => "desc"
                );
                ?>
                <?$APPLICATION->IncludeComponent(
                    "custom:shop.list",
                    "index",
                    array(
                        "USER_TITLE" => $arUser["TITLE"],
                        "SEF_FOLDER" => "/shop/",
                        "SEF_URL_TEMPLATES" => array(
                            "list" => "",
                            "detail" => "#CONSPECT_CODE#/",
                            "user" => "user/#USER_TITLE#/",
                            "cart" => "cart/",
                            "history" => "history/",
                        ),
                        "COMPONENT_TEMPLATE" => ".default",
                        "CACHE_TYPE" => "N",
                        "CACHE_TIME" => "36000",
                        "MASTER" => "N",
                        "SET_TITLE" => "N",
                        "PAGEN_COUNT" => "4",
                        "PAGEN_TEMPLATE" => "",
                        "FILTER" => $arNewShopFilter,
                        "SORT" => $arNewShopSort
                    ),
                    false
                );?>
                <div class="section__more-btn-wrap">
                    <a class="section__more-btn" href="/shop/?sort=new"><?=Loc::getMessage("MORE_BUTTON");?></a>
                </div>
            </div>
        </section>
<!--        --><?//$APPLICATION->IncludeComponent(
//            "custom:categories_list",
//            "main_page",
//            array(
//                "USER_TITLE" => "",
//                "COMPONENT_TEMPLATE" => "main_page",
//                "CACHE_TYPE" => "A",
//                "CACHE_TIME" => "36000",
//                "FILTER" => array(
//                    "UF_MAIN_PAGE" => 1
//                ),
//                "SEF_URL_TEMPLATES" => array(
//                    "main_page" => "",
//                    "conspect_add" => "conspects/add/",
//                    "conspect_detail" => "conspects/#USER_TITLE#/#CONSPECT_ID#/",
//                    "conspect_edit" => "conspects/#USER_TITLE#/#CONSPECT_ID#/edit/",
//                    "conspect_list" => "conspects/#USER_TITLE#/",
//                    "category_detail" => "category/#USER_TITLE#/#CATEGORY_ID#/",
//                    "categories_list" => "categories/#USER_TITLE#/",
//                    "exam" => "conspects/#USER_TITLE#/#CONSPECT_ID#/exam/",
//                    "lesson" => "conspects/#USER_TITLE#/#CONSPECT_ID#/lesson/",
//                    "search" => "search/",
//                    "new" => "new/",
//                    "exam_result" => "conspects/#USER_TITLE#/#CONSPECT_ID#/exam/#RESULT_ID#/",
//                ),
//                "MASTER" => "N",
//                "CONSPECT_COUNT" => 4
//            ),
//            false
//        );?>
        <? require($_SERVER["DOCUMENT_ROOT"] . "/include/shop_library_buttons.php"); ?>
    </main>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>