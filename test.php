<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 05.04.17
 * Time: 15:50
 */
?>
<div id="loader">
    <div class="loader"></div>
    <div class="block"></div>
</div>

<style>
    #loader {
        animation: spin 2s linear infinite;
        width: 120px;
        height: 120px;
        position: relative;
        display: flex;
        align-items: center;
        justify-content: center;
        margin: 20px;
    }
    .loader {
        border: 16px solid #f3f3f3; /* Light grey */
        border-top: 16px solid #3498db; /* Blue */
        border-radius: 50%;
        position: absolute;
        width: 100%;
        height: 100%;
        z-index: 1;
    }
    .block {
        position: absolute;
        width: 100%;
        height: 100%;
        border-radius: 50%;
        background: #fff;
        z-index: 2;
        animation: width 5s linear infinite;
    }

    @keyframes spin {
        0% { transform: rotate(0deg); }
        100% { transform: rotate(360deg); }
    }
    @keyframes width {
        0% {
            width: 90%;
            height: 90%;
        }
        50% {
            width: 130%;
            height: 130%;
        }
        100% {
            width: 90%;
            height: 90%;
        }
    }
</style>
<?/*
<div>
    <svg class="svg" height="200px" width="200px">
        <path class="path"
              fill="#FFFFFF"
              stroke="#a4e35d"
              stroke-width="4"
              stroke-miterlimit="10"
              d="
            M 100, 100
            m -96, 0
            a 96,96 0 1,0 192,0
            a 96,96 0 1,0 -192,0
            "></path>
        <style>
            .svg{
                transform: rotateZ(90deg) rotateX(180deg);
                animation: rotate 2s linear infinite;
            }
            .path {
                stroke-dasharray: 1000;
                stroke-dashoffset: 1000;
                animation: dash 2s linear infinite;
            }

            @keyframes rotate {
                0{
                    transform: rotateZ(90deg) rotateX(180deg);
                }
                49.999%{
                    transform: rotateZ(90deg) rotateX(180deg);
                }
                50%{
                    transform: rotateZ(90deg) rotateX(0deg);
                }
                100%{
                    transform: rotateZ(90deg) rotateX(0deg);
                }
            }

            @keyframes dash {
                0% {
                    stroke-dashoffset: 1000;
                }
                50% {
                    stroke-dashoffset: 0;
                }
                100% {
                    stroke-dashoffset: 1000;
                }
            }
        </style>
    </svg>
</div>
*/?>
<?/*
<div class="loader_circle-wrap">
    <div class="loader_cirlce-part">
        <div class="loader_circle-green_spinner"></div>
        <div class="center"></div>
    </div>
    <div class="loader_cirlce-part">
        <div class="loader_circle-white_spinner"></div>
        <div class="center"></div>
    </div>
</div>
<style>
    .loader_circle-wrap{
        height:100px;
        width:100px;
        position:relative;
    }
    .loader_cirlce-part{
        position:absolute;
        width:100%;
        height:100%;
    }
    .center {
        position:absolute;
        height:90%;
        top:5%;
        left:5%;
        width:90%;
        border-radius:100%;
        border-width: 0 0 0 50px;
        background-color:white;
        z-index:100;
    }
    .loader_circle-green_spinner {
        position: relative;
        display: inline-block;
        width: 50%;
        height: 100%;
        border: solid transparent;
        border-width: 0 0 0 50px;
        border-radius: 100%;
        outline: none;
        overflow: hidden;
        -webkit-animation: background1 10s linear infinite;
        animation: background1 10s linear infinite;
    }
    .loader_circle-green_spinner:before {
        content: "";
        position: absolute;
        left: -50%;
        height: 100%;
        border-right: 50px solid transparent;
        border-left: 50px solid green;
        border-radius: 100%;
        -webkit-animation: rotate1 10s linear infinite;
        animation: rotate1 10s linear infinite;
    }
    @-webkit-keyframes background1 {
        24.999% {border-width: 0 0 0 50px; border-color: transparent;}
        25% {border-width: 0 50px 0 0; border-color: green;}
        49.999%{opacity:1;}
        50% {opacity:0;}
        99.999%{opacity:0;}
        100% {border-width: 0 50px 0 0; border-color: green;}
    }
    @-webkit-keyframes rotate1 {
        24.999% {left: -50%;}
        25% {left: 0;}
        49.999%{opacity:1;}
        50% {opacity:0;}
        99.999%{opacity:0;}
        100% {-webkit-transform: rotate(720deg); left: 0;}
    }
    @keyframes background1 {
        24.999% {border-width: 0 0 0 50px; border-color: transparent;}
        25% {border-width: 0 50px 0 0; border-color: green;}
        49.999%{opacity:1;}
        50% {opacity:0;}
        99.999%{opacity:0;}
        100% {border-width: 0 50px 0 0; border-color: green;}
    }
    @keyframes rotate1 {
        24.999% {left: -50%;}
        25% {left: 0;}
        49.999%{opacity:1;}
        50% {opacity:0;}
        99.999%{opacity:0;}
        100% {transform: rotate(720deg); left: 0;}
    }


    .loader_circle-white_spinner {
        position: relative;
        display: inline-block;
        width: 50%;
        height: 100%;
        border: solid green;
        border-width: 0 0 0 50px;
        border-radius: 100%;
        outline: none;
        overflow: hidden;
        z-index:10;
        -webkit-animation: background 10s linear infinite;
        animation: background 10s linear infinite;
    }
    .loader_circle-white_spinner:before {
        content: "";
        position: absolute;
        left: -50%;
        height: 100%;
        border-left: 50px solid transparent;
        border-right: 50px solid green;
        border-radius: 100%;
        -webkit-animation: rotate 10s linear infinite;
        animation: rotate 10s linear infinite;
    }
    @-webkit-keyframes rotate {
        0% {opacity:0;}
        49.999%{opacity:0;}
        50% {opacity:1;}
        74.999% {left:-50%;}
        75% {left:0;}
        100% {-webkit-transform: rotate(720deg); left: 0;}

    }
    @-webkit-keyframes background {
        0% {opacity:0;}
        49.999%{opacity:0;}
        50% {opacity:1;}
        74.999% {border-width: 0 0 0 50px; border-color: green;}
        75%, 100% {border-width: 0 50px 0 0; border-color: transparent;}
    }
    @keyframes rotate {
        0% {opacity:0;}
        49.999%{opacity:0;}
        50% {opacity:1;}
        74.999% {left:-50%;}
        75% {left:0;}
        100% {transform: rotate(720deg); left: 0;}
    }
    @keyframes background {
        0% {opacity:0;}
        49.999%{opacity:0;}
        50% {opacity:1;}
        74.999% {border-width: 0 0 0 50px; border-color: green;}
        75%, 100% {border-width: 0 50px 0 0; border-color: transparent;}
    }
</style>