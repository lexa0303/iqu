<? use Bitrix\Main\Localization\Loc; ?>
<div class="footer__content">
    <?/*
    <a class="footer__content-link button js-skip-disabled" rel="nofollow" target="_blank" href="https://vk.com" title="<?= Loc::getMessage("FOOTER_VK"); ?>">
        <span class="icon icon-vk"></span>
    </a>
*/?>
    <a class="footer__content-link button js-skip-disabled <?/*link-disabled*/?>" rel="nofollow" target="_blank" href="https://facebook.com" title="<?= Loc::getMessage("FOOTER_FACEBOOK"); ?>">
        <span class="icon icon-fb"></span>
    </a>
    <a class="footer__content-link footer__content-link--about button js-skip-disabled"  href="/about/">
        <span class="footer__content-link--txt"><?= Loc::getMessage("FOOTER_ABOUT"); ?></span>
<!--        <span class="icon icon-about"></span>-->
    </a>
</div>
