<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use Bitrix\Main\Loader,
    Bitrix\Iblock;

Loader::includeModule("iblock");

$cUser = new CUser;
$dbUser = $cUser->GetList($by = array(), $order = array(), array("ID"=>$arParams["USER_ID"]));
$arrUser = $dbUser->Fetch();
$userID = $arrUser["ID"];

$arResult["CATEGORIES"] = Categories::GetAll($userID);
$arResult["USER"] = $arrUser;
$arResult["LINKS"] = array();

if ($_REQUEST["category_id"] > 0) {
    $arCategory = Categories::GetByID($_REQUEST["category_id"]);
    $arResult["CATEGORY"] = $arCategory;

    $arResult["LINKS"]["CANCEL"] = $arParams["SEF_FOLDER"] . $arParams["SEF_URL_TEMPLATES"]["category_detail"];
    $arResult["LINKS"]["CANCEL"] = str_replace("#USER_TITLE#", $arResult["USER"]["TITLE"], $arResult["LINKS"]["CANCEL"]);
    $arResult["LINKS"]["CANCEL"] = str_replace("#CATEGORY_ID#", $arCategory["ID"], $arResult["LINKS"]["CANCEL"]);
} else {
    $arResult["LINKS"]["CANCEL"] = $arParams["SEF_FOLDER"] . $arParams["SEF_URL_TEMPLATES"]["conspect_list"];
    $arResult["LINKS"]["CANCEL"] = str_replace("#USER_TITLE#", $arResult["USER"]["TITLE"], $arResult["LINKS"]["CANCEL"]);
}

?>
<? $this->IncludeComponentTemplate();
?>

