<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;

$this->setFrameMode(true);
?>
<? if (empty($arResult["ERRORS"])) : ?>
    <section class="user-conspects__head">
        <div class="container">
            <div class="row">
                <div class="user-conspects__head-wrap">
                    <div class="col-md-2 col-sm-3">
                        <div class="auther-conspects__photo-wrap"
                             style="background-image: url(<?= $arResult["USER"]["IMAGE"]; ?>)"></div>
                    </div>
                    <div class="col-md-10 col-sm-9">
                        <div class="user-conspects__title">
                            <?= Loc::getMessage("CONSPECT_DETAIL_ANOTHER_HEADING"); ?>
                            <span><?= $arResult["USER"]["TITLE"]; ?></span>
                        </div>
                        <a class="user-conspects__btn--back js-go_back" href="javascript:void(0);">
                            <span class="user-conspects__icon-back"></span>
                            <span><?= Loc::getMessage("GO_BACK"); ?></span>
                        </a>
                        <div class="user-conspects__info">
                            <div class="user-conspects__info-numbers">
                                <div class="user-conspects__info-title"><?= Loc::getMessage("CONSPECT_DETAIL_ANOTHER_CONSPECT_COUNT"); ?></div>
                                <span class="user-conspects__info-icon icon icon-conspect"></span>
                                <span class="user-conspects__ammount"><?= $arResult["COUNT_CONSPECTS"]; ?></span>
                            </div>
                            <div class="user-conspects__info-numbers">
                                <div class="user-conspects__info-title"><?= Loc::getMessage("CONSPECT_DETAIL_ANOTHER_DOWNLOADS_COUNT"); ?></div>
                                <span class="user-conspects__info-icon icon icon-download2"></span>
                                <span class="user-conspects__ammount"><?= $arResult["COUNT_DOWNLOADS"]; ?></span>
                            </div>
                            <div class="user-conspects__info-numbers">
                                <div class="user-conspects__info-title"><?= Loc::getMessage("CONSPECT_DETAIL_ANOTHER_SUBSCRIBERS_COUNT"); ?></div>
                                <span class="user-conspects__info-icon icon icon-users"></span>
                                <span class="user-conspects__ammount"><?= $arResult["USER"]["COUNT_SUBSCRIBERS"]; ?></span>
                            </div>
                        </div>
                        <div class="my-cards__header">
                            <div class="my-cards__tabs-wrapper">
                                <ul class="main-tabs">
                                    <li class="main-tabs__item">
                                        <a class="main-tabs__link active" href="javascript:void(0)"
                                           data-tabanchor="Conspects">Конспекты</a>
                                    </li>
                                    <li class="main-tabs__item">
                                        <a class="main-tabs__link" href="javascript:void(0)" data-tabanchor="Courses">Курсы</a>
                                    </li>
                                    <li class="main-tabs__item">
                                        <a class="main-tabs__link" href="javascript:void(0)"
                                           data-tabanchor="aboutAuthor">Об авторе</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="auth-shoop__content container">
        <article class="main-tabs__content" data-tabanchor="Conspects" style="display: block;">
            <div class="fcards__wrapper row">
                <? foreach ($arResult["ITEMS"] as $arConspect) : ?>
                    <? $APPLICATION->IncludeComponent(
                        "custom:conspect_card_shop",
                        "shop",
                        Array(
                            "CACHE_TIME"         => $arParams["CACHE_TIME"],
                            "CACHE_TYPE"         => $arParams["CACHE_TYPE"],
                            "CARDS_HLBLOCK"      => "4",
                            "CATEGORIES_HLBLOCK" => "3",
                            "CONSPECTS_HLIBLOCK" => "6",
                            "CONSPECT_ID"        => $arConspect["ID"],
                            "SEF_MODE"           => $arParams["SEF_URL_TEMPLATES"],
                            "SEF_FODLER"         => $arParams["SEF_FOLDER"],
                            "MASTER"             => "N",
                        )
                    ); ?>
                <? endforeach; ?>
            </div>
        </article>
        <script>
        var test = <?=Bitrix\Main\Web\Json::encode($arResult);?>;

        console.log(test);
        </script>
        <article class="main-tabs__content" data-tabanchor="Courses" style="display: none;">
            <? foreach ($arResult["CATEGORIES"] as $arCategory) : ?>
                <div class="row">
                    <div class="col-md-offset-2 col-md-10">
                        <div class="courses__conspect-row">
                            <div class="row">
                                <div class="col-sm-7 col-md-7 col-lg-6">
                                    <div class="courses-conspect__desc">
                                        <div class="courses-conspects__title"><?=$arCategory["UF_NAME"];?></div>
                                        <div class="courses-count__conspects"><?=$arCategory["COUNT_CONSPECTS"];?></div>
                                        <div class="conspect-preview__buy-buttons">
                                            <div class="courses__button-holder">
                                                <div class="courses-conspects__title order"><?=$arCategory["PRICE_TRIAL_FORMATTED"];?></div>
                                                <a href="javascript:void(0);"
                                                   onclick='shop_buy_lots(<?=Bitrix\Main\Web\Json::encode($arCategory["CONSPECTS"]);?>, true);'
                                                   class="button button--rounded categories-listing__btn--create-conspect">взять в аренду</a>
                                            </div>
                                            <div class="courses__button-holder">
                                                <div class="courses-conspects__title buy"><?=$arCategory["PRICE_BUY_FORMATTED"];?></div>
                                                <a href="javascript:void(0);"
                                                   onclick='shop_buy_lots(<?=Bitrix\Main\Web\Json::encode($arCategory["CONSPECTS"]);?>, false)'
                                                   class="categories-listing__btn--create-category button">выкупить навсегда</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4 col-md-3 col-lg-2">
                                    <div class="conspect__block">
                                        <div class="conspect__block-img" style="background-image: url(<?=$arCategory["PREVIEW_CONSPECT"]["UF_IMAGE"]["RESIZED"];?>);"></div>
                                        <div class="conspect__block-title"><?=$arCategory["PREVIEW_CONSPECT"]["UF_NAME"];?></div>
                                        <a class="conspect__block-link" href="#">еще <?=(count($arCategory["CONSPECTS"]) -1);?>...</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <? endforeach; ?>
        </article>
        <article class="main-tabs__content" data-tabanchor="aboutAuthor" style="display: none;">
            <div class="row">
                <div class="col-md-offset-2 col-md-10">
                    <div class="auther-conspects__about-title">Об авторе</div>
                    <div class="shop-conspect__desc"><?= $arResult["USER"]["ADMIN_NOTES"]; ?></div>
                </div>
            </div>
        </article>
    </section>

    <script>
        /*START main-tabs*/
        var mtTab = body.find('.main-tabs__link'); // main-tabs tab
        var mtTabActive = body.find('.main-tabs__link.active'); // main-tabs active tab
        var mtTabAttr = mtTabActive.attr('data-tabAnchor'); //attr of active tab
        var mtTabContent = body.find('.main-tabs__content');
        var mtTabActiveContent = body.find('.main-tabs__content[data-tabAnchor=' + mtTabAttr + ']');
        mtTabActiveContent.show(); //load active tab(first) content when document is ready
        mtTab.on('click', function () {
            mtTab.removeClass('active'); //removing class 'active' from all tabs
            $(this).addClass('active'); //adding class 'active' to current tab
            var mtTabAttr = $(this).attr('data-tabAnchor'); //locally define new value
            var mtTabActiveContent = body.find('.main-tabs__content[data-tabAnchor=' + mtTabAttr + ']'); //locally define new value
            mtTabContent.hide(); //hiding all other tabs
            mtTabActiveContent.fadeIn(); //showing current active tab content
        });
        /*END main-tabs*/
    </script>
<? else : ?>
    <div class="container">
        <? foreach ($arResult["ERRORS"] as $error): ?>
            <div style="font-size: 24px; padding: 20px; text-align: center;">
                <?= $error; ?>
            </div>
        <? endforeach; ?>
    </div>
<? endif; ?>
<? require($_SERVER["DOCUMENT_ROOT"] . "/include/shop_library_buttons.php"); ?>

