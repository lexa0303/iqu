<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use Bitrix\Main\Loader,
    Bitrix\Main\Localization\Loc,
    Bitrix\Iblock;

Loader::includeModule("highloadblock");
$сache = Bitrix\Main\Data\Cache::createInstance();
CPageOption::SetOptionString("main", "nav_page_in_session", "N");

$cUser = new CUser;
$dbUser = $cUser->GetList($by = array(), $order = array(), array("TITLE"=>$arParams["USER_TITLE"]));
$arrUser = $dbUser->Fetch();
$arrUser["IMAGE"] = LenalHelp::img($arrUser["PERSONAL_PHOTO"], 200, 200);
$userID = $arrUser["ID"];
if (!$userID) {
    $arResult["ERRORS"][] = "Пользователь не найден";
} else {
    if ($_POST["ajax"] != "Y" && $arParams["CACHE_TYPE"] != "N" && $сache->initCache(
            1800,
            'shop' . $_REQUEST["PAGEN_1"] . "-" . SITE_ID . md5(serialize($arParams["FILTER"])) . "-" . md5(serialize($arParams["SORT"])),
            '/shop/' . $userID . '/list/')
    ) {
        $arResult = $сache->getVars();
    } elseif ($сache->startDataCache()) {

        $arResult = array();

        $dbUsers = $cUser->GetList($by = array(), $order = array(), array("UF_SUBSCRIBED" => $userID));
        while ($res = $dbUsers->Fetch())
            $arUsers[] = $res;

        $arResult["USER"] = $arrUser;
        $arResult["USER"]["COUNT_SUBSCRIBERS"] = count($arUsers);

        $arFilter = array(
            "UF_SHOP" => 1,
            "UF_ACTIVE" => 1
        );
        $arSort = array(
            "UF_UPDATE_TIME" => "DESC"
        );

        if (is_array($arParams["FILTER"]))
            $arFilter = array_merge($arFilter, $arParams["FILTER"]);

        if (is_array($arParams["SORT"]))
            $arSort = array_merge($arParams["SORT"], $arSort);

        $dbConspects = Conspects::GetAll($userID, $arFilter, false, $arSort);

        if ($dbConspects) {
            $dbConspects = LenalHelp::makePagination($dbConspects, $arParams["PAGEN_COUNT"], $arParams["PAGEN_TEMPLATE"]);

            while ($res = $dbConspects->Fetch()) {
                $arResult["ITEMS"][] = $res;
            }

            $arResult["NAV_STRING"] = LenalHelp::$pagintaionStr;

            foreach ($arResult["ITEMS"] as &$conspect) {
                makeShopConspectDetails($conspect, SITE_DIR . "/shop/", $arParams["SEF_URL_TEMPLATES"]);

                $arResult["COUNT_DOWNLOADS"] += (int)$conspect["UF_SAVES_COUNT"];
            }

            $arResult["COUNT_CONSPECTS"] = count($arResult["ITEMS"]);
        }

        $arResult["CATEGORIES"] = array();
        $arCategories = Categories::GetAll($userID);

        foreach ($arCategories as $arCategory) {
            if (!empty($arCategory["UF_CONSPECTS"])){
                $arFilter = array(
                    "ID" => $arCategory["UF_CONSPECTS"],
                    "UF_SHOP" => 1,
                    "UF_ACTIVE" => 1
                );
                $arConspects = Conspects::GetAll($userID, $arFilter);
                if ($arConspects) {
                    $arCategory["PREVIEW_CONSPECT"] = $arConspects[0];
                    if ($arCategory["PREVIEW_CONSPECT"]["UF_IMAGE"]["ID"] > 0){
                        $img = CFile::ResizeImageGet(
                            $arCategory["PREVIEW_CONSPECT"]["UF_IMAGE"]["ID"],
                            array(
                                "height" => 200,
                                "width" => 200
                            ),
                            BX_RESIZE_IMAGE_PROPORTIONAL
                        );
                        $arCategory["PREVIEW_CONSPECT"]["UF_IMAGE"]["RESIZED"] = $img["src"];
                    }
                    $price = 0;
                    $currency = Bitrix\Currency\CurrencyManager::getBaseCurrency();
                    foreach ($arConspects as $arConspect) {
                        $price += $arConspect["UF_SHOP_PRICE"];
                        $arCategory["CONSPECTS"][] = array(
                            "ID" => $arConspect["ID"],
                            "TRIAL_ID" => $arConspect["UF_TRIAL_SHOP_ID"],
                            "BUY_ID" => $arConspect["UF_SHOP_CONSPECT_ID"]
                        );
                    }
                    $count = count($arConspects);
                    if ($count == 1) {
                        $arCategory["COUNT_CONSPECTS"] = Loc::getMessage("CONSPECT_AMOUNT_1", array("#COUNT#" => $count));
                    } elseif ($count <= 4) {
                        $arCategory["COUNT_CONSPECTS"] = Loc::getMessage("CONSPECT_AMOUNT_2", array("#COUNT#" => $count));
                    } else {
                        $arCategory["COUNT_CONSPECTS"] = Loc::getMessage("CONSPECT_AMOUNT_5", array("#COUNT#" => $count));
                    }
                    $arCategory["PRICE_TRIAL"] = $price;
                    $arCategory["PRICE_TRIAL_FORMATTED"] = CCurrencyLang::CurrencyFormat($arCategory["PRICE_TRIAL"], $currency);
                    $arCategory["PRICE_BUY"] = $price * PRICE_COEF;
                    $arCategory["PRICE_BUY_FORMATTED"] = CCurrencyLang::CurrencyFormat($arCategory["PRICE_BUY"], $currency);
                    $arResult["CATEGORIES"][] = $arCategory;
                }
            }
        }


//     ...
        if ($isInvalid) {
            $cache->abortDataCache();
        }
//     ...
//
        $сache->endDataCache($arResult);
    }
}
?>
<? $this->IncludeComponentTemplate();
?>

