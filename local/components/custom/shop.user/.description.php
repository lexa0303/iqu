<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
    "NAME" => 'Библиотека',
    "DESCRIPTION" => 'Список конспектов всех пользователей',
    "ICON" => "",
    "CACHE_PATH" => "Y",
    "PATH" => array(
        "ID" => "utility",
    ),
);
?>