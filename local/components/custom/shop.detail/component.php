<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Loader;

Loader::includeModule("highloadblock");
$cache = Bitrix\Main\Data\Cache::createInstance();

$cUser = new CUser;

if ($arParams["CACHE_TYPE"] != "N" && $cache->initCache($arParams['CACHE_TIME'], 'shop_detail' . $arParams["CONSPECT_CODE"] . SITE_ID, '/shop/detail/')) {
    $arResult = $cache->getVars();
} elseif ($cache->startDataCache()) {

    $arResult = Conspects::GetByCode($arParams["CONSPECT_CODE"], true);
    if ($arResult == false)
        $arResult = Conspects::GetByID($arParams["CONSPECT_CODE"]);

    if ($arResult == false)
        $arResult = array(
            "ERRORS" => array(
                "Конспект не найден",
            ),
        );

    if (empty($arResult["ERRORS"])) :
        if ($arResult["USER"]["PERSONAL_PHOTO"] > 0) {
            $arResult["USER"]["IMAGE"] = array(
                "SRC"     => CFile::GetPath($arResult["USER"]["PERSONAL_PHOTO"]),
                "RESIZED" => LenalHelp::img($arResult["USER"]["PERSONAL_PHOTO"], 75, 75),
            );
        }

        makeShopConspectDetails($arResult, $arParams["SEF_FOLDER"], $arParams["SEF_URL_TEMPLATES"]);

        $arCardsFilter = array(
            "ID" => $arResult["UF_CARDS"],
        );
        $arResult["CARDS"] = Cards::GetAll($arCardsFilter, true, false, 3);

        $el = new CIBlockElement;

        $arFilter = array(
            "ID" => array(
                $arResult["UF_SHOP_CONSPECT_ID"],
                $arResult["UF_TRIAL_SHOP_ID"],
            ),
        );

        $rsElements = $el->GetList(
            array(),
            $arFilter,
            false,
            false,
            array("ID", "CATALOG_GROUP_1")
        );

        while ($res = $rsElements->Fetch()) {
            if ($res["ID"] == $arResult["UF_SHOP_CONSPECT_ID"]) {
                $arResult["BUY"] = array(
                    "ID"              => $res["ID"],
                    "PRICE"           => $res["CATALOG_PRICE_1"],
                    "PRICE_FORMATTED" => CCurrencyLang::CurrencyFormat($res["CATALOG_PRICE_1"], $res["CATALOG_CURRENCY_1"]),
                );
            }

            if ($res["ID"] == $arResult["UF_TRIAL_SHOP_ID"]) {
                $arResult["TRIAL"] = array(
                    "ID"              => $res["ID"],
                    "PRICE"           => $res,
                    "PRICE_FORMATTED" => CCurrencyLang::CurrencyFormat($res["CATALOG_PRICE_1"], $res["CATALOG_CURRENCY_1"]),
                );
            }
        }

        /* CATEGORY CONSPECTS */
        $arCategory = Categories::GetAll(
            $arResult["UF_USER_ID"],
            array(
                "UF_CONSPECTS" => $arResult["ID"],
            )
        );

        $arCategoryConspects = array();

        foreach ($arCategory[0]["UF_CONSPECTS"] as $conspect) {
            if ($conspect != $arResult["ID"]) {
                $arCategoryConspectsIDs[] = $conspect;
            }
        }

        $arCategoryConspects = Conspects::GetAll(
            '',
            array(
                "ID"        => $arCategoryConspectsIDs,
                "UF_SHOP"   => 1,
                "UF_ACTIVE" => 1,
            )
        );

        foreach ($arCategoryConspects as $arCategoryConspect) {
            $ids[] = $arCategoryConspect["ID"];
        }

        if (!empty($arCategoryConspects)) {
            $arResult["CATEGORY_CONSPECTS"] = $ids;
        }

        /* CATEGORY CONSPECTS */
    endif;

    // ...
    if ($isInvalid) {
        $cache->abortDataCache();
    }
    // ...


    $cache->endDataCache($arResult);
}


$arResult["MASTER"] = $arParams["MASTER"];

$APPLICATION->SetTitle($arResult["UF_NAME"]);

if ($_SESSION["VISITED_CONSPECTS"][$arResult["ID"]] != "Y" && $arResult["USER"]["ID"] != $USER->GetID()) {
    $_SESSION["VISITED_CONSPECTS"][$arResult["ID"]] = "Y";
    Conspects::AddViewCount($arResult["ID"]);
}

?>
<? $this->IncludeComponentTemplate(); ?>

