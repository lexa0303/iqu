<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use Bitrix\Main\Loader,
    Bitrix\Iblock;

Loader::includeModule("iblock");
Loader::includeModule("highloadblock");

$cUser = new CUser;
$dbUser = $cUser->GetList($by = array(), $order = array(), array("TITLE"=>$arParams["USER_TITLE"]));
$arrUser = $dbUser->Fetch();
$userID = $arrUser["ID"];

$arResult = Conspects::GetByCode($arParams["CONSPECT_ID"], $userID);
if ($arResult == false)
    $arResult = Conspects::GetByID($arParams["CONSPECT_ID"]);

if ($arResult == false) {
    $redirectUrl = str_replace("#USER_TITLE#", $arParams["USER_TITLE"], $arParams["SEF_FOLDER"] . $arParams["SEF_URL_TEMPLATES"]["conspect_list"]);
    $сache = Bitrix\Main\Data\Cache::createInstance();
    $сache->cleanDir('/conspects/' . $arParams["USER_TITLE"] . "/");
    LocalRedirect($redirectUrl);
}

if (empty($arResult["ERRORS"])) :

    makeConspectDetails($arResult, $arParams["SEF_URL_TEMPLATES"]);

    /* CHECK IF CONSPECT WAS PURCHASED AND EXPIRED */
    if ($arResult["UF_ORIGIN_CONSPECT"]) {
        $arOriginConspect = Conspects::GetByID($arResult["UF_ORIGIN_CONSPECT"]);
        if ($arOriginConspect["UF_SHOP"]){
            $AvailableConspects = new HL("sys_available_conspects");

            $arAvailable = $AvailableConspects->GetList(
                array(
                    "filter" => array(
                        "UF_USER_ID" => $arResult["UF_USER_ID"],
                        "UF_CONSPECT" => $arResult["ID"]
                    )
                )
            )->Fetch();

            LocalRedirect($arResult["DETAIL_PAGE_URL"]);
        }
    }

    $arResult["USER"] = $arrUser;

    if ($arResult["USER"]["PERSONAL_PHOTO"] > 0){
        $arResult["USER"]["IMAGE"] = array(
            "SRC" => CFile::GetPath($arResult["USER"]["PERSONAL_PHOTO"]),
            "RESIZED" => LenalHelp::img($arResult["USER"]["PERSONAL_PHOTO"], 75, 75)
        );
    }

    $arCardsFilter = array(
        "ID" => $arResult["UF_CARDS"]
    );
    $arResult["CARDS"] = Cards::GetAll($arCardsFilter, true, true);

    $arResult["CATEGORIES"] = Categories::GetAll($userID);

    $arResult["CATEGORY"] = Categories::GetAll($userID, array("UF_CONSPECTS"=>$arResult["ID"]))[0];

    $arResult["LINKS"] = array();

    $arResult["LINKS"]["CANCEL"] = $arParams["SEF_FOLDER"] . $arParams["SEF_URL_TEMPLATES"]["conspect_detail"];
    $arResult["LINKS"]["CANCEL"] = str_replace("#USER_TITLE#", $arResult["USER"]["TITLE"], $arResult["LINKS"]["CANCEL"]);
    $arResult["LINKS"]["CANCEL"] = str_replace("#CONSPECT_ID#", $arResult["UF_CODE"], $arResult["LINKS"]["CANCEL"]);

endif;
?>
<? $this->IncludeComponentTemplate();
?>

