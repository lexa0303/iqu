<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use Bitrix\Main\Localization\Loc;
$this->setFrameMode(true);
?>
<a class="modal__btn-close icon icon-cross" href="javascript:void(0)"></a>
<form class="download-conspect__body ajax_submit">
    <input type="hidden" name="user_id" value="<?= $arResult["USER_ID"]; ?>">
    <input type="hidden" name="conspect_id" value="<?= $arResult["CONSPECT_ID"]; ?>">
    <input type="hidden" name="action" value="save_conspect">
    <span class="download-conspect__heading"><?=Loc::getMessage("MODAL_DOWNLOAD_CONSPECT_HEADING");?></span>
    <span class="download-conspect__text"><?=Loc::getMessage("MODAL_DOWNLOAD_CONSPECT_TEXT");?></span>
    <div class="download-conspect__selection selection-group modal__fieldset">
        <label class="conspect-choose__descr" for="dCategorySelect"><?=Loc::getMessage("INPUT_HEADING_CHOOSE_CATEGORY");?></label>
        <select required name="category_id" class="download-conspect__select selection__item" id="dCategorySelect">
            <option value=""><?=Loc::getMessage("CONSPECT_SAVE_CHOOSE_CATEGORY");?></option>
            <? foreach ($arResult["CATEGORIES"] as $arCategory) : ?>
                <option value="<?=$arCategory["ID"];?>"><?=$arCategory["UF_NAME"];?></option>
            <?endforeach;?>
            <option value="create_category_modal"><?=Loc::getMessage("CREATE_CONSPECT_CREATE_CATEGORY");?></option>
        </select>
    </div>
    <button class="download-conspect__btn download-conspect__btn--submit button button--rounded" href=""><?=Loc::getMessage("SAVE");?></button>
    <a class="download-conspect__btn download-conspect__btn--dismiss" href=""><?=Loc::getMessage("CANCEL");?></a>
</form>