<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use Bitrix\Main\Localization\Loc;
$this->setFrameMode(true);
global $arUser;
?>
<section class="personal">
    <div class="container">
        <section class="personal-info">
            <div class="row">
                <div class="col-md-3">
                    <div class="personal-info--left">
                        <div class="section__heading">
                            <span class="section__heading-txt"><?=Loc::getMessage("PERSONAL_HEADING");?></span>
                        </div>
                        <form class="ajax_submit" id="personal_form">
                            <?=bitrix_sessid_post();?>
                            <input type="hidden" name="user_id" value="<?=$arResult["ID"];?>">
                            <input type="hidden" name="action" value="personal_edit">
                            <div class="change-photo__wrapper">
                                <div class="js-personal_image change-photo__img <?=($arResult["PHOTO"]) ? "triggered" : "";?>"
                                     style="background-image: url(<?=$arResult["PHOTO"];?>);"></div>
                                <fieldset>
                                    <label class="change-photo__trigger-wrapper js-change_personal_image <?=($arResult["PHOTO"]) ? "triggered" : "";?>"
                                           for="personal_photo">
                                        <span class="change-photo__trigger-icon icon icon-photo"></span>
                                        <span class="change-photo__trigger-add"><?=Loc::getMessage("PERSONAL_ADD_PHOTO");?></span>
                                        <span class="change-photo__trigger-refresh"><?=Loc::getMessage("PERSONAL_CHANGE_PHOTO");?></span>
                                    </label>
                                    <input style="display:none;"
                                           class="change-photo__loader"
                                           type="text"
                                           id="personal_photo"
                                           name="photo">
                                </fieldset>
                            </div>
                            <div class="personal-info__change">
                                <div class="personal-info__login-wrapper">
                                    <span class="personal-info__login-descr"><?=Loc::getMessage("PERSONAL_LOGIN_HEADING");?></span>
                                    <span class="personal-info__login"><?=$arResult["LOGIN"];?></span>
                                </div>
                                <fieldset class="personal-data__group">
                                    <label class="personal-data__descr" for="personal_title"><?=Loc::getMessage("PERSONAL_TITLE_HEADING");?></label>
                                    <input class="personal-data__change"
                                           name="title"
                                           type="text"
                                           id="personal_title" value="<?=$arResult["TITLE"];?>">
                                </fieldset>
                                <fieldset class="personal-data__group">
                                    <label class="personal-data__descr"
                                           for="personal_city"><?=Loc::getMessage("PERSONAL_CITY_HEADING");?></label>
                                    <input class="personal-data__change"
                                           type="text"
                                           name="city"
                                           id="personal_city" value="<?=$arResult["PERSONAL_CITY"];?>">
                                </fieldset>
                                <fieldset class="personal-data__group">
                                    <label class="personal-data__descr"
                                           for="personal_email"><?=Loc::getMessage("PERSONAL_EMAIL_HEADING");?></label>
                                    <input class="personal-data__change"
                                           type="email"
                                           name="email"
                                           id="personal_email"
                                           placeholder="<?=Loc::getMessage("PLACEHOLDER_EMAIL");?>"
                                           value="<?=$arResult["EMAIL"];?>">
                                </fieldset>
                                <fieldset class="personal-data__group">
                                    <label class="personal-data__descr"
                                           for="personal_phone"><?=Loc::getMessage("PERSONAL_PHONE_HEADING");?></label>
                                    <input class="personal-data__change js-phone_mask"
                                           type="tel"
                                           name="phone"
                                           value="<?=$arResult["PERSONAL_PHONE"];?>"
                                           id="personal_phone"
                                           placeholder="<?=Loc::getMessage("PLACEHOLDER_PHONE");?>">
                                </fieldset>
                            </div>
                            <input class="personal-data__btn--save"
                                   type="submit"
                                   value="<?=Loc::getMessage("SAVE");?>"
                                   title="<?=Loc::getMessage("SAVE_CHANGES");?>">
                        </form>
                    </div>
                </div>
                <div class="col-md-offset-2 col-md-6">
                    <div class="personal-info--right">
                        <div class="section__heading">
                            <span class="section__heading-txt"><?=Loc::getMessage("ACCOUNT");?></span>
                        </div>
                        <div class="personal-info__descr"><?=Loc::getMessage("PERSONAL_ACCOUNT_INFO");?></div>
                        <? if ($arUser["TURBO"] == "Y") : ?>
                            <a class="pro-btn active" href="javascript:void(0);">pro</a>
                            <span class="pro-btn__descr"><?=Loc::getMessage("PRO_ACCOUNT_ENABLED");?></span>
                        <? else : ?>
                            <a class="pro-btn js-shop_select"
                               data-conspect_id="<?=TURBO_CONSPECT_ID;?>"
                               href="javascript:void(0);">pro</a>
                        <? endif; ?>
                        <section class="personal-data--password">
                            <div class="section__heading">
                                <span class="section__heading-txt"><?=Loc::getMessage("PASSWORD");?></span>
                            </div>
                            <button class="personal-data__btn--pass reset-pass-modal-call open-modal"
                                    data-modal="change-pass-modal"
                                    type="button"
                                    title="Вызвать форму изменения пароля"><?=Loc::getMessage("CHANGE_PASSWORD");?></button>
                        </section>
                        <section class="personal-data--socials">
                            <div class="section__heading">
                                <span class="section__heading-txt"><?=Loc::getMessage("PERSONAL_SOCIALS_HEADING");?></span>
                            </div>
                            <?$APPLICATION->IncludeComponent("bitrix:socserv.auth.split",
                                "personal",
                                Array(
                                    "SHOW_PROFILES" => "Y",	// Показывать объединенные профили
                                    "ALLOW_DELETE" => "Y",	// Разрешить удалять объединенные профили
                                ),
                                false
                            );?>
                        </section>
                    </div>
                </div>
            </div>
        </section>

        <section class="personal-stats">
            <div class="section__heading">
                <span class="section__heading-txt"><?=Loc::getMessage("PERSONAL_STAT_HEADING");?></span>
            </div>
            <div class="personal-stats__wrapper">
                <div class="personal-stats__item">
                    <span class="personal-stats__title"><?=Loc::getMessage("PERSONAL_CONSPECT_COUNT");?></span>
                    <div class="personal-stats__descr">
                        <span class="personal-stats__icon icon icon-conspect"></span>
                        <span class="personal-stats__ammount"><?=$arResult["COUNT_CONSPECTS"];?></span>
                    </div>
                </div>
                <div class="personal-stats__item">
                    <span class="personal-stats__title"><?=Loc::getMessage("PERSONAL_CATEGORY_COUNT");?></span>
                    <div class="personal-stats__descr">
                        <span class="personal-stats__icon icon icon-category"></span>
                        <span class="personal-stats__ammount"><?=$arResult["COUNT_CATEGORIES"];?></span>
                    </div>
                </div>
                <div class="personal-stats__item">
                    <span class="personal-stats__title"><?=Loc::getMessage("PERSONAL_SAVES_COUNT");?></span>
                    <div class="personal-stats__descr">
                        <span class="personal-stats__icon icon icon-download2"></span>
                        <span class="personal-stats__ammount"><?=$arResult["COUNT_SAVES"];?></span>
                    </div>
                </div>
                <div class="personal-stats__item">
                    <span class="personal-stats__title"><?=Loc::getMessage("PERSONAL_SUBSCRIBERS_COUNT");?></span>
                    <div class="personal-stats__descr">
                        <span class="personal-stats__icon icon icon-users"></span>
                        <span class="personal-stats__ammount"><?=$arResult["COUNT_SUBSCRIBERS"];?></span>
                    </div>
                </div>
            </div>
        </section>
        <section class="personal__logout">
<!--            <div class="section__heading">-->
<!--                <span class="section__heading-txt">--><?//=Loc::getMessage("LOGIN_EXIT_ACCOUNT");?><!--</span>-->
<!--            </div>-->
            <a href="<?=SITE_DIR;?>?logout=yes" class="personal__logout-btn" type="button"><?=Loc::getMessage("LOGIN_EXIT");?></a>
        </section>
    </div>
</section>
<div class="modal" id="change-pass-modal">
    <a class="modal__btn-close icon icon-cross" href="javascript:void(0)"></a>
    <form class="new-pass-modal__body ajax_submit">
        <span class="new-pass-modal__heading"><?=Loc::getMessage("MODAL_NEW_PASSWORD_HEADING");?></span>
        <span class="new-pass-modal__text"><?=Loc::getMessage("MODAL_NEW_PASSWORD_TEXT");?></span>
        <input type="hidden" name="user_id" value="<?=$arResult["ID"];?>">
        <input type="hidden" name="action" value="change_password">
        <fieldset class="modal__fieldset">
            <label class="modal__label" for="change_pass_old_password"><?=Loc::getMessage("INPUT_HEADING_OLD_PASSWORD");?></label>
            <input class="modal__input" required name="old_password" id="change_pass_old_password" type="password" placeholder="<?=Loc::getMessage("PLACEHOLDER_PASSWORD");?>">
        </fieldset>
        <fieldset class="modal__fieldset">
            <label class="modal__label" for="change_pass_password"><?=Loc::getMessage("INPUT_HEADING_NEW_PASSWORD");?></label>
            <input class="modal__input" required name="password" id="change_pass_password" type="password" placeholder="<?=Loc::getMessage("PLACEHOLDER_PASSWORD");?>">
        </fieldset>
        <fieldset class="modal__fieldset">
            <label class="modal__label" for="change_pass_password_repeat"><?=Loc::getMessage("INPUT_HEADING_PASSWORD_REPEAT");?></label>
            <input class="modal__input" required name="password_repeat" id="change_pass_password_repeat" type="password" placeholder="<?=Loc::getMessage("PLACEHOLDER_PASSWORD");?>">
        </fieldset>
        <button style="margin-top: 20px;" class="button button--rounded new-pass-modal__btn" href="javascript:void(0)"><?=Loc::getMessage("SAVE");?></button>
        <a class="new-pass-modal__btn--txt" href="javascript:void(0)"><?=Loc::getMessage("CANCEL");?></a>
    </form>
</div>
<script>
    (function() {
        var params = {
            input_file: document.querySelector("#personal_photo"),
            input_label: document.querySelector(".js-change_personal_image"),
            image: document.querySelector(".js-personal_image"),
            show_preview: true,
            personal: true
        };

        var personalImageCropper = new imageCropper(params);
        personalImageCropper.Init();
    })();
</script>