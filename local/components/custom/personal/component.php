<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use Bitrix\Main\Loader,
    Bitrix\Iblock;

Loader::includeModule("iblock");

global $USER;

$userID = $USER->GetID();
if ($userID) {
    $dbUser = $USER->GetList($by = array(), $order = array(), array("ID" => $userID));
    $arrUser = $dbUser->Fetch();

    if ($arrUser["PERSONAL_PHOTO"] > 0)
        $arrUser["PHOTO"] = CFile::GetPath($arrUser["PERSONAL_PHOTO"]);

    $arResult = $arrUser;

    $arConspects = Conspects::GetAll($userID, array("UF_ORIGIN_USER"=>false));
    $arResult["COUNT_CONSPECTS"] = count($arConspects);

    foreach ($arConspects as $arConspect){
        $arResult["COUNT_SAVES"] += $arConspect["UF_SAVES_COUNT"];
    }

    $arCategories = Categories::GetAll($userID);
    $arResult["COUNT_CATEGORIES"] = count($arCategories);

    $dbSubscribers = $USER->GetList($by = array(), $order = array(), array("UF_SUBSCRIBED"=>$userID));

    $countSubs = 0;
    while ($res = $dbSubscribers->Fetch())
        $countSubs++;

    $arResult["COUNT_SUBSCRIBERS"] = $countSubs;
} else {
    LocalRedirect(SITE_DIR);
}

?>
<? $this->IncludeComponentTemplate();
?>

