<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
    "NAME" => 'Личный кабинет',
    "DESCRIPTION" => 'Редактирование личных данных',
    "ICON" => "",
    "CACHE_PATH" => "Y",
    "PATH" => array(
        "ID" => "utility",
    ),
);
?>