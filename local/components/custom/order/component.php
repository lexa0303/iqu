<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use Bitrix\Main\Loader,
    Bitrix\Sale,
    Bitrix\Iblock;

Loader::includeModule("iblock");
Loader::includeModule("sale");
Loader::includeModule("catalog");

$arResult = array();

$basket = Sale\Basket::loadItemsForFUser(Sale\Fuser::getId(), Bitrix\Main\Context::getCurrent()->getSite());

$arResult["PRICE"] = $basket->getPrice();
$arResult["PRICE_FORMATTED"] = CCurrencyLang::CurrencyFormat($arResult["PRICE"], Bitrix\Currency\CurrencyManager::getBaseCurrency());

foreach ($basket->getBasketItems() as $basketItem){
    $arBasketItem = $basketItem->getFieldValues();
    $arBasketItem["PROPERTIES"] = $basketItem->getPropertyCollection()->getPropertyValues();

    if ($arBasketItem["PROPERTIES"]["CONSPECT_ID"]["VALUE"] > 0) {
        $arBasketItem["CONSPECT"] = Conspects::GetByID($arBasketItem["PROPERTIES"]["CONSPECT_ID"]["VALUE"]);
        $arItem = $arBasketItem;
        $arResult["ITEMS"][] = $arItem;
    }
}

?>
<? $this->IncludeComponentTemplate();
?>

