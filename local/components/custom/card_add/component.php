<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use Bitrix\Main\Loader,
    Bitrix\Iblock;

Loader::includeModule("iblock");

$arResult["CONSPECT_ID"] = $arParams["CONSPECT_ID"];
$arResult["USER_ID"] = $arParams["USER_ID"];

?>
<? $this->IncludeComponentTemplate();
?>

