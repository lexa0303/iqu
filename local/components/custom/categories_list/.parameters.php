<?
if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)die();
CModule::IncludeModule("iblock");
CModule::IncludeModule("highloadblock");
$dbHLBlock = \Bitrix\Highloadblock\HighloadBlockTable::getList(array());
while ($res = $dbHLBlock->fetch())
    $arHLBlocks[$res["ID"]] = $res["NAME"];

$arComponentParameters = array(
    'PARAMETERS' => array(
        'CONSPECTS_HLIBLOCK' => Array(
            'PARENT' => 'BASE',
            'NAME' => 'ID инфоблока конспектов',
            'TYPE' => 'LIST',
            'VALUES' => $arHLBlocks,
            'DEFAULT' => '',
            'ADDITIONAL_VALUES' => 'Y',
            'REFRESH' => 'Y',
        ),
        "CATEGORIES_HLBLOCK" => Array(
            'PARENT' => 'BASE',
            'NAME' => 'ID HL-блока категорий',
            'TYPE' => 'LIST',
            'VALUES' => $arHLBlocks,
            'DEFAULT' => '',
            'ADDITIONAL_VALUES' => 'Y',
            'REFRESH' => 'Y',
        ),
        "CARDS_HLBLOCK" => Array(
            'PARENT' => 'BASE',
            'NAME' => 'ID HL-блока карточек',
            'TYPE' => 'LIST',
            'VALUES' => $arHLBlocks,
            'DEFAULT' => '',
            'ADDITIONAL_VALUES' => 'Y',
            'REFRESH' => 'Y',
        ),
        'USER_TITLE' => Array(
            'PARENT' => 'BASE',
            'NAME' => 'Никнейм пользователя',
            'TYPE' => 'TEXT',
            'DEFAULT' => '',
            "MULTIPLE" => "N",
        ),
        'PAGEN_COUNT' => Array(
            'PARENT' => 'BASE',
            'NAME' => 'Количество категорий на странице',
            'TYPE' => 'TEXT',
            'DEFAULT' => '',
            "MULTIPLE" => "N",
        ),
        'PAGEN_TEMPLATE' => Array(
            'PARENT' => 'BASE',
            'NAME' => 'Шаблон постраничной навигации',
            'TYPE' => 'TEXT',
            'DEFAULT' => '.default',
            "MULTIPLE" => "N",
        ),
        "SEF_MODE" => array(
            "main_page" => array(
                "NAME" => "Главная страница",
                "DEFAULT" => "",
                "VARIABLES" => array(
                ),
            ),
            "conspect_detail" => array(
                "NAME" => "Детальная страница конспекта",
                "DEFAULT" => "conspects/#USER_TITLE#/#CONSPECT_ID#/",
                "VARIABLES" => array(
                    "CONSPECT_ID",
                    "USER_TITLE"
                ),
            ),
            "conspect_edit" => array(
                "NAME" => "Редактирование конспекта",
                "DEFAULT" => "conspects/#USER_TITLE#/#CONSPECT_ID#/edit/",
                "VARIABLES" => array(
                    "CONSPECT_ID",
                    "USER_TITLE"
                ),
            ),
            "conspect_add" => array(
                "NAME" => "Добавление конспекта",
                "DEFAULT" => "conspects/#USER_TITLE#/add/",
                "VARIABLES" => array(
                    "USER_TITLE"
                ),
            ),
            "conspect_list" => array(
                "NAME" => "Список конспектов",
                "DEFAULT" => "conspects/#USER_TITLE#/",
                "VARIABLES" => array(
                    "USER_TITLE"
                ),
            ),
            "category_detail" => array(
                "NAME" => "Детальная страница категории",
                "DEFAULT" => "category/#USER_TITLE#/#CATEGORY_ID#/",
                "VARIABLES" => array(
                    "CATEGORY_ID",
                    "USER_TITLE"
                ),
            ),
            "categories_list" => array(
                "NAME" => "Список категорий",
                "DEFAULT" => "category/#USER_TITLE#/",
                "VARIABLES" => array(
                    "USER_TITLE"
                ),
            ),
            "exam" => array(
                "NAME" => "Екзамен по конспекту",
                "DEFAULT" => "conspects/#USER_TITLE#/#CONSPECT_ID#/exam/",
                "VARIABLES" => array(
                    "CONSPECT_ID",
                    "USER_TITLE"
                ),
            ),
            "lesson" => array(
                "NAME" => "Урок по конспекту",
                "DEFAULT" => "conspects/#USER_TITLE#/#CONSPECT_ID#/lesson/",
                "VARIABLES" => array(
                    "CONSPECT_ID",
                    "USER_TITLE"
                ),
            ),
        ),
        'CACHE_TIME'  =>  array('DEFAULT'=>36000),
    ),
);
?>