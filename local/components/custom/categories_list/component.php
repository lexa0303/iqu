<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use Bitrix\Main\Loader,
    Bitrix\Main\Localization\Loc,
    Bitrix\Iblock;

Loader::includeModule("highloadblock");
$сache = Bitrix\Main\Data\Cache::createInstance();
CPageOption::SetOptionString("main", "nav_page_in_session", "N");

$cUser = new CUser;
$dbUser = $cUser->GetList($by = array(), $order = array(), array("TITLE"=>$arParams["USER_TITLE"]));
$arrUser = $dbUser->Fetch();
$userID = $arrUser["ID"];

if (!$userID){
    LocalRedirect(SITE_DIR);
}

if ($arParams["CACHE_TYPE"] != "N" && $сache->initCache($arParams['CACHE_TIME'], 'categories_list' . $userID . SITE_ID . serialize($arParams["FILTER"]) . $_REQUEST["PAGEN_1"], '/categories/' . $userID . '/list/')) {
    $arResult = $сache->getVars();
} elseif ($сache->startDataCache()) {
    $arResult = array();

    if (!$arParams["PAGEN_COUNT"])
        $arParams["PAGEN_COUNT"] = 5;
    if (!$arParams["PAGEN_TEMPLATE"])
        $arParams["PAGEN_TEMPLATE"] = ".default";

    $arrFilter = (is_array($arParams["FILTER"])) ? $arParams["FILTER"] : array();

    $arSort = array(
        "UF_NAME" => "asc"
    );

    if (is_array($arParams["SORT"]))
        $arSort = array_merge($arSort, $arParams["SORT"]);

    $dbCategories = Categories::GetAll($userID, $arrFilter, false, $arSort);

    if ($dbCategories) {
        $dbCategories = LenalHelp::makePagination($dbCategories, $arParams["PAGEN_COUNT"], $arParams["PAGEN_TEMPLATE"]);

        while ($res = $dbCategories->Fetch())
            $arResult["CATEGORIES"][] = $res;

        $arResult["NAV_STRING"] = LenalHelp::$pagintaionStr;

        foreach ($arResult["CATEGORIES"] as &$arCategory) {
            $arCategory["USER"] = CUser::GetByID($arCategory["UF_USER_ID"])->Fetch();

            makeCategoryDetails($arCategory, $arParams["SEF_URL_TEMPLATES"], $arParams["MASTER"]);

            $arConspectsIDs = Categories::GetConspects($arCategory["ID"]);

            $arConspects = Conspects::GetAll(
                $userID,
                array(
                    "ID" => $arConspectsIDs,
                    "COUNT" => 4,
                    "UF_ACTIVE" => 1
                )
            );

            foreach ($arConspects as &$conspect) {
                makeConspectDetails($conspect, $arParams["SEF_URL_TEMPLATES"]);

                $arCategory["CONSPECTS"][] = $conspect;
            }
        }
    } else {
        $arResult["ERROR"] = "У вас нет категорий";
    }

    // ...
    if ($isInvalid) {
        $cache->abortDataCache();
    }
    // ...

    $сache->endDataCache($arResult);
}

global $APPLICATION;

if ($arParams["SET_TITLE"] == "Y") :
    if ($arParams["MASTER"] == "Y")
        $APPLICATION->SetTitle(Loc::getMessage("META_SELF_CATEGORIES_LIST"));
    else
        $APPLICATION->SetTitle(Loc::getMessage("META_USER_CATEGORIES_LIST", array("#USER_TITLE#" => $arrUser["TITLE"])));
endif;

?>
<? $this->IncludeComponentTemplate(); ?>