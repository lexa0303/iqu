<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

CPageOption::SetOptionString("main", "nav_page_in_session", "N");

$arAllowed = array(
    "library", "shop", "users",
);

$where = ($_GET["w"]) ? $_GET["w"] : "library";

if (!in_array($where, $arAllowed)) {
    $where = "library";
}

if (!$USER->IsAuthorized()){
    $where = "library";
}

if ($_GET["q"]) {
    switch ($where) :
        case "shop":
            $arResult = array();
            $dbConspects = Conspects::Search($_GET["q"], true);
            if ($dbConspects) {
                $dbConspects = LenalHelp::makePagination($dbConspects, $arParams["PAGEN_COUNT"], $arParams["PAGEN_TEMPLATE"]);
                $arResult["NAV_STRING"] = LenalHelp::$pagintaionStr;
                while ($res = $dbConspects->Fetch()) {
                    $arResult["ITEMS"][] = $res;
                }
            } else {
                $arResult["ERROR"] = true;
            }
            break;
        case "users":
        case "library":
        default:
            $arResult = array();
            $dbConspects = Conspects::Search($_GET["q"]);
            if ($dbConspects) {
                $dbConspects = LenalHelp::makePagination($dbConspects, $arParams["PAGEN_COUNT"], $arParams["PAGEN_TEMPLATE"]);
                $arResult["NAV_STRING"] = LenalHelp::$pagintaionStr;
                while ($res = $dbConspects->Fetch()) {
                    $arResult["ITEMS"][] = $res;
                }
            } else {
                $arResult["ERROR"] = true;
            }
            break;
    endswitch;
} else {
    $arResult["ERROR"] = true;
}

$arResult["WHERE"] = $where;
?>
<? $this->IncludeComponentTemplate();
?>

