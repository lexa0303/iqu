<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use Bitrix\Main\Localization\Loc;
global $APPLICATION;
$this->setFrameMode(true);
$APPLICATION->SetTitle(Loc::getMessage("SEARCH_HEADING"));
?>
<!--<article class="search-results__heading">-->
<!--    <div class="container">-->
<!--        <div class="section__heading">-->
<!--            <span class="section__heading-txt">--><?//=Loc::getMessage("SEARCH_HEADING");?><!--</span>-->
<!--        </div>-->
<!--    </div>-->
<!--</article>-->
<section class="big-search__section">
    <div class="container">
        <form class="big-search__input-wrap" action="<?=SITE_DIR;?>search/">
            <input class="big-search__input"
                   type="text"
                   name="q"
                   value="<?=$_GET["q"];?>"
                   placeholder="<?=Loc::getMessage("SEARCH_PLACEHOLDER");?>">
            <label class="big-search__submit-box" for="index-big-search">
                <input class="big-search__submit" type="submit" value="" id="index-big-search">
                <span class="icon icon-search big-search__submit-icon"></span>
            </label>
            <div class="exam-settings__kind">
                <div class="exam-settings__kind-wrapper">
                    <input value="library"
                           name="w"
                           <? if ($arResult["WHERE"] == "library")
                               echo "checked";?>
                           class="exam-settings__kind-input"
                           type="radio"
                           id="library">
                    <label class="exam-settings__kind-label" for="library">Библиотека</label>
                </div>
                <div class="exam-settings__kind-wrapper">
                    <input value="shop"
                           name="w"
                           <? if (!$USER->IsAuthorized())
                               echo "disabled";?>
                            <? if ($arResult["WHERE"] == "shop")
                                echo "checked";?>
                           class="exam-settings__kind-input"
                           type="radio"
                           id="shop">
                    <label class="exam-settings__kind-label" for="shop">Магазин</label>
                </div>
                <div class="exam-settings__kind-wrapper">
                    <input value="users"
                           disabled=""
                           name="w"
                            <? if ($arResult["WHERE"] == "users")
                                echo "checked";?>
                           class="exam-settings__kind-input"
                           type="radio"
                           id="users">
                    <label class="exam-settings__kind-label" for="users">Пользователи</label>
                </div>
            </div>
        </form>
    </div>
</section>
<?if (count($arResult["ITEMS"])) : ?>
    <section class="search-results__global">
        <div class="container js-pagination_container">
            <?if ($_POST["ajax"] == "Y")
                $APPLICATION->RestartBuffer();?>
            <div class="fcards__wrapper row">
                <? foreach ($arResult["ITEMS"] as $arConspect) : ?>
                    <?$APPLICATION->IncludeComponent(
                        ($arResult["WHERE"] == "shop") ? "custom:conspect_card_shop" : "custom:conspect_card",
                        ($arResult["WHERE"] == "shop") ? "shop" : "",
                        Array(
                            "CACHE_TIME" => $arParams["CACHE_TIME"],
                            "CACHE_TYPE" => $arParams["CACHE_TYPE"],
                            "CARDS_HLBLOCK" => "4",
                            "CATEGORIES_HLBLOCK" => "3",
                            "CONSPECTS_HLIBLOCK" => "6",
                            "CONSPECT_ID" => $arConspect["ID"],
                            "SEF_MODE" => $arParams["SEF_URL_TEMPLATES"],
                            "MASTER" => "N",
                        )
                    );?>
                <?endforeach;?>
            </div>
            <?=$arResult["NAV_STRING"];?>
            <?if ($_POST["ajax"] == "Y")
                die();?>
        </div>
    </section>
<? else : ?>
    <section class="search-results__global">
        <div class="container">По данному запросу ничего не найдено.</div>
    </section>
<?endif;?>