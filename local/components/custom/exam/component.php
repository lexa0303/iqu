<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use Bitrix\Main\Loader,
    Bitrix\Iblock;

Loader::includeModule("highloadblock");

$cUser = new CUser;
$dbUser = $cUser->GetList($by = array(), $order = array(), array("TITLE"=>$arParams["USER_TITLE"]));
$arrUser = $dbUser->Fetch();
$userID = $arrUser["ID"];

global $arUser;

$arResult = Conspects::GetByCode($arParams["CONSPECT_ID"], $userID);
if (!$arResult)
    $arResult = Conspects::GetByID($arParams["CONSPECT_ID"]);

$arResult["TURBO"] = $arUser["TURBO"];

makeConspectDetails($arResult, $arParams["SEF_URL_TEMPLATES"]);

/* CHECK IF CONSPECT WAS PURCHASED AND EXPIRED */
if ($arResult["UF_ORIGIN_CONSPECT"]) {
    $arOriginConspect = Conspects::GetByID($arResult["UF_ORIGIN_CONSPECT"]);
    if ($arOriginConspect["UF_SHOP"]){
        $AvailableConspects = new HL("sys_available_conspects");

        $arAvailable = $AvailableConspects->GetList(
            array(
                "filter" => array(
                    "UF_USER_ID" => $arResult["UF_USER_ID"],
                    "UF_CONSPECT" => $arResult["UF_ORIGIN_CONSPECT"]
                )
            )
        )->Fetch();

        if (!$arAvailable) {
//            LocalRedirect($arResult["DETAIL_PAGE_URL"]);
        }
    }
}

if ($arResult["USER"]["PERSONAL_PHOTO"] > 0){
    $arResult["USER"]["IMAGE"] = array(
        "SRC" => CFile::GetPath($arResult["USER"]["PERSONAL_PHOTO"]),
        "RESIZED" => LenalHelp::img($arResult["USER"]["PERSONAL_PHOTO"], 75, 75)
    );
}

if ($arResult["ORIGIN_USER"]["PERSONAL_PHOTO"] > 0){
    $arResult["ORIGIN_USER"]["IMAGE"] = array(
        "SRC" => CFile::GetPath($arResult["ORIGIN_USER"]["PERSONAL_PHOTO"]),
        "RESIZED" => LenalHelp::img($arResult["ORIGIN_USER"]["PERSONAL_PHOTO"], 75, 75)
    );
}

$arResult["CONSPECTS"] = Conspects::GetAll($userID);

foreach ($arResult["CONSPECTS"] as &$arConspect) {
    makeConspectDetails($arConspect, $arParams["SEF_URL_TEMPLATES"]);

    if ($arConspect["ID"] == $arResult["ID"])
        $arConspect["ACTIVE"] = "Y";
}

if ($arParams["CARDS_NEEDED"] == "Y") {
    $arCardsFilter = array(
        "ID" => $arResult["UF_CARDS"]
    );
    if ($_REQUEST["nonstudied"] == 1){
        $arCardsFilter["UF_STUDIED"] = false;
    }
    $arResult["CARDS"] = Cards::GetAll($arCardsFilter);
    Exams::MakeAnswers($arResult["CARDS"], $arParams["ANSWERS_COUNT"]);
    if ($_REQUEST["random"]) {
        shuffle_assoc($arResult["CARDS"]);
    }
} else {
    $cache = Bitrix\Main\Data\Cache::createInstance();
    if ($arParams["CACHE_TYPE"] != "N" && $cache->initCache($arParams['CACHE_TIME'], $userID . '_exam-settings' . SITE_ID, '/exam_settings/')) {
        $arResult["EXAMS"] = $cache->getVars();
    } elseif ($cache->startDataCache()) {
        $arExams = Exams::GetExamTypes();
        foreach ($arExams as &$arExam) {
            $arExam["SETTINGS"] = Exams::GetExamSettings($arExam["UF_SETTINGS"], ($arResult["TURBO"] == "Y"));
        }
        $arResult["EXAMS"] = $arExams;

        // ...
        if ($isInvalid) {
            $cache->abortDataCache();
        }
        // ...

        $cache->endDataCache($arResult["EXAMS"]);
    }
}


?>
<? $this->IncludeComponentTemplate();
?>

