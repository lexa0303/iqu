<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use Bitrix\Main\Localization\Loc;
$this->setFrameMode(true);
?>
<section class="section-exam">
    <div class="container">
        <form class="exam__wrapper ajax_submit">
            <input type="hidden" name="start_exam" value="Y">
            <input type="hidden" name="action" value="exam_settings">
            <input type="hidden" name="user_id" value="<?=$USER->GetID();?>">
            <input type="hidden" name="conspect_id" value="<?=$arResult["ID"];?>">
            <div class="row">
                <?/*
                <div class="col-xs-12 col-lg-3 col-lg-push-9">
                    <section class="exam__navigation">
                        <div class="examNav-burger">
                            <img class="examNav-burger__icon" src="<?=SITE_TEMPLATE_PATH;?>/images/settings.svg" alt="">
                            <span class="examNav-burger__item"></span>
                            <span class="examNav-burger__item"></span>
                        </div>
                        <nav class="studying-nav">
                            <div class="studying-nav__controls">
                                <a class="studying-nav__control-item" href="<?=$arResult["LESSON_PAGE_URL"];?>">
                                    <span class="studying-nav__control-icon icon icon-lesson"></span>
                                    <span class="studying-nav__control-text"><?=Loc::getMessage("LESSON");?></span>
                                </a>
                                <a class="studying-nav__control-item active" href="<?=$arResult["EXAM_PAGE_URL"];?>">
                                    <span class="studying-nav__control-icon icon icon-exam"></span>
                                    <span class="studying-nav__control-text"><?=Loc::getMessage("EXAM");?></span>
                                </a>
                                <a class="studying-nav__control-item open-modal"
                                   data-modal="create-notification-modal"
                                   data-ajax="Y"
                                   data-action="notification_create"
                                   data-conspect_id="<?=$arResult["ID"];?>"
                                   data-user_id="<?=$USER->GetID();?>"
                                   href="javascript:void(0);">
                                    <span class="studying-nav__control-icon icon icon-alert"></span>
                                    <span class="studying-nav__control-text"><?=Loc::getMessage("NOTIFICATION");?></span>
                                </a>
<!--                                <a class="studying-nav__control-item" href="--><?//=$arResult["LIST_PAGE_URL"];?><!--">-->
<!--                                    <span class="studying-nav__control-icon icon icon-arrow-back"></span>-->
<!--                                    <span class="studying-nav__control-text">--><?//=Loc::getMessage("BACK");?><!--</span>-->
<!--                                </a>-->
                            </div>
                            <div class="studying-nav__settings">
                                <a class="studying-nav__settings-link" href="<?=$arResult["EXAM_PAGE_URL"];?>">
                                    <span class="studying-nav__settings-icon icon icon-settings"></span>
                                    <span class="studying-nav__settings-text"><?=Loc::getMessage("CONSPECT_EXAM_SETTINGS");?></span>
                                </a>
                            </div>
                            <div class="studying-nav__conspect-info">
                                <div class="studying-nav__user-photo"
                                     style="background-image: url('<?=($arResult["UF_ORIGIN_USER"]) ? $arResult["ORIGIN_USER"]["IMAGE"]["RESIZED"] : $arResult["USER"]["IMAGE"]["RESIZED"];?>');"></div>
                                <div class="studying-nav__user-info">
                                    <span class="studying-nav__username"><?=($arResult["UF_ORIGIN_USER"]) ? $arResult["ORIGIN_USER"]["TITLE"] : $arResult["USER"]["TITLE"];?></span>
                                    <?if (!$arResult["UF_ORIGIN_USER"]) : ?>
                                        <div class="studying-nav__user-info">
                                            <div class="studying-nav__user-watches">
                                                <span class="icon icon-views"></span>
                                                <span><?=(int)$arResult["UF_VIEW_COUNT"];?></span>
                                            </div>
                                            <div class="studying-nav__user-loads">
                                                <span class="icon icon-download2"></span>
                                                <span><?=(int)$arResult["UF_SAVES_COUNT"];?></span>
                                            </div>
                                        </div>
                                    <?endif;?>
                                </div>
                            </div>
                            <div class="studying-nav__conspect-choose">
                                <div class="conspect-choose__group selection-group">
                                    <label class="conspect-choose__descr" for="exam_conspect"><?=Loc::getMessage("CONSPECT_EXAM_CONSPECT_CHOICE_HEADING");?></label>
                                    <select class="conspect-choose__select selection__item js-exam_conspect" id="exam_conspect" name="conspect">
                                        <?php foreach($arResult["CONSPECTS"] as $arConspect):?>
                                            <option <?=($arConspect["ACTIVE"] == "Y") ? "selected" : "";?>
                                                    value="<?=$arConspect["EXAM_PAGE_URL"];?>"><?=$arConspect["UF_NAME"];?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                        </nav>
                    </section>
                </div>
*/?>
                <div class="col-xs-12 col-lg-9">
                    <div class="exam-settings__wrapper">
                        <div class="section__heading">
                            <span class="section__heading-txt"><?=Loc::getMessage("CONSPECT_EXAM_SETTINGS_HEADING");?></span>
                        </div>
                        <div class="exam-settings__descr">
                            <span><?=Loc::getMessage("CONSPECT_EXAM_SETTINGS_DESCRIPTION");?></span>
                        </div>
                        <div class="exam-settings__kind">
                            <?foreach ($arResult["EXAMS"] as $arExam) : ?>
                                <div class="exam-settings__kind-wrapper">
                                    <input value="<?=$arExam["UF_XML_ID"];?>" required
                                           <?=($arExam["DISABLED"]) ? "disabled" : "";?>
                                           name="exam_type"
                                           class="exam-settings__kind-input js-exam_type"
                                           data-type="<?=$arExam["UF_XML_ID"];?>"
                                           data-tabName="<?=$arExam["UF_XML_ID"];?>"
                                           type="radio"
                                           id="<?=$arExam["UF_INPUT_ID"];?>">
                                    <label class="exam-settings__kind-label" for="<?=$arExam["UF_INPUT_ID"];?>"><?=$arExam["UF_NAME"];?><?//=Loc::getMessage("CONSPECT_EXAM_QA");?></label>
                                </div>
                            <?endforeach;?>
                        </div>
                        <?foreach ($arResult["EXAMS"] as $arExam) : ?>
                            <div class="exam-settings__drops exam-settings__drops--<?=$arExam["UF_XML_ID"];?> clearfix" style="display:none;">
                                <?foreach ($arExam["SETTINGS"] as $arSetting) :
                                    switch ($arSetting["TYPE"]) :
                                        case "checkbox":?>
                                            <div class="exam-settings__drops-item selection-group">
                                                <input
                                                       name="<?=$arExam["UF_XML_ID"];?>_<?=$arSetting["UF_XML_ID"];?>"
                                                       class="random-flow"
                                                       type="checkbox"
                                                       id="<?=$arExam["UF_XML_ID"];?>_<?=$arSetting["UF_XML_ID"];?>">
                                                <label class="exam-settings__kind-label" for="<?=$arExam["UF_XML_ID"];?>_<?=$arSetting["UF_XML_ID"];?>">
                                                    <?=$arSetting["UF_NAME"];?>
                                                </label>
<!--                                                <div class="onoffswitch">-->
<!--                                                    <input class="onoffswitch-checkbox"-->
<!--                                                           id="--><?//=$arExam["UF_XML_ID"];?><!--_--><?//=$arSetting["UF_XML_ID"];?><!--"-->
<!--                                                           type="checkbox"-->
<!--                                                           name="--><?//=$arExam["UF_XML_ID"];?><!--_--><?//=$arSetting["UF_XML_ID"];?><!--">-->
<!--                                                    <label class="onoffswitch-label"-->
<!--                                                           for="--><?//=$arExam["UF_XML_ID"];?><!--_--><?//=$arSetting["UF_XML_ID"];?><!--">-->
<!--                                                        <span class="onoffswitch-inner"></span>-->
<!--                                                        <span class="onoffswitch-switch"></span>-->
<!--                                                    </label>-->
<!--                                                </div>-->
                                            </div>
                                            <?break;
                                        case "select":
                                        default:?>
                                            <div class="exam-settings__drops-item selection-group">
                                                <label class="exam-settings__drops-descr" for="<?=$arSetting["UF_XML_ID"];?>"><?=$arSetting["UF_NAME"];?></label>
                                                <select name="<?=$arSetting["UF_XML_ID"];?>" class="exam-settings__drops-select selection__item" id="<?=$arSetting["UF_XML_ID"];?>">
                                                    <?foreach ($arSetting["UF_VALUES"] as $value) : ?>
                                                        <option value="<?=$value;?>"><?=$value;?> <?=$arSetting["UF_UNIT"];?></option>
                                                    <?endforeach;?>
                                                </select>
                                            </div>
                                            <?break;
                                        endswitch;
                                    endforeach;?>
                            </div>
                        <?endforeach;?>
                        <? if (!$arResult["TURBO"]) : ?>
                            <a class="exam-settings__link--turbo exam-settings__drops-item js-shop_select"
                               data-conspect_id="<?=TURBO_CONSPECT_ID;?>"
                               href="javascript:void(0);">Пройти экзамен в режиме TURBO</a>
                        <? endif; ?>
                    </div>
                    <div class="exam-settings__footer">
                        <input class="exam-settings__btn--submit button" type="submit" value="Начать экзамен">
                        <a class="exam-settings__btn--dismiss" href="<?=$arResult["DETAIL_PAGE_URL"];?>">Отмена</a>
                    </div>
                </div>
            </div>
        </form>
    </div>
</section>
<script>
    var firstTab = document.querySelector(".js-exam_type");
    $(firstTab).click();
</script>