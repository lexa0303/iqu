<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 15.02.17
 * Time: 15:53
 */

foreach ($arResult["CARDS"] as &$arCard) :
    if ($arCard["UF_ANSWER_IMAGE"]["ID"] > 0) {
        $arCard["UF_ANSWER_IMAGE"]["RESIZED"] = LenalHelp::img($arCard["UF_ANSWER_IMAGE"]["ID"], 200, 200);
    }
    if ($arCard["UF_QUESTION_IMAGE"]["ID"] > 0) {
        $arCard["UF_QUESTION_IMAGE"]["RESIZED"] = LenalHelp::img($arCard["UF_QUESTION_IMAGE"]["ID"], 200, 200);
    }
endforeach;

foreach ($arResult["EXAMS"] as &$arExam) {
    if ($arExam["UF_XML_ID"] == "mch" && count($arResult["UF_CARDS"]) < 4){
        $arExam["DISABLED"] = true;
    }
}