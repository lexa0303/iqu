<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use Bitrix\Main\Loader,
    Bitrix\Iblock;

Loader::includeModule("highloadblock");
$сache = Bitrix\Main\Data\Cache::createInstance();

$cUser = new CUser;
$dbUser = $cUser->GetList($by = array(), $order = array(), array("TITLE"=>$arParams["USER_TITLE"]));
$arrUser = $dbUser->Fetch();
$userID = $arrUser["ID"];

if ($arParams["CACHE_TYPE"] != "N" && $сache->initCache($arParams['CACHE_TIME'], 'conspects_card' .  $arParams["CONSPECT_ID"] . $arParams["MASTER"] . SITE_ID, '/conspects_card/' . $arParams["CONSPECT_ID"] . "/" . $arParams["MASTER"] . "/")) {
    $arResult = $сache->getVars();
} elseif ($сache->startDataCache()) {

    $arFilter = array();
    $arFilter = array_merge($arParams["FILTER"], $arFilter);
    
    $arResult = Conspects::GetByID($arParams["CONSPECT_ID"]);

    if (!is_array($arParams["SEF_URL_TEMPLATES"]))
        $arParams["SEF_URL_TEMPLATES"] = $arParams["SEF_MODE"];
    makeConspectDetails($arResult, $arParams["SEF_URL_TEMPLATES"]);

    if ($arResult["UF_ORIGIN_CONSPECT"]){
        $arOrigin = Conspects::GetByID($arResult["UF_ORIGIN_CONSPECT"]);
        if ($arOrigin["UF_SHOP"]) {
            $arResult["FROM_SHOP"] = "Y";
        }
    }

    // ...
    if ($isInvalid) {
        $cache->abortDataCache();
    }
    // ...


    $сache->endDataCache($arResult);
}

$arResult["MASTER"] = $arParams["MASTER"];

?>
<? $this->IncludeComponentTemplate();
?>

