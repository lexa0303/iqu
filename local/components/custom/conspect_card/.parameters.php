<?
if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)die();
CModule::IncludeModule("iblock");
$arTypesEx = CIBlockParameters::GetIBlockTypes(Array('-'=>' '));
$arTypesEx2 = CIBlockParameters::GetIBlockTypes(Array('-'=>' '));
$arIBlocks=Array();
$db_iblock = CIBlock::GetList(Array('SORT'=>'ASC'), Array('SITE_ID'=>$_REQUEST['site'],
    'TYPE' => ($arCurrentValues['IBLOCK_TYPE']!='-'?$arCurrentValues['IBLOCK_TYPE']:'')));
while($arRes = $db_iblock->Fetch())
        $arIBlocks[$arRes['ID']] = $arRes['NAME'];
$arTypesEx2 = CIBlockParameters::GetIBlockTypes(Array('-'=>' '));

$arIBlocks2=Array();
$db_iblock2 = CIBlock::GetList(Array('SORT'=>'ASC'), Array('SITE_ID'=>$_REQUEST['site'],
    'TYPE' => ($arCurrentValues2['IBLOCK_TYPE']!='-'?$arCurrentValues2['IBLOCK_TYPE']:'')));
while($arRes2 = $db_iblock2->Fetch())
        $arIBlocks2[$arRes2['ID']] = $arRes2['NAME'];

$arComponentParameters = array(
    'PARAMETERS' => array(
        'CONSPECTS_HLIBLOCK' => Array(
            'PARENT' => 'BASE',
            'NAME' => 'ID инфоблока конспектов',
            'TYPE' => 'LIST',
            'VALUES' => $arHLBlocks,
            'DEFAULT' => '',
            'ADDITIONAL_VALUES' => 'Y',
            'REFRESH' => 'Y',
        ),
        "CATEGORIES_HLBLOCK" => Array(
            'PARENT' => 'BASE',
            'NAME' => 'ID HL-блока категорий',
            'TYPE' => 'LIST',
            'VALUES' => $arHLBlocks,
            'DEFAULT' => '',
            'ADDITIONAL_VALUES' => 'Y',
            'REFRESH' => 'Y',
        ),
        "CARDS_HLBLOCK" => Array(
            'PARENT' => 'BASE',
            'NAME' => 'ID HL-блока карточек',
            'TYPE' => 'LIST',
            'VALUES' => $arHLBlocks,
            'DEFAULT' => '',
            'ADDITIONAL_VALUES' => 'Y',
            'REFRESH' => 'Y',
        ),
        'CONSPECT_ID' => Array(
            'PARENT' => 'BASE',
            'NAME' => 'ID конспекта',
            'TYPE' => 'TEXT',
            'DEFAULT' => '',
            "MULTIPLE" => "N",
        ),
        "SEF_MODE" => array(
            "main_page" => array(
                "NAME" => "Главная страница",
                "DEFAULT" => "",
                "VARIABLES" => array(
                ),
            ),
            "conspect_detail" => array(
                "NAME" => "Детальная страница конспекта",
                "DEFAULT" => "conspects/#USER_TITLE#/#CONSPECT_ID#/",
                "VARIABLES" => array(
                    "CONSPECT_ID",
                    "USER_TITLE"
                ),
            ),
            "conspect_edit" => array(
                "NAME" => "Редактирование конспекта",
                "DEFAULT" => "conspects/#USER_TITLE#/#CONSPECT_ID#/edit/",
                "VARIABLES" => array(
                    "CONSPECT_ID",
                    "USER_TITLE"
                ),
            ),
            "conspect_add" => array(
                "NAME" => "Добавление конспекта",
                "DEFAULT" => "conspects/#USER_TITLE#/add/",
                "VARIABLES" => array(
                    "USER_TITLE"
                ),
            ),
            "conspect_list" => array(
                "NAME" => "Список конспектов",
                "DEFAULT" => "conspects/#USER_TITLE#/",
                "VARIABLES" => array(
                    "USER_TITLE"
                ),
            ),
            "category_detail" => array(
                "NAME" => "Детальная страница категории",
                "DEFAULT" => "category/#USER_TITLE#/#CATEGORY_ID#/",
                "VARIABLES" => array(
                    "CATEGORY_ID",
                    "USER_TITLE"
                ),
            ),
            "categories_list" => array(
                "NAME" => "Список категорий",
                "DEFAULT" => "category/#USER_TITLE#/",
                "VARIABLES" => array(
                    "USER_TITLE"
                ),
            ),
            "exam" => array(
                "NAME" => "Екзамен по конспекту",
                "DEFAULT" => "conspects/#USER_TITLE#/#CONSPECT_ID#/exam/",
                "VARIABLES" => array(
                    "CONSPECT_ID",
                    "USER_TITLE"
                ),
            ),
            "lesson" => array(
                "NAME" => "Урок по конспекту",
                "DEFAULT" => "conspects/#USER_TITLE#/#CONSPECT_ID#/lesson/",
                "VARIABLES" => array(
                    "CONSPECT_ID",
                    "USER_TITLE"
                ),
            ),
        ),
        'CACHE_TIME'  =>  array('DEFAULT'=>36000),
    ),
);
?>