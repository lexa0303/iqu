<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 15.02.17
 * Time: 15:53
 */

/* RESIZE IMAGES */
if (!is_array($arResult["UF_IMAGE"]))
    $arResult["UF_IMAGE"] = array(
        "SRC" => DEFAULT_IMAGE
    );
$arResult["UF_IMAGE"]["RESIZED"] = LenalHelp::img($arResult["UF_IMAGE"]["ID"], 480, 480);
