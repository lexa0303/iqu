<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;

$this->setFrameMode(true);
$allowedStatuses = array(14, 18, 19);
$disabled = false;
if (!in_array($arResult["UF_PUBLISH_STATUS"], $allowedStatuses) && $arResult["IS_AUTHOR"]) :
    $disabled = true;
endif;
?>
<div class="col-lg-3 col-md-4 col-sm-4 col-xs-6 fcard__box-wrap js-conspect <?= ($disabled) ? "js-disabled" : ""; ?>"
     data-id="<?= $arResult["ID"]; ?>"
    <? if ($arParams["SHOW_STUDY"] == "Y") : ?>
        data-study="<?= (int)$arResult["UF_STUDIED"]; ?>"
    <? endif; ?>
    <? if ($arParams["IS_AUTHOR"] == "Y") : ?>
        data-status="<?= $arResult["UF_PUBLISH_STATUS"]; ?>"
    <? endif; ?>
    <? if ($arParams["INDEX"] > 0) : ?>
        data-sort_index="<?= $arParams["INDEX"]; ?>"
    <? endif; ?>>
    <div class="fcard__box fcard__box--large ripple">
        <? if ($arParams["SHOW_STUDY"] == "Y") : ?>
            <span class="fcard__learning-sticker js-set_study"
                  data-user_id="<?= $arResult["USER"]["ID"]; ?>"
                  data-conspect_id="<?= $arResult["ID"]; ?>"
                <? if ($arResult["UF_STUDIED"]) : ?>
                  title="Конспект выучен">
                    <? else : ?>
                        title="<?= Loc::getMessage("CONSPECT_LIST_LEARNING_STICKER_TITLE"); ?>">
                    <? endif; ?>
                <span class="icon <?= ($arResult["UF_STUDIED"]) ? "icon-done" : "icon-studying"; ?>"></span>
            </span>
        <? endif; ?>
        <? if ($arResult["MASTER"] == "Y" && !$disabled) : ?>
            <div class="my-category__stickers">
                <? if (!$arResult["UF_ORIGIN_USER"]) : ?>
                    <a class="fcard__category-sticker" href="<?= $arResult["EDIT_PAGE_URL"]; ?>"
                       title="<?= Loc::getMessage("TOOLTIP_EDIT_CONSPECT"); ?>">
                        <span class="icon icon-edit"></span>
                    </a>
                <? endif; ?>
                <span class="fcard__category-sticker open-modal"
                      data-modal="delete-conspect-radio-modal"
                      data-ajax="Y"
                      data-action="conspect_delete"
                      data-conspect_id="<?= $arResult["ID"]; ?>"
                      data-user_id="<?= $arResult["USER"]["ID"]; ?>"
                      title="<?= Loc::getMessage("TOOLTIP_DELETE_CONSPECT"); ?>">
                    <span class="icon icon-trashbox"></span>
                </span>
            </div>
        <? endif; ?>


        <!--        =========-->
        <!--        ICONS-->
        <!--        =========-->


        <div class="fcard__promo-stickers">
            <? if ($arResult["FROM_SHOP"] == "Y") : ?>
                <span class="fcard__promo-sticker fcard__promo-sticker--paid">
                <span class="icon icon-coin"></span>
            </span>
            <? endif; ?>
<!--                        <span class="fcard__promo-sticker fcard__promo-sticker--green">топ</span>-->
        </div>
        <a class="fcard__img-wrap"
            <? if (!$disabled) : ?>
                href="<?= $arResult["DETAIL_PAGE_URL"]; ?>"
            <? endif; ?>
           style="background-image: url('<?= $arResult["UF_IMAGE"]["RESIZED"]; ?>');">
        </a>
        <div class="fcard__lower">
            <div class="fcard__heading-wrap">
                <a class="fcard__heading js-conspect_name"
                   title="<?= $arResult["UF_NAME"]; ?>"
                    <? if (!$disabled) : ?>
                        href="<?= $arResult["DETAIL_PAGE_URL"]; ?>"
                    <? endif; ?>><?= $arResult["UF_NAME"]; ?></a>
            </div>
            <div class="fcard__author-wrap">
                <? if ($arResult["UF_ORIGIN_USER"]) : ?>
                    <a class="fcard__author"
                       href="<?= $arResult["ORIGIN_USER"]["DETAIL_PAGE_URL"]; ?>"><?= $arResult["ORIGIN_USER"]["TITLE"]; ?></a>
                <? else : ?>
                    <a class="fcard__author"
                       href="<?= $arResult["USER"]["DETAIL_PAGE_URL"]; ?>"><?= $arResult["USER"]["TITLE"]; ?></a>
                <? endif; ?>
                <? if ($arResult["UF_USER_ID"] != $USER->GetID() && $USER->IsAuthorized()) : ?>
                    <a class="fcard__unsubscribe js-unsubscribe js-sub" data-user_id="<?= $arResult["UF_USER_ID"]; ?>"
                       href="javascript:void(0);">
                        <span class="icon icon-unsubscribe"></span>
                        <span><?= Loc::getMessage("UNSUBSCRIBE"); ?></span>
                    </a>
                    <a class="fcard__unsubscribe js-subscribe js-sub" data-user_id="<?= $arResult["UF_USER_ID"]; ?>"
                       href="javascript:void(0);">
                        <span class="icon icon-subscribe"></span>
                        <span><?= Loc::getMessage("SUBSCRIBE"); ?></span>
                    </a>
                <? endif; ?>
            </div>
            <div class="fcard__social-info">
                <? if ($arResult["UF_ORIGIN_USER"] <= 0) : ?>
                    <div class="fcard__views">
                        <span class="icon icon-views fcard__views-icon"
                              title="<?= Loc::getMessage("VIEW_COUNT"); ?>"></span>
                        <span class="fcard__views-quant"
                              title="<?= Loc::getMessage("VIEW_COUNT"); ?>"><?= (int)$arResult["UF_VIEW_COUNT"]; ?></span>
                    </div>
                    <div class="fcard__downloads">
                        <span class="icon icon-download2" title="<?= Loc::getMessage("SAVES_COUNT"); ?>"></span>
                        <span class="fcard__downloads-text"
                              title="<?= Loc::getMessage("SAVES_COUNT"); ?>"><?= (int)$arResult["UF_SAVES_COUNT"]; ?></span>
                    </div>
                <? endif; ?>
            </div>
            <div class="fcard__extra-info">
                <div class="fcard__ammount" title="<?= Loc::getMessage("CONSPECT_LIST_CARD_COUNT_TITLE"); ?>">
                    <span><?= $arResult["CARD_COUNT"]; ?></span>
                </div>
                <div class="fcard__cdate" title="<?= Loc::getMessage("CONSPECT_LIST_DATE_TITLE"); ?>">
                    <span><?= Loc::getMessage('VERSION_OT'); ?> <?= ($arResult["MASTER"] == "Y") ? $arResult["UF_CREATE_TIME"]->format("d.m.Y") : $arResult["UF_UPDATE_TIME"]->format("d.m.Y"); ?></span>
                </div>
                <? if ($arResult["UF_USER_ID"] != $USER->GetID() && $USER->IsAuthorized()) : ?>
                    <a class="fcard__download js-save_conspect"
                       data-conspect_id="<?= $arResult["ID"]; ?>"
                       data-user_id="<?= $arResult["USER"]["ID"]; ?>"
                       href="javascript:void(0);"
                       style="background-image: url(/local/templates/iq/images/download-card-icon.png)"></a>
                <? endif; ?>
            </div>
        </div>
    </div>
</div>