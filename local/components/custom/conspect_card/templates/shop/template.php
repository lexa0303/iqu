<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use Bitrix\Main\Localization\Loc;
$this->setFrameMode(true);
?>
<div class="col-md-3 col-sm-4 col-xs-6 fcard__box-wrap js-conspect"
     data-id="<?=$arResult["ID"];?>"
     data-study="<?= (int)$arResult["UF_STUDIED"]; ?>">
    <div class="fcard__box fcard__box--large ripple">
        <a class="fcard__img-wrap" href="<?= $arResult["DETAIL_PAGE_URL"]; ?>">
            <img class="fcard__img" src="<?= $arResult["UF_IMAGE"]["RESIZED"]; ?>" alt="">
        </a>
        <div class="fcard__lower">
            <div class="fcard__heading-wrap">
                <a class="fcard__heading js-conspect_name"
                   href="<?= $arResult["DETAIL_PAGE_URL"]; ?>"><?= $arResult["UF_NAME"]; ?></a>
            </div>
            <div class="fcard__author-wrap">
                <a class="fcard__author"
                   href="<?= $arResult["USER"]["DETAIL_PAGE_URL"]; ?>"><?= $arResult["USER"]["TITLE"]; ?></a>
                <?if ($arResult["UF_USER_ID"] != $USER->GetID()) : ?>
                    <a class="fcard__unsubscribe js-unsubscribe js-sub" data-user_id="<?=$arResult["UF_USER_ID"];?>" href="javascript:void(0);">
                        <span class="icon icon-unsubscribe"></span>
                        <span><?= Loc::getMessage("UNSUBSCRIBE"); ?></span>
                    </a>
                    <a class="fcard__unsubscribe js-subscribe js-sub" data-user_id="<?=$arResult["UF_USER_ID"];?>" href="javascript:void(0);">
                        <span class="icon icon-subscribe"></span>
                        <span><?= Loc::getMessage("SUBSCRIBE"); ?></span>
                    </a>
                <?endif;?>
            </div>
            <div class="fcard__social-info">
                <div class="fcard__views">
                    <span class="icon icon-views fcard__views-icon"
                          title="<?= Loc::getMessage("VIEW_COUNT"); ?>"></span>
                    <span class="fcard__views-quant"
                          title="<?= Loc::getMessage("VIEW_COUNT"); ?>"><?= (int)$arResult["UF_VIEW_COUNT"]; ?></span>
                </div>
                <div class="fcard__downloads">
                    <span class="icon icon-download2" title="<?=Loc::getMessage("SAVES_COUNT");?>"></span>
                    <span class="fcard__downloads-text" title="<?=Loc::getMessage("SAVES_COUNT");?>"><?= (int)$arResult["UF_SAVES_COUNT"]; ?></span>
                </div>
            </div>
            <div class="fcard__price">
                12 $
            </div>
            <div class="fcard__extra-info">
                <div class="fcard__ammount" title="<?= Loc::getMessage("CONSPECT_LIST_CARD_COUNT_TITLE"); ?>">
                    <span><?= $arResult["CARD_COUNT"]; ?></span>
                </div>
                <div class="fcard__cdate" title="<?= Loc::getMessage("CONSPECT_LIST_DATE_TITLE"); ?>">
                    <span><?=Loc::getMessage('VERSION_OT');?> <?= ($arResult["MASTER"] == "Y") ? $arResult["UF_CREATE_TIME"]->format("d.m.Y") : $arResult["UF_UPDATE_TIME"]->format("d.m.Y"); ?></span>
                </div>
                <a class="fcard__download js-buy_conspect"
                   data-conspect_id="<?=$arResult["ID"];?>"
                   href="javascript:void(0);"
                   style="background-image: url(/local/templates/iq/images/buy-card-icon.png)"></a>
            </div>
        </div>
    </div>
</div>