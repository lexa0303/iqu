<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use Bitrix\Main\Loader,
    Bitrix\Iblock;

Loader::includeModule("iblock");

$arResult = Cards::GetByID($arParams["CARD_ID"]);

if ($arResult["UF_IMAGE"] > 0) {
    $arResult["IMAGE"] = CFile::GetPath($arResult["UF_IMAGE"]);
}

$arResult["USER_ID"] = $arParams["USER_ID"];

?>
<? $this->IncludeComponentTemplate();
?>

