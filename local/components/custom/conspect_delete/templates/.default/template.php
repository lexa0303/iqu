<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use Bitrix\Main\Localization\Loc;
$this->setFrameMode(true);
?>
<a class="modal__btn-close icon icon-cross" href="javascript:void(0)"></a>
<form class="delete-conspect-radio__body ajax_submit" action="">
    <input type="hidden" name="user_id" value="<?= $arResult["USER_ID"]; ?>">
    <input type="hidden" name="conspect_id" value="<?= $arResult["ID"]; ?>">
    <input type="hidden" name="action" value="delete_conspect">
    <span class="delete-conspect-radio__heading"><?=Loc::getMessage("MODAL_DELETE_CONSPECT_HEADING");?></span>
    <span class="delete-conspect-radio__text"><?=Loc::getMessage("MODAL_DELETE_CONSPECT_TEXT");?></span>
    <div class="delete-conspect-radio__kinds">
        <?if ($arResult["UF_ORIGIN_USER"] <= 0 && $arResult["UF_SHOW_IN_LIBRARY"]) : ?>
            <div class="delete-conspect-radio__kind-wrapper">
                <input class="delete-conspect-radio__kind-input"
                       type="radio"
                       value="profile"
                       id="fromprofile"
                       name="delete_from">
                <label class="delete-conspect-radio__kind-label" for="fromprofile"><?=Loc::getMessage("CONSPECT_DELETE_PROFILE");?></label>
            </div>
            <div class="delete-conspect-radio__kind-wrapper">
                <input class="delete-conspect-radio__kind-input"
                       type="radio"
                       value="library"
                       <?=(!$arResult["UF_SHOW_IN_LIBRARY"]) ? "disabled": "";?>
                       id="fromLibrary"
                       name="delete_from">
                <label class="delete-conspect-radio__kind-label" for="fromLibrary"><?=Loc::getMessage("CONSPECT_DELETE_LIBRARY");?></label>
            </div>
            <div class="delete-conspect-radio__kind-wrapper">
                <input class="delete-conspect-radio__kind-input"
                       type="radio"
                       value="full"
                       id="fromBoth"
                       checked
                       name="delete_from">
                <label class="delete-conspect-radio__kind-label" for="fromBoth"><?=Loc::getMessage("CONSPECT_DELETE_BOTH");?></label>
            </div>
        <? else : ?>
            <div class="delete-conspect-radio__kind-wrapper">
                <input type="hidden" name="delete_from" value="full">
            </div>
        <?endif;?>
    </div>
    <div class="delete-conspect-buttons">
        <a class="delete-conspect-radio__cancel button" href="">Отмена</a>
        <input class="delete-conspect-radio__submit button" type="submit" value="Удалить">
    </div>
</form>