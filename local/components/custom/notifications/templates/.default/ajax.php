<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 16.03.17
 * Time: 15:25
 */

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
use \Bitrix\Main\Application;
use \Bitrix\Main\Localization\Loc;

Loc::loadLanguageFile($_SERVER["DOCUMENT_ROOT"]."/local/templates/".SITE_TEMPLATE_ID."/template.php");

global $APPLICATION;
$doc_root = Application::getDocumentRoot();

$request_list = Application::getInstance()->getContext()->getRequest()->toArray();
foreach ($request_list as $key => $item) {
    $request[$key] = CUtil::JSEscape($item);
}

