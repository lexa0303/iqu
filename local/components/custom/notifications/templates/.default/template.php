<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use Bitrix\Main\Localization\Loc;
$this->setFrameMode(true);
?>
<script>
    var UserNotificate = new UserNotifications(<?=Bitrix\Main\Web\Json::encode($arResult);?>);
    UserNotificate.checkNotifications();
</script>