/**
 * Created by alex on 16.03.17.
 */

function UserNotifications(arNotifications){
    this.notifications = arNotifications;
    this.setPermissions();
}

UserNotifications.prototype.add = function(notification){
    this.notifications[notification.id] = notification;
    this.getPermissions();
};

UserNotifications.prototype.update = function(notification_id){
    var ajaxOptions = {
        data: {
            action: "notification_done",
            id: notification_id
        },
        callback: function(res){
        }
    };

    ajaxRequest(ajaxOptions);
    delete this.notifications[notification_id];
};

UserNotifications.prototype.checkNotifications = function () {
    var date = +new Date();
    var notifications = this.notifications;
    var flag = false;
    var self = this;
    var index, notification;

    for (index in notifications){
        if (notifications.hasOwnProperty(index)){
            notification = notifications[index];

            if ((notification.when * 1000) < date){
                self.notificate(index);
            }
        }
        flag = true;
    }

    if (flag){
        setTimeout(function(){
            self.checkNotifications();
        }, 5000);
    }
};

UserNotifications.prototype.notificate = function(notification_id){
    var notification = this.notifications[notification_id];

    if (notification != undefined) {
        var title = "Пора учиться";
        var options = {
            body: "Пора учить конспект \"" + notification.conspect.UF_NAME + "\"",
            icon: notification.conspect.UF_IMAGE.SRC
        };
        if (this.permission == "granted"){
            var note = new Notification(title, options);
            note.onclick = function(){
                window.open("/conspects/" + notification.conspect.USER.TITLE + "/" + notification.conspect.UF_CODE + "/lesson/", '_blank');
            };
        } else if (this.permission == "default") {
            this.getPermissions();
            toastr.warning("Пора учить конспект \"" + notification.conspect.UF_NAME + "\"");
        } else {
            toastr.warning("Пора учить конспект \"" + notification.conspect.UF_NAME + "\"");
        }
        // this.update(notification_id);
    }
};

UserNotifications.prototype.setPermissions = function(){
    var self = this;
    if (window.Notification == undefined) {
        self.permission = "denied";
    } else {
        self.permission = Notification.permission;
    }
};

UserNotifications.prototype.getPermissions = function(){
    var self = this;
    if (window.Notification == undefined) {
        self.permission = "denied";
    } else {
        if (self.permission != "denied") {
            Notification.requestPermission(function (permission) {
                self.permission = permission;
            });
        } else {
            /* TODO - PERMISSION GET POPUP */
        }
    }
};

let testArray = ['qwer', 'tqwetqw', '', 'awefawe', 'wqetwet', ''];

testArray = testArray.filter(item => {
    return (!!item);
});

testArray = testArray.map(item => {
    return item.toUpperCase();
});

const result = testArray.reduce((prev, current) => {
     return prev + '_' + current;
});

console.log(result);

// filter - no empty
// map - uppercase
// reduce - to string with _