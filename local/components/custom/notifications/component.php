<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use Bitrix\Main\Loader,
    Bitrix\Iblock;

Loader::includeModule("iblock");

global $USER;

$сache = Bitrix\Main\Data\Cache::createInstance();

if ($arParams["CACHE_TYPE"] != "N" && $сache->initCache($arParams['CACHE_TIME'], 'notifications' .  $arParams["USER_ID"] . SITE_ID, '/notifications/' . $arParams["USER_ID"] . "/" . $arParams["SHOW_ALL"])) {
    $arResult = $сache->getVars();
} elseif ($сache->startDataCache()) {

    $dbPeriods = CUserfieldEnum::GetList(array(), array("USER_FIELD_NAME"=>"UF_PERIOD"));

    while ($res = $dbPeriods->Fetch())
        $arPeriods[$res["ID"]] = $res["XML_ID"];

    $arNotifications = Notifications::GetNotifications();

    foreach ($arNotifications as $arNotification){
        if ($arNotification["UF_NEXT_DATE"]->getTimestamp() <= time() || ($arParams["SHOW_ALL"] == "Y")) {
            $item = array(
                "id" => $arNotification["ID"],
                "when" => $arNotification["UF_NEXT_DATE"]->getTimestamp(),
                "conspect" => $arNotification["CONSPECT"],
                "period" => $arPeriods[$arNotification["UF_PERIOD"]]
            );
            $arResult[] = $item;
        }
    }

    // ...
    if ($isInvalid) {
        $cache->abortDataCache();
    }
    // ...


    $сache->endDataCache($arResult);
}

?>
<? $this->IncludeComponentTemplate();
?>

