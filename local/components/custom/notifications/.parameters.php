<?
if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)die();
CModule::IncludeModule("iblock");
CModule::IncludeModule("highloadblock");
$dbHLBlock = \Bitrix\Highloadblock\HighloadBlockTable::getList(array());
while ($res = $dbHLBlock->fetch())
    $arHLBlocks[$res["ID"]] = $res["NAME"];

global $USER;

$arComponentParameters = array(
    'PARAMETERS' => array(
        'NOTIFICATIONS_HLBLOCK' => Array(
            'PARENT' => 'BASE',
            'NAME' => 'ID HL-блока уведомлений',
            'TYPE' => 'LIST',
            'VALUES' => $arHLBlocks,
            'DEFAULT' => '',
            'ADDITIONAL_VALUES' => 'Y',
            'REFRESH' => 'Y',
        ),
        'USER_ID' => Array(
            'PARENT' => 'BASE',
            'NAME' => 'ID пользователя',
            'TYPE' => 'TEXT',
            'DEFAULT' => $USER->GetID(),
            "MULTIPLE" => "N",
        ),
    ),
);
?>