<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use Bitrix\Main\Localization\Loc;
$this->setFrameMode(true);
?>
<a class="modal__btn-close icon icon-cross" href="javascript:void(0)"></a>
<form class="delete-conspect__body ajax_submit">
    <input type="hidden" name="user_id" value="<?= $arResult["USER_ID"]; ?>">
    <input type="hidden" name="category_id" value="<?= $arResult["ID"]; ?>">
    <input type="hidden" name="action" value="delete_category">
    <span class="delete-conspect__heading"><?=Loc::getMessage("MODAL_DELETE_CATEGORY_HEADING");?></span>
    <span class="delete-conspect__text"><?=Loc::getMessage("MODAL_DELETE_CATEGORY_TEXT");?></span>
    <div class="delete-conspect__bottom">
        <a class="delete-conspect__btn delete-conspect__btn--dismiss button button--rounded js-modal_close"><?=Loc::getMessage("NO");?></a>
        <input class="delete-conspect__btn delete-conspect__btn--submit button button--rounded" type="submit" value="<?=Loc::getMessage("YES");?>">
    </div>
</form>