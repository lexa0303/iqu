<?
if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)die();
CModule::IncludeModule("iblock");
$arTypesEx = CIBlockParameters::GetIBlockTypes(Array('-'=>' '));
$arTypesEx2 = CIBlockParameters::GetIBlockTypes(Array('-'=>' '));
$arIBlocks=Array();
$db_iblock = CIBlock::GetList(Array('SORT'=>'ASC'), Array('SITE_ID'=>$_REQUEST['site'],
    'TYPE' => ($arCurrentValues['IBLOCK_TYPE']!='-'?$arCurrentValues['IBLOCK_TYPE']:'')));
while($arRes = $db_iblock->Fetch())
        $arIBlocks[$arRes['ID']] = $arRes['NAME'];
$arTypesEx2 = CIBlockParameters::GetIBlockTypes(Array('-'=>' '));

$arIBlocks2=Array();
$db_iblock2 = CIBlock::GetList(Array('SORT'=>'ASC'), Array('SITE_ID'=>$_REQUEST['site'],
    'TYPE' => ($arCurrentValues2['IBLOCK_TYPE']!='-'?$arCurrentValues2['IBLOCK_TYPE']:'')));
while($arRes2 = $db_iblock2->Fetch())
        $arIBlocks2[$arRes2['ID']] = $arRes2['NAME'];

$arComponentParameters = array(
    'PARAMETERS' => array(
        'IBLOCK_TYPE' => Array(
            'PARENT' => 'BASE',
            'NAME' => 'Тип информационного блока (используется только для проверки)',
            'TYPE' => 'LIST',
            'VALUES' => $arTypesEx,
            'DEFAULT' => 'news',
            'REFRESH' => 'Y',
        ),
        'IBLOCK_ID' => Array(
            'PARENT' => 'BASE',
            'NAME' => 'Код информационного блока',
            'TYPE' => 'LIST',
            'VALUES' => $arIBlocks,
            'DEFAULT' => '',
            'ADDITIONAL_VALUES' => 'Y',
            'REFRESH' => 'Y',
        ),
        'CONSPECT_ID' => Array(
            'PARENT' => 'BASE',
            'NAME' => 'ID конспекта',
            'TYPE' => 'TEXT',
            'DEFAULT' => '',
            "MULTIPLE" => "N",
        ),
        'USER_ID' => Array(
            'PARENT' => 'BASE',
            'NAME' => 'ID пользователя',
            'TYPE' => 'TEXT',
            'DEFAULT' => '',
            "MULTIPLE" => "N",
        ),
        'PATH_TO_CONSPECT_LIST' => Array(
            'PARENT' => 'BASE',
            'NAME' => 'Шаблон пути к списку карточек',
            'TYPE' => 'TEXT',
            'DEFAULT' => '',
            "MULTIPLE" => "N",
        ),
        'PATH_TO_ADD_CARD' => Array(
            'PARENT' => 'BASE',
            'NAME' => 'Шаблон пути к добавлению карточки',
            'TYPE' => 'TEXT',
            'DEFAULT' => '',
            "MULTIPLE" => "N",
        ),
        'PATH_TO_CARD_EDIT' => Array(
            'PARENT' => 'BASE',
            'NAME' => 'Шаблон пути к редактированию карточки',
            'TYPE' => 'TEXT',
            'DEFAULT' => '',
            "MULTIPLE" => "N",
        ),
        'CACHE_TIME'  =>  array('DEFAULT'=>36000),
    ),
);
?>