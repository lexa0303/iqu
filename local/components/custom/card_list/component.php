<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use Bitrix\Main\Loader,
    Bitrix\Iblock;

Loader::includeModule("iblock");
$сache = Bitrix\Main\Data\Cache::createInstance();

if ($сache->initCache($arParams['CACHE_TIME'], 'cards_list' . $arParams['USER_ID'] . SITE_ID . $arParams["CONSPECT_ID"] . $arParams["CATEGORY_ID"], '/')) {
    $arResult = $сache->getVars();
} elseif ($сache->startDataCache()) {

    $arResult = Conspects::GetByID($arParams["CONSPECT_ID"]);

    $arResult["PATH_TO_CONSPECT_LIST"] = $arParams["PATH_TO_CONSPECT_LIST"];
    $arResult["PATH_TO_CONSPECT_EDIT"] = $arParams["PATH_TO_CONSPECT_EDIT"];
    $arResult["PATH_TO_ADD_CARD"] = $arParams["PATH_TO_ADD_CARD"];
    $arResult["PATH_TO_EXAM"] = $arParams["PATH_TO_EXAM"];

    $arResult["ITEMS"] = Cards::GetByConspect($arParams["USER_ID"], $arParams["CONSPECT_ID"]);

    foreach ($arResult["ITEMS"] as &$arItem) {

        $arItem["DETAIL_PAGE_URL"] = $arParams["SEF_FOLDER"] . str_replace(
                "#CARD_ID#",
                $arItem["ID"],
                $arParams["PATH_TO_CARD_EDIT"]
            );

    }

    // ...
    if ($isInvalid) {
        $cache->abortDataCache();
    }
    // ...


    $сache->endDataCache($arResult);
}
?>
<? $this->IncludeComponentTemplate();
?>

