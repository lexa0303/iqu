<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$this->setFrameMode(true);
?>
<div class="card_list">
    <?if (count($arResult["ITEMS"]) > 0) : ?>
    <?foreach ($arResult["ITEMS"] as $arItem) :?>
        <div class="card_item">
            <div class="card_item-image">
                <?$image = CFile::ResizeImageGet(
                    $arItem["UF_IMAGE"]["ID"], array("width" => 100, "height" => 100), "BX_RESIZE_IMAGE_PROPORTIONAL"
                );?>
                <img src="<?=$image["src"];?>"/>
            </div>
            <div class="card_item-questions_wrap">
                <div class="card_item-row">
                    <div class="card_item-question_name">Вопрос: </div>
                    <div class="card_item-question_value"><?=$arItem["UF_QUESTION"]?></div>
                </div>
                <div class="card_item-row">
                    <div class="card_item-answer_name">Ответ</div>
                    <div class="card_item-answer_value"><?=$arItem["UF_ANSWER"]?></div>
                </div>
            </div>
        </div>
        <div style="clear: both"></div>
    <?endforeach;?>
    <?else : ?>
        <div class="card_list-error">В этом конспекте нет карточек</div>
    <?endif;?>
</div>