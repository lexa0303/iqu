<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use Bitrix\Main\Loader,
    Bitrix\Iblock;

Loader::includeModule("highloadblock");
$сache = Bitrix\Main\Data\Cache::createInstance();

$cUser = new CUser;
$dbUser = $cUser->GetList($by = array(), $order = array(), array("TITLE"=>$arParams["USER_TITLE"]));
$arrUser = $dbUser->Fetch();
$userID = $arrUser["ID"];

if ($arParams["CACHE_TYPE"] != "N" && $сache->initCache($arParams['CACHE_TIME'], 'conspects_detail' . $arParams["CONSPECT_ID"] . $arParams["USER_TITLE"] . SITE_ID, '/conspects/' . $userID . '/detail/')) {
    $arResult = $сache->getVars();
} elseif ($сache->startDataCache()) {

    $arResult = Conspects::GetByCode($arParams["CONSPECT_ID"], $userID);
    if ($arResult == false)
        $arResult = Conspects::GetByID($arParams["CONSPECT_ID"]);

    if ($arResult == false)
        $arResult = array(
            "ERRORS" => array(
                "Конспект не найден"
            )
        );

    /* CHECK IF CONSPECT WAS PURCHASED AND EXPIRED */
    if ($arResult["UF_ORIGIN_CONSPECT"]) {
        $arOriginConspect = Conspects::GetByID($arResult["UF_ORIGIN_CONSPECT"]);
        if ($arOriginConspect["UF_SHOP"]){
            $AvailableConspects = new HL("sys_available_conspects");

            $arAvailable = $AvailableConspects->GetList(
                array(
                    "filter" => array(
                        "UF_USER_ID" => $arResult["UF_USER_ID"],
                        "UF_CONSPECT" => $arResult["UF_ORIGIN_CONSPECT"]
                    )
                )
            )->Fetch();

            if (!$arAvailable) {
                $arResult["ERRORS"][] = "Доступ к конспекту закончился";
            }
        }
    }

    if (empty($arResult["ERRORS"])) :

        if ($arResult["USER"]["PERSONAL_PHOTO"] > 0){
            $arResult["USER"]["IMAGE"] = array(
                "SRC" => CFile::GetPath($arResult["USER"]["PERSONAL_PHOTO"]),
                "RESIZED" => LenalHelp::img($arResult["USER"]["PERSONAL_PHOTO"], 75, 75)
            );
        }

        if ($arResult["ORIGIN_USER"]["PERSONAL_PHOTO"] > 0){
            $arResult["ORIGIN_USER"]["IMAGE"] = array(
                "SRC" => CFile::GetPath($arResult["ORIGIN_USER"]["PERSONAL_PHOTO"]),
                "RESIZED" => LenalHelp::img($arResult["ORIGIN_USER"]["PERSONAL_PHOTO"], 75, 75)
            );
        }

        makeConspectDetails($arResult, $arParams["SEF_URL_TEMPLATES"]);

        $arCardsFilter = array(
            "ID" => $arResult["UF_CARDS"]
        );
        if ($arParams["NON_STUDIED_ONLY"] == "Y"){
            $arCardsFilter["UF_STUDIED"] = false;
        }
        $arResult["CARDS"] = Cards::GetAll($arCardsFilter);

    endif;

    // ...
    if ($isInvalid) {
        $cache->abortDataCache();
    }
    // ...


    $сache->endDataCache($arResult);
}

if ($arResult["USER"]["TITLE"] !=  $arParams["USER_TITLE"]){
    LocalRedirect(SITE_DIR);
}

$arResult["MASTER"] = $arParams["MASTER"];

$APPLICATION->SetTitle($arResult["UF_NAME"]);

if ($_SESSION["VISITED_CONSPECTS"][$arResult["ID"]] != "Y" && $arResult["USER"]["ID"] != $USER->GetID()){
    $_SESSION["VISITED_CONSPECTS"][$arResult["ID"]] = "Y";
    Conspects::AddViewCount($arResult["ID"]);
}

if ($arResult["MASTER"] == "Y"){
    Conspects::SetViewed($arResult["ID"]);
    $arNotifications = Notifications::GetAll(array("UF_USER_ID"=>$USER->GetID(), "UF_CONSPECT"=>$arResult["ID"], "<=UF_NEXT_DATE"=>ConvertTimeStamp(time(), "FULL")));

    foreach ($arNotifications as $arNotification){
        Notifications::UpdateDate($arNotification["ID"]);
    }
}

$arNewCookies = unserialize($_COOKIE["VIEWED_CONSPECTS"]);
if (!($arNewCookies[$arResult["ID"]])){
    $arNewCookies[$arResult["ID"]] = $arResult["ID"];
    setcookie("VIEWED_CONSPECTS", serialize($arNewCookies), time()+60*60*24*7, "/");
}
?>
<? $this->IncludeComponentTemplate(); ?>
<script>
    (function(){
        var viewedConspects;
        if (localStorage.viewedConspects !== undefined){
            viewedConspects = JSON.parse(localStorage.viewedConspects);
        } else {
            viewedConspects = {};
        }

        viewedConspects['<?=$arResult["ID"];?>'] = '<?=$arResult["ID"];?>';
        localStorage.viewedConspects = JSON.stringify(viewedConspects);
    })();
</script>