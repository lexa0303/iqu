<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use Bitrix\Main\Loader,
    Bitrix\Iblock;

Loader::includeModule("iblock");

if (!$arParams["NOTIFICATION_ID"]) {
    $arResult = Conspects::GetByID($arParams["CONSPECT_ID"]);
} else {
    $arResult = Notifications::GetByID($arParams["NOTIFICATION_ID"]);
    $arResult["CONSPECT"] = Conspects::GetByID($arResult["UF_CONSPECT"]);
}
$arResult["USER_ID"] = $arParams["USER_ID"];

$dbPeriods = CUserfieldEnum::GetList(array(), array("USER_FIELD_NAME"=>"UF_PERIOD"));
while ($res = $dbPeriods->Fetch())
    $arPeriods[$res["XML_ID"]] = $res["ID"];

$arResult["PERIODS"] = $arPeriods;

?>
<? $this->IncludeComponentTemplate();
?>

