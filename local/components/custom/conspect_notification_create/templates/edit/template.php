<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use Bitrix\Main\Localization\Loc;
$this->setFrameMode(true);
?>
<a class="modal__btn-close icon icon-cross" href="javascript:void(0)"></a>
<form class="create-notification__body ajax_submit" action="" id="edit_notificate">
    <input type="hidden" name="user_id" value="<?= $arResult["USER_ID"]; ?>">
    <input type="hidden" name="id" value="<?= $arResult["ID"]; ?>">
    <input type="hidden" name="action" value="edit_notification">
    <span class="create-notification__heading"><?=Loc::getMessage("MODAL_EDIT_NOTIFICATION_HEADING");?></span>
    <span class="create-notification__text"><?=Loc::getMessage("MODAL_CREATE_NOTIFICATION_TEXT");?></span>
    <div class="create-notification__datepicker">
<!--        <span class="create-notification__datepicker-title">--><?//=Loc::getMessage("MODAL_CREATE_NOTIFICATION_WHEN");?><!--</span>-->
        <div class="form-group">
            <div class="input-group date">
                <?php
                /**
                 * @var $arResult["UF_NEXT_DATE"] Bitrix\Main\Type\DateTime
                 *
                 * 07.07.2017 11:00
                 */
                $culture = new \Bitrix\Main\Context\Culture(
                    array(
                        "FORMAT_DATE" => "DD.MM.YYYY",
                        "FORMAT_DATETIME" => "DD.MM.YYYY HH:MI",
                        "FORMAT_NAME" => "#NAME# #LAST_NAME#",
                        "CHARSET" => "UTF-8",
                        "DIRECTION" => 'Y',
                        "WEEK_START" => 0
                    )
                );
                ?>
                <input value="<?=$arResult["UF_NEXT_DATE"]->toString($culture);?>"
                       id="notificationDatePicker"
                       required
                       placeholder="Дата"
                       name="notification_date"
                       class="form-control create-notification__date-input"
                       type="text">
            </div>
        </div>
    </div>
    <div class="create-notification__selection selection-group">
<!--        <label class="conspect-choose__descr" for="notification_period">--><?//=Loc::getMessage("MODAL_CREATE_NOTIFICATION_PERIOD");?><!--</label>-->
        <select required name="notification_period" class="create-notification__select selection__item" id="notification_period">
            <option disabled value="">Период</option>
            <option <?=($arResult["UF_PERIOD"] == 4) ? "selected" : "";?> value="day">Каждый день</option>
            <option <?=($arResult["UF_PERIOD"] == 6) ? "selected" : "";?> value="week">Раз в неделю</option>
            <option <?=($arResult["UF_PERIOD"] == 5) ? "selected" : "";?> value="month">Раз в месяц</option>
        </select>
    </div>
    <button class="create-notification__btn download-conspect__btn--submit button button--rounded"><?=Loc::getMessage("MODAL_EDIT_NOTIFICATION_SUBMIT");?></button>
    <a href="<?=$arResult["CONSPECT"]["DETAIL_PAGE_URL"];?>" class="create-notification__conspect-link">Перейти на конспект</a>
    <button type="button" class="edit-notification__delete-btn js-delete_notification">Удалить напоминание</button>
    <a class="download-conspect__btn--dismiss js-close_modal" href="javascript:void(0);"><?=Loc::getMessage("CANCEL");?></a>
</form>
