<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use Bitrix\Main\Localization\Loc;
$this->setFrameMode(true);
?>

<a class="modal__btn-close icon icon-cross" href="javascript:void(0)"></a>
<form class="create-notification__body ajax_submit" action="" id="create_notificate">
    <input type="hidden" name="user_id" value="<?= $arResult["USER_ID"]; ?>">
    <input type="hidden" name="conspect_id" value="<?= $arResult["ID"]; ?>">
    <input type="hidden" name="action" value="create_notification">
    <span class="create-notification__heading"><?=Loc::getMessage("MODAL_CREATE_NOTIFICATION_HEADING");?></span>
    <a href="/notifications/calendar/" class="creation-data__img-txt"><?=Loc::getMessage("MODAL_CREATE_NOTIFICATION_CALENDAR");?></a>
    <span class="create-notification__text"><?=Loc::getMessage("MODAL_CREATE_NOTIFICATION_TEXT");?></span>
    <div class="create-notification__datepicker">
<!--        <span class="create-notification__datepicker-title">--><?//=Loc::getMessage("MODAL_CREATE_NOTIFICATION_WHEN");?><!--</span>-->
        <div class="form-group">
            <div class="input-group date" >
                <input id="notificationDatePicker"
                       required
                       placeholder="Дата"
                       name="notification_date"
                       class="form-control create-notification__date-input"
                       type="text">
            </div>
        </div>
    </div>
    <div class="create-notification__selection selection-group">
<!--        <label class="conspect-choose__descr" for="notification_period">--><?//=Loc::getMessage("MODAL_CREATE_NOTIFICATION_PERIOD");?><!--</label>-->
        <select required name="notification_period" class="create-notification__select selection__item" id="notification_period">
            <option value="">Период</option>
            <option value="day">Каждый день</option>
            <option value="week">Раз в неделю</option>
            <option value="month">Раз в месяц</option>
        </select>
    </div>
    <button class="create-notification__btn download-conspect__btn--submit button button--rounded" href=""><?=Loc::getMessage("MODAL_CREATE_NOTIFICATION_SUBMIT");?></button>
    <a class="download-conspect__btn--dismiss js-close_modal" href="javascript:void(0);"><?=Loc::getMessage("CANCEL");?></a>
</form>