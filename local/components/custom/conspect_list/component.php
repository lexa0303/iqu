<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use Bitrix\Main\Loader,
    Bitrix\Iblock;

Loader::includeModule("highloadblock");
$сache = Bitrix\Main\Data\Cache::createInstance();

$cUser = new CUser;
$dbUser = $cUser->GetList($by = array(), $order = array(), array("TITLE"=>$arParams["USER_TITLE"]));
$arrUser = $dbUser->Fetch();
$arrUser["IMAGE"] = LenalHelp::img($arrUser["PERSONAL_PHOTO"], 200, 200);
$userID = $arrUser["ID"];

if ($arParams["CACHE_TYPE"] != "N" && $сache->initCache($arParams['CACHE_TIME'], 'conspects_list' . $arParams["USER_TITLE"] . SITE_ID . $arParams["CATEGORY_ID"] . $arParams["MASTER"], '/conspects/' . $userID . "/list/")) {
    $arResult = $сache->getVars();
} elseif ($сache->startDataCache()) {

    $arResult = array();

    $dbUsers = $cUser->GetList($by = array(), $order = array(), array("UF_SUBSCRIBED"=>$userID));
    while ($res = $dbUsers->Fetch())
        $arUsers[] = $res;

    $arResult["USER"] = $arrUser;
    $arResult["USER"]["COUNT_SUBSCRIBERS"] = count($arUsers);

    if ($arParams["IS_AUTHOR"] == "Y"){
        $arFilter = array();
    } else {
        $arFilter = array(
            "UF_ACTIVE" => 1
        );
    }

    if ($arParams["CATEGORY_ID"] > 0) {
        $arCategory = Categories::GetByID($arParams["CATEGORY_ID"]);
        $arCategory["USER"]= $arResult["USER"];
        makeCategoryDetails($arCategory, $arParams["SEF_URL_TEMPLATES"], $arParams["MASTER"]);

        $arFilter["ID"] = $arCategory["UF_CONSPECTS"];
        $arResult["CATEGORY"] = $arCategory;
    }

    $arSort = array();
    if ($arParams["MASTER"] != "Y")
        $arFilter["UF_SHOW_IN_LIBRARY"] = 1;
    if (is_array($arParams["FILTER"]))
        $arFilter = array_merge($arFilter, $arParams["FILTER"]);
    if (is_array($arParams["SORT"]))
        $arSort = array_merge($arSort, $arParams["SORT"]);

    $arResult["ITEMS"] = Conspects::GetAll($userID, $arFilter, true, $arSort);

    foreach ($arResult["ITEMS"] as &$conspect){
        makeConspectDetails($conspect, $arParams["SEF_URL_TEMPLATES"]);

        $arResult["COUNT_DOWNLOADS"] += (int)$conspect["UF_SAVES_COUNT"];
    }

    $arResult["COUNT_CONSPECTS"] = count($arResult["ITEMS"]);



    // ...
    if ($isInvalid) {
        $cache->abortDataCache();
    }
    // ...


    $сache->endDataCache($arResult);
}

if ($arResult["USER"]["TITLE"] !=  $arParams["USER_TITLE"]){
    LocalRedirect(SITE_DIR);
}
?>
<? $this->IncludeComponentTemplate();
?>

