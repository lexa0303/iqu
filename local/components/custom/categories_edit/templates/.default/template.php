<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use Bitrix\Main\Localization\Loc;
$this->setFrameMode(true);
?>
<a class="modal__btn-close icon icon-cross" href="javascript:void(0)"></a>
<form class="new-category-modal__body ajax_submit">
    <input type="hidden" name="user_id" value="<?= $arResult["USER_ID"]; ?>">
    <input type="hidden" name="category_id" value="<?= $arResult["ID"]; ?>">
    <input type="hidden" name="action" value="edit_category">
    <span class="new-category-modal__heading"><?=Loc::getMessage("MODAL_EDIT_CATEGORY_HEADING");?></span>
    <span class="new-category-modal__text"><?=Loc::getMessage("MODAL_EDIT_CATEGORY_TEXT");?></span>
    <fieldset class="modal__fieldset">
        <label class="modal__label" for="category_edit_name"><?=Loc::getMessage("INPUT_HEADING_CATEGORY_NAME");?></label>
        <input
                required
                value="<?=$arResult["UF_NAME"];?>"
                class="modal__input"
                name="name"
                id="category_edit_name"
                type="text"
                placeholder="<?=Loc::getMessage("PLACEHOLDER_CATEGORY_NAME");?>">
    </fieldset>
    <button class="button button--rounded new-category-modal__btn" href="javascript:void(0)"><?=Loc::getMessage("SAVE");?></button>
    <a class="new-category-modal__btn--txt" href="javascript:void(0)"><?=Loc::getMessage("CANCEL");?></a>
</form>