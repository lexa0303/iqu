<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 15.02.17
 * Time: 15:53
 */

$el = new CIBlockElement;

$arFilter = array(
    "ID" => array(
        $arResult["UF_SHOP_CONSPECT_ID"],
        $arResult["UF_TRIAL_SHOP_ID"]
    )
);

$rsElements = $el->GetList(
    array(),
    $arFilter,
    false,
    false,
    array("ID", "CATALOG_GROUP_1")
);

while ($res = $rsElements->Fetch()){
    if ($res["ID"] == $arResult["UF_SHOP_CONSPECT_ID"]){
        $arResult["BUY"] = array(
            "ID" => $res["ID"],
            "PRICE" => $res["CATALOG_PRICE_1"],
            "PRICE_FORMATTED" => CCurrencyLang::CurrencyFormat($res["CATALOG_PRICE_1"], $res["CATALOG_CURRENCY_1"])
        );
    }

    if ($res["ID"] == $arResult["UF_TRIAL_SHOP_ID"]){
        $arResult["TRIAL"] = array(
            "ID" => $res["ID"],
            "PRICE" => $res,
            "PRICE_FORMATTED" => CCurrencyLang::CurrencyFormat($res["CATALOG_PRICE_1"], $res["CATALOG_CURRENCY_1"])
        );
    }
}

