<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Loader;

global $arUser;

Loader::includeModule("sale");
$currency = Bitrix\Currency\CurrencyManager::getBaseCurrency();
$this->setFrameMode(true);
?>
    <a class="modal__btn-close icon icon-cross" href="javascript:void(0)"></a>
<? if (!$arParams["OWNED"]) : ?>
    <form class="cart-modal__body" action="">
        <span class="new-category-modal__heading"><?= $arResult["UF_NAME"]; ?></span>
        <span class="new-category-modal__text">Купить или арендовать</span>
        <div class="modal-buttons__rent">
            <div class="modal-rent__link">
                <div class="rent__сonditions">Навсегда = <?= $arResult["BUY"]["PRICE_FORMATTED"]; ?></div>
                <a href="javascript:void(0);"
                   data-shop_conspect="<?= $arResult["BUY"]["ID"]; ?>"
                   data-conspect_id="<?= $arResult["ID"]; ?>"
                   class="btn button--rounded buy js-shop_buy">Купить навсегда</a>
            </div>
            <div class="modal-rent__link">
                <div class="rent__сonditions">30 дней = <?= $arResult["TRIAL"]["PRICE_FORMATTED"]; ?></div>
                <a href="javascript:void(0);"
                   data-shop_conspect="<?= $arResult["TRIAL"]["ID"]; ?>"
                   data-conspect_id="<?= $arResult["ID"]; ?>"
                   class="btn button--rounded continue js-shop_buy">Арендовать</a>
            </div>
        </div>
    </form>
<? else : ?>
    <form class="cart-modal__body" action="">
        <span class="new-category-modal__heading"><?= $arResult["UF_NAME"]; ?></span>
        <span class="new-category-modal__text">Продлить срок аренды</span>
        <div class="modal-buttons__rent">
            <div class="modal-rent__link">
                <div class="rent__сonditions">Навсегда = <?= $arResult["BUY"]["PRICE_FORMATTED"]; ?></div>
                <a href="javascript:void(0);"
                   data-shop_conspect="<?= $arResult["BUY"]["ID"]; ?>"
                   data-conspect_id="<?= $arResult["ID"]; ?>"
                   class="btn button--rounded buy js-shop_buy">купить навсегда</a>
            </div>
            <div class="modal-rent__link">
                <div class="rent__сonditions">30 дней = <?= $arResult["TRIAL"]["PRICE_FORMATTED"]; ?></div>
                <a href="javascript:void(0);"
                   data-shop_conspect="<?= $arResult["TRIAL"]["ID"]; ?>"
                   data-conspect_id="<?= $arResult["ID"]; ?>"
                   class="btn button--rounded continue js-shop_buy">продлить аренду</a>
            </div>
        </div>
        <div class="modal-buttons__delete">
            <a href="javascript:void(0);"
               class="open-modal"
               data-modal="delete-conspect-radio-modal"
               data-ajax="Y"
               data-action="conspect_delete"
               data-conspect_id="<?=$arParams["OWNED_CONSPECT_ID"];?>"
               data-user_id="<?=$USER->GetID();?>">
                <span class="icon icon-trashbox"></span>
                удалить конспект
            </a>
        </div>
    </form>
<? endif; ?>