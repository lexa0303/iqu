<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use Bitrix\Main\Localization\Loc;
$this->setFrameMode(true);
?>
<div class="col-md-3 col-sm-4 col-xs-6 fcard__box-wrap js-conspect"
     data-id="<?=$arResult["ID"];?>"
     data-study="<?= (int)$arResult["UF_STUDIED"]; ?>">
    <div class="fcard__box fcard__box--large ripple">
        <a class="fcard__img-wrap" href="<?= $arParams["BASKET_ITEM"]["CONSPECT"]["DETAIL_PAGE_URL"]; ?>">
            <img class="fcard__img" src="<?= $arResult["UF_IMAGE"]["RESIZED"]; ?>" alt="">
        </a>
        <div class="fcard__lower">
            <div class="fcard__heading-wrap">
                <a class="fcard__heading js-conspect_name"
                   title="<?= $arParams["BASKET_ITEM"]["NAME"]; ?>"
                   href="<?= $arParams["BASKET_ITEM"]["CONSPECT"]["DETAIL_PAGE_URL"]; ?>"><?= $arParams["BASKET_ITEM"]["NAME"]; ?></a>
            </div>
            <div class="fcard__author-wrap">
                <a class="fcard__author"
                   href="<?= $arResult["USER"]["DETAIL_PAGE_URL"]; ?>"><?= $arResult["USER"]["TITLE"]; ?></a>
            </div>
            <div class="fcard__price">
                <?=CCurrencyLang::CurrencyFormat($arParams["BASKET_ITEM"]["PRICE"], $arParams["BASKET_ITEM"]["CURRENCY"]);?>
            </div>
            <div class="fcard__extra-info">
                <div class="fcard__ammount" title="<?= Loc::getMessage("CONSPECT_LIST_CARD_COUNT_TITLE"); ?>">
                    <span><?= $arResult["CARD_COUNT"]; ?></span>
                </div>
                <div class="fcard__cdate" title="<?= Loc::getMessage("CONSPECT_LIST_DATE_TITLE"); ?>">
                    <span><?=Loc::getMessage('VERSION_OT');?> <?= ($arResult["MASTER"] == "Y") ? $arResult["UF_CREATE_TIME"]->format("d.m.Y") : $arResult["UF_UPDATE_TIME"]->format("d.m.Y"); ?></span>
                </div>
                <a class="fcard__download js-shop_delete"
                   data-id="<?=$arParams["CART_ITEM_ID"];?>"
                   href="javascript:void(0);"
                   title="Удалить конспект из корзины"
                   style="background-image: url(/local/templates/iq/images/delete-card-icon.png)"></a>
            </div>
        </div>
    </div>
</div>