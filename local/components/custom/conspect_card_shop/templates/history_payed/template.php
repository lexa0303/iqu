<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use Bitrix\Main\Localization\Loc;
$this->setFrameMode(true);
?>
<div class="col-md-3 col-sm-4 col-xs-6 fcard__box-wrap js-conspect"
     data-id="<?=$arResult["ID"];?>">
    <div class="fcard__box fcard__box--large ripple">
        <a class="fcard__img-wrap" href="<?= $arResult["DETAIL_PAGE_URL"]; ?>">
            <img class="fcard__img" src="<?= $arResult["UF_IMAGE"]["RESIZED"]; ?>" alt="">
        </a>
        <div class="fcard__lower">
            <div class="fcard__heading-wrap">
                <a class="fcard__heading js-conspect_name"
                   href="<?= $arResult["DETAIL_PAGE_URL"]; ?>"><?= $arResult["UF_NAME"]; ?></a>
            </div>
            <div class="fcard__author-wrap">
                <a class="fcard__author"
                   href="<?= $arResult["USER"]["DETAIL_PAGE_URL"]; ?>"><?= $arResult["USER"]["TITLE"]; ?></a>
            </div>
            <div class="fcard__extra-info">
                <div class="fcard__ammount" title="<?= Loc::getMessage("CONSPECT_LIST_CARD_COUNT_TITLE"); ?>">
                    <span><?= $arResult["CARD_COUNT"]; ?></span>
                </div>
                <div class="fcard__cdate" title="<?= Loc::getMessage("CONSPECT_LIST_DATE_TITLE"); ?>">
                    <span><?=Loc::getMessage('VERSION_OT');?> <?= ($arResult["MASTER"] == "Y") ? $arResult["UF_CREATE_TIME"]->format("d.m.Y") : $arResult["UF_UPDATE_TIME"]->format("d.m.Y"); ?></span>
                </div>
                <?if ($arResult["UF_USER_ID"] != $USER->GetID() && $USER->IsAuthorized()) : ?>
                    <a class="fcard__download js-save_conspect"
                       data-conspect_id="<?=$arResult["ID"];?>"
                       data-user_id="<?=$arResult["USER"]["ID"];?>"
                       href="javascript:void(0);"
                       style="background-image: url(/local/templates/iq/images/download-card-icon.png)"></a>
                <?endif;?>
            </div>
        </div>
    </div>
</div>