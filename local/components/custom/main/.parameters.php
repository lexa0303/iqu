<?
if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)die();
CModule::IncludeModule("iblock");
CModule::IncludeModule("highloadblock");
$arTypesEx = CIBlockParameters::GetIBlockTypes(Array('-'=>' '));
$arIBlocks=Array();
$db_iblock = CIBlock::GetList(
    Array('SORT'=>'ASC'),
    Array(
        'TYPE' => ($arCurrentValues['CONSPECTS_IBLOCK_TYPE']!='-'?$arCurrentValues['CONSPECTS_IBLOCK_TYPE']:'')
    )
);
while($arRes = $db_iblock->Fetch())
        $arIBlocks[$arRes['ID']] = $arRes['NAME'];

$dbHLBlock = \Bitrix\Highloadblock\HighloadBlockTable::getList(
    array(

    )
);
while ($res = $dbHLBlock->fetch())
    $arHLBlocks[$res["ID"]] = $res["NAME"];

$arPages = array(
    "main_page" => "Главная",
    "search" => "Поиск",
    "conspect_edit" => "Редактирование конспекта",
    "conspect_add" => "Добавление конспекта",
    "conspect_detail" => "Детальная страница конспекта",
    "conspect_list" => "Список конспектов",
    "category_detail" => "Детальная страница категории",
    "categories_list" => "Список категорий",
    "exam" => "Страница экзамена",
    "lesson" => "Страница урока",

);

$arComponentParameters = array(
    'PARAMETERS' => array(
        'CONSPECTS_HLBLOCK' => Array(
            'PARENT' => 'BASE',
            'NAME' => 'ID инфоблока конспектов',
            'TYPE' => 'LIST',
            'VALUES' => $arHLBlocks,
            'DEFAULT' => '',
            'ADDITIONAL_VALUES' => 'Y',
            'REFRESH' => 'Y',
        ),
        "CATEGORIES_HLBLOCK" => Array(
            'PARENT' => 'BASE',
            'NAME' => 'ID HL-блока категорий',
            'TYPE' => 'LIST',
            'VALUES' => $arHLBlocks,
            'DEFAULT' => '',
            'ADDITIONAL_VALUES' => 'Y',
            'REFRESH' => 'Y',
        ),
        "CARDS_HLBLOCK" => Array(
            'PARENT' => 'BASE',
            'NAME' => 'ID HL-блока карточек',
            'TYPE' => 'LIST',
            'VALUES' => $arHLBlocks,
            'DEFAULT' => '',
            'ADDITIONAL_VALUES' => 'Y',
            'REFRESH' => 'Y',
        ),
        "CATEGORY_SET_LIST_TITLE" => Array(
            'PARENT' => 'BASE',
            'NAME' => 'Задавать заголовок на странице категорий',
            'TYPE' => 'CHECKBOX',
            'DEFAULT' => 'Y',
        ),
        'MASTER_NEEDED' => Array(
            'PARENT' => 'BASE',
            'NAME' => 'Страницы для владельца',
            'TYPE' => 'LIST',
            'VALUES' => $arPages,
            'DEFAULT' => '',
            'ADDITIONAL_VALUES' => 'Y',
            'REFRESH' => 'Y',
            "MULTIPLE" => "Y",
        ),
        'NO_AUTH_PAGES' => Array(
            'PARENT' => 'BASE',
            'NAME' => 'Страницы доступные неавторизированному пользователю',
            'TYPE' => 'LIST',
            'VALUES' => $arPages,
            'DEFAULT' => '',
            'ADDITIONAL_VALUES' => 'Y',
            'REFRESH' => 'Y',
            "MULTIPLE" => "Y",
        ),
        'CATEGORY_PAGEN_COUNT' => Array(
            'PARENT' => 'BASE',
            'NAME' => 'Количество категорий на странице',
            'TYPE' => 'TEXT',
            'DEFAULT' => '',
            "MULTIPLE" => "N",
        ),
        'CATEGORY_PAGEN_TEMPLATE' => Array(
            'PARENT' => 'BASE',
            'NAME' => 'Шаблон постраничной навигации на странице категорий',
            'TYPE' => 'TEXT',
            'DEFAULT' => '.default',
            "MULTIPLE" => "N",
        ),
        "VARIABLE_ALIASES" => array(
            "CARD_LIST" => array(
                "NAME" => "Список карточек",
            ),
            "SECTION_ID" => array(
                "NAME" => "SECTION_ID",
            ),

        ),
        'SEF_FOLDER' => Array(
            'PARENT' => 'BASE',
            'NAME' => 'Корневой раздел ЧПУ',
            'TYPE' => 'TEXT',
            'DEFAULT' => '/',
            "MULTIPLE" => "N",
        ),
        "SEF_MODE" => array(
            "main_page" => array(
                "NAME" => "Главная страница",
                "DEFAULT" => "",
                "VARIABLES" => array(
                ),
            ),
            "conspect_add" => array(
                "NAME" => "Добавление конспекта",
                "DEFAULT" => "conspects/#USER_TITLE#/add/",
                "VARIABLES" => array(
                    "USER_TITLE"
                ),
            ),
            "conspect_detail" => array(
                "NAME" => "Детальная страница конспекта",
                "DEFAULT" => "conspects/#USER_TITLE#/#CONSPECT_ID#/",
                "VARIABLES" => array(
                    "CONSPECT_ID",
                    "USER_TITLE"
                ),
            ),
            "conspect_edit" => array(
                "NAME" => "Редактирование конспекта",
                "DEFAULT" => "conspects/#USER_TITLE#/#CONSPECT_ID#/edit/",
                "VARIABLES" => array(
                    "CONSPECT_ID",
                    "USER_TITLE"
                ),
            ),
            "conspect_list" => array(
                "NAME" => "Список конспектов",
                "DEFAULT" => "conspects/#USER_TITLE#/",
                "VARIABLES" => array(
                    "USER_TITLE"
                ),
            ),
            "category_detail" => array(
                "NAME" => "Детальная страница категории",
                "DEFAULT" => "category/#USER_TITLE#/#CATEGORY_ID#/",
                "VARIABLES" => array(
                    "CATEGORY_ID",
                    "USER_TITLE"
                ),
            ),
            "categories_list" => array(
                "NAME" => "Список категорий",
                "DEFAULT" => "category/#USER_TITLE#/",
                "VARIABLES" => array(
                    "USER_TITLE"
                ),
            ),
            "exam" => array(
                "NAME" => "Екзамен по конспекту",
                "DEFAULT" => "conspects/#USER_TITLE#/#CONSPECT_ID#/exam/",
                "VARIABLES" => array(
                    "CONSPECT_ID",
                    "USER_TITLE"
                ),
            ),
            "lesson" => array(
                "NAME" => "Урок по конспекту",
                "DEFAULT" => "conspects/#USER_TITLE#/#CONSPECT_ID#/lesson/",
                "VARIABLES" => array(
                    "CONSPECT_ID",
                    "USER_TITLE"
                ),
            ),
            "search" => array(
                "NAME" => "Поиск",
                "DEFAULT" => "search/",
                "VARIABLES" => array(),
            ),
        ),
        'CACHE_TIME'  =>  array('DEFAULT'=>36000),
    ),
);
?>