<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$arComponentVariables = array(
    'USER_TITLE',
    'CONSPECT_ID',
    'CATEGORY_ID',
);
$arDefaultUrlTemplates404 = array(
    "main_page" => "",
    "conspect_add" => "conspects/add/",
    "conspect_detail" => "conspects/#USER_TITLE#/#CONSPECT_ID#/",
    "conspect_edit" => "conspects/#USER_TITLE#/#CONSPECT_ID#/edit/",
    "conspect_list" => "conspects/#USER_TITLE#/",
    "category_detail" => "category/#USER_TITLE#/#CATEGORY_ID#/",
    "categories_list" => "categories/#USER_TITLE#/",
    "exam" => "conspects/#USER_TITLE#/#CONSPECT_ID#/exam/",
    "library" => "library/",
    "search" => "search/",
    "new" => "new/",
);

$arVariables = array();
$arUrlTemplates = CComponentEngine::MakeComponentUrlTemplates($arDefaultUrlTemplates404, $arParams['SEF_URL_TEMPLATES']);

$componentPage = CComponentEngine::ParseComponentPath($arParams['SEF_FOLDER'], $arUrlTemplates, $arVariables);

if (($componentPage == "conspect_detail") && (strlen($arVariables["CONSPECT_ID"]) > 0) && (strlen($arVariables["USER_TITLE"]) > 0)) {
    $component = "conspect_detail";
} elseif (($componentPage == "conspect_edit") && (strlen($arVariables["CONSPECT_ID"]) > 0) && (strlen($arVariables["USER_TITLE"]) > 0)) {
    $component = "conspect_edit";
} elseif (($componentPage == "conspect_detail") && (strlen($arVariables["CONSPECT_ID"]) > 0) && (strlen($arVariables["USER_TITLE"]) > 0)) {
    $component = "conspect_detail";
} elseif ($componentPage == "conspect_add") {
    $component = "conspect_add";
} elseif (($componentPage == "conspect_list") && (strlen($arVariables["USER_TITLE"]) > 0)) {
    $component = "conspect_list";
} elseif (($componentPage == "category_detail") && (strlen($arVariables["CATEGORY_ID"]) > 0) && (strlen($arVariables["USER_TITLE"]) > 0)) {
    $component = "category";
} elseif (($componentPage == "categories_list") && (strlen($arVariables["USER_TITLE"]) > 0)) {
    $component = "categories_list";
} elseif (($componentPage == "exam") && (strlen($arVariables["CONSPECT_ID"]) > 0) && (strlen($arVariables["USER_TITLE"]) > 0)) {
    $component = "exam";
} elseif (($componentPage == "exam_result") && (strlen($arVariables["CONSPECT_ID"]) > 0) && (strlen($arVariables["USER_TITLE"]) > 0) && intval($arVariables["RESULT_ID"])) {
    $component = "exam_result";
} elseif (($componentPage == "lesson") && (strlen($arVariables["CONSPECT_ID"]) > 0) && (strlen($arVariables["USER_TITLE"]) > 0)) {
    $component = "lesson";
} elseif ($componentPage == "library") {
$component = "library";
} elseif ($componentPage == "new") {
    $component = "new";
} elseif ($componentPage == "search") {
    $component = "search";
} else {
    $component = "main_page";
}

$arResult["VARIABLES"] = $arVariables;

if ($arParams["USER_TITLE"] == $arResult["VARIABLES"]["USER_TITLE"])
    $arResult["MASTER"] = "Y";
else
    $arResult["MASTER"] = "N";

/* ACCESS CHECK FOR PAGES */
if ($arResult["MASTER"] == "N" && in_array($component, $arParams["MASTER_NEEDED"]))
    LocalRedirect(SITE_DIR);

if (!$USER->IsAuthorized() && in_array($component, $arParams["NO_AUTH_PAGES"])) {
    LocalRedirect(SITE_DIR);
}

if ($component == "main_page" && $APPLICATION->GetCurPage(false) != SITE_DIR)
    LocalRedirect(SITE_DIR);
/* ACCESS CHECK FOR PAGES */

?>
<? $this->IncludeComponentTemplate($component);