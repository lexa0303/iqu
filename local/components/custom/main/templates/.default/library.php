<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 20.03.17
 * Time: 10:08
 */
?>
<?php
$arLibraryFilter = array();
$arLibrarySort = false;

global $arUser;
$arSelfConspectIDs = array();
if ($USER->IsAuthorized()) {
    $arSelfConspects = Conspects::GetAll($USER->GetID());

    foreach ($arSelfConspects as $arConspect) {
        if ($arConspect["UF_ORIGIN_USER"] && $arConspect["UF_ORIGIN_CONSPECT"])
            $arSelfConspectIDs[] = $arConspect["UF_ORIGIN_CONSPECT"];
    }
}

switch ($_GET["sort"]) :
    case "popular":
        $arLibrarySort = array(
            "UF_VIEW_COUNT_MONTH" => "DESC"
        );
        $arLibraryFilter = array(
            "!ID" => $arSelfConspectIDs
        );
        break;
    case "subscribed":
        global $arUser;
        $arLibraryFilter = array(
            "UF_USER_ID" => $arUser["UF_SUBSCRIBED"],
        );
        $arLibrarySort = array("UF_UPDATE_TIME"=>"desc");
        break;
    case "all":
        $arLibrarySort = false;
        break;
    default:
        $arLibrarySort = array("UF_UPDATE_TIME"=>"desc");
        $arLibraryFilter = array(
            "!ID" => $arSelfConspectIDs
        );
        break;
endswitch;

?>
<?$APPLICATION->IncludeComponent(
    "custom:library",
    "",
    array(
        "CONSPECTS_HLBLOCK" => $arParams["CONSPECTS_HLBLOCK"],
        "CATEGORIES_HLBLOCK" => $arParams["CATEGORIES_HLBLOCK"],
        "CARDS_HLBLOCK" => $arParams["CARDS_HLBLOCK"],
        "USER_TITLE" => $arResult["VARIABLES"]["USER_TITLE"],
        "SEF_FOLDER" => $arParams["SEF_FOLDER"],
        "SEF_URL_TEMPLATES" => $arParams["SEF_URL_TEMPLATES"],
        "COMPONENT_TEMPLATE" => ".default",
        "CACHE_TYPE" => "N",
        "CACHE_TIME" => $arParams["CACHE_TIME"],
        "MASTER" => $arResult["MASTER"],
        "SET_TITLE" => $arParams["CATEGORY_CATEGORY_LIST_TITLE"],
        "PAGEN_COUNT" => $arParams["LIBRARY_PAGEN_COUNT"],
        "PAGEN_TEMPLATE" => $arParams["LIBRARY_PAGEN_TEMPLATE"],
        "FILTER" => $arLibraryFilter,
        "SORT" => $arLibrarySort
    ),
    $component
);?>
