<?php
/**
 * Created by PhpStorm.
 * User: al
 * Date: 01.11.2016
 * Time: 10:07
 */

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$this->setFrameMode(true);
?>
<?$APPLICATION->IncludeComponent(
    "custom:categories_add",
    ".default",
    array(
        "IBLOCK_ID" => "4",
        "IBLOCK_TYPE" => "conspect",
        "USER_ID" => $arResult["VARIABLES"]["USER_ID"],
        "FORM_ACTION" => $arParams["SEF_FOLDER"] . $arParams["SEF_URL_TEMPLATES"]["category"],
        "COMPONENT_TEMPLATE" => ".default",
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => $arParams["CACHE_TIME"]
    ),
    $component
);?>