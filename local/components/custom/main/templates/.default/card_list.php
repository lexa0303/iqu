<?php
/**
 * Created by PhpStorm.
 * User: al
 * Date: 01.11.2016
 * Time: 10:06
 */
?>
<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$this->setFrameMode(true);
?>
<?php
$pathToConspectList = ($arResult["VARIABLES"]["CATEGORY_REFERER"] > 0) ?
        $arParams["SEF_FOLDER"] . str_replace(
            "#CATEGORY_ID#",
            $arResult["VARIABLES"]["CATEGORY_REFERER"],
            $arParams["SEF_URL_TEMPLATES"]["category"]
        )
    :
        $arParams["SEF_FOLDER"];
?>
<?$APPLICATION->IncludeComponent(
    "custom:card_list",
    ".default",
    array(
        "IBLOCK_ID" => "4",
        "IBLOCK_TYPE" => "conspect",
        "CONSPECT_ID" => $arResult["VARIABLES"]["CONSPECT_ID"],
        "USER_ID" => $arResult["VARIABLES"]["USER_ID"],
        "CATEGORY_ID" => $arResult["VARIABLES"]["CATEGORY_REFERER"],
        "PATH_TO_CONSPECT_LIST" => $pathToConspectList,
        "PATH_TO_ADD_CARD" => $arParams["SEF_FOLDER"] . str_replace(
                "#CONSPECT_ID#",
                $arResult["VARIABLES"]["CONSPECT_ID"],
                $arParams["SEF_URL_TEMPLATES"]["card_add"]
            ),
        "PATH_TO_CONSPECT_EDIT" => $arParams["SEF_FOLDER"] . str_replace(
                "#CONSPECT_ID#",
                $arResult["VARIABLES"]["CONSPECT_ID"],
                $arParams["SEF_URL_TEMPLATES"]["conspect_edit"]
            ),
        "PATH_TO_CARD_EDIT" => $arParams["SEF_FOLDER"] . $arParams["SEF_URL_TEMPLATES"]["card_edit"],
        "PATH_TO_EXAM" => $arParams["SEF_FOLDER"] . str_replace(
                "#CONSPECT_ID#",
                $arResult["VARIABLES"]["CONSPECT_ID"],
                $arParams["SEF_URL_TEMPLATES"]["exam"]
            ),
        "COMPONENT_TEMPLATE" => ".default",
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => $arParams["CACHE_TIME"]
    ),
    $component
);?>