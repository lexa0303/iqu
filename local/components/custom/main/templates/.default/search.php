<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 20.03.17
 * Time: 15:21
 */
?>
<?$APPLICATION->IncludeComponent(
    "custom:search",
    "",
    array(
        "CONSPECTS_HLBLOCK" => $arParams["CONSPECTS_HLBLOCK"],
        "CATEGORIES_HLBLOCK" => $arParams["CATEGORIES_HLBLOCK"],
        "CARDS_HLBLOCK" => $arParams["CARDS_HLBLOCK"],
        "USER_TITLE" => $arResult["VARIABLES"]["USER_TITLE"],
        "SEF_FOLDER" => $arParams["SEF_FOLDER"],
        "SEF_URL_TEMPLATES" => $arParams["SEF_URL_TEMPLATES"],
        "COMPONENT_TEMPLATE" => ".default",
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => $arParams["CACHE_TIME"],
        "MASTER" => $arResult["MASTER"],
        "SET_TITLE" => $arParams["CATEGORY_CATEGORY_LIST_TITLE"],
        "PAGEN_COUNT" => $arParams["LIBRARY_PAGEN_COUNT"],
        "PAGEN_TEMPLATE" => $arParams["LIBRARY_PAGEN_TEMPLATE"],
        "FILTER" => $arLibraryFilter,
        "SORT" => $arLibrarySort
    ),
    $component
);?>
