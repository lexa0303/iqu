<?php
/**
 * Created by PhpStorm.
 * User: al
 * Date: 01.11.2016
 * Time: 10:08
 */

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$this->setFrameMode(true);
?>
<?$APPLICATION->IncludeComponent(
    "custom:conspect_detail",
    "lesson",
    array(
        "CONSPECT_ID" => $arResult["VARIABLES"]["CONSPECT_ID"],
        "CONSPECTS_HLBLOCK" => $arParams["CONSPECTS_HLBLOCK"],
        "CATEGORIES_HLBLOCK" => $arParams["CATEGORIES_HLBLOCK"],
        "CARDS_HLBLOCK" => $arParams["CARDS_HLBLOCK"],
        "USER_TITLE" => $arResult["VARIABLES"]["USER_TITLE"],
        "SEF_FOLDER" => $arParams["SEF_FOLDER"],
        "SEF_URL_TEMPLATES" => $arParams["SEF_URL_TEMPLATES"],
        "COMPONENT_TEMPLATE" => "lesson",
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => $arParams["CACHE_TIME"],
        "MASTER" => $arResult["MASTER"],
        "SET_TITLE" => $arParams["CATEGORY_CATEGORY_LIST_TITLE"],
        "PAGEN_COUNT" => $arParams["CATEGORY_PAGEN_COUNT"],
        "PAGEN_TEMPLATE" => $arParams["CATEGORY_PAGEN_TEMPLATE"],
        "NON_STUDIED_ONLY" => "Y"
    ),
    $component
);?>