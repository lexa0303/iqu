<?php
/**
 * Created by PhpStorm.
 * User: al
 * Date: 01.11.2016
 * Time: 10:08
 */

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$this->setFrameMode(true);
?>
<section>
    <div class="container">
        <?$APPLICATION->IncludeComponent(
            "custom:conspects_edit",
            ".default",
            array(
                "CONSPECTS_HLBLOCK" => $arParams["CONSPECTS_HLBLOCK"],
                "CATEGORIES_HLBLOCK" => $arParams["CATEGORIES_HLBLOCK"],
                "CARDS_HLBLOCK" => $arParams["CARDS_HLBLOCK"],
                "USER_TITLE" => $arResult["VARIABLES"]["USER_TITLE"],
                "COMPONENT_TEMPLATE" => ".default",
                "CACHE_TYPE" => "A",
                "CACHE_TIME" => $arParams["CACHE_TIME"],
                "SEF_URL_TEMPLATES" => $arParams["SEF_URL_TEMPLATES"],
                "SEF_FOLDER" => $arParams["SEF_FOLDER"],
                "CONSPECT_ID" => $arResult["VARIABLES"]["CONSPECT_ID"]
            ),
            $component
        );?>
    </div>
</section>