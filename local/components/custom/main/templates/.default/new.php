<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 26.04.17
 * Time: 9:31
 */
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
?>
<?global $arUser;
$arSelfConspects = Conspects::GetAll($USER->GetID());

foreach ($arSelfConspects as $arConspect){
    if ($arConspect["UF_ORIGIN_USER"] && $arConspect["UF_ORIGIN_CONSPECT"])
        $arSelfConspectIDs[] = $arConspect["UF_ORIGIN_CONSPECT"];
}

$arLibraryFilter = array(
    "!ID" => $arSelfConspectIDs,
    "UF_SHOW_IN_LIBRARY" => 1,
    "UF_USER_ID" => $arUser["UF_SUBSCRIBED"],
    ">=UF_CREATE_TIME" => array(ConvertTimeStamp(time()-86400, "FULL")),
    "UF_ORIGIN_USER" => false,
);
$arLibrarySort = array(
    "UF_UPDATE_TIME" => "desc"
);
if ($_COOKIE["VIEWED_CONSPECTS"]){
    $arViewed = unserialize($_COOKIE["VIEWED_CONSPECTS"]);
    $arLibraryFilter["!ID"] = array_merge($arLibraryFilter["!ID"], $arViewed);
}
?>
<?$APPLICATION->IncludeComponent(
    "custom:library",
    "new",
    array(
        "CONSPECTS_HLBLOCK" => $arParams["CONSPECTS_HLBLOCK"],
        "CATEGORIES_HLBLOCK" => $arParams["CATEGORIES_HLBLOCK"],
        "CARDS_HLBLOCK" => $arParams["CARDS_HLBLOCK"],
        "USER_TITLE" => $arResult["VARIABLES"]["USER_TITLE"],
        "SEF_FOLDER" => $arParams["SEF_FOLDER"],
        "SEF_URL_TEMPLATES" => $arParams["SEF_URL_TEMPLATES"],
        "COMPONENT_TEMPLATE" => ".default",
        "CACHE_TYPE" => $arParams["CACHE_TYPE"],
        "CACHE_TIME" => $arParams["CACHE_TIME"],
        "MASTER" => $arResult["MASTER"],
        "SET_TITLE" => $arParams["CATEGORY_CATEGORY_LIST_TITLE"],
        "PAGEN_COUNT" => $arParams["LIBRARY_PAGEN_COUNT"],
        "PAGEN_TEMPLATE" => $arParams["LIBRARY_PAGEN_TEMPLATE"],
        "FILTER" => $arLibraryFilter,
        "SORT" => $arLibrarySort
    ),
    $component
);?>

