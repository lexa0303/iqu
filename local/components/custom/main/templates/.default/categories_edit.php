<?php
/**
 * Created by PhpStorm.
 * User: al
 * Date: 01.11.2016
 * Time: 10:08
 */

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$this->setFrameMode(true);
?>
<?$APPLICATION->IncludeComponent(
    "custom:categories_edit",
    ".default",
    array(
        "CATEGORY_ID" => $arResult["VARIABLES"]["CATEGORY_ID"],
        "USER_ID" => $arResult["VARIABLES"]["USER_ID"],
        "FORM_ACTION" => $arParams["SEF_FOLDER"] . str_replace(
                "#CATEGORY_ID#",
                $arResult["VARIABLES"]["CATEGORY_ID"],
                $arParams["SEF_URL_TEMPLATES"]["category"]
            ),
        "COMPONENT_TEMPLATE" => ".default",
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => $arParams["CACHE_TIME"]
    ),
    $component
);?>