<?php
/**
 * Created by PhpStorm.
 * User: al
 * Date: 01.11.2016
 * Time: 10:08
 */

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$this->setFrameMode(true);
global $arUser;
if ($arUser["IS_AUTHOR"] == "Y") {
    $template = "author";
} else {
    $template = ($arResult["MASTER"] == "Y") ? "user_conspects" : "another_user_conspects";
}
if ($template === "user_conspects") {
    $arSort = array(
        "UF_VIEWED_TIME" => "DESC"
    );
} elseif ($template == "another_user_conspects"){
    $arFilter = array(
        "UF_ORIGIN_USER" => false
    );
}

?>
<?$APPLICATION->IncludeComponent(
    "custom:conspect_list",
    $template,
    array(
        "CONSPECTS_HLBLOCK" => $arParams["CONSPECTS_HLBLOCK"],
        "CATEGORIES_HLBLOCK" => $arParams["CATEGORIES_HLBLOCK"],
        "CARDS_HLBLOCK" => $arParams["CARDS_HLBLOCK"],
        "USER_TITLE" => $arResult["VARIABLES"]["USER_TITLE"],
        "SEF_FOLDER" => $arParams["SEF_FOLDER"],
        "SEF_URL_TEMPLATES" => $arParams["SEF_URL_TEMPLATES"],
        "COMPONENT_TEMPLATE" => ".default",
        "CACHE_TYPE" => $arParams["CACHE_TYPE"],
        "CACHE_TIME" => $arParams["CACHE_TIME"],
        "MASTER" => $arResult["MASTER"],
        "SET_TITLE" => $arParams["CATEGORY_CATEGORY_LIST_TITLE"],
        "PAGEN_COUNT" => $arParams["CATEGORY_PAGEN_COUNT"],
        "PAGEN_TEMPLATE" => $arParams["CATEGORY_PAGEN_TEMPLATE"],
        "SORT" => $arSort,
        "IS_AUTHOR" => $arUser["IS_AUTHOR"]
    ),
    $component
);?>