<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$this->setFrameMode(true);
?>
<form id="card_add_form" action="<?=$arParams["FORM_ACTION"];?>">
    <input type="hidden" name="conspect" value="<?=$arResult["CONSPECT_ID"];?>"/>
    <input type="hidden" name="user_id" value="<?=$arResult["USER_ID"];?>"/>
    <div class="card_fields_wrap wrap">
        <div class="card_fields_heading heading">Заполните поля</div>
        <input type="text" name="question" id="card_add_question">
        <label for="card_add_question">Вопрос</label>
        <br>
        <input type="text" name="answer" id="card_add_answer">
        <label for="card_add_answer">Ответ</label>
        <img
            alt=""
            id="card_add_image"
        >
        <button id="delete_image" class="hidden">Удалить изображение</button>
        <input type="file" name="image" id="card_add_file">
        <input type="hidden" name="image_url" id="card_add_image_url">
        <div class="progress_bar"></div>
        <input type="hidden" name="action" value="add">
        <input type="submit" id="card_add_submit">
    </div>
    <div id="errors_cards" class="wrap"></div>
</form>