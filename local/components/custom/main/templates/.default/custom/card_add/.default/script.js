/**
 * Created by al on 01.11.2016.
 */

$(document).ready(function(){
    $("body").find("#card_add_submit").on("click", function(e){
        e.preventDefault();

        var data = $(e.target.closest("form")).serialize();
        var errorContainer = document.getElementById("errors_cards");

        $.ajax({
            url: globals.ajax + "cards.php",
            data: data,
            type: "post",
            success: function(result) {
                if (result == true) {
                    errorContainer.innerHTML = "Карточка успешно добавлена";
                    setTimeout(function(){
                        location.href = e.target.closest("form").getAttribute("action");
                    }, 1000);
                } else {
                    var errors = "";

                    var res = $.parseJSON(result);

                    for (var key in res.errors){
                        errors += res.errors[key];
                        errors += "<br>";
                    };

                    errorContainer.innerHTML = errors;
                }
            }
        });
    });

    document.getElementById("card_add_file").addEventListener("change", function(e){
        sendFile(this, function(result) {
            console.log(result);
            document.getElementById('card_add_image_url').value = result.file;
            document.getElementById('card_add_image').src = result.file;

            var file = e.target;
            var button = file.closest("form").querySelector("button");
            var progress_bar = file.closest("form").querySelector(".progress_bar");

            button.classList.remove("hidden");
            file.classList.add("hidden");
        });
    });

    document.getElementById("delete_image").addEventListener("click", function(e){
        e.preventDefault();
        var button = e.target;
        var img = button.closest("form").querySelector("img");
        var file = button.closest("form").querySelector("input[type=file]");
        var progress_bar = button.closest("form").querySelector(".progress_bar");

        button.classList.add("hidden");
        progress_bar.classList.add("hidden");
        file.classList.remove("hidden");
        img.src = "";
    });
});