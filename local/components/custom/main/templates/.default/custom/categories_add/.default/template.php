<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$this->setFrameMode(true);
?>
<form id="categories_add_form" action="<?=$arParams["FORM_ACTION"];?>">
    <input type="hidden" name="action" value="add">
    <div class="heading">Добавление категории</div>
    <div class="categories_fields_wrap wrap">
        <input type="text" name="name" id="categories_add_name">
        <label for="categories_add_name">Название категории</label>
        <img
            alt=""
            id="categories_add_image"
        >
        <button id="delete_image" class="hidden">Удалить изображение</button>
        <input type="file" name="image" id="categories_add_file">
        <input type="hidden" name="image_url" id="categories_add_image_url">
        <div class="progress_bar"></div>
        <input type="hidden" name="user_id" value="<?=$arResult["USER_ID"];?>">
        <input type="submit" id="categories_add_submit">
    </div>
    <div id="errors_categories" class="wrap"></div>
</form>