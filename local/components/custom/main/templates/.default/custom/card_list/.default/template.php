<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$this->setFrameMode(true);
?>
<div class="card_list-heading_wrap">
    <div class="card_list-heading_image">
        <img src="<?= $arResult["IMAGE"]["SRC"]; ?>">
    </div>
    <div class="card_list-heading_name heading">Конспект - <?= $arResult["NAME"]; ?></div>
</div>
<div class="card_list">
    <? if (count($arResult["ITEMS"]) > 0) : ?>
        <? foreach ($arResult["ITEMS"] as $arItem) : ?>
            <div class="card_item">
                <div class="card_item-image">
                    <? $image = CFile::ResizeImageGet(
                        $arItem["UF_IMAGE"]["ID"], array("width" => 100, "height" => 100), "BX_RESIZE_IMAGE_PROPORTIONAL"
                    ); ?>
                    <img src="<?= $image["src"]; ?>"/>
                </div>
                <div class="card_item-questions_wrap">
                    <div class="card_item-row">
                        <div class="card_item-question_name">Вопрос:</div>
                        <div class="card_item-question_value"><?= $arItem["UF_QUESTION"] ?></div>
                    </div>
                    <div class="card_item-row">
                        <div class="card_item-answer_name">Ответ</div>
                        <div class="card_item-answer_value"><?= $arItem["UF_ANSWER"] ?></div>
                    </div>
                </div>
                <div class="card_item-edit_link_wrap">
                    <a class="card_item-edit_link" href="<?=$arItem["DETAIL_PAGE_URL"];?>">Изменить карточку</a>
                </div>
                <div class="card_item-delete_link_wrap">
                    <a class="card_item-delete_link delete_card" href="javascript:void(0);" data-id="<?=$arItem["ID"];?>">Удалить карточку</a>
                </div>
            </div>
            <div style="clear: both"></div>
        <? endforeach; ?>
    <? else : ?>
        <div class="card_list-error">В этом конспекте нет карточек</div>
    <? endif; ?>
    <div class="card_list-back_button button">
        <a href="<?= $arResult["PATH_TO_CONSPECT_LIST"]; ?>">Вернуться к списку конспектов</a>
    </div>
    <div class="card_list-delete_button button">
        <a href="<?= $arResult["PATH_TO_CONSPECT_EDIT"]; ?>">Изменить конспект</a>
    </div>
    <div class="card_list-edit_button button">
        <a class="delete_conspect" data-id="<?=$arResult["ID"];?>" data-href="<?= $arResult["PATH_TO_CONSPECT_LIST"]; ?>" href="javascript:void(0);">Удалить конспект</a>
    </div>
    <div class="card_list-add_button button">
        <a href="<?= $arResult["PATH_TO_ADD_CARD"]; ?>">Добавить новую карточку</a>
    </div>
    <div class="card_list-exam_button button">
        <a href="<?= $arResult["PATH_TO_EXAM"]; ?>">Пройти экзамен</a>
    </div>
</div>