<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$this->setFrameMode(true);
?>
<form id="card_edit_form" action="<?=str_replace("#CONSPECT_ID#", $arResult["UF_CONSPECTS"], $arParams["FORM_ACTION"]);?>">
    <input type="hidden" name="card_id" value="<?=$arResult["ID"];?>">
    <input type="hidden" name="action" value="update">
    <div class="heading">Редактирование карточки</div>
    <div class="card_fields_wrap wrap">
        <input type="text" name="question" id="card_edit_question" value="<?=$arResult["UF_QUESTION"];?>">
        <label for="card_edit_name">Вопрос</label>
        <input type="text" name="answer" id="card_edit_answer" value="<?=$arResult["UF_ANSWER"];?>">
        <label for="card_edit_name">Ответ</label>
        <img
            src="<?= $arResult["UF_IMAGE"]["SRC"]; ?>"
            alt=""
            id="card_edit_image"
        >
        <button id="delete_image" class="<?=(!$arResult["UF_IMAGE"]) ? "hidden" : "";?>">Удалить изображение</button>
        <input
            type="file"
            name="image"
            id="card_edit_file"
            class="<?=($arResult["UF_IMAGE"]) ? "hidden" : "";?>"
        >
        <input type="hidden" name="image_url" id="card_image_url">
        <div class="progress_bar"></div>
        <input type="hidden" name="user_id" value="<?=$arResult["USER_ID"];?>">
        <input type="submit" id="card_edit_submit">
    </div>
    <div id="errors_cards" class="wrap"></div>
</form>