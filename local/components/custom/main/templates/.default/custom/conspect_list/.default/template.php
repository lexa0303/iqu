<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$this->setFrameMode(true);
?>
<div class="conspects_list-heading heading">Список конспектов</div>
<div class="conspects_list">
    <?if (count($arResult["ITEMS"]) > 0) : ?>
        <? foreach ($arResult["ITEMS"] as $arItem) : ?>
            <div class="conspects_item">
                <div class="conspects_item-img">
                    <a href="<?=$arItem["DETAIL_PAGE_URL"];?>">
                        <img src="<?= $arItem["IMAGE"]["SRC"]; ?>"/>
                    </a>
                </div>
                <div class="conspects_item-detail_link">
                    <a href="<?=$arItem["DETAIL_PAGE_URL"];?>">
                        <span><?= $arItem["NAME"]; ?></span>
                    </a>
                </div>
                <div class="conspects_item-delete_link">
                    <a href="javascript:void(0);" class="delete_conspect" data-id="<?=$arItem['ID']?>">Удалить конспект</a>
                </div>
            </div>

        <? endforeach; ?>
    <?else : ?>
        <div class="conspects_list-error">У вас еще нет конспектов</div>
    <?endif;?>
    <div style="clear:both;"></div>
    <div class="conspects_list-category_button button">
        <a href="<?= $arResult["PATH_TO_CATEGORIES_LIST"]; ?>">К списку категорий</a>
    </div>
    <div class="conspects_list-add_button button">
        <a href="<?= $arResult["PATH_TO_CONSPECT_ADD"]; ?>">Создать новый конспект</a>
    </div>
</div>