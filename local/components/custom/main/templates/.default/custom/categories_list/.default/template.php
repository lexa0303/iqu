<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$this->setFrameMode(true);
?>
<div class="categories_list-heading heading">Список категорий</div>
<div class="categories_list">
    <?if (count($arResult["ITEMS"]) > 0) : ?>
        <? foreach ($arResult["ITEMS"] as $arItem) : ?>
            <div class="categories_item">
                <div class="categories_item-img">
                    <a href="<?=$arItem["DETAIL_PAGE_URL"];?>">
                        <?if ($arItem["UF_IMAGE"]) : ?>
                            <img src="<?= $arItem["UF_IMAGE"]["SRC"]; ?>"/>
                        <?endif;?>
                    </a>
                </div>
                <div class="categories_item-detail_link">
                    <a href="<?=$arItem["DETAIL_PAGE_URL"];?>">
                        <span><?= $arItem["UF_NAME"]; ?></span>
                    </a>
                </div>
                <div class="categories_item-delete_link">
                    <a href="javascript:void(0);" class="delete_category" data-id="<?=$arItem['ID']?>">Удалить категорию</a>
                </div>
            </div>

        <? endforeach; ?>
    <?else : ?>
        <div class="categories_list-error">У вас еще нет конспектов</div>
    <?endif;?>
    <div style="clear:both;"></div>
    <div class="categories_list-back_button button">
        <a href="<?= $arResult["PATH_TO_CONSPECT_LIST"]; ?>">Вернутся в общий список конспектов</a>
    </div>
    <div class="categories_list-add_button button">
        <a href="<?= $arResult["PATH_TO_CATEGORIES_ADD"]; ?>">Создать новую категорию</a>
    </div>
</div>