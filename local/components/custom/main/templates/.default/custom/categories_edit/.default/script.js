/**
 * Created by al on 01.11.2016.
 */

$(document).ready(function(){
    $("body").find("#categories_edit_submit").on("click", function(e){
        e.preventDefault();

        var data = $(e.target.closest("form")).serialize();
        var errorContainer = document.getElementById("errors_categories");

        $.ajax({
            url: globals.ajax + "categories.php",
            data: data,
            type: "post",
            success: function(result) {
                var res = $.parseJSON(result);
                if (typeof res == "number") {
                    errorContainer.innerHTML = "Категория успешно изменена";
                    var href = e.target.closest("form").getAttribute("action").replace("#CATEGORY_ID#", res);
                    setTimeout(function(){
                        location.href = href;
                    }, 1000);
                } else {
                    var errors = "";

                    for (var key in res.errors){
                        errors += res.errors[key];
                    };

                    errorContainer.innerHTML = errors;
                }
            }
        });
    });

    document.getElementById("delete_image").addEventListener("click", function(e){
        e.preventDefault();
        var button = e.target;
        var img = button.closest("form").querySelector("img");
        var file = button.closest("form").querySelector("input[type=file]");
        var progress_bar = button.closest("form").querySelector(".progress_bar");

        button.classList.add("hidden");
        progress_bar.classList.add("hidden");
        file.classList.remove("hidden");
        img.src = "";
    });

    document.getElementById("categories_edit_file").addEventListener("change", function(e){
        sendFile(this, function(result) {
            document.getElementById('categories_image_url').value = result.file;
            document.getElementById('categories_edit_image').src = result.file;

            var file = e.target;
            var button = file.closest("form").querySelector("button");
            var progress_bar = file.closest("form").querySelector(".progress_bar");

            button.classList.remove("hidden");
            file.classList.add("hidden");
        });
    });
});