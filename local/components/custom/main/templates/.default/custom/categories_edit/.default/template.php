<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$this->setFrameMode(true);
?>
<form id="categories_edit_form" action="<?=$arParams["FORM_ACTION"];?>">
    <input type="hidden" name="categories_id" value="<?=$arResult["ID"];?>">
    <input type="hidden" name="action" value="update">
    <div class="heading">Редактирование категории</div>
    <div class="categories_fields_wrap wrap">
        <input type="text" name="name" id="categories_edit_name" value="<?=$arResult["UF_NAME"];?>">
        <label for="categories_edit_name">Название категории</label>
        <img
            src="<?= $arResult["UF_IMAGE"]["SRC"]; ?>"
            alt=""
            id="categories_edit_image"
        >
        <button id="delete_image" class="<?=(!$arResult["IMAGE"]) ? "hidden" : "";?>">Удалить изображение</button>
        <input
            type="file"
            name="image"
            id="categories_edit_file"
            class="<?=($arResult["IMAGE"]) ? "hidden" : "";?>"
        >
        <input type="hidden" name="image_url" id="categories_image_url">
        <div class="progress_bar"></div>
        <input type="hidden" name="user_id" value="<?=$arResult["USER_ID"];?>">
        <input type="submit" id="categories_edit_submit">
    </div>
    <div class="categories_conspects-wrap wrap">
        <div class="categories_conspects-heading heading">Конспекты в данной категории:</div>
        <?foreach ($arResult["CONSPECTS"] as $arConspect) :?>
            <div class="categories_conspects-item">
                <?if ($arConspect["IMAGE"]["ID"] > 0)
                    $image = CFile::ResizeImageGet($arConspect["IMAGE"]["ID"], array("width" => 100, "height" => 100), "BX_RESIZE_IMAGE_PROPORTIONAL");
                else
                    $image = '';?>
                <input
                    type="checkbox"
                    name="conspects[]"
                    id="conspect-<?= $arConspect["ID"]; ?>"
                    value="<?=$arConspect["ID"];?>"
                    <?if (in_array($arConspect["ID"], $arResult["UF_CONSPECTS"]))
                        echo "checked";?>
                >
                <label for="conspect-<?= $arConspect["ID"]; ?>">
                    <img src="<?= $image["src"]; ?>" alt="">
                    <?=$arConspect["NAME"];?>
                </label>
            </div>
        <?endforeach;?>
    </div>
    <div id="errors_categories" class="wrap"></div>
</form>