<?php
/**
 * Created by PhpStorm.
 * User: al
 * Date: 10.11.2016
 * Time: 16:05
 */

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$this->setFrameMode(true);
?>
<?php
switch ($_GET["exam_type"]) :
    case "question_answer":
        $template = "question_answer";
        $answersCount = 1;
        break;
    case "yes_no":
        $template = "yes_no";
        $answersCount = 1;
        break;
    case "mchoice":
        $template = "mchoice";
        $answersCount = 4;
        break;
    default:
        $template = ".default";
        break;
endswitch;
?>
<?$APPLICATION->IncludeComponent(
    "custom:exam",
    $template,
    array(
        "TEMPLATE" => $template,
        "CONSPECT_ID" => $arResult["VARIABLES"]["CONSPECT_ID"],
        "CONSPECTS_HLBLOCK" => $arParams["CONSPECTS_HLBLOCK"],
        "CATEGORIES_HLBLOCK" => $arParams["CATEGORIES_HLBLOCK"],
        "CARDS_HLBLOCK" => $arParams["CARDS_HLBLOCK"],
        "USER_TITLE" => $arResult["VARIABLES"]["USER_TITLE"],
        "SEF_FOLDER" => $arParams["SEF_FOLDER"],
        "SEF_URL_TEMPLATES" => $arParams["SEF_URL_TEMPLATES"],
        "COMPONENT_TEMPLATE" => ".default",
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => $arParams["CACHE_TIME"],
        "MASTER" => $arResult["MASTER"],
        "SET_TITLE" => $arParams["CATEGORY_CATEGORY_LIST_TITLE"],
        "CARDS_NEEDED" => ($template == ".default") ? "N" : "Y",
        "ANSWERS_COUNT" => $answersCount
    ),
    $component
);?>