<?php
/**
 * Created by PhpStorm.
 * User: al
 * Date: 10.11.2016
 * Time: 16:05
 */

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$this->setFrameMode(true);
?>
<?$APPLICATION->IncludeComponent(
    "custom:exam_result",
    "",
    array(
        "TEMPLATE" => $template,
        "CONSPECT_ID" => $arResult["VARIABLES"]["CONSPECT_ID"],
        "CONSPECTS_HLBLOCK" => $arParams["CONSPECTS_HLBLOCK"],
        "CATEGORIES_HLBLOCK" => $arParams["CATEGORIES_HLBLOCK"],
        "CARDS_HLBLOCK" => $arParams["CARDS_HLBLOCK"],
        "USER_TITLE" => $arResult["VARIABLES"]["USER_TITLE"],
        "RESULT_ID" => $arResult["VARIABLES"]["RESULT_ID"],
        "SEF_FOLDER" => $arParams["SEF_FOLDER"],
        "SEF_URL_TEMPLATES" => $arParams["SEF_URL_TEMPLATES"],
        "COMPONENT_TEMPLATE" => ".default",
        "CACHE_TYPE" => $arParams["CACHE_TYPE"],
        "CACHE_TIME" => $arParams["CACHE_TIME"],
        "MASTER" => $arResult["MASTER"],
        "SET_TITLE" => $arParams["CATEGORY_CATEGORY_LIST_TITLE"],
        "CARDS_NEEDED" => ($template == ".default") ? "N" : "Y",
        "ANSWERS_COUNT" => $answersCount
    ),
    $component
);?>