<?php
/**
 * Created by PhpStorm.
 * User: al
 * Date: 01.11.2016
 * Time: 10:07
 */

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$this->setFrameMode(true);
?>
<?$APPLICATION->IncludeComponent(
    "custom:card_add",
    ".default",
    array(
        "IBLOCK_ID" => "4",
        "IBLOCK_TYPE" => "conspect",
        "CONSPECT_ID" => $arResult["VARIABLES"]["CONSPECT_ID"],
        "USER_ID" => $arResult["VARIABLES"]["USER_ID"],
        "FORM_ACTION" =>
            $arParams["SEF_FOLDER"] . str_replace(
                "#CONSPECT_ID#",
                $arResult["VARIABLES"]["CONSPECT_ID"],
                $arParams["SEF_URL_TEMPLATES"]["card_list"]
            ),
        "COMPONENT_TEMPLATE" => ".default",
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => $arParams["CACHE_TIME"]
    ),
    $component
);?>