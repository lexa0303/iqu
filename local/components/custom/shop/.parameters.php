<?
if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)die();
CModule::IncludeModule("iblock");
CModule::IncludeModule("highloadblock");
$arTypesEx = CIBlockParameters::GetIBlockTypes(Array('-'=>' '));
$arIBlocks=Array();
$db_iblock = CIBlock::GetList(
    Array('SORT'=>'ASC'),
    Array(
        'TYPE' => ($arCurrentValues['CONSPECTS_IBLOCK_TYPE']!='-'?$arCurrentValues['CONSPECTS_IBLOCK_TYPE']:'')
    )
);
while($arRes = $db_iblock->Fetch())
        $arIBlocks[$arRes['ID']] = $arRes['NAME'];

$dbHLBlock = \Bitrix\Highloadblock\HighloadBlockTable::getList(
    array(

    )
);
while ($res = $dbHLBlock->fetch())
    $arHLBlocks[$res["ID"]] = $res["NAME"];

$arPages = array(
    "list" => "Список конспектов",
    "detail" => "Детальная страница конспекна",
    "cart" => "Корзина",
    "history" => "История покупок"
);

$arComponentParameters = array(
    'PARAMETERS' => array(
        'CONSPECTS_HLBLOCK' => Array(
            'PARENT' => 'BASE',
            'NAME' => 'ID инфоблока конспектов',
            'TYPE' => 'LIST',
            'VALUES' => $arHLBlocks,
            'DEFAULT' => '',
            'ADDITIONAL_VALUES' => 'Y',
            'REFRESH' => 'Y',
        ),
        "CATEGORIES_HLBLOCK" => Array(
            'PARENT' => 'BASE',
            'NAME' => 'ID HL-блока категорий',
            'TYPE' => 'LIST',
            'VALUES' => $arHLBlocks,
            'DEFAULT' => '',
            'ADDITIONAL_VALUES' => 'Y',
            'REFRESH' => 'Y',
        ),
        "CARDS_HLBLOCK" => Array(
            'PARENT' => 'BASE',
            'NAME' => 'ID HL-блока карточек',
            'TYPE' => 'LIST',
            'VALUES' => $arHLBlocks,
            'DEFAULT' => '',
            'ADDITIONAL_VALUES' => 'Y',
            'REFRESH' => 'Y',
        ),
        "CATEGORY_SET_LIST_TITLE" => Array(
            'PARENT' => 'BASE',
            'NAME' => 'Задавать заголовок на странице категорий',
            'TYPE' => 'CHECKBOX',
            'DEFAULT' => 'Y',
        ),
        'MASTER_NEEDED' => Array(
            'PARENT' => 'BASE',
            'NAME' => 'Страницы для владельца',
            'TYPE' => 'LIST',
            'VALUES' => $arPages,
            'DEFAULT' => '',
            'ADDITIONAL_VALUES' => 'Y',
            'REFRESH' => 'Y',
            "MULTIPLE" => "Y",
        ),
        'NO_AUTH_PAGES' => Array(
            'PARENT' => 'BASE',
            'NAME' => 'Страницы доступные неавторизированному пользователю',
            'TYPE' => 'LIST',
            'VALUES' => $arPages,
            'DEFAULT' => '',
            'ADDITIONAL_VALUES' => 'Y',
            'REFRESH' => 'Y',
            "MULTIPLE" => "Y",
        ),
        'CATEGORY_PAGEN_COUNT' => Array(
            'PARENT' => 'BASE',
            'NAME' => 'Количество категорий на странице',
            'TYPE' => 'TEXT',
            'DEFAULT' => '',
            "MULTIPLE" => "N",
        ),
        'CATEGORY_PAGEN_TEMPLATE' => Array(
            'PARENT' => 'BASE',
            'NAME' => 'Шаблон постраничной навигации на странице категорий',
            'TYPE' => 'TEXT',
            'DEFAULT' => '.default',
            "MULTIPLE" => "N",
        ),
        "VARIABLE_ALIASES" => array(
        ),
        'SEF_FOLDER' => Array(
            'PARENT' => 'BASE',
            'NAME' => 'Корневой раздел ЧПУ',
            'TYPE' => 'TEXT',
            'DEFAULT' => '/shop/',
            "MULTIPLE" => "N",
        ),
        "SEF_MODE" => array(
            "list" => array(
                "NAME" => "Список конспектов",
                "DEFAULT" => "",
                "VARIABLES" => array(
                ),
            ),
            "detail" => array(
                "NAME" => "Детальная страница конспекта",
                "DEFAULT" => "#CONSPECT_CODE#/",
                "VARIABLES" => array(
                    "CONSPECT_CODE"
                ),
            ),
            "cart" => array(
                "NAME" => "Корзина",
                "DEFAULT" => "cart/",
                "VARIABLES" => array(
                ),
            ),
            "history" => array(
                "NAME" => "История покупок",
                "DEFAULT" => "history/",
                "VARIABLES" => array(
                ),
            ),
        ),
        'CACHE_TIME'  =>  array('DEFAULT'=>36000),
    ),
);
?>