<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

global $USER;
//if (!$USER->IsAdmin())
//    LocalRedirect("/");

$arComponentVariables = array(
    'USER_TITLE',
    'CONSPECT_ID',
    'CATEGORY_ID',
);
$arDefaultUrlTemplates404 = array(
    "list" => "",
    "detail" => "#CONSPECT_CODE#/",
    "cart" => "cart/",
    "history" => "history/",
);

$arVariables = array();
$arUrlTemplates = CComponentEngine::MakeComponentUrlTemplates($arDefaultUrlTemplates404, $arParams['SEF_URL_TEMPLATES']);

$componentPage = CComponentEngine::ParseComponentPath($arParams['SEF_FOLDER'], $arUrlTemplates, $arVariables);

if (($componentPage == "detail") && (strlen($arVariables["CONSPECT_CODE"]) > 0)) {
    $component = "detail";
} elseif ($componentPage == "list") {
$component = "list";
} elseif ($componentPage == "user") {
    $component = "user";
} elseif ($componentPage == "cart") {
    $component = "cart";
} elseif ($componentPage == "history") {
    $component = "history";
} else {
    $component = "list";
}

$arResult["VARIABLES"] = $arVariables;

$arResult["MASTER"] = "N";

/* ACCESS CHECK FOR PAGES */
if ($arResult["MASTER"] == "N" && in_array($component, $arParams["MASTER_NEEDED"]))
    LocalRedirect(SITE_DIR);

if ($component == "main_page" && $APPLICATION->GetCurPage(false) != SITE_DIR)
    LocalRedirect(SITE_DIR);
/* ACCESS CHECK FOR PAGES */

?>
<? $this->IncludeComponentTemplate($component);