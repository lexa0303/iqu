<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 20.03.17
 * Time: 10:08
 */

use Bitrix\Main\Localization\Loc;

?>
<?php
$arLibraryFilter = array();
$arLibrarySort = array();

switch ($_GET["sort"]) :
    case "popular":
        $arLibrarySort = array(
            "UF_VIEW_COUNT_MONTH" => "DESC"
        );
        break;
    case "subscribed":
        global $arUser;
        $arLibraryFilter = array(
            "UF_USER_ID" => $arUser["UF_SUBSCRIBED"],
        );
        $arLibrarySort = array("UF_UPDATE_TIME"=>"desc");
        break;
    case "all":
        $arLibrarySort = false;
        break;
    default:
        $arLibrarySort = array("UF_UPDATE_TIME"=>"desc");
        break;
endswitch;

?>
<section class="main-slider__section shop">
    <div class="main-slider__box">
        <div class="main-slider">
            <? $APPLICATION->IncludeComponent("bitrix:advertising.banner", "shop_page", Array(
                "CACHE_TIME"         => "0",    // Время кеширования (сек.)
                "CACHE_TYPE"         => "A",    // Тип кеширования
                "NOINDEX"            => "N",    // Добавлять в ссылки noindex/nofollow
                "QUANTITY"           => "5",    // Количество баннеров для показа
                "TYPE"               => "shop_banner",    // Тип баннера
                "COMPONENT_TEMPLATE" => ".default",
            ),
                false
            ); ?>
        </div>
    </div>
</section>
<? $APPLICATION->IncludeComponent(
    "custom:shop.list",
    "",
    array(
        "CONSPECTS_HLBLOCK"  => $arParams["CONSPECTS_HLBLOCK"],
        "CATEGORIES_HLBLOCK" => $arParams["CATEGORIES_HLBLOCK"],
        "CARDS_HLBLOCK"      => $arParams["CARDS_HLBLOCK"],
        "USER_TITLE"         => $arResult["VARIABLES"]["USER_TITLE"],
        "SEF_FOLDER"         => $arParams["SEF_FOLDER"],
        "SEF_URL_TEMPLATES"  => $arParams["SEF_URL_TEMPLATES"],
        "COMPONENT_TEMPLATE" => ".default",
        "CACHE_TIME"         => $arParams["CACHE_TIME"],
        "CACHE_TYPE"         => $arParams["CACHE_TYPE"],
        "MASTER"             => $arResult["MASTER"],
        "SET_TITLE"          => $arParams["CATEGORY_CATEGORY_LIST_TITLE"],
        "PAGEN_COUNT"        => $arParams["LIBRARY_PAGEN_COUNT"],
        "PAGEN_TEMPLATE"     => $arParams["LIBRARY_PAGEN_TEMPLATE"],
        "FILTER"             => $arLibraryFilter,
        "SORT"               => $arLibrarySort,
    ),
    $component
); ?>
