<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use Bitrix\Main\Localization\Loc;
$this->setFrameMode(true);
?>
<section class="library__wrapper">
    <div class="container">
        <div class="section__heading">
            <span class="section__heading-txt"><?=Loc::getMessage("LIBRARY_HEADING");?></span>
        </div>
        <section class="my-cards__header">
            <div class="my-cards__tabs-wrapper">
                <?switch ($_GET["sort"]) :
                    case "all":
                        $activeButton = "all";
                        break;
                    case "popular":
                        $activeButton = "popular";
                        break;
                    case "subscribed":
                        $activeButton = "subscribed";
                        break;
                    default:
                        $activeButton = "new";
                        break;
                endswitch;?>
                <ul class="main-tabs">
                    <li class="main-tabs__item">
                        <a class="main-tabs__link <?=($activeButton == "new") ? "active" : "";?>"
                           href="<?=$APPLICATION->GetCurPageParam("sort=new", array("sort", "PAGEN_1"));?>"><?=Loc::getMessage("LIBRARY_TAB_NEW");?></a>
                    </li>
                    <li class="main-tabs__item">
                        <a class="main-tabs__link <?=($activeButton == "popular") ? "active" : "";?>"
                           href="<?=$APPLICATION->GetCurPageParam("sort=popular", array("sort", "PAGEN_1"));?>"><?=Loc::getMessage("LIBRARY_TAB_POPULAR");?></a>
                    </li>
                    <?/*global $arUser;
                    if (count($arUser["UF_SUBSCRIBED"]) > 0) : ?>
                        <li class="main-tabs__item">
                            <a class="main-tabs__link <?=($activeButton == "subscribed") ? "active" : "";?>"
                               href="<?=$APPLICATION->GetCurPageParam("sort=subscribed", array("sort", "PAGEN_1"));?>"><?=Loc::getMessage("LIBRARY_TAB_SUBSCRIBED");?></a>
                        </li>
                    <?endif;*/?>
                    <li class="main-tabs__item">
                        <a class="main-tabs__link <?=($activeButton == "all") ? "active" : "";?>"
                           href="<?=$APPLICATION->GetCurPageParam("sort=all", array("sort", "PAGEN_1"));?>"><?=Loc::getMessage("LIBRARY_TAB_ALL_CONSPECTS");?></a>
                    </li>
                </ul>
            </div>
            <form class="my-cards__search-wrapper" action="<?=SITE_DIR;?>search/">
                <label class="my-cards__search-descr" for="myCardsSearch">
                    <span class="my-cards__search-icon icon icon-search"></span>
                </label>
                <input class="my-cards__search" type="search" name="q" id="myCardsSearch" placeholder="<?=Loc::getMessage("LIBRARY_SEARCH");?>">
            </form>
        </section>
        <section class="my-cards__content">
            <article class="main-tabs__content js-pagination_container">
                <?if ($_POST["ajax"] == "Y")
                    $APPLICATION->RestartBuffer();?>
                <div class="fcards__wrapper row">
                    <? foreach ($arResult["ITEMS"] as $arConspect) : ?>
                        <?$APPLICATION->IncludeComponent(
                            "custom:conspect_card",
                            "",
                            Array(
                                "CACHE_TIME" => $arParams["CACHE_TIME"],
                                "CACHE_TYPE" => $arParams["CACHE_TYPE"],
                                "CARDS_HLBLOCK" => "4",
                                "CATEGORIES_HLBLOCK" => "3",
                                "CONSPECTS_HLIBLOCK" => "6",
                                "CONSPECT_ID" => $arConspect["ID"],
                                "SEF_MODE" => $arParams["SEF_URL_TEMPLATES"],
                                "MASTER" => "N",
                            )
                        );?>
                    <?endforeach;?>
                </div>
                <?=$arResult["NAV_STRING"];?>
                <?if ($_POST["ajax"] == "Y")
                    die();?>
            </article>
        </section>
    </div>
</section>
<script>
    globals.page = "library";
</script>