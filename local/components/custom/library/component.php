<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use Bitrix\Main\Loader,
    Bitrix\Iblock;

Loader::includeModule("highloadblock");
$сache = Bitrix\Main\Data\Cache::createInstance();
CPageOption::SetOptionString("main", "nav_page_in_session", "N");

$cUser = new CUser;
$dbUser = $cUser->GetList($by = array(), $order = array(), array("TITLE"=>$arParams["USER_TITLE"]));
$arrUser = $dbUser->Fetch();
$arrUser["IMAGE"] = LenalHelp::img($arrUser["PERSONAL_PHOTO"], 200, 200);
$userID = $arrUser["ID"];

if ($_POST["ajax"] != "Y" && $arParams["CACHE_TYPE"] != "N" && $сache->initCache($arParams["CACHE_TIME"], 'library' . $_REQUEST["PAGEN_1"] . "-" . SITE_ID . md5(serialize($arParams["FILTER"])) . "-" . md5(serialize($arParams["SORT"])), '/library/')) {
    $arResult = $сache->getVars();
} elseif ($сache->startDataCache()) {

    $arResult = array();
    $arFilter = array(
        "!ID" => HELP_CONSPECT_ID,
        "UF_SHOW_IN_LIBRARY" => 1,
        "UF_SHOP" => false,
    );
    $arSort = array(
        "UF_NAME" => "ASC"
    );
    
    if (is_array($arParams["FILTER"]))
        $arFilter = array_merge($arFilter, $arParams["FILTER"]);

    if (is_array($arParams["SORT"]))
        $arSort = array_merge($arParams["SORT"], $arSort);

    $dbConspects = Conspects::GetAll("", $arFilter, false, $arSort);

    if ($dbConspects) {
        $dbConspects = LenalHelp::makePagination($dbConspects, $arParams["PAGEN_COUNT"], $arParams["PAGEN_TEMPLATE"]);

        while ($res = $dbConspects->Fetch())
            $arResult["ITEMS"][] = $res;

        $arResult["NAV_STRING"] = LenalHelp::$pagintaionStr;

        foreach ($arResult["ITEMS"] as &$conspect) {
            makeConspectDetails($conspect, $arParams["SEF_URL_TEMPLATES"]);

            $arResult["COUNT_DOWNLOADS"] += (int)$conspect["UF_SAVES_COUNT"];
        }
    }


//     ...
    if ($isInvalid) {
        $cache->abortDataCache();
    }
//     ...
//
    $сache->endDataCache($arResult);
}
?>
<? $this->IncludeComponentTemplate();
?>