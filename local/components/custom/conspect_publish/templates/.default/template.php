<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use Bitrix\Main\Localization\Loc;
$this->setFrameMode(true);
global $arUser;
?>

<a class="modal__btn-close icon icon-cross" href="javascript:void(0)"></a>
<form class="new-category-modal__body ajax_submit">
    <input type="hidden" name="conspect_id" value="<?=$arResult["ID"];?>">
    <?=bitrix_sessid_post();?>
    <input type="hidden" name="user_id" value="<?=$USER->GetID();?>">
    <input type="hidden" name="action" value="conspect_final_publish">
    <span class="new-category-modal__heading"><?=Loc::getMessage("CONSPECT_PUBLISH_HEADING");?></span>
    <div class="delete-conspect-radio__kinds">
        <div class="delete-conspect-radio__kind-wrapper">
            <input class="delete-conspect-radio__kind-input js-publish_action" required type="radio" id="publish" name="publish" value="publish">
            <label class="delete-conspect-radio__kind-label" for="publish">Опубликовать</label>
        </div>
        <div class="delete-conspect-radio__kind-wrapper">
            <input class="delete-conspect-radio__kind-input js-publish_action" type="radio" id="return" name="publish" value="return">
            <label class="delete-conspect-radio__kind-label" for="return">Вернуть на доработку</label>
        </div>
    </div>
    <div class="modal__fieldset conspect_publish__textarea-wrap js-comment hidden">
        <label class="modal__label" for="conspect_publish_text">Комментарий</label>
        <textarea class="modal__textarea conspect_publish__textarea"
                  name="comment"
                  id="conspect_publish_text"
                  type="text"></textarea>
    </div>
    <button class="button button--rounded new-category-modal__btn" href="javascript:void(0)"><?=Loc::getMessage("SEND");?></button>
    <a class="new-category-modal__btn--txt js-close_modals" href="javascript:void(0)"><?=Loc::getMessage("CANCEL");?></a>
</form>
<script>

</script>