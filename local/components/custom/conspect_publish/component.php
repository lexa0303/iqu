<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use Bitrix\Main\Loader,
    Bitrix\Iblock;

Loader::includeModule("iblock");

$arResult = Conspects::GetByID($arParams["CONSPECT_ID"]);
if ($arResult["UF_USER_ID"] != $arParams["USER_ID"])
    die();
$arResult["USER_ID"] = $arParams["USER_ID"];

?>
<? $this->IncludeComponentTemplate();
?>

