<?
if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)die();
CModule::IncludeModule("highloadblock");

$arComponentParameters = array(
    'PARAMETERS' => array(
        'CONSPECTS_HLIBLOCK' => Array(
            'PARENT' => 'BASE',
            'NAME' => 'ID инфоблока конспектов',
            'TYPE' => 'LIST',
            'VALUES' => $arHLBlocks,
            'DEFAULT' => '',
            'ADDITIONAL_VALUES' => 'Y',
            'REFRESH' => 'Y',
        ),
        "CATEGORIES_HLBLOCK" => Array(
            'PARENT' => 'BASE',
            'NAME' => 'ID HL-блока категорий',
            'TYPE' => 'LIST',
            'VALUES' => $arHLBlocks,
            'DEFAULT' => '',
            'ADDITIONAL_VALUES' => 'Y',
            'REFRESH' => 'Y',
        ),
        "CARDS_HLBLOCK" => Array(
            'PARENT' => 'BASE',
            'NAME' => 'ID HL-блока карточек',
            'TYPE' => 'LIST',
            'VALUES' => $arHLBlocks,
            'DEFAULT' => '',
            'ADDITIONAL_VALUES' => 'Y',
            'REFRESH' => 'Y',
        ),
        'USER_ID' => Array(
            'PARENT' => 'BASE',
            'NAME' => 'ID пользователя',
            'TYPE' => 'TEXT',
            'DEFAULT' => '',
            "MULTIPLE" => "N",
        ),
        'CONSPECT_ID' => Array(
            'PARENT' => 'BASE',
            'NAME' => 'ID конспекта',
            'TYPE' => 'TEXT',
            'DEFAULT' => '',
            "MULTIPLE" => "N",
        ),
        'CACHE_TIME'  =>  array('DEFAULT'=>36000),
    ),
);
?>