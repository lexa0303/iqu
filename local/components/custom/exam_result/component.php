<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use Bitrix\Main\Loader,
    Bitrix\Iblock;

Loader::includeModule("highloadblock");

$cUser = new CUser;
$dbUser = $cUser->GetList($by = array(), $order = array(), array("TITLE"=>$arParams["USER_TITLE"]));
$arrUser = $dbUser->Fetch();
$userID = $arrUser["ID"];

$arResult = Conspects::GetByCode($arParams["CONSPECT_ID"], $userID);
if (!$arResult)
    $arResult = Conspects::GetByID($arParams["CONSPECT_ID"]);

//if (!$arResult)
//    LocalRedirect(getLink("MY_CONSPECTS"));

makeConspectDetails($arResult, $arParams["SEF_URL_TEMPLATES"]);

$arExam = Exams::GetByID($arParams["RESULT_ID"], $userID);

//if (!$arExam)
//    LocalRedirect($arConspect["EXAM_PAGE_URL"]);

$arResult["EXAM"] = $arExam;

if ($arResult["USER"]["PERSONAL_PHOTO"] > 0){
    $arResult["USER"]["IMAGE"] = array(
        "SRC" => CFile::GetPath($arResult["USER"]["PERSONAL_PHOTO"]),
        "RESIZED" => LenalHelp::img($arResult["USER"]["PERSONAL_PHOTO"], 75, 75)
    );
}



?>
<? $this->IncludeComponentTemplate();
?>

