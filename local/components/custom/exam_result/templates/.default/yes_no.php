<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 26.04.17
 * Time: 15:51
 */
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;
?>
<article class="conspect-cards__content exam-yn__tasks-wrap exam-results">
    <? foreach ($arResult["EXAM"]["RESULT"]["cards"] as $cardId => $arCard) : ?>
        <?php
        $bNoAnswer = false;
        if (isset($arResult["EXAM"]["RESULT"]["answers"][$cardId])) {
            $bExamAnswer = !!$arResult["EXAM"]["RESULT"]["answers"][$cardId];
            $bNoAnswer = false;
        } else {
            $bExamAnswer = false;
            $bNoAnswer = true;
        }
        ?>
        <div class="conspect-cards__item js-card" data-card_id="<?=$arCard["ID"];?>">
            <input type="hidden" name="answers[<?=$arCard["ID"];?>][answer_text]" value="<?=$arCard["ANSWER"]["ID"];?>">
            <div class="row">
                <div class="col-md-6">
                    <div class="conspect-card__wrapper">
                        <div class="conspect-card__title"><?=Loc::getMessage("QUESTION");?></div>
                        <div class="conspect-card__body">
                            <div class="conspect-card__img">
                                <img src="<?=$arCard["UF_QUESTION_IMAGE"]["RESIZED"];?>" alt="">
                            </div>
                            <div class="conspect-card__text">
                                <span><?=$arCard["UF_QUESTION"];?></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="conspect-card__wrapper">
                        <div class="conspect-card__title"><?=Loc::getMessage("ANSWER");?></div>
                        <div class="conspect-card__body">
                            <div class="conspect-card__img">
                                <img src="<?=$arCard["ANSWER"]["UF_ANSWER_IMAGE"]["RESIZED"];?>" alt="">
                            </div>
                            <div class="conspect-card__text">
                                <span><?=$arCard["ANSWER"]["UF_ANSWER"];?></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="exam-yn__answers">
                        <fieldset class="exam-yn__answer-wrap">
                            <label class="exam-yn__answer exam-yn__answer--no
                            <?
                            if (!$bNoAnswer) {
                                if (!$bExamAnswer) {
                                    if ($arCard["ANSWER"]["ID"] != $arCard["CORRECT_ANSWER"]["ID"]) {
                                        echo " correct_answer";
                                    } else {
                                        echo " wrong_answer";
                                    }
                                }
                            }
                            ?>" for="no-<?=$arCard["ID"];?>"><?=Loc::getMessage("NO");?></label>
                        </fieldset>
                        <fieldset class="exam-yn__answer-wrap">
                            <label class="exam-yn__answer exam-yn__answer--yes
                                <?
                                if (!$bNoAnswer) {
                                    if ($bExamAnswer) {
                                        if ($arCard["ANSWER"]["ID"] == $arCard["CORRECT_ANSWER"]["ID"]) {
                                            echo " correct_answer";
                                        } else {
                                            echo " wrong_answer";
                                        }
                                    }
                                }
                                ?>"
                                   for="yes-<?=$arCard["ID"];?>"><?=Loc::getMessage("YES");?></label>
                        </fieldset>
                    </div>
                </div>
            </div>
        </div>
    <?endforeach;?>
</article>
