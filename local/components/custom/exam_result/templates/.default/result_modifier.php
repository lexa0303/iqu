<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 15.02.17
 * Time: 15:53
 */

foreach ($arResult["CARDS"] as &$arCard) :
    if ($arCard["UF_ANSWER_IMAGE"]["ID"] > 0) {
        $arCard["UF_ANSWER_IMAGE"]["RESIZED"] = LenalHelp::img($arCard["UF_ANSWER_IMAGE"]["ID"], 200, 200);
    }
    if ($arCard["UF_QUESTION_IMAGE"]["ID"] > 0) {
        $arCard["UF_QUESTION_IMAGE"]["RESIZED"] = LenalHelp::img($arCard["UF_QUESTION_IMAGE"]["ID"], 200, 200);
    }
endforeach;

$correctCount = 0;

foreach ($arResult["EXAM"]["RESULT"]["answers"] as $cardId => $arAnswer){
    if (is_array($arAnswer)){
        if ($arAnswer["correct"])
            $correctCount++;
    } else {
        if ($arAnswer && $arResult["EXAM"]["RESULT"]["cards"][$cardId]["ANSWER"]["ID"] == $arResult["EXAM"]["RESULT"]["cards"][$cardId]["CORRECT_ANSWER"]["ID"])
            $correctCount++;
        if (!$arAnswer && $arResult["EXAM"]["RESULT"]["cards"][$cardId]["ANSWER"]["ID"] != $arResult["EXAM"]["RESULT"]["cards"][$cardId]["CORRECT_ANSWER"]["ID"])
            $correctCount++;
    }
}

$arResult["EXAM"]["CORRECT_COUNT"] = $correctCount;