<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 26.04.17
 * Time: 15:52
 */
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

LocalRedirect($arResult["EXAM_PAGE_URL"]);
