<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use Bitrix\Main\Localization\Loc;
$this->setFrameMode(true);
?>
<section class="section-exam">
    <div class="container">
        <form class="exam__wrapper ajax_submit">
            <input type="hidden" name="start_exam" value="Y">
            <input type="hidden" name="action" value="exam_settings">
            <input type="hidden" name="user_id" value="<?=$USER->GetID();?>">
            <input type="hidden" name="conspect_id" value="<?=$arResult["ID"];?>">
            <div class="row">
                <div class="col-xs-12 col-lg-3 col-lg-push-9">
                    <section class="exam__navigation">
                        <div class="examNav-burger">
                            <img class="examNav-burger__icon" src="<?=SITE_TEMPLATE_PATH;?>/images/settings.svg" alt="">
                            <span class="examNav-burger__item"></span>
                            <span class="examNav-burger__item"></span>
                        </div>
                        <nav class="studying-nav">
                            <div class="studying-nav__controls">
                                <a class="studying-nav__control-item" href="<?=$arResult["LESSON_PAGE_URL"];?>">
                                    <span class="studying-nav__control-icon icon icon-lesson"></span>
                                    <span class="studying-nav__control-text"><?=Loc::getMessage("LESSON");?></span>
                                </a>
                                <a class="studying-nav__control-item active" href="<?=$arResult["EXAM_PAGE_URL"];?>">
                                    <span class="studying-nav__control-icon icon icon-exam"></span>
                                    <span class="studying-nav__control-text"><?=Loc::getMessage("EXAM");?></span>
                                </a>
                                <a class="studying-nav__control-item open-modal"
                                   data-modal="create-notification-modal"
                                   data-ajax="Y"
                                   data-action="notification_create"
                                   data-conspect_id="<?=$arResult["ID"];?>"
                                   data-user_id="<?=$USER->GetID();?>"
                                   href="javascript:void(0);">
                                    <span class="studying-nav__control-icon icon icon-alert"></span>
                                    <span class="studying-nav__control-text"><?=Loc::getMessage("NOTIFICATION");?></span>
                                </a>
<!--                                <a class="studying-nav__control-item" href="--><?//=$arResult["LIST_PAGE_URL"];?><!--">-->
<!--                                    <span class="studying-nav__control-icon icon icon-arrow-back"></span>-->
<!--                                    <span class="studying-nav__control-text">--><?//=Loc::getMessage("BACK");?><!--</span>-->
<!--                                </a>-->
                            </div>
                            <div class="studying-nav__conspect-info">
                                <div class="studying-nav__user-photo"
                                     style="background-image: url('<?=$arResult["USER"]["IMAGE"]["RESIZED"];?>');"></div>
                                <div class="studying-nav__user-info">
                                    <a href="<?=$arResult["USER"]["DETAIL_PAGE_URL"];?>" class="studying-nav__username"><?=$arResult["USER"]["TITLE"];?></a>
                                    <div class="studying-nav__user-info">
                                        <div class="studying-nav__user-watches">
                                            <span class="icon icon-views"></span>
                                            <span><?=(int)$arResult["UF_VIEW_COUNT"];?></span>
                                        </div>
                                        <div class="studying-nav__user-loads">
                                            <span class="icon icon-download2"></span>
                                            <span><?=(int)$arResult["UF_SAVES_COUNT"];?></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </nav>
                    </section>
                </div>
                <div class="col-xs-12 col-lg-9 col-lg-pull-3">
                    <div class="exam-settings__wrapper">
                        <div class="exam-results__wrap">
                            <div class="exam-results__content">
                                <div class="exam-results__content">
                                    <div class="exam-results__title"><?=Loc::getMessage("CONSPECT_EXAM_RESULT_TITLE");?></div>
                                    <div class="exam-results__answers">
                                        <span class="exam-results__answers-num exam-results__answers-num--right js-result">
                                            <span class="js-correct_answers"><?=$arResult["EXAM"]["CORRECT_COUNT"];?></span>
                                        </span>
                                        <span class="exam-results__answers-desc"><?=Loc::getMessage("CONSPECT_EXAM_RESULT_CORRECT_ANSWER");?></span>
                                    </div>
                                    <div class="exam-results__answers">
                                        <span class="exam-results__answers-num exam-results__answers-num--middle js-result">
                                            <span><?=round(($arResult["EXAM"]["CORRECT_COUNT"] / count($arResult["EXAM"]["RESULT"]["cards"]) * 100 ), 0);?>%</span>
                                        </span>
<!--                                        <span class="exam-results__answers-desc">--><?//=Loc::getMessage("CONSPECT_EXAM_RESULT_DESCRIPTION");?><!--</span>-->
                                    </div>
                                    <div class="exam-results__answers">
                                        <span class="exam-results__answers-num exam-results__answers-num--wrong js-result">
                                            <span class="js-correct_answers"><?=(count($arResult["EXAM"]["RESULT"]["cards"]) - $arResult["EXAM"]["CORRECT_COUNT"]);?></span>
                                        </span>
                                        <span class="exam-results__answers-desc"><?=Loc::getMessage("CONSPECT_EXAM_RESULT_WRONG_ANSWER");?></span>
                                    </div>
                                    <div class="exam-results__btn-wrap">
                                        <a class="exam-results__btn exam-results__btn--repeat button button--rounded" href="<?=$arResult["DETAIL_PAGE_URL"];?>"><?=Loc::getMessage("CONSPECT_EXAM_TO_CONSPECT");?></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?$examTypes = array_flip(EXAM_TYPES);
                    $examType = $examTypes[$arResult["EXAM"]["UF_EXAM_TYPE"]];

                    require($_SERVER["DOCUMENT_ROOT"] . $templateFolder . "/" . $examType . ".php");?>
<!--                    <div class="exam-settings__footer">-->
<!--                        <input class="exam-settings__btn--submit button" type="submit" value="Начать экзамен">-->
<!--                        <a class="exam-settings__btn--dismiss" href="javascript:void(0)">Отмена</a>-->
<!--                    </div>-->
                </div>
            </div>
        </form>
    </div>
</section>
<script>
    var firstTab = document.querySelector(".js-exam_type");
    $(firstTab).click();
</script>