<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 26.04.17
 * Time: 15:51
 */
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;

?>
<article class="exam-mchoice__wrap exam-results">
    <?foreach ($arResult["EXAM"]["RESULT"]["cards"] as $cardId => $arCard) : ?>
        <?php
            $arExamAnswer = $arResult["EXAM"]["RESULT"]["answers"][$cardId];
        ?>
        <div class="js-card" data-card_id="<?=$arCard["ID"];?>">
            <div class="exam-mchoice__question">
                <div class="exam-mchoice__title"><?=$questionNumber;?> <?=Loc::getMessage("QUESTION");?></div>
                <div class="exam-mchoice__block-wrap">
                    <div class="conspect-card__body">
                        <div class="conspect-card__img">
                            <img src="<?=$arCard["UF_QUESTION_IMAGE"]["SRC"];?>" alt="">
                        </div>
                        <div class="conspect-card__text">
                            <span><?=$arCard["UF_QUESTION"];?></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <?$answerNumber = 1;?>
                <input type="radio"
                       checked
                       class="hidden"
                       name="answers[<?= $arCard["ID"]; ?>][answer_text]"
                       value="wrong_answer"
                       id="mch-answer_<?= $arCard["ID"]; ?>_0">
                <?foreach ($arCard["ANSWERS"] as $arAnswer) : ?>
                    <div class="col-md-6">
                        <div class="exam-mchoice__answers-wrap
                        <?php
                        if (!$arExamAnswer["correct"] && $arAnswer["ID"] == $arExamAnswer["answer"]) {
                            echo " wrong_answer";
                        } else {
                            if ($arAnswer["ID"] == $arCard["CORRECT_ANSWER"]["ID"]) {
                                if (!$arExamAnswer["correct"])
                                    echo " correct_answer_false";
                                else
                                    echo " correct_answer";
                            }
                        }
                        ?>">
                            <div class="exam-mchoice__answer">
                                <div class="exam-mchoice__title"><?=Loc::getMessage("ANSWER");?> <?=$answerNumber;?></div>
                                <div class="exam-mchoice__block-wrap">
                                    <div class="exam-mchoice__block conspect-card__body" for="mch-answer_<?=$arCard["ID"];?>_<?=$arAnswer["ID"];?>">
                                                        <span class="conspect-card__img">
                                                            <img src="<?=$arAnswer["UF_ANSWER_IMAGE"]["SRC"];?>" alt="">
                                                        </span>
                                        <span class="conspect-card__text">
                                                            <span><?=$arAnswer["UF_ANSWER"];?></span>
                                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?$answerNumber++;?>
                <?endforeach;?>
            </div>
        </div>
        <?$questionNumber++;?>
    <?endforeach;?>
</article>
