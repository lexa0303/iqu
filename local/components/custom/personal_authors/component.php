<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use Bitrix\Main\Loader,
    Bitrix\Iblock;

if (!function_exists("compare_conspect_quantities")){
    function compare_conspect_quantities($a, $b){
        if ($a["QUANTITY"] == $b["QUANTITY"]){
            return 0;
        }
        return ($a["QUANTITY"] > $b["QUANTITY"]) ? -1 : 1;
    }
}

Loader::includeModule("iblock");
Loader::includeModule("sale");

global $arUser;


$arConspects = Conspects::GetAll($arUser["ID"], array("UF_SHOP" => 1, "UF_ACTIVE"=>1));
$arResult["CONSPECTS"] = $arConspects;

foreach ($arConspects as $arConspect){
    $arShopConspects[$arConspect["UF_SHOP_CONSPECT_ID"]] = $arConspect["UF_SHOP_CONSPECT_ID"];
}

$Basket = new CSaleBasket;
$Order = new CSaleOrder;

$arFilter = array("PRODUCT_ID"=>$arShopConspects, "!ORDER_ID"=>false);

if (isset($_REQUEST["date_from"])){
    $arFilter[">=DATE_INSERT"] = ConvertTimeStamp(strtotime($_REQUEST["date_from"]), "FULL");
}
if (isset($_REQUEST["date_to"])){
    $arFilter["<=DATE_INSERT"] = ConvertTimeStamp((strtotime($_REQUEST["date_to"]) + 86399), "FULL");
}

$rsBasketItems = $Basket->GetList(
    array(),
    $arFilter
);
while ($res = $rsBasketItems->Fetch()){
    $arConspectOrders[$res["PRODUCT_ID"]][] = $res["ORDER_ID"];
}

foreach ($arResult["CONSPECTS"] as &$arConspect){
    $arConspect["QUANTITY"] = count($arConspectOrders[$arConspect["UF_SHOP_CONSPECT_ID"]]);
}

usort($arResult["CONSPECTS"], "compare_conspect_quantities");
?>
<? $this->IncludeComponentTemplate(); ?>

