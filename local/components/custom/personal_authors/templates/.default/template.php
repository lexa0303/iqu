<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use Bitrix\Main\Localization\Loc;
$this->setFrameMode(true);
global $APPLICATION;
?>
<section class="personal" id="stat">
    <div class="container">
        <section class="personal-stats">
            <div class="section__heading">
                <span class="section__heading-txt">Статистика продаж</span>
            </div>
            <form class="js-author_stat_form personal-author__datetime-form" action="<?= $APPLICATION->GetCurPage(false);?>">
                <input type="hidden" name="ajax" value="Y">
                <div class="personal-author__datetime-wrap">
                    <label for="personal_author-date_from" class="">Начало:</label>
                    <div class="">
                        <div class="input-group date">
                            <input id="personal_author-date_from"
                                   required
                                   name="date_from"
                                   class="js-date_from personal-author__datetime-input"
                                   placeholder="DD.MM.YYYY"
                                   type="text">
                        </div>
                    </div>
                </div>
                <div class="personal-author__datetime-wrap">
                    <label for="personal_author-date_to" class="">Конец:</label>
                    <div class="">
                        <div class="input-group date">
                            <input id="personal_author-date_to"
                                   required name="date_to"
                                   class="js-date_to personal-author__datetime-input"
                                   placeholder="DD.MM.YYYY"
                                   type="text">
                        </div>
                    </div>
                </div>
                <div class="personal-author__datetime-wrap">
                    <button class="personal-author__datetime-show">Показать</button>
                </div>
            </form>
            <div class="personal-author__stat js-stat_wrap">
                <?if ($_SERVER["REQUEST_METHOD"] == "POST" && $_REQUEST["ajax"] == "Y") :
                    $APPLICATION->RestartBuffer();
                endif;?>
                <div class="personal-author__stat-row">
                    <div class="personal-author__stat-name personal-author__stat-heading personal-author__stat-col">Название</div>
                    <div class="personal-author__stat-count personal-author__stat-heading personal-author__stat-col">Количество</div>
                    <div class="personal-author__stat-price personal-author__stat-heading personal-author__stat-col">Цена</div>
                    <div class="personal-author__stat-sum personal-author__stat-heading personal-author__stat-col">Сумма</div>
                </div>
                <? foreach ($arResult["CONSPECTS"] as $arConspect) : ?>
                    <div class="personal-author__stat-row">
                        <div class="personal-author__stat-name personal-author__stat-col"><?=$arConspect["UF_NAME"];?></div>
                        <div class="personal-author__stat-count personal-author__stat-col"><?=$arConspect["QUANTITY"];?></div>
                        <div class="personal-author__stat-price personal-author__stat-col"><?=$arConspect["UF_SHOP_PRICE"];?></div>
                        <div class="personal-author__stat-sum personal-author__stat-col"><?=($arConspect["QUANTITY"] * $arConspect["UF_SHOP_PRICE"]);?></div>
                    </div>
                <?endforeach;?>
                <?if ($_SERVER["REQUEST_METHOD"] == "POST" && $_REQUEST["ajax"] == "Y") :
                    die();
                endif;?>
            </div>
        </section>
    </div>
</section>
<script>
    var date_from = body.find(".js-date_from");
    var date_to = body.find(".js-date_to");
    date_from.datetimepicker({
        locale: 'ru',
        format: 'DD.MM.YYYY'
    });
    date_to.datetimepicker({
        locale: 'ru',
        useCurrent: false,
        format: 'DD.MM.YYYY'
    });
    date_from.on("dp.change", function (e) {
        date_to.data("DateTimePicker").minDate(e.date);
    });
    date_to.on("dp.change", function (e) {
        date_from.data("DateTimePicker").maxDate(e.date);
    });

    var form = document.querySelector(".js-author_stat_form");
    var statTable = document.querySelector(".js-stat_wrap");
    form.addEventListener("submit", function(e){
        e.preventDefault();

        var formData = new FormData(this);

        var ajaxOptions = {
            url: this.action,
            body: formData,
            callback: function(res){
                statTable.innerHTML = res;
            }
        };

        ajaxRequest(ajaxOptions);
    });
</script>
