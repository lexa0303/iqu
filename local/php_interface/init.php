<?php
/**
 * Created by PhpStorm.
 * User: al
 * Date: 27.10.2016
 * Time: 10:38
 */

use Bitrix\Highloadblock as HL;
use \Bitrix\Main\Loader as Loader;
use Bitrix\Main\Entity;
Loader::includeModule('catalog');
Loader::includeModule('highloadblock');

function loadIncludes(){
    $includeFiles = scandir($_SERVER["DOCUMENT_ROOT"] . "/local/php_interface/include/");

    foreach ($includeFiles as $include) :
        if ($include == "." || $include == "..")
            continue;

        if (file_exists($_SERVER["DOCUMENT_ROOT"] . "/local/php_interface/include/" . $include)) {
            require_once($_SERVER["DOCUMENT_ROOT"] . "/local/php_interface/include/" . $include);
        }
    endforeach;
}

loadIncludes();
