<?php

/**
 * Created by PhpStorm.
 * User: al
 * Date: 03.11.2016
 * Time: 11:16
 */
class Categories
{

    const iblock = CATEGORIES_HL;

    /**
     * Add category
     * @param $arParams
     * @return bool|int
     */
    public static function Add($arParams) {

        $bReturn = false;

        if (!empty($arParams["UF_IMAGE"])) {
            $params["UF_IMAGE"] = CFile::MakeFileArray($_SERVER["DOCUMENT_ROOT"] . $arParams["UF_IMAGE"]);
        }

        if (!empty($arParams["UF_USER_ID"])) {
            $params["UF_USER_ID"] = $arParams["UF_USER_ID"];
        } else {
            $arErrors["errors"][] = "Нужна авторизация";
        }

        if (!empty($arParams["UF_CODE"]))
            $params["UF_CODE"] = $arParams["UF_CODE"];

        if (!empty($arParams["UF_NAME"])) {
            $params["UF_NAME"] = $arParams["UF_NAME"];
            if (!$params["UF_CODE"])
                $params["UF_CODE"] = translit($arParams["UF_NAME"]);
        } else {
            $arErrors["errors"][] = "Введите название категории";
        }

        if (empty($arErrors)) {
            $params["UF_CREATE_TIME"] = ConvertTimeStamp(time(), "FULL");
            $params["UF_UPDATE_TIME"] = ConvertTimeStamp(time(), "FULL");
            $bReturn = LenalHelp::addToHighloadBlock(self::iblock, $params);
        } else {
            $bReturn = $arErrors;
        }

        $сache = Bitrix\Main\Data\Cache::createInstance();
        $сache->cleanDir('/categories/' . $params['UF_USER_ID'] . "/");

        unlink($_SERVER["DOCUMENT_ROOT"] . $arParams["image_url"]);

        return $bReturn;

    }

    /**
     * Get single category by ID
     * @param $category_id
     * @param bool $fetch
     * @return array|bool|CDBResult
     */
    public static function GetByID($category_id, $fetch = true) {
        $arFilter = array(
            "ID" => $category_id
        );

        $ob = LenalHelp::getHighLoadBlockList(self::iblock, array(), $arFilter);

        $res = $ob->fetch();
        if ($res["UF_IMAGE"] > 0) {
            $image_id = $res["UF_IMAGE"];
            $res["UF_IMAGE"] = array(
                "ID" => $image_id
            );
            $res["UF_IMAGE"]["SRC"] = CFile::GetPath($image_id);
        }

        if (!$res) {
            return false;
        }

        if ($fetch) {
            return $res;
        } else {
            return $ob;
        }
    }

    /**
     * Get single category by CODE
     * @param $category_code
     * @param bool $fetch
     * @return array|bool|CDBResult
     */
    public static function GetByCode($category_code, $fetch = true) {
        $arFilter = array(
            "UF_CODE" => $category_code
        );

        $ob = LenalHelp::getHighLoadBlockList(self::iblock, array(), $arFilter);

        $res = $ob->fetch();
        if ($res["UF_IMAGE"] > 0) {
            $image_id = $res["UF_IMAGE"];
            $res["UF_IMAGE"] = array(
                "ID" => $image_id
            );
            $res["UF_IMAGE"]["SRC"] = CFile::GetPath($image_id);
        }

        if (!$res) {
            return false;
        }

        if ($fetch) {
            return $res;
        } else {
            return $ob;
        }
    }

    /**
     * Get list of categories
     * @param $user_id
     * @param array $arrFilter
     * @param bool $fetch
     * @param array $arrSort
     * @return array|CDBResult|null
     */
    public static function GetAll($user_id, $arrFilter = array(), $fetch = true, $arrSort = array()) {

        $arFilter = array();

        if ($user_id > 0)
            $arFilter = array("UF_USER_ID" => $user_id);

        if (!empty($arrFilter))
            $arFilter = array_merge($arFilter, $arrFilter);

//        $arSort = array("UF_SORT"=>"ASC", "ID"=>"DESC");
        $arSort = array();
        if (!empty($arrSort))
            $arSort = array_merge($arrSort, $arSort);


        $ob = LenalHelp::getHighLoadBlockList(self::iblock, $arSort, $arFilter);

        while ($res = $ob->Fetch()) {
            if ($res["UF_IMAGE"] > 0) {
                $image_id = $res["UF_IMAGE"];
                $res["UF_IMAGE"] = array(
                    "ID" => $image_id
                );
                $res["UF_IMAGE"]["SRC"] = CFile::GetPath($image_id);
            }

            $res["USER"] = CUser::GetByID($res["UF_USER_ID"])->Fetch();

            $arResult[] = $res;
        }

        if (!empty($arResult)) :
            if ($fetch) {
                return $arResult;
            } else {
                return $ob;
            }
        else :
            return null;
        endif;
    }

    /**
     * Get conspects IDs related to category
     * @param $category_id
     * @return mixed
     */
    public static function GetConspects($category_id) {
        $category = self::GetByID($category_id);
        return $category["UF_CONSPECTS"];
    }

    /**
     * Update category
     * @param $category_id
     * @param $arParams
     * @return array|bool|int
     */
    public static function Update($category_id, $arParams) {
        $card = self::GetByID($category_id);

        if ($card !== false) {

            $arFields = array(
                "ID" => $category_id
            );

            if ($arParams["UF_NAME"]) {
                $arFields["UF_NAME"] = $arParams["UF_NAME"];
                $arFields["UF_CODE"] = translit($arParams["UF_NAME"]);
            }

            if ($arParams["UF_CONSPECTS"])
                $arFields["UF_CONSPECTS"] = $arParams["UF_CONSPECTS"];

            $arFields["UF_UPDATE_TIME"] = ConvertTimeStamp(time(), "FULL");
            $return = LenalHelp::addToHighloadBlock(self::iblock, $arFields, "ID");

            $сache = Bitrix\Main\Data\Cache::createInstance();
            $сache->cleanDir('/categories/' . $arParams['UF_USER_ID'] . "/");

            return $return;

        } else {
            return array(
                "errors" => "Категория с таким ID не найдена"
            );
        }
    }

    /**
     * Delete category
     * @param $categoryID
     * @param $userID
     * @return bool
     */
    public static function Delete($categoryID, $userID) {
        $arCategory = self::GetByID($categoryID);

        if (!is_array($arCategory))
            return true;

        if ($arCategory["UF_USER_ID"] == $userID) {
            $result = self::_deleteConspects($arCategory);
            if ($result !== true){
                $error = $result;
                return $error;
            }

            LenalHelp::deleteFromHighloadBlock(self::iblock, $categoryID);

            $сache = Bitrix\Main\Data\Cache::createInstance();
            $сache->cleanDir('/categories/' . $userID . "/");

            return true;
        } else {
            $error["errors"] = "Wrong user Category";
            $error["item"] = $arCategory;
            $error["id"] = $categoryID;
            return $error;
        }
    }

    /**
     * Delete conspects related to category
     * @internal
     * @param $arCategory
     * @return bool
     */
    protected function _deleteConspects($arCategory){
        $error = false;
        
        foreach ($arCategory["UF_CONSPECTS"] as $conspectID) {
            $result = Conspects::Delete($conspectID, $arCategory["UF_USER_ID"]);
            if ($result !== true) {
                $error = $result;
                return $error;
            }
        }

        return true;
    }

}