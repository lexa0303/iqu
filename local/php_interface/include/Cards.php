<?php

/**
 * Created by PhpStorm.
 * User: al
 * Date: 27.10.2016
 * Time: 10:41
 */

class Cards
{

    const iblock = CARDS_HL;

    /**
     * Add card function
     * @param $arParams
     * @return bool|int
     */
    public static function Add($arParams) {

        $bReturn = false;

        if (isset($arParams["ID"])) {
            $arParams["ID"] = null;
            unset($arParams["ID"]);
        }

        if (is_array($arParams["UF_QUESTION_IMAGE"])) {
            $params["UF_QUESTION_IMAGE"] = CFile::CopyFile($arParams["UF_QUESTION_IMAGE"]["ID"]);
        } elseif (!empty($arParams["UF_QUESTION_IMAGE"])) {
            $params["UF_QUESTION_IMAGE"] = CFile::MakeFileArray($_SERVER["DOCUMENT_ROOT"] . $arParams["UF_QUESTION_IMAGE"]);
            unlink($arParams["UF_QUESTION_IMAGE"]);
        }

        if (is_array($arParams["UF_ANSWER_IMAGE"])) {
            $params["UF_ANSWER_IMAGE"] = CFile::CopyFile($arParams["UF_ANSWER_IMAGE"]["ID"]);
        } elseif (!empty($arParams["UF_ANSWER_IMAGE"])) {
            $params["UF_ANSWER_IMAGE"] = CFile::MakeFileArray($_SERVER["DOCUMENT_ROOT"] . $arParams["UF_ANSWER_IMAGE"]);
            unlink($arParams["UF_ANSWER_IMAGE"]);
        }

        if (!empty($arParams["UF_QUESTION"])) {
            $params["UF_QUESTION"] = strip_tags($arParams["UF_QUESTION"], "<div>");
            $params["UF_QUESTION"] = preg_replace("/<div([^>])*>/", "<div>", $params["UF_QUESTION"]);
//            $params["UF_QUESTION"] = substr($params["UF_QUESTION"], 0, 140);
        }

        if (!empty($arParams["UF_ANSWER"])) {
            $params["UF_ANSWER"] = strip_tags(htmlspecialchars_decode($arParams["UF_ANSWER"]), "<div>");
            $params["UF_ANSWER"] = preg_replace("/<div([^>])*>/", "<div>", $params["UF_ANSWER"]);
//            $params["UF_ANSWER"] = substr($params["UF_ANSWER"], 0, 140);
        }

        if (!empty($arParams["UF_USER_ID"])) {
            $params["UF_USER_ID"] = $arParams["UF_USER_ID"];
        } else {
            $arErrors["errors"][] = "Нужна авторизация";
        }

        if (!$params["UF_ANSWER"] && !$params["UF_ANSWER_IMAGE"])
            $arErrors["errors"][] = "Не заполнен ответ";
        if (!$params["UF_QUESTION"] && !$params["UF_QUESTION_IMAGE"])
            $arErrors["errors"][] = "Не заполнен вопрос";

        if (empty($arErrors)) {
            $params["UF_CREATE_TIME"] = ConvertTimeStamp(time(), "FULL");
            $params["UF_UPDATE_TIME"] = ConvertTimeStamp(time(), "FULL");
            $bReturn = LenalHelp::addToHighloadBlock(self::iblock, $params);
        } else {
            $bReturn = $arErrors;
        }

        $сache = Bitrix\Main\Data\Cache::createInstance();
        $сache->cleanDir('/conspects/' . $arParams["UF_USER_ID"] . "/");

        return $bReturn;
    }

    /**
     * Get list of cards function
     * @param array $arrFilter
     * @param bool $fetch
     * @param bool $sortByID
     * @param null $limit
     * @return array|CDBResult|null
     */
    public static function GetAll($arrFilter = array(), $fetch = true, $sortByID = false, $limit = null) {
        $arFilter = array();
        $arFilter = array_merge($arFilter, $arrFilter);

        $ob = LenalHelp::getHighLoadBlockList(self::iblock, array(), $arFilter, array("*"), $limit);

        while ($res = $ob->fetch()){
            if ($res["UF_QUESTION_IMAGE"] > 0) {
                $image_id = $res["UF_QUESTION_IMAGE"];
                $res["UF_QUESTION_IMAGE"] = array(
                    "ID" => $image_id
                );
                $res["UF_QUESTION_IMAGE"]["SRC"] = CFile::GetPath($image_id);
            }
            if ($res["UF_ANSWER_IMAGE"] > 0) {
                $image_id = $res["UF_ANSWER_IMAGE"];
                $res["UF_ANSWER_IMAGE"] = array(
                    "ID" => $image_id
                );
                $res["UF_ANSWER_IMAGE"]["SRC"] = CFile::GetPath($image_id);
            }
            $result[$res["ID"]] = $res;
        }

        if ($sortByID) :
            if (is_array($arrFilter["ID"])) :
                foreach ($arrFilter["ID"] as $id) :
                    if ($result[$id])
                        $return[] = $result[$id];
                endforeach;
                $result = $return;
            endif;
        endif;

        if (!empty($result)) {
            if ($fetch) {
                return $result;
            } else {
                return $ob;
            }
        } else {
            return null;
        }
    }

    /**
     * Delete card function
     * @param $cardID
     * @param $userID
     * @return bool
     */
    public static function Delete($cardID, $userID){
        $arCard = self::GetByID($cardID);
        $error = false;

        if (!is_array($arCard))
            return true;

        if ($arCard["UF_USER_ID"] == $userID) {
            $arConspects = Conspects::GetAll($userID, array("UF_CARDS"=>$cardID));

            foreach ($arConspects as $arConspect) :
                foreach ($arConspect["UF_CARDS"] as $card_id)
                    if ($card_id != $cardID)
                        $arUpdatedConspect["UF_CARDS"][] = $card_id;

                Conspects::Update($arConspect["ID"], $arUpdatedConspect);
            endforeach;

            LenalHelp::deleteFromHighloadBlock(self::iblock, $cardID);

            $сache = Bitrix\Main\Data\Cache::createInstance();
            $сache->cleanDir('/conspects/' . $userID . '/detail/');

            return true;
        } else {
            $error["errors"] = "Wrong user Card";
            $error["item"] = $arCard;
            $error["id"] = $cardID;
            return $error;
        }

    }

    /**
     * Get single card by ID
     * @param $cardID
     * @param bool $fetch
     * @return array|CDBResult|null
     */
    public static function GetByID($cardID, $fetch = true) {

        $arFilter = array(
            "ID" => $cardID
        );

        $ob = LenalHelp::getHighLoadBlockList(self::iblock, array(), $arFilter);

        $res = $ob->fetch();

        if ($res["UF_IMAGE"] > 0) {
            $image_id = $res["UF_IMAGE"];
            $res["UF_IMAGE"] = array(
                "ID" => $image_id
            );
            $res["UF_IMAGE"]["SRC"] = CFile::GetPath($image_id);
        }

        if (!$res) {
            return null;
        }

        if ($fetch) {
            return $res;
        } else {
            return $ob;
        }

    }

    /**
     * Update card function
     * @param $cardID
     * @param $arParams
     * @param bool $bClearCache
     * @return array
     */
    public static function Update($cardID, $arParams, $bClearCache = true) {

        $card = self::GetByID($cardID);

        if ($card !== false) {
            $arFields = array(
                "ID" => $cardID
            );

            if ($arParams["UF_QUESTION"]) {
                $arFields["UF_QUESTION"] = strip_tags($arParams["UF_QUESTION"], "<div>");
                $arFields["UF_QUESTION"] = preg_replace("/<div([^>])*>/", "<div>", $arFields["UF_QUESTION"]);
//                $arFields["UF_QUESTION"] = substr($arFields["UF_QUESTION"], 0, 140);
            }

            if ($arParams["UF_ANSWER"]) {
                $arFields["UF_ANSWER"] = strip_tags($arParams["UF_ANSWER"], "<div>");
                $arFields["UF_ANSWER"] = preg_replace("/<div([^>])*>/", "<div>", $arFields["UF_ANSWER"]);
//                $arFields["UF_ANSWER"] = substr($arFields["UF_ANSWER"], 0, 140);
            }

            if (!empty($arParams["UF_QUESTION_IMAGE"])) {
                $arFields["UF_QUESTION_IMAGE"] = CFile::MakeFileArray($_SERVER["DOCUMENT_ROOT"] . $arParams["UF_QUESTION_IMAGE"]);
//                $arFields["UF_QUESTION_IMAGE"] = $arParams["UF_QUESTION_IMAGE"];
            }

            if (!empty($arParams["UF_ANSWER_IMAGE"])) {
                $arFields["UF_ANSWER_IMAGE"] = CFile::MakeFileArray($_SERVER["DOCUMENT_ROOT"] . $arParams["UF_ANSWER_IMAGE"]);
//                $arFields["UF_ANSWER_IMAGE"] = $arParams["UF_ANSWER_IMAGE"];
            }

            if (isset($arParams["UF_STUDIED"])){
                $arFields["UF_STUDIED"] = $arParams["UF_STUDIED"];
            }

            $arFields["UF_UPDATE_TIME"] = ConvertTimeStamp(time(), "FULL");
            $return = LenalHelp::addToHighloadBlock(self::iblock, $arFields, "ID");

            if ($bClearCache) {
                $cache = Bitrix\Main\Data\Cache::createInstance();
                $cache->clean('/conspects/' . $card["UF_USER_ID"] . '/detail/');
            }

            return $cardID;
        } else {
            return array(
                "errors" => "Карточка с таким ID не найдена"
                );
        }

    }

    /**
     * @param $conspectID
     * @param $quantity
     * @return array
     */
    public static function GetRandom($conspectID, $quantity) {

        $arFilter = array("!UF_CONSPECTS" => $conspectID);

        $ob = LenalHelp::getHighLoadBlockList(self::iblock, array(), $arFilter);

        while ($res = $ob->Fetch()) {
            $arResult[] = $res;
        }

        $rKeys = array_rand($arResult, $quantity);

        foreach ($rKeys as $key) {
            $arReturn[] = $arResult[$key];
        }

        return $arReturn;

    }

    /**
     * Copy card function
     * @param $cardID
     * @param $userID
     * @return bool|int
     */
    public static function Copy($cardID, $userID){
        $arCard = self::GetByID($cardID);

        $arNewCardParams = array(
            "UF_ANSWER" => $arCard["UF_ANSWER"],
            "UF_ANSWER_IMAGE" => ($arCard["UF_ANSWER_IMAGE"] > 0) ? array("ID" => $arCard["UF_ANSWER_IMAGE"]) : false,
            "UF_QUESTION" => $arCard["UF_QUESTION"],
            "UF_QUESTION_IMAGE" => ($arCard["UF_QUESTION_IMAGE"] > 0) ? array("ID" => $arCard["UF_QUESTION_IMAGE"]) : false,
            "UF_USER_ID" => $userID,
            "UF_ORIGIN_USER" => $arCard["UF_USER_ID"]
        );

        return self::Add($arNewCardParams);
    }

    public static function SetStudied($cardID){
        $arCard = self::GetByID($cardID);

        if ($arCard){
            $newStudy = (int)!$arCard["UF_STUDIED"];
            self::Update($cardID, array("UF_STUDIED" => $newStudy));
            return $newStudy;
        } else {
            return array(
                "errors" => "Карточка с таким ID не найдена"
            );
        }
    }

}