<?php
/**
 * Created by PhpStorm.
 * User: al
 * Date: 20.12.2016
 * Time: 09:17
 */

use Bitrix\Main\Entity;
use Bitrix\Main\EventManager;

EventManager::getInstance()->addEventHandler(
    "main",
    "OnBeforeUserAdd",
    array(
        "Handlers",
        "OnBeforeUserAddHandler",
    )
);
EventManager::getInstance()->addEventHandler(
    "sale",
    "OnSalePaymentEntitySaved",
    array(
        "Handlers",
        "OnSalePaymentEntitySavedHandler",
    )
);
//EventManager::getInstance()->addEventHandler(
//    "",
//    "SysConspectsOnBeforeAdd",
//    array(
//        "Handlers",
//        "SysConspectsOnBeforeAddUpdateHandler"
//    )
//);
EventManager::getInstance()->addEventHandler(
    "",
    "SysConspectsOnBeforeUpdate",
    array(
        "Handlers",
        "SysConspectsOnBeforeAddUpdateHandler",
    )
);
EventManager::getInstance()->addEventHandler(
    "",
    "SysConspectsOnBeforeDelete",
    array(
        "Handlers",
        "SysConspectsOnBeforeDeleteHandler",
    )
);
EventManager::getInstance()->addEventHandler(
    "main",
    "OnAfterUserAdd",
    array(
        "Handlers",
        "OnAfterUserAddHandler",
    )
);
EventManager::getInstance()->addEventHandler(
    "sale",
    "OnSaleOrderSaved",
    array(
        "Handlers",
        "OnSaleOrderSavedHandler",
    )
);

class Handlers
{

    protected static $handlerDisallow = false;

    function OnBeforeUserAddHandler(&$arFields)
    {

        /* BLOCK EXECUTINGS OTHER HANDLERS */
        if (self::$handlerDisallow === true)
            return false;
        self::$handlerDisallow = true;

        if (strlen($arFields["TITLE"]) <= 0)
            $arFields["TITLE"] = $arFields["LOGIN"];

        /* UNBLOCK OTHER HANDLERS */
        self::$handlerDisallow = false;

        return true;
    }

    function SysConspectsOnBeforeAddUpdateHandler(\Bitrix\Main\Entity\Event $event)
    {

        $arParams = $event->getParameters();

        $ID = $arParams["id"];
        if (is_array($ID))
            $ID = $ID["ID"];

        if (!$ID)
            return true;

        $arErrors = array();

        if ($ID == TURBO_CONSPECT_ID) {
            $arErrors[] = new Entity\EntityError("Cant modify this");
        }

        $arFields = $arParams["fields"];

        $result = new \Bitrix\Main\Entity\EventResult();
        $entity = $event->getEntity();
        $entityDataClass = $entity->GetDataClass();

        $arChangedFields = array();
        $bError = false;

        $priceCoef = PRICE_COEF;

        /* HANDLE DEFAULT CONSPECT */
        if (isset($arFields["UF_SHOP_CONSPECT_ID"]) && $arFields["UF_SHOP_CONSPECT_ID"] > 0) {
            if ($arFields["UF_SHOP"]) {
                if (!$arFields["UF_SHOP_PRICE"]) {
                    $arErrors[] = new \Bitrix\Main\Entity\FieldError($entity->getField("UF_SHOP_PRICE"), "Ошибка в поле UF_SHOP_PRICE");
                    $bError = true;
                } else {
                    $arNewProduct = array(
                        "NAME"          => ($arFields["UF_NAME"]) ? $arFields["UF_NAME"] : "КонспектБезНазвания",
                        "PREVIEW_IMAGE" => ($arFields["UF_IMAGE"]["old_id"]) ? $arFields["UF_IMAGE"]["old_id"] : "",
                        "CODE"          => ($arFields["UF_CODE"]) ? $arFields["UF_CODE"] : false,
                        "ACTIVE"        => "Y",
                        "IBLOCK_ID"     => CONSPECTS_SHOP_IBLOCK,
                    );

                    $cElement = new CIBlockElement;
                    $cProduct = new CCatalogProduct;
                    $cPrice = new CPrice;

                    $shopConspectId = $cElement->Update($arFields["UF_SHOP_CONSPECT_ID"], $arNewProduct);

                    $cElement->SetPropertyValuesEx(
                        $arFields["UF_SHOP_CONSPECT_ID"],
                        CONSPECTS_SHOP_IBLOCK,
                        array(
                            "CONSPECT_ID" => $ID,
                            "TRIAL"       => 0,
                        )
                    );

                    $rsPrice = $cPrice->GetListEx(
                        array(),
                        array(
                            "PRODUCT_ID" => $arFields["UF_SHOP_CONSPECT_ID"],
                        )
                    );

                    if ($arPrice = $rsPrice->Fetch()) {
                        $cPrice->Update(
                            $arPrice["ID"],
                            array(
                                "PRICE" => $arFields["UF_SHOP_PRICE"] * $priceCoef,
                            )
                        );
                    } else {
                        $res = $cPrice->Add(
                            array(
                                "PRODUCT_ID"       => $arFields["UF_SHOP_CONSPECT_ID"],
                                "CATALOG_GROUP_ID" => 1,
                                "CURRENCY"         => "USD",
                                "PRICE"            => $arFields["UF_SHOP_PRICE"] * $priceCoef,
                            )
                        );
                    }
                }
            } else {
//                CIBlockElement::Delete($arFields["UF_SHOP_CONSPECT_ID"]);
            }
        } else {
            if ($arFields["UF_SHOP"]) {
                if (!$arFields["UF_SHOP_PRICE"]) {
                    $arErrors[] = new \Bitrix\Main\Entity\FieldError($entity->getField("UF_SHOP_PRICE"), "Ошибка в поле UF_SHOP_PRICE");
                    $bError = true;
                } else {
                    $arNewProduct = array(
                        "NAME"          => ($arFields["UF_NAME"]) ? $arFields["UF_NAME"] : "КонспектБезНазвания",
                        "PREVIEW_IMAGE" => ($arFields["UF_IMAGE"]["old_id"]) ? $arFields["UF_IMAGE"]["old_id"] : "",
                        "CODE"          => ($arFields["UF_CODE"]) ? $arFields["UF_CODE"] : false,
                        "ACTIVE"        => "Y",
                        "IBLOCK_ID"     => CONSPECTS_SHOP_IBLOCK,
                    );

                    $cElement = new CIBlockElement;
                    $cProduct = new CCatalogProduct;
                    $cPrice = new CPrice;

                    $shopConspectId = $cElement->Add($arNewProduct);

                    if ($shopConspectId) {
                        $cElement->SetPropertyValuesEx(
                            $shopConspectId,
                            CONSPECTS_SHOP_IBLOCK,
                            array(
                                "CONSPECT_ID" => $ID,
                                "TRIAL"       => 0,
                            )
                        );
                        $cProduct->Add(array("ID" => $shopConspectId));
                        $cPrice->Add(
                            array(
                                "PRODUCT_ID"       => $shopConspectId,
                                "CURRENCY"         => "USD",
                                "PRICE"            => $arFields["UF_SHOP_PRICE"] * $priceCoef,
                                "CATALOG_GROUP_ID" => 1,
                            )
                        );

                        $arFields["UF_SHOP_CONSPECT_ID"] = $shopConspectId;
                        $arChangedFields["UF_SHOP_CONSPECT_ID"] = $shopConspectId;
                        $event->SetParameter("fields", $arFields);
                    } else {

                    }
                }
            }
        }

        unset($shopConspectId);
        unset($arPrice);
        unset($arNewProduct);

        /* HANDLE TRIAL CONSPECT */
        if (isset($arFields["UF_TRIAL_SHOP_ID"]) && $arFields["UF_TRIAL_SHOP_ID"] > 0) {
            if ($arFields["UF_SHOP"]) {
                if (!$arFields["UF_SHOP_PRICE"]) {
                    $arErrors[] = new \Bitrix\Main\Entity\FieldError($entity->getField("UF_SHOP_PRICE"), "Ошибка в поле UF_SHOP_PRICE");
                    $bError = true;
                } else {
                    $arNewProduct = array(
                        "NAME"          => ($arFields["UF_NAME"]) ? $arFields["UF_NAME"] . " - 30 дней" : "КонспектБезНазвания",
                        "PREVIEW_IMAGE" => ($arFields["UF_IMAGE"]["old_id"]) ? $arFields["UF_IMAGE"]["old_id"] : "",
                        "CODE"          => ($arFields["UF_CODE"]) ? $arFields["UF_CODE"] : false,
                        "ACTIVE"        => "Y",
                        "IBLOCK_ID"     => CONSPECTS_SHOP_IBLOCK,
                    );

                    $cElement = new CIBlockElement;
                    $cProduct = new CCatalogProduct;
                    $cPrice = new CPrice;

                    $shopConspectId = $cElement->Update($arFields["UF_TRIAL_SHOP_ID"], $arNewProduct);

                    $cElement->SetPropertyValuesEx(
                        $arFields["UF_TRIAL_SHOP_ID"],
                        CONSPECTS_SHOP_IBLOCK,
                        array(
                            "CONSPECT_ID" => $ID,
                            "TRIAL"       => 1,
                        )
                    );

                    $rsPrice = $cPrice->GetListEx(
                        array(),
                        array(
                            "PRODUCT_ID" => $arFields["UF_TRIAL_SHOP_ID"],
                        )
                    );

                    if ($arPrice = $rsPrice->Fetch()) {
                        $cPrice->Update($arPrice["ID"], array("PRICE" => $arFields["UF_SHOP_PRICE"]));
                    } else {
                        $res = $cPrice->Add(
                            array(
                                "PRODUCT_ID"       => $arFields["UF_TRIAL_SHOP_ID"],
                                "CATALOG_GROUP_ID" => 1,
                                "CURRENCY"         => "USD",
                                "PRICE"            => $arFields["UF_SHOP_PRICE"],
                            )
                        );
                    }
                }
            } else {
//                CIBlockElement::Delete($arFields["UF_SHOP_CONSPECT_ID"]);
            }
        } else {
            if ($arFields["UF_SHOP"]) {
                if (!$arFields["UF_SHOP_PRICE"]) {
                    $arErrors[] = new \Bitrix\Main\Entity\FieldError($entity->getField("UF_SHOP_PRICE"), "Ошибка в поле UF_SHOP_PRICE");
                    $bError = true;
                } else {
                    $arNewProduct = array(
                        "NAME"          => ($arFields["UF_NAME"]) ? $arFields["UF_NAME"] . " - 30 дней" : "КонспектБезНазвания",
                        "PREVIEW_IMAGE" => ($arFields["UF_IMAGE"]["old_id"]) ? $arFields["UF_IMAGE"]["old_id"] : "",
                        "CODE"          => ($arFields["UF_CODE"]) ? $arFields["UF_CODE"] : false,
                        "ACTIVE"        => "Y",
                        "IBLOCK_ID"     => CONSPECTS_SHOP_IBLOCK,
                    );

                    $cElement = new CIBlockElement;
                    $cProduct = new CCatalogProduct;
                    $cPrice = new CPrice;

                    $shopConspectId = $cElement->Add($arNewProduct);

                    if ($shopConspectId) {
                        $cElement->SetPropertyValuesEx(
                            $shopConspectId,
                            CONSPECTS_SHOP_IBLOCK,
                            array(
                                "CONSPECT_ID" => $ID,
                                "TRIAL"       => 1,
                            )
                        );
                        $cProduct->Add(array("ID" => $shopConspectId));
                        $cPrice->Add(
                            array(
                                "PRODUCT_ID"       => $shopConspectId,
                                "CURRENCY"         => "USD",
                                "PRICE"            => $arFields["UF_SHOP_PRICE"],
                                "CATALOG_GROUP_ID" => 1,
                            )
                        );

                        $arFields["UF_TRIAL_SHOP_ID"] = $shopConspectId;
                        $arChangedFields["UF_TRIAL_SHOP_ID"] = $shopConspectId;
                        $event->SetParameter("fields", $arFields);
                    } else {

                    }
                }
            }
        }

        /* */
        if ($ID == HELP_CONSPECT_ID) {
            $arFields["UF_CODE"] = "help";
            $arChangedFields["UF_CODE"] = "help";
        }
        /* */

        if (!$bError || !empty($arErrors)) {
            if ($arFields["UF_XML_ID"] != $ID) {
                $arFields["UF_XML_ID"] = $ID;
                $arChangedFields["UF_XML_ID"] = $ID;
            }
            $event->setParameter("fields", $arFields);

            $result->modifyFields($arChangedFields);
        }

        if (!empty($arErrors)) {
            $result->setErrors($arErrors);
        }

        return $result;
    }

    /* PREVENT TURBO CONSPECT FROM BEING DELETED */
    function SysConspectsOnBeforeDeleteHandler(\Bitrix\Main\Entity\Event $event)
    {

        $arParams = $event->getParameters();

        $ID = $arParams["id"];
        if (is_array($ID))
            $ID = $ID["ID"];

        $errors = array();

        if ($ID == TURBO_CONSPECT_ID) {
            $errors[] = new Entity\EntityError('Cant delete this conspect');
        }

        $result = new Entity\EventResult();

        if (!empty($errors)) {
            $result->setErrors($errors);
        } else {
            $arConspect = Conspects::GetByID($ID);
            $res = Conspects::_deleteNotifications($arConspect);
            if ($res !== true){
                $errors[] = new Entity\EntityError($res);
            }
            Conspects::_deleteCards($arConspect);
            if ($res !== true){
                $errors[] = new Entity\EntityError($res);
            }

            if (!empty($errors)) {
                $result->setErrors($errors);
            }
        }

        return $result;
    }

    function OnSalePaymentEntitySavedHandler(Bitrix\Main\Event $event)
    {
        /** @var Payment $payment */
        $payment = $event->getParameter("ENTITY");
        $oldValues = $event->getParameter("VALUES");


    }

    function OnAfterUserAddHandler($arFields)
    {
        /* BLOCK EXECUTINGS OTHER HANDLERS */
        if (self::$handlerDisallow === true)
            return false;
        self::$handlerDisallow = true;

        $arCategoryParams = array(
            "UF_USER_ID" => $arFields["ID"],
            "UF_NAME"    => "Помощь",
            "UF_CODE"    => "help",
        );
        $categoryID = Categories::Add($arCategoryParams);

        Conspects::SaveConspect(HELP_CONSPECT_ID, $arFields["ID"], $categoryID);

        /* UNBLOCK OTHER HANDLERS */
        self::$handlerDisallow = false;

        return true;
    }

    function OnSaleOrderSavedHandler($event)
    {
        $arOrder = $event->getParameter("ENTITY")->getFieldValues();
        $arOldValues = $event->getParameter("VALUES");
        if ($arOrder["STATUS_ID"] == "F" && $arOrder["PAYED"] == "Y" && $arOrder["STATUS_ID"] != $arOldValues["STATUS_ID"]) {
            Conspects::ProcessOrder($arOrder["ID"]);
        }
    }

}