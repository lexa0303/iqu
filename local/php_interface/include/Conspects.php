<?php

use Bitrix\Main\Loader;

/**
 * Created by PhpStorm.
 * User: al
 * Date: 27.10.2016
 * Time: 14:34
 */
class Conspects
{

    const iblock = CONSPECTS_HL;

    /**
     * Get single conspect by CODE
     * @param $conspect_code
     * @param string $user_id
     * @param bool $fetch
     * @return array|CDBResult|null
     */
    public static function GetByCode($conspect_code, $user_id = "", $fetch = true)
    {
        $arFilter = array(
            "UF_CODE" => $conspect_code,
//            "UF_USER_ID" => $user_id
        );

        if (is_numeric($user_id)) {
            $arFilter["UF_USER_ID"] = $user_id;
        } elseif ($user_id == true) {
            $arFilter["UF_SHOP"] = 1;
        }

        $ob = LenalHelp::getHighLoadBlockList(self::iblock, array(), $arFilter);
        $res = $ob->fetch();

        if ($res) {
            if ($res["UF_ORIGIN_USER"]) {
                $res["ORIGIN_USER"] = CUser::GetByID($res["UF_ORIGIN_USER"])->Fetch();
            }
            if ($res["UF_USER_ID"]) {
                $res["USER"] = CUser::GetByID($res["UF_USER_ID"])->Fetch();
                $res["USER"]["DETAIL_PAGE_URL"] = SITE_DIR . "conspects/" . $res["USER"]["TITLE"] . "/";
                $res["DETAIL_PAGE_URL"] = $res["USER"]["DETAIL_PAGE_URL"] . $res["UF_CODE"] . "/";
            }

            if ($res["UF_IMAGE"] > 0) {
                $image_id = $res["UF_IMAGE"];
                $res["UF_IMAGE"] = array(
                    "ID" => $image_id
                );
                $res["UF_IMAGE"]["SRC"] = CFile::GetPath($image_id);
            }
        }

        if (!$res) {
            return null;
        }

        if ($fetch) {
            return $res;
        } else {
            return $ob;
        }
    }

    /**
     * Delete conspect
     * @param $conspectID
     * @param $userID
     * @return bool
     */
    public static function Delete($conspectID, $userID)
    {
        $arConspect = self::GetByID($conspectID);

        if (!is_array($arConspect)) {
            return true;
        }

        if ($arConspect["UF_USER_ID"] == $userID) {
            $result = self::_deleteCards($arConspect);
            if ($result !== true) {
                $error = $result;
                return $error;
            }
            $result = self::_deleteNotifications($arConspect);
            if ($result !== true) {
                $error = $result;
                return $error;
            }

            $arCategories = Categories::GetAll($userID, array("UF_CONSPECTS" => $conspectID));
            foreach ($arCategories as $arCategory) {
                foreach ($arCategory["UF_CONSPECTS"] as $key => $conspect_id) {
                    if ($conspect_id == $conspectID) {
                        $arCategory["UF_CONSPECTS"][$key] = null;
                        unset($arCategory["UF_CONSPECTS"][$key]);
                    }
                }

                $arUpdate = array(
                    "UF_CONSPECTS" => $arCategory["UF_CONSPECTS"]
                );

                Categories::Update($arCategory["ID"], $arUpdate);
            }

            $сache = Bitrix\Main\Data\Cache::createInstance();
            $сache->cleanDir('/categories/' . $arConspect["UF_USER_ID"] . "/");
            $сache->cleanDir('/conspects/' . $arConspect["UF_USER_ID"] . "/");
            $сache->cleanDir('/conspects_card/' . $arConspect["ID"] . "/");

            LenalHelp::deleteFromHighloadBlock(self::iblock, $conspectID);
            return true;
        } else {
            $error["errors"] = "Wrong user Conspect";
            $error["item"] = $arConspect;
            $error["id"] = $conspectID;
            return $error;
        }
    }

    /**
     * Get single conspect by ID
     * @param $conspect_id
     * @param bool $fetch
     * @return array|CDBResult|null
     */
    public static function GetByID($conspect_id, $fetch = true)
    {
        $arFilter = array(
            "ID" => $conspect_id
        );

        $ob = LenalHelp::getHighLoadBlockList(self::iblock, array(), $arFilter);
        $res = $ob->fetch();

        if ($res["UF_ORIGIN_USER"]) {
            $res["ORIGIN_USER"] = CUser::GetByID($res["UF_ORIGIN_USER"])->Fetch();
        }
        if ($res["UF_USER_ID"]) {
            $res["USER"] = CUser::GetByID($res["UF_USER_ID"])->Fetch();
            $res["USER"]["DETAIL_PAGE_URL"] = SITE_DIR . "conspects/" . $res["USER"]["TITLE"] . "/";
            $res["DETAIL_PAGE_URL"] = $res["USER"]["DETAIL_PAGE_URL"] . $res["UF_CODE"] . "/";
        }

        if ($res["UF_IMAGE"] > 0) {
            $image_id = $res["UF_IMAGE"];
            $res["UF_IMAGE"] = array(
                "ID" => $image_id
            );
            $res["UF_IMAGE"]["SRC"] = CFile::GetPath($image_id);
        }

        if (!$res) {
            return null;
        }

        if ($fetch) {
            return $res;
        } else {
            return $ob;
        }
    }

    /**
     * Delete cards related to conspect
     * @internal
     * @param $arConspect
     * @return bool
     */
    public static function _deleteCards($arConspect)
    {
        if (!is_array($arConspect) || !isset($arConspect["ID"]) || !isset($arConspect["UF_USER_ID"]))
            return false;

        $error = false;

        foreach ($arConspect["UF_CARDS"] as $cardID) {
            $delete = Cards::Delete($cardID, $arConspect["UF_USER_ID"]);
            if ($delete !== true) {
                $error = $delete;
                return $error;
            }
        }

        return true;
    }

    /**
     * Delete notifications related to conspect
     * @internal
     * @param $arConspect
     * @return bool
     */
    public static function _deleteNotifications($arConspect)
    {
        if (!is_array($arConspect) || !isset($arConspect["ID"]) || !isset($arConspect["UF_USER_ID"]))
            return false;

        $error = false;

        $arNotifications = Notifications::GetAll(
            array(
                "UF_USER_ID" => $arConspect["UF_USER_ID"],
                "UF_CONSPECT" => $arConspect["ID"]
            )
        );

        foreach ($arNotifications as $arNotification) {
            $delete = Notifications::Delete($arNotification["ID"]);
            if ($delete !== true) {
                $error = $delete;
                return $error;
            }
        }

        return true;
    }

    /**
     *  Add view count
     * @param $conspectID
     * @return bool|int
     */
    public static function AddViewCount($conspectID)
    {
        $arConspect = Conspects::GetByID($conspectID);

        if (!is_array($arConspect))
            return true;

        if (!$arConspect["UF_VIEW_COUNT"])
            $arConspect["UF_VIEW_COUNT"] = 0;

        if (!$arConspect["UF_VIEW_COUNT_MONTH"])
            $arConspect["UF_VIEW_COUNT_MONTH"] = 0;

        $arFields = array(
            "UF_VIEW_COUNT" => ++$arConspect["UF_VIEW_COUNT"],
            "UF_VIEW_COUNT_MONTH" => ++$arConspect["UF_VIEW_COUNT_MONTH"]
        );

        $bReturn = self::Update($arConspect["ID"], $arFields, false);

        return $bReturn;
    }

    /**
     * Update conspect
     * @param $conspectID
     * @param $arParams
     * @param bool $bClearCache
     * @return bool|int
     */
    public static function Update($conspectID, $arParams, $bClearCache = true)
    {
        $arConspect = self::GetByID($conspectID);

        if (!is_array($arConspect)) {
            return true;
        }

        $arFields = array(
            "ID" => $arConspect["ID"]
        );

        if ($arParams["CATEGORY_ID"]) {
            $bFlag = Conspects::ChangeCategory($conspectID, $arParams["CATEGORY_ID"]);

            if (!$bFlag) {
                $return = array(
                    "status" => "error",
                    "message" => "Ошибка при изменении категории"
                );
                return $return;
            }
        }

        $arFields = array(
            "ID" => $arConspect["ID"]
        );

        foreach ($arParams["CARDS"] as $arCard) {
            if ($arCard["ID"])
                $cardID = Cards::Update($arCard["ID"], $arCard);
            else
                $cardID = Cards::Add($arCard);

            $arFields["UF_CARDS"][] = $cardID;
        }

        if ($arParams["UF_VIEWED_TIME"])
            $arFields["UF_VIEWED_TIME"] = $arParams["UF_VIEWED_TIME"];

        if (is_array($arParams["UF_TAGS"])) {
            $arFields["UF_TAGS"] = $arParams["UF_TAGS"];
        }
        if ($arParams["UF_VIEW_COUNT"] > 0)
            $arFields["UF_VIEW_COUNT"] = $arParams["UF_VIEW_COUNT"];
        if ($arParams["UF_VIEW_COUNT_MONTH"] > 0)
            $arFields["UF_VIEW_COUNT_MONTH"] = $arParams["UF_VIEW_COUNT_MONTH"];
        if ($arParams["UF_SAVES_COUNT"] > 0)
            $arFields["UF_SAVES_COUNT"] = $arParams["UF_SAVES_COUNT"];
        if ($arParams["UF_NAME"] && $arFields["ID"] != 290) {
            $arFields["UF_NAME"] = $arParams["UF_NAME"];
            $arFields["UF_CODE"] = translit($arParams["UF_NAME"]);
        }
//        if ($arParams["UF_CARDS"])
//            $arFields["UF_CARDS"] = $arParams["UF_CARDS"];
        if (isset($arParams["UF_STUDIED"]))
            $arFields["UF_STUDIED"] = $arParams["UF_STUDIED"];
        if (isset($arParams["UF_SHOW_IN_LIBRARY"]))
            $arFields["UF_SHOW_IN_LIBRARY"] = $arParams["UF_SHOW_IN_LIBRARY"];
        if (isset($arParams["UF_ACTIVE"]))
            $arFields["UF_ACTIVE"] = $arParams["UF_ACTIVE"];
        if (isset($arParams["UF_IMAGE"])) {
            if ($arParams["UF_IMAGE"])
                $arFields["UF_IMAGE"] = CFile::MakeFileArray($arParams["UF_IMAGE"]);
            else
                $arFields["UF_IMAGE"] = false;
        }
        if (isset($arParams["UF_NOTE"])){
            $arFields["UF_NOTE"] = $arParams["UF_NOTE"];
        }
        if (isset($arParams["UF_PUBLISH_STATUS"])) {
            switch ($arParams["UF_PUBLISH_STATUS"]):
                case "FREE":
                case "14":
                    $arFields["UF_PUBLISH_STATUS"] = 14;
                    break;
                case "PUBLISH_REQUESTED":
                case "15":
                    $arFields["UF_PUBLISH_STATUS"] = 15;
                    break;
                case "CORRECTED":
                case "16":
                    $arFields["UF_PUBLISH_STATUS"] = 16;
                    break;
                case "DESIGNED":
                case "17":
                    $arFields["UF_PUBLISH_STATUS"] = 17;
                    break;
                case "APPROVED":
                case "18":
                    $arFields["UF_PUBLISH_STATUS"] = 18;
                    break;
                case "PUBLISHED":
                case "19":
                    $arFields["UF_PUBLISH_STATUS"] = 19;
                    break;
                case "PUBLISH_ALLOWED";
                case "20":
                    $arFields["UF_PUBLISH_STATUS"] = 20;
                    break;
            endswitch;
        }
        if (isset($arParams["UF_SHOP_PRICE"]))
            $arFields["UF_SHOP_PRICE"] = $arParams["UF_SHOP_PRICE"];

        if ($bClearCache) {
            $arFields["UF_UPDATE_TIME"] = ConvertTimeStamp(time(), "FULL");
            $arFields["UF_VIEWED_TIME"] = ConvertTimeStamp(time(), "FULL");
            $arFields["UF_VERSION"] = $arConspect["UF_VERSION"] + 1;
        }

        $bReturn = LenalHelp::addToHighloadBlock(self::iblock, $arFields, "ID");

        $сache = Bitrix\Main\Data\Cache::createInstance();

        if ($bClearCache) {
            $сache->cleanDir('/categories/' . $arConspect["UF_USER_ID"] . "/");
            $сache->cleanDir('/conspects/' . $arConspect["UF_USER_ID"] . "/");
            $сache->cleanDir('/conspects_card/' . $arConspect["ID"] . "/");
        }

        return $bReturn;

    }

    /**
     * Change conspects category
     * @param $conspectID
     * @param $newCategoryID
     * @return bool
     */
    public static function ChangeCategory($conspectID, $newCategoryID)
    {
        $arConspect = Conspects::GetByID($conspectID);

        if (!is_array($arConspect))
            return false;

        $arNewCategory = Categories::GetByID($newCategoryID);

        if (!is_array($arNewCategory))
            return false;

        $arCategory = Categories::GetAll(
            $arConspect["UF_USER_ID"],
            array(
                "UF_CONSPECTS" => $conspectID
            )
        )[0];

        if (is_array($arCategory)) {
            foreach ($arCategory["UF_CONSPECTS"] as $key => $conspect_id) {
                if ($conspect_id == $conspectID) {
                    $arCategory["UF_CONSPECTS"][$key] = null;
                    unset($arCategory["UF_CONSPECTS"][$key]);
                }
            }

            $arCategory["UF_CONSPECTS"] = array_unique($arCategory["UF_CONSPECTS"]);

            $arUpdate = array(
                "UF_CONSPECTS" => $arCategory["UF_CONSPECTS"]
            );

            Categories::Update($arCategory["ID"], $arUpdate);
        }

        $arConspects = $arNewCategory["UF_CONSPECTS"];
        $arConspects[] = $conspectID;

        $arConspects = array_unique($arConspects);

        $arUpdate = array(
            "UF_CONSPECTS" => $arConspects
        );

        Categories::Update($arNewCategory["ID"], $arUpdate);

        return true;
    }

    /**
     * Get total count of conspects
     * @return int
     */
    public static function GetCount()
    {
        $сache = Bitrix\Main\Data\Cache::createInstance();
        if ($сache->initCache(3600, 'total_conspects_amount-' . SITE_ID, '/total_amount/')) {
            $count = $сache->getVars();
        } elseif ($сache->startDataCache()) {

            $count = 0;
            $ob = LenalHelp::getHighLoadBlockList(self::iblock, array(), array("UF_ACTIVE"=>1, "UF_SHOW_IN_LIBRARY"=>1), array("ID"));
            while ($res = $ob->fetch()) :
                $count++;
            endwhile;

            // ...
            if ($isInvalid) {
                $сache->abortDataCache();
            }
            // ...

            $сache->endDataCache($count);
        }

        return $count;
    }

    /**
     * Swap "Studied" flag for conspect
     * @param $conspectID
     * @param $userID
     * @return bool|int
     */
    public static function SwapStudy($conspectID, $userID)
    {
        $arConspect = Conspects::GetByID($conspectID);

        if ($arConspect["UF_USER_ID"] == $userID) {
            if (is_array($arConspect)) {
                $studied = (int)!$arConspect["UF_STUDIED"];
                Conspects::Update($conspectID, array("UF_STUDIED" => $studied), false);
                return $studied;
            }
        } else {
            return false;
        }

    }

    /**
     * Search function
     * @param $request
     * @return array|CDBResult|null
     */
    public static function Search($request, $bShop = false)
    {
        $filter = array(
            "UF_ORIGIN_USER" => false,
            "UF_ACTIVE" => 1,
            array(
                "LOGIC" => "OR",
                "%UF_NAME" => $request,
                "=UF_TAGS" => $request
            )
        );
        if (!!$bShop) {
            $filter["UF_SHOP"] = 1;
        } else {
            $filter["UF_SHOW_IN_LIBRARY"] = 1;
        }

        $rsConspects = self::GetAll("", $filter, false, array("UF_NAME" => "ASC"));

        return $rsConspects;
    }

    /**
     * Get list of conspects
     * @param string $userID
     * @param array $arrFilter
     * @param bool $fetch
     * @param array $arrSort
     * @return array|CDBResult|null
     */
    public static function GetAll($userID = "", $arrFilter = array(), $fetch = true, $arrSort = array())
    {
        if (isset($arrFilter["COUNT"])) {
            $limit = $arrFilter["COUNT"];
            $arrFilter["COUNT"] = null;
            unset($arrFilter["COUNT"]);
        } else {
            $limit = null;
        }

        if ($userID > 0) {
            $arFilter = array(
                "UF_USER_ID" => $userID
            );
        } else {
            $arFilter = array();
        }

        if (is_array($arrFilter)) {
            $arFilter = array_merge($arFilter, $arrFilter);
        }

        if (empty($arrSort)) {
            $arrSort = array(
                "UF_UPDATE_TIME" => "DESC"
            );
        }

        $arSelect = array("*");

        $ob = LenalHelp::getHighLoadBlockList(self::iblock, $arrSort, $arFilter, $arSelect, $limit);
        while ($res = $ob->fetch()) :
            if ($res["UF_ORIGIN_USER"]) {
                $res["ORIGIN_USER"] = CUser::GetByID($res["UF_ORIGIN_USER"])->Fetch();
            }
            if ($res["UF_USER_ID"]) {
                $res["USER"] = CUser::GetByID($res["UF_USER_ID"])->Fetch();
                $res["USER"]["DETAIL_PAGE_URL"] = SITE_DIR . "conspects/" . $res["USER"]["TITLE"] . "/";
                $res["DETAIL_PAGE_URL"] = $res["USER"]["DETAIL_PAGE_URL"] . $res["UF_CODE"] . "/";
            }
            if ($res["UF_IMAGE"] > 0) {
                $image_id = $res["UF_IMAGE"];
                $res["UF_IMAGE"] = array(
                    "ID" => $image_id
                );
                $res["UF_IMAGE"]["SRC"] = CFile::GetPath($image_id);
            }
            $arConspects[] = $res;
        endwhile;

        if (!$arConspects) {
            return null;
        }

        if ($fetch) {
            return $arConspects;
        } else {
            return $ob;
        }
    }

    /**
     * Save function from one user to another
     * @param $conspectID
     * @param $userID
     * @param $categoryID
     * @return array|bool|int
     */
    public static function SaveConspect($conspectID, $userID, $categoryID)
    {
        $arConspect = Conspects::GetByID($conspectID);
        if ($categoryID == "shop") {
            $arCategories = Categories::GetAll($userID);
            foreach ($arCategories as $arCategory){
                if ($arCategory["UF_CODE"] == "shop"){
                    $categoryID = $arCategory["ID"];
                }
            }
            if ($categoryID == "shop"){
                $arShopCategory = array(
                    "UF_USER_ID" => $userID,
                    "UF_CODE" => "shop",
                    "UF_NAME" => "Платные конспекты"
                );
                $categoryID = Categories::Add($arShopCategory);
            }
        } else {
            if (!$arConspect["UF_SHOW_IN_LIBRARY"]) {
                $allow = false;
                if ($arConspect["UF_SHOP"]){
                    $arUserOwnedConspects = Conspects::GetOwnedConspects($userID);
                    if (in_array($arConspect["ID"], $arUserOwnedConspects)){
                        $allow = true;
                    }
                }
                if (!$allow) {
                    return array(
                        "status" => "error",
                        "message" => "Пользователь не добавил этот конспект в библиотеку"
                    );
                }
            }

            $arCategory = Categories::GetByID($categoryID);
            if ($userID != $arCategory["UF_USER_ID"])
                return array(
                    "status" => "error",
                    "message" => "Нельзя сохранить конспект в эту категорию"
                );
        }
        
        global $USER;
        if ($arConspect["UF_USER_ID"] == $userID) {
            if ($arConspect["UF_ACTIVE"]) {
                return array(
                    "status" => "error",
                    "message" => "Нельзя скачать свой конспект"
                );
            } else {
                Conspects::Update($arConspect["ID"], array("UF_ACTIVE" => 1));
                return array(
                    "status" => "success",
                    "message" => "Конспект успешно скачан"
                );
            }
        } else {
            if ($arConspect["UF_ACTIVE"] != 1) {
                $return = array(
                    "status" => "error",
                    "message" => "Конспект не найден"
                );
            } else {
                $arOldConspect = Conspects::GetAll($userID, array("UF_ORIGIN_CONSPECT" => $arConspect["ID"]));
                if ($arOldConspect !== null) {
                    Conspects::Delete($arOldConspect[0]["ID"], $userID);
//                Conspects::Update($arOldConspect[0]["ID"], array("UF_ACTIVE" => 1));
//                return array(
//                    "status" => "error",
//                    "message" => "Вы уже сохранили этот конспект"
//                );
                }

                $arNewConspectParams = array(
                    "UF_NAME"            => $arConspect["UF_NAME"],
                    "UF_ORIGIN_USER"     => $arConspect["UF_USER_ID"],
                    "UF_ORIGIN_CONSPECT" => $arConspect["ID"],
                    "UF_USER_ID"         => $userID,
                    "UF_IMAGE"           => $arConspect["UF_IMAGE"],
                    "UF_TAGS"            => $arConspect["UF_TAGS"],
                    "UF_CODE"            => $arConspect["UF_CODE"],
                    "UF_VIEWED_TIME"     => $arFields["UF_UPDATE_TIME"] = ConvertTimeStamp(time(), "FULL")
                );

                foreach ($arConspect["UF_CARDS"] as $cardID) {
                    $arNewConspectParams["UF_CARDS"][] = Cards::Copy($cardID, $userID);
                }

                $return = Conspects::Add($arNewConspectParams, $categoryID);

                if ($return > 0 && $arOldConspect === null) {
                    Conspects::AddSaveCount($conspectID);
                }
            }
        }

        return $return;
    }

    /**
     * Add conspect function
     * @param $arParams
     * @param $categoryID
     * @return array|bool|int
     */
    public static function Add($arParams, $categoryID)
    {
        $bReturn = false;

        $arCategory = Categories::GetByID($categoryID);
        if ($arCategory["UF_USER_ID"] != $arParams["UF_USER_ID"])
            $bReturn = array(
                "error" => "wrong user"
            );

        if (is_array($arCategory)) {
            if (!$arParams["UF_CODE"])
                $arParams["UF_CODE"] = translit($arParams["UF_NAME"]);

            $arConspects = Conspects::GetAll("", array("UF_CODE" => $arParams["UF_CODE"], "UF_USER_ID" => $arParams["UF_USER_ID"]));
            if (is_array($arConspects)) {
                $bReturn = array(
                    "error" => "У вас уже есть конспект с таким названием (кодом)"
                );
            } else {

                if (isset($arParams["UF_CARDS"]) && is_array($arParams["UF_CARDS"])) {

                } else {
                    foreach ($arParams["CARDS"] as $arCard) :
                        $cardID = Cards::Add($arCard);

                        if (is_array($cardID) && isset($cardID["errors"])) {
                            return $cardID["errors"];
                        }

                        $arCards[] = $cardID;
                    endforeach;

                    $arParams["CARDS"] = null;
                    unset($arParams["CARDS"]);

                    $arParams["UF_CARDS"] = $arCards;
                }

                if (is_array($arParams["UF_IMAGE"])) :
                    $arParams["UF_IMAGE"] = CFile::CopyFile($arParams["UF_IMAGE"]["ID"]);
                elseif ($arParams["UF_IMAGE"]) :
                    $arParams["UF_IMAGE"] = CFile::MakeFileArray($_SERVER["DOCUMENT_ROOT"] . $arParams["UF_IMAGE"]);
                    unlink($arParams["UF_IMAGE"]);
                endif;

                if (isset($arParams["UF_SHOW_IN_LIBRARY"]))
                    $arFields["UF_SHOW_IN_LIBRARY"] = $arParams["UF_SHOW_IN_LIBRARY"];

                $arParams["UF_CREATE_TIME"] = ConvertTimeStamp(time(), "FULL");
                $arParams["UF_UPDATE_TIME"] = ConvertTimeStamp(time(), "FULL");
                $arParams["UF_VIEWED_TIME"] = ConvertTimeStamp(time(), "FULL");
                $arParams["UF_ACTIVE"] = 1;
                $arParams["UF_PUBLISH_STATUS"] = 14;
                $conspectID = LenalHelp::addToHighloadBlock(self::iblock, $arParams);

                $arCategoryUpdate["UF_CONSPECTS"] = $arCategory["UF_CONSPECTS"];
                $arCategoryUpdate["UF_CONSPECTS"][] = $conspectID;

                $arCategoryUpdate["UF_CONSPECTS"] = array_unique($arCategoryUpdate["UF_CONSPECTS"]);

                Categories::Update($arCategory["ID"], $arCategoryUpdate);

                $сache = Bitrix\Main\Data\Cache::createInstance();
                $сache->cleanDir('/categories/' . $arParams["UF_USER_ID"] . "/");
                $сache->cleanDir('/conspects/' . $arParams["UF_USER_ID"] . "/");

                $bReturn = $conspectID;
            }
        }

        return $bReturn;
    }

    /**
     * Add save count
     * @param $conspectID
     * @return bool|int
     */
    public static function AddSaveCount($conspectID)
    {
        $arConspect = Conspects::GetByID($conspectID);

        if (!is_array($arConspect))
            return true;

        if (!$arConspect["UF_SAVES_COUNT"])
            $arConspect["UF_SAVES_COUNT"] = 0;

        $arFields = array(
            "UF_SAVES_COUNT" => ++$arConspect["UF_SAVES_COUNT"]
        );

        $bReturn = self::Update($arConspect["ID"], $arFields, false);

        return $bReturn;
    }

    public static function SetViewed($conspectID){
        $arConspect = Conspects::GetByID($conspectID);

        if (!is_array($arConspect))
            return true;

        $arFields["UF_VIEWED_TIME"] = ConvertTimeStamp(time(), "FULL");

        return Conspects::Update($conspectID, $arFields, false);
    }

    public static function ProcessOrder($orderID){
        Loader::includeModule("sale");
        Loader::includeModule("iblock");

        $order = Bitrix\Sale\Order::load($orderID);
        $basket = $order->getBasket()->getBasketItems();
        $userId = $order->getUserId();

        foreach ($basket as $basketItem){
            $arProps = $basketItem->getPropertyCollection()->getPropertyValues();
            $arConspects[$basketItem->getProductId()] = $arProps["CONSPECT_ID"]["VALUE"];
            $arProducts[] = $basketItem->getProductId();
        }

        $el = new CIBlockElement;

        $rsElements = $el->GetList(array(), array("ID"=>$arProducts), false, false, array("ID", "PROPERTY_TRIAL"));
        while ($res = $rsElements->Fetch()){
            $arElements[$res["ID"]] = $res;
        }

        $AvailableConspect = new HL("sys_available_conspects");
        
        foreach ($arElements as $key => $arElement){
            /* HANDLE TRIAL CONSPECTS */
            if ($arElement["PROPERTY_TRIAL_VALUE"] == 1){
                $arAvailableConspect = $AvailableConspect->GetList(
                    array(
                        "filter" => array(
                            "UF_USER_ID" => $userId,
                            "UF_CONSPECT" => $arConspects[$key]
                        )
                    )
                )->Fetch();
                if ($arAvailableConspect){
                    /* EXTENDED TRIAL TIME */
                    $AvailableConspect->Update(
                        $arAvailableConspect["ID"],
                        array(
                            "UF_EXPIRE"   => ConvertTimeStamp($arAvailableConspect["UF_EXPIRE"]->getTimestamp() + 60 * 60 * 24 * 30, "FULL")
                        )
                    );
                } else {
                    $AvailableConspect->Add(
                        array(
                            "UF_USER_ID"  => $userId,
                            "UF_CONSPECT" => $arConspects[$key],
                            "UF_EXPIRE"   => ConvertTimeStamp(time() + 60 * 60 * 24 * 30, "FULL")
                        )
                    );
                }
            } else {
                /* HANDLE PERMANENT CONSPECTS */
                $arAvailableConspect = $AvailableConspect->GetList(
                    array(
                        "filter" => array(
                            "UF_USER_ID" => $userId,
                            "UF_CONSPECT" => $arConspects[$key]
                        )
                    )
                )->Fetch();
                if (!$arAvailableConspect){
                    $AvailableConspect->Add(
                        array(
                            "UF_USER_ID"  => $userId,
                            "UF_CONSPECT" => $arConspects[$key],
                            "UF_EXPIRE"   => ConvertTimeStamp(time() * 1000, "FULL")
                        )
                    );
                }
            }
        }

        foreach ($arConspects as $conspectID){
            $res = Conspects::SaveConspect($conspectID, $order->getUserId(), "shop");
            if ($res["status"] != "success"){
                $return[] = array(
                    "status" => $res["status"],
                    "message" => $res["message"],
                    "conspect" => $conspectID
                );
            }
        }

        if (empty($return)) {
            return array(
                "status" => "success"
            );
        } else {
            return array(
                "status" => "error",
                "data" => $return
            );
        }
    }

    public static function GetOwnedConspects($userID){
        Loader::includeModule("sale");

        $rsOrders = Bitrix\Sale\Order::getList(
            array(
                "filter" => array(
                    "USER_ID" => $userID,
                    "PAYED" => "Y"
                )
            )
        );

        while ($res = $rsOrders->Fetch()){
            $arOrders[] = $res["ID"];
        }
        unset($res);

        $rsBasketItems = Bitrix\Sale\Basket::getList(
            array(
                "filter" => array(
                    "ORDER_ID" => $arOrders
                )
            )
        );

        while ($res = $rsBasketItems->Fetch()){
            $arBasketItems[] = $res["PRODUCT_ID"];
        }
        unset($res);

        $cElem = new CIBlockElement;
        $rsItems = $cElem->GetList(
            array(),
            array(
                "ID" => $arBasketItems,
                "IBLOCK_ID"=>1
            ),
            false,
            false,
            array(
                "ID",
                "IBLOCK_ID",
                "PROPERTY_CONSPECT_ID"
            )
        );

        $arConspects = array();
        while ($res = $rsItems->Fetch()){
            if ($res["PROPERTY_CONSPECT_ID_VALUE"]) {
                $arConspects[] = $res["PROPERTY_CONSPECT_ID_VALUE"];
            }
        }

        return $arConspects;
    }
}