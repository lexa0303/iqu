<?php

use Bitrix\Highloadblock;
use Bitrix\Main\Loader;

Loader::includeModule("highloadblock");

/**
 * Created by PhpStorm.
 * User: alex
 * Date: 28.04.17
 * Time: 12:03
 */
class HL
{

    /**
     * Table name
     * @var string
     */
    protected $hl;
    /**
     * @var \Bitrix\Main\Entity\DataManager
     */
    protected $dataClass;
    /**
     * @var \Bitrix\Main\Entity\Base
     */
    protected $entity;

    private $arTranslitParams = array("replace_space"=>"_","replace_other"=>"-");

    /**
     * HL constructor.
     * @param $table_name
     * @throws Exception
     */
    function __construct($table_name){
        $this->hl = $table_name;

        $hlblock = Bitrix\Highloadblock\HighloadBlockTable::getList(
            array("filter" => array('TABLE_NAME' => $this->hl))
        )->fetch();

        if (!$hlblock){
            throw new Exception("HL not found");
        }

        $entity = Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock);
        $entityDataClass = $entity->getDataClass();

        $this->entity = $entity;
        $this->dataClass = $entityDataClass;

        $return = array();
        $rsHlFields = CUserTypeEntity::GetList(
            array(),
            array(
                "ENTITY_ID" => "HLBLOCK_" . $this->getHighloadBlock()["ID"]
            )
        );
        while ($res = $rsHlFields->Fetch())
            $return[$res["FIELD_NAME"]] = $res;
        $this->map = $return;
        unset($return);
    }

    private function _translit($input){
        return CUtil::translit($input,"ru", $this->arTranslitParams);
    }

    public function Query($sql){
        global $DB;

        $result = $DB->Query($sql);
//        $result = $DB->Query("SELECT * FROM " . $this->hl . " ORDER BY " . $this->hl . ".`UF_NAME` < 'а', " . $this->hl . ".`UF_NAME`");

        return $result;
    }

    /**
     * @param array $params
     * @return \Bitrix\Main\Entity\AddResult|string
     */
    public function Add(array $params){
        if (!is_array($params)){
            return "Wrong params";
        }

        $arParams = $params;

        if (!isset($arParams["UF_SORT"]) && array_key_exists("UF_SORT", $this->getMap()))
            $arParams["UF_SORT"] = 500;
        if (!isset($arParams["UF_CODE"]) && array_key_exists("UF_CODE", $this->getMap()))
            $arParams["UF_CODE"] = $this->_translit($arParams["UF_NAME"]);
        if (!isset($arParams["UF_XML_ID"]) && array_key_exists("UF_CODE", $this->getMap()))
            $arParams["UF_XML_ID"] = $this->_translit($arParams["UF_NAME"]);

        $dataClass = $this->dataClass;
        return $dataClass::add($arParams);
    }

    /**
     * @param $id
     * @param array $params
     * @return \Bitrix\Main\Entity\UpdateResult|string
     */
    public function Update($id, array $params){
        if (!is_array($params) || !is_numeric($id)){
            return "Wrong params";
        }

        $arParams = $params;
        $arParams["ID"] = $id;
//        if (array_key_exists("UF_UPDATE_TIME", $this->getMap())) {
//            $arParams["UF_UPDATE_TIME"] = $arFields["UF_UPDATE_TIME"] = ConvertTimeStamp(time(), "FULL");
//        }

        $dataClass = $this->dataClass;
        return $dataClass::update($id, $arParams);
    }

    /**
     * @param array $params
     * @return \Bitrix\Main\DB\Result|string
     */
    public function GetList(array $params){
        if (!is_array($params)){
            return "Wrong params";
        }

        $arParams = $params;

        $arParams["order"] = (is_array($params["order"])) ? $params["order"] : array("ID"=>"desc");
        $arParams["filter"] = (is_array($params["filter"])) ? $params["filter"] : array();
        $arParams["select"] = (is_array($params["select"])) ? $params["select"] : array("*");
        $arParams["limit"] = (is_numeric($params["limit"])) ? $params["limit"] : null;

        $dataClass = $this->dataClass;
        return $dataClass::getList(
            $arParams
        );
    }

    /**
     * @param $id
     * @return \Bitrix\Main\Entity\DeleteResult|string
     */
    public function Delete($id){
        if (!is_numeric($id)){
            return "Wrong params";
        }

        $dataClass = $this->dataClass;
        $result = $dataClass::delete($id);

        return $result;
    }

    public function getEntity(){
        return $this->entity;
    }

    public function getMap(){
        return $this->map;
    }

    public function getHighloadBlock(){
        $dataClass = $this->dataClass;
        return $dataClass::getHighloadBlock();
    }

    public function PrepareUpdate($arParams){
        $fieldsMap = $this->getMap();

        $arItem = array();
        foreach ($arParams as $key => &$value){
            $curField = $fieldsMap[$key];

            if ($curField["MULTIPLE"] == "Y" && !is_array($value)){
                $value = array($value);
            }
            if ($curField["MULTIPLE"] == "N" && is_array($value)){
                $value = $value[0];
            }

            if ($curField["MULTIPLE"] == "Y"){
                foreach ($value as $key => $val){
                    if (!$val){
                        $value[$key] = null;
                        unset($value[$key]);
                    }
                }
                if (empty($value))
                    $value = false;
            }

            if ($curField["USER_TYPE_ID"] == "file"){
                if ($curField["MULTIPLE"] == "Y"){
                    foreach ($value as &$val){
                        if ($val){
                            $val = CFile::MakeFileArray($val);
                        }
                    }
                } else {
                    $value = CFile::MakeFileArray($value);
                }
            }
        }

        return $arParams;
    }

}