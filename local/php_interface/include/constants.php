<?php
/**
 * Created by PhpStorm.
 * User: al
 * Date: 27.10.2016
 * Time: 10:50
 */

DEFINE("CONSPECTS_SHOP_IBLOCK", 1);
DEFINE("CARDS_HL", "sys_cards");
DEFINE("CATEGORIES_HL", "sys_categories");
DEFINE("EXAMS_HL", "sys_exams");
DEFINE("EXAM_TYPES_HL", "sys_exam_types");
DEFINE("EXAM_SETTINGS_HL", "sys_exam_settings");
DEFINE("CONSPECTS_HL", "sys_conspects");
DEFINE("NOTIFICATIONS_HL", "sys_notifications");
DEFINE("AJAX", "/local/ajax/");
DEFINE("AJAX_FILE", "/local/ajax/ajax.php");
DEFINE("DEFAULT_IMAGE", "/local/templates/iq/images/no-image.png");
DEFINE("USER_UPLOAD_PATH_RELATIVE", "/upload/user_files/");
DEFINE("USER_UPLOAD_PATH", $_SERVER["DOCUMENT_ROOT"] . USER_UPLOAD_PATH_RELATIVE);
DEFINE("SERVER_URL", "http://" . $_SERVER["HTTP_HOST"]);
DEFINE("HELP_CONSPECT_ID", 290);
DEFINE("AUTHORS_GROUP_ID", 9);
DEFINE("TURBO_CONSPECT_ID", 968);
DEFINE("TURBO_GROUP_ID", 13);
DEFINE("PRICE_COEF", 12);

/* LINKS */
const LINKS = array(
    "MY_CONSPECTS" => SITE_DIR . "conspects/#USER_LOGIN#/",
    "MY_CATEGORIES" => SITE_DIR . "categories/#USER_LOGIN#/",
    "CREATE_CONSPECT" => SITE_DIR . "conspects/add/",
    "CREATE_CATEGORY" => SITE_DIR . "categories/add/",
    "LIBRARY" => SITE_DIR . "library/",
    "SHOP" => SITE_DIR . "shop/",
    "NEW" => SITE_DIR . "new/",
    "NOTIFICATIONS" => SITE_DIR . "notifications/"
);
/* END LINKS */

/* EXAM TYPES - USED FOR NOT REQUESTING FROM DB */
const EXAM_TYPES = array(
    "question_answer" => 10,
    "yes_no" => 9,
    "mchoice" => 11
);
/* END EXAM TYPES */