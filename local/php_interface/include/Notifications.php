<?php

/**
 * Created by PhpStorm.
 * User: alex
 * Date: 15.03.17
 * Time: 16:35
 */
class Notifications
{

    const iblock = NOTIFICATIONS_HL;

    public static function Add($arParams) {
        $bReturn = false;

        if (isset($arParams["ID"])) {
            $arParams["ID"] = null;
            unset($arParams["ID"]);
        }

        if (!empty($arParams["UF_PERIOD"])) {
            $params["UF_PERIOD"] = $arParams["UF_PERIOD"];
        }

        if (!empty($arParams["UF_NEXT_DATE"])) {
            $params["UF_NEXT_DATE"] = $arParams["UF_NEXT_DATE"];
        }

        if (!empty($arParams["UF_CONSPECT"])) {
            $params["UF_CONSPECT"] = $arParams["UF_CONSPECT"];
        }

        if (!empty($arParams["UF_USER_ID"])) {
            $params["UF_USER_ID"] = $arParams["UF_USER_ID"];
        } else {
            $arErrors["errors"][] = "Нужна авторизация";
        }

        if (empty($arErrors)) {
            $params["UF_CREATE_TIME"] = ConvertTimeStamp(time(), "FULL");
            $params["UF_UPDATE_TIME"] = ConvertTimeStamp(time(), "FULL");
            $bReturn = LenalHelp::addToHighloadBlock(self::iblock, $params);
        } else {
            $bReturn = $arErrors;
        }

        $сache = Bitrix\Main\Data\Cache::createInstance();
        $сache->cleanDir('/notifications/' . $arParams["UF_USER_ID"] . "/");

        return $bReturn;
    }

    public static function GetByID($id) {
        if (!$id)
            return null;

        $arFilter = array(
            "ID" => $id
        );

        $ob = LenalHelp::getHighLoadBlockList(self::iblock, array(), $arFilter);

        while ($res = $ob->fetch()){
            $result = $res;
        }

        if (!empty($result)) {
            return $result;
        } else {
            return null;
        }
    }

    public static function GetAll($arrFilter = array()) {
        $arFilter = array();
        $arFilter = array_merge($arFilter, $arrFilter);

        $ob = LenalHelp::getHighLoadBlockList(self::iblock, array(), $arFilter);

        while ($res = $ob->fetch()){
            $result[$res["ID"]] = $res;
        }

        if (!empty($result)) {
            return $result;
        } else {
            return null;
        }
    }

    public static function GetNotifications($userID = false){
        global $USER;
        if (!$userID)
            $userID = $USER->GetID();

        $arFilter = array(
            "UF_USER_ID" => $userID,
        );

        $arNotifications = self::GetAll($arFilter);

        foreach ($arNotifications as $arNotification) {
            $arConspectIDs[] = $arNotification["UF_CONSPECT"];
        }

        $arConspects = Conspects::GetAll($userID, array("ID"=>$arConspectIDs));

        foreach ($arNotifications as &$arNotification) {
            foreach ($arConspects as $arConspect){
                if ($arConspect["ID"] == $arNotification["UF_CONSPECT"]){
                    $arNotification["CONSPECT"] = $arConspect;
                }
            }
        }

        return $arNotifications;
    }

    public static function Delete($id) {

        $arNotification = self::GetByID($id);

        if (!$arNotification)
            return true;

        LenalHelp::deleteFromHighloadBlock(self::iblock, $id);

        global $USER;

        $сache = Bitrix\Main\Data\Cache::createInstance();
        $сache->cleanDir('/notifications/' . $arNotification["UF_USER_ID"] . '/');

        return true;
    }

    public static function Update($id, $arParams){

        if (!$id)
            return true;
        else
            $arParams["ID"] = $id;

        return LenalHelp::addToHighloadBlock(self::iblock, $arParams, "ID");
    }

    public static function UpdateDate($id){
        $arNotification = self::GetByID($id);

        $dbPeriods = CUserfieldEnum::GetList(array(), array("USER_FIELD_NAME"=>"UF_PERIOD", "ID"=>$arNotification["UF_PERIOD"]));
        while ($res = $dbPeriods->Fetch())
            $arPeriod = $res;

        $date = new DateTime();

        while ($arNotification["UF_NEXT_DATE"]->getTimestamp() < $date->getTimestamp())
            $arNotification["UF_NEXT_DATE"]->add("1 " . $arPeriod["VALUE"]);

        return self::Update(
            $arNotification["ID"],
            array(
                "UF_NEXT_DATE" => $arNotification["UF_NEXT_DATE"]
            )
        );
    }

}