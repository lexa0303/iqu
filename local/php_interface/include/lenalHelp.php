<?php
/**
 * Created by PhpStorm.
 * User: al
 * Date: 19.08.2016
 * Time: 14:43
 */

use Bitrix\Highloadblock as HL;

class LenalHelp
{
    static $pagintaionStr;

    static function query($sql){

    }

    static function getHighLoadBlockList($hlbl, $arOrder = array(), $filter = array(), $arSelect = array('*'), $limit = null)
    {
        $hlblock = HL\HighloadBlockTable::getList(
            array("filter" => array('TABLE_NAME' => $hlbl))
        )->fetch();
        $entity = HL\HighloadBlockTable::compileEntity($hlblock);
        $Query = new \Bitrix\Main\Entity\Query($entity);

        $Query->setSelect($arSelect);
        $Query->setFilter($filter);
        $Query->setOrder($arOrder);
        $Query->setLimit($limit);

        $result = $Query->exec();

        $arResult = new CDBResult($result);
        return $arResult;
    }

    static function addToHighloadBlock($hlbl, $data = array(), $unical = '')
    {
        $res = null;
        $id = 0;
        if (!empty($unical)) {
            $res = self::getHighLoadBlockList(
                $hlbl, array(), array($unical => $data[$unical]), array('ID')
            )->Fetch();
        }

        if (!empty($data)) {
            $hlblock = HL\HighloadBlockTable::getList(
                array("filter" => array('TABLE_NAME' => $hlbl))
            )->fetch();
            $entity = HL\HighloadBlockTable::compileEntity($hlblock);
            $entityDataClass = $entity->getDataClass();

            if ($res != null) {
                $id = $res["ID"];
                $res = $entityDataClass::update($res["ID"], $data);
            } else {
                $id = $entityDataClass::add($data);
                return $id->getId();
            }
        }

        if ($id > 0) {
            return true;
        }
    }

    /** deleteFromHighloadBlock - удаляет запись в hl блоке
     * @param $hlbl - имя таблицы БД hl блока
     * @param $id - ID удаляемой записи
     */
    static function deleteFromHighloadBlock($hlbl, $id)
    {
        $hlblock = HL\HighloadBlockTable::getList(
            array("filter" => array('TABLE_NAME' => $hlbl))
        )->fetch();
        $entity = HL\HighloadBlockTable::compileEntity($hlblock);
        $entityDataClass = $entity->getDataClass();
        $result = $entityDataClass::delete($id);

        return $result;
    }

    static function superpre($arr)
    {
        global $USER;
        if ($USER->isAdmin()) {
            echo '<pre style="color:red; font-style: italic; font-size: 12px;">';
            print_r($arr);
            echo '</pre>';
        }
    }

    public static function makePagination(CDBResultMysql $res, $limit, $template = ".default")
    {
        $res->navStart($limit);
        self::$pagintaionStr = $res->GetPageNavStringEx($navComponentObject, '', $template);
        return $res;
    }

    /**
     * @return mixed - html шаблон пагинации
     */
    public static function getPagination()
    {
        return self::$pagintaionStr;
    }

    public static function img($id, $width, $height, $resize_type = "BX_RESIZE_IMAGE_PROPORTIONAL", $waterMark = false)
    {
        $arWaterMark = array();
        if ($waterMark) {
            $arWaterMark = Array(
                array(
                    "name" => "watermark",
                    "position" => "center", // Положение
                    "alpha_level" => 20,
                    "type" => "image",
                    "size" => "real",
                    "file" => $_SERVER["DOCUMENT_ROOT"] . '/local/templates/fame/img/logo_b.svg', // Путь к картинке
                    "fill" => "exact",
                )
            );
        }
        if (is_numeric($id) && !empty($id) && $id>0) {
            $resize_img = CFile::ResizeImageGet(
                $id, array("width" => $width, "height" => $height), $resize_type, false, $arWaterMark
            );
        } else {
            $resize_img['src'] = DEFAULT_IMAGE;
        }

        return $resize_img['src'];
    }

}