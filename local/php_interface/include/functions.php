<?php
/**
 * Created by PhpStorm.
 * User: al
 * Date: 27.10.2016
 * Time: 10:39
 */

use Bitrix\Main\Localization\Loc;

function getLink($param, $user_id = null){
    if ($user_id) :
        $cUser = new CUser;
        $arUser = $cUser->GetByID($user_id)->Fetch();
        $title = $arUser["TITLE"];
    else :
        global $arUser;
        global $USER;
        $title = "";
        if ($USER->IsAuthorized()) {
            $title = $arUser["TITLE"];
        }
    endif;

    return str_replace("#USER_LOGIN#", $title, LINKS[$param]);
}

function AddMessage2File($sText, $fileName = LOG_FILENAME, $sModule = "", $traceDepth = 6, $bShowArgs = false)
{
    if ($fileName && strlen($fileName)>0)
    {
        if(!is_string($sText))
        {
            $sText = var_export($sText, true);
        }
        if (strlen($sText)>0)
        {
            ignore_user_abort(true);
            if ($fp = @fopen($fileName, "ab"))
            {
                if (flock($fp, LOCK_EX))
                {
                    @fwrite($fp, "Host: ".$_SERVER["HTTP_HOST"]."\nDate: ".date("Y-m-d H:i:s")."\nModule: ".$sModule."\n".$sText."\n");
                    $arBacktrace = Bitrix\Main\Diag\Helper::getBackTrace($traceDepth, ($bShowArgs? null : DEBUG_BACKTRACE_IGNORE_ARGS));
                    $strFunctionStack = "";
                    $strFilesStack = "";
                    $firstFrame = (count($arBacktrace) == 1? 0: 1);
                    $iterationsCount = min(count($arBacktrace), $traceDepth);
                    for ($i = $firstFrame; $i < $iterationsCount; $i++)
                    {
                        if (strlen($strFunctionStack)>0)
                            $strFunctionStack .= " < ";

                        if (isset($arBacktrace[$i]["class"]))
                            $strFunctionStack .= $arBacktrace[$i]["class"]."::";

                        $strFunctionStack .= $arBacktrace[$i]["function"];

                        if(isset($arBacktrace[$i]["file"]))
                            $strFilesStack .= "\t".$arBacktrace[$i]["file"].":".$arBacktrace[$i]["line"]."\n";
                        if($bShowArgs && isset($arBacktrace[$i]["args"]))
                        {
                            $strFilesStack .= "\t\t";
                            if (isset($arBacktrace[$i]["class"]))
                                $strFilesStack .= $arBacktrace[$i]["class"]."::";
                            $strFilesStack .= $arBacktrace[$i]["function"];
                            $strFilesStack .= "(\n";
                            foreach($arBacktrace[$i]["args"] as $value)
                                $strFilesStack .= "\t\t\t".$value."\n";
                            $strFilesStack .= "\t\t)\n";

                        }
                    }

                    if (strlen($strFunctionStack)>0)
                    {
                        @fwrite($fp, "    ".$strFunctionStack."\n".$strFilesStack);
                    }

                    @fwrite($fp, "----------\n");
                    @fflush($fp);
                    @flock($fp, LOCK_UN);
                    @fclose($fp);
                }
            }
            ignore_user_abort(false);
        }
    }
}

function saveBase64Image($imageName, $base64Content) {
    $arQuestionImageName = explode("\\", $imageName);
    $questionImagePath = USER_UPLOAD_PATH_RELATIVE . end($arQuestionImageName);
    $fQuestionImage = fopen($_SERVER["DOCUMENT_ROOT"] . $questionImagePath, "w+");
    $data = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $base64Content));
    fwrite($fQuestionImage, $data);
    fclose($fQuestionImage);

    return $questionImagePath;
}

/*
  $x и $y - координаты левого верхнего угла выходного изображения на исходном
  $width и $height - ширина и высота выходного изображения
  */
function cropImage($arImage, $x, $y, $width, $height) {
    if (($x < 0) || ($y < 0) || ($width < 0) || ($height < 0)) {
        echo "Некорректные входные параметры";
        return false;
    }
    if (!is_array($arImage) || !isset($arImage["ID"]) || !isset($arImage["SRC"])){
        echo "Некорректные входные параметры изображения";
        return false;
    } else {
        $image = $arImage["SRC"];
    }
    list($w_i, $h_i, $type) = getimagesize($image); // Получаем размеры и тип изображения (число)
    $types = array("", "gif", "jpeg", "png", "jpg"); // Массив с типами изображений
    $ext = $types[$type]; // Зная "числовой" тип изображения, узнаём название типа
    if ($ext) {
        $func = 'imagecreatefrom'.$ext; // Получаем название функции, соответствующую типу, для создания изображения
        $img_i = $func($image); // Создаём дескриптор для работы с исходным изображением
    } else {
        echo 'Некорректное изображение'; // Выводим ошибку, если формат изображения недопустимый
        return false;
    }
    if ($x + $width > $w_i) $width = $w_i - $x; // Если ширина выходного изображения больше исходного (с учётом x_o), то уменьшаем её
    if ($y + $height > $h_i) $height = $h_i - $y; // Если высота выходного изображения больше исходного (с учётом y_o), то уменьшаем её
    $img_o = imagecreatetruecolor($width, $height); // Создаём дескриптор для выходного изображения
    imagecopy($img_o, $img_i, 0, 0, $x, $y, $width, $height); // Переносим часть изображения из исходного в выходное
    $func = 'image'.$ext; // Получаем функция для сохранения результата
    $newImageID = CFile::CopyFile($arImage["ID"]);
    $newImage = CFile::GetPath($newImageID);
    $flag = $func($img_o, $_SERVER["DOCUMENT_ROOT"] . $newImage); // Сохраняем изображение в тот же файл, что и исходное, возвращая результат этой операции
    if ($flag){
        return array(
            "id" => $newImageID,
            "image" => $newImage
        );
    } else {
        return false;
    }
}

function makeCategoryDetails(&$category, $arSefTemplates, $master){
    $category["DETAIL_PAGE_URL"] = SITE_DIR . $arSefTemplates["category_detail"];
    $category["DETAIL_PAGE_URL"] = str_replace("#CATEGORY_ID#", $category["ID"], $category["DETAIL_PAGE_URL"]);
    $category["DETAIL_PAGE_URL"] = str_replace("#USER_TITLE#", $category["USER"]["TITLE"], $category["DETAIL_PAGE_URL"]);

    $category["LIST_PAGE_URL"] = SITE_DIR . $arSefTemplates["categories_list"];
    $category["LIST_PAGE_URL"] = str_replace("#USER_TITLE#", $category["USER"]["TITLE"], $category["LIST_PAGE_URL"]);

    if ($master == "Y") :
        $category["ADD_CONSPECT_URL"] = SITE_DIR . $arSefTemplates["conspect_add"];
        $category["ADD_CONSPECT_URL"] = str_replace("#USER_TITLE#", $category["USER"]["TITLE"], $category["ADD_CONSPECT_URL"]);
        $category["ADD_CONSPECT_URL"] .= "?category_id=" . $category["ID"];
    endif;
}

function makeConspectDetails(&$conspect, $arSefTemplates){
    if (!$conspect["UF_CODE"])
        $conspect["UF_CODE"] = $conspect["ID"];

    $conspect["DETAIL_PAGE_URL"] = SITE_DIR . $arSefTemplates["conspect_detail"];
    $conspect["DETAIL_PAGE_URL"] = str_replace("#CONSPECT_ID#", $conspect["UF_CODE"], $conspect["DETAIL_PAGE_URL"]);
    $conspect["DETAIL_PAGE_URL"] = str_replace("#USER_TITLE#", $conspect["USER"]["TITLE"], $conspect["DETAIL_PAGE_URL"]);

    $conspect["EDIT_PAGE_URL"] = SITE_DIR . $arSefTemplates["conspect_edit"];
    $conspect["EDIT_PAGE_URL"] = str_replace("#CONSPECT_ID#", $conspect["UF_CODE"], $conspect["EDIT_PAGE_URL"]);
    $conspect["EDIT_PAGE_URL"] = str_replace("#USER_TITLE#", $conspect["USER"]["TITLE"], $conspect["EDIT_PAGE_URL"]);

    $conspect["LIST_PAGE_URL"] = SITE_DIR . $arSefTemplates["conspect_list"];
    $conspect["LIST_PAGE_URL"] = str_replace(
        "#USER_TITLE#",
        ($conspect["ORIGIN_USER"]["TITLE"]) ? $conspect["ORIGIN_USER"]["TITLE"] : $conspect["USER"]["TITLE"],
        $conspect["LIST_PAGE_URL"]
    );

    $conspect["EXAM_PAGE_URL"] = SITE_DIR . $arSefTemplates["exam"];
    $conspect["EXAM_PAGE_URL"] = str_replace("#CONSPECT_ID#", $conspect["UF_CODE"], $conspect["EXAM_PAGE_URL"]);
    $conspect["EXAM_PAGE_URL"] = str_replace("#USER_TITLE#", $conspect["USER"]["TITLE"], $conspect["EXAM_PAGE_URL"]);

    $conspect["LESSON_PAGE_URL"] = SITE_DIR . $arSefTemplates["lesson"];
    $conspect["LESSON_PAGE_URL"] = str_replace("#CONSPECT_ID#", $conspect["UF_CODE"], $conspect["LESSON_PAGE_URL"]);
    $conspect["LESSON_PAGE_URL"] = str_replace("#USER_TITLE#", $conspect["USER"]["TITLE"], $conspect["LESSON_PAGE_URL"]);

    $conspect["USER"]["DETAIL_PAGE_URL"] = SITE_DIR . $arSefTemplates["conspect_list"];
    $conspect["USER"]["DETAIL_PAGE_URL"] = str_replace("#USER_TITLE#", $conspect["USER"]["TITLE"], $conspect["USER"]["DETAIL_PAGE_URL"]);

    if ($conspect["UF_ORIGIN_USER"]) {
        $conspect["ORIGIN_USER"]["DETAIL_PAGE_URL"] = SITE_DIR . $arSefTemplates["conspect_list"];
        $conspect["ORIGIN_USER"]["DETAIL_PAGE_URL"] = str_replace("#USER_TITLE#", $conspect["ORIGIN_USER"]["TITLE"], $conspect["ORIGIN_USER"]["DETAIL_PAGE_URL"]);
    }

    $cardsCount = count($conspect["UF_CARDS"]);
    if ($cardsCount == 1)
        $conspect["CARD_COUNT"] = Loc::getMessage("CONSPECT_CARD_AMOUNT_1", array("#COUNT#"=>$cardsCount));
    elseif ($cardsCount < 5 && $cardsCount > 1)
        $conspect["CARD_COUNT"] = Loc::getMessage("CONSPECT_CARD_AMOUNT_2", array("#COUNT#"=>$cardsCount));
    else
        $conspect["CARD_COUNT"] = Loc::getMessage("CONSPECT_CARD_AMOUNT_5", array("#COUNT#"=>$cardsCount));
}

function makeShopConspectDetails(&$conspect, $sefFolder, $arSefTemplates){
    if (!$conspect["UF_CODE"])
        $conspect["UF_CODE"] = $conspect["ID"];

    $conspect["DETAIL_PAGE_URL"] = $sefFolder . $arSefTemplates["detail"];
    $conspect["DETAIL_PAGE_URL"] = str_replace("#CONSPECT_CODE#", $conspect["UF_CODE"], $conspect["DETAIL_PAGE_URL"]);

    $conspect["LIST_PAGE_URL"] = $sefFolder . $arSefTemplates["list"];

    $conspect["USER"]["DETAIL_PAGE_URL"] = str_replace("/conspects/", "/shop/user/", $conspect["USER"]["DETAIL_PAGE_URL"]);

    $cardsCount = count($conspect["UF_CARDS"]);
    if ($cardsCount == 1)
        $conspect["CARD_COUNT"] = Loc::getMessage("CONSPECT_CARD_AMOUNT_1", array("#COUNT#"=>$cardsCount));
    elseif ($cardsCount < 5 && $cardsCount > 1)
        $conspect["CARD_COUNT"] = Loc::getMessage("CONSPECT_CARD_AMOUNT_2", array("#COUNT#"=>$cardsCount));
    else
        $conspect["CARD_COUNT"] = Loc::getMessage("CONSPECT_CARD_AMOUNT_5", array("#COUNT#"=>$cardsCount));
}

function translit($input){
    $arParams = array("replace_space"=>"_","replace_other"=>"-");
    return CUtil::translit($input,"ru",$arParams);
}

function isEmail($input){
    return (filter_var($input, FILTER_VALIDATE_EMAIL) != false) ? true : false;
}

function checkPassword($userId, $password)
{
    $userData = CUser::GetByID($userId)->Fetch();

    $salt = substr($userData['PASSWORD'], 0, (strlen($userData['PASSWORD']) - 32));

    $realPassword = substr($userData['PASSWORD'], -32);
    $password = md5($salt.$password);

    return ($password == $realPassword);
}

function shuffle_assoc(&$array) {
    $keys = array_keys($array);

    shuffle($keys);

    foreach($keys as $key) {
        $new[$key] = $array[$key];
    }

    $array = $new;

    return true;
}