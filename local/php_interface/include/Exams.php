<?php

/**
 * Created by PhpStorm.
 * User: al
 * Date: 10.11.2016
 * Time: 14:54
 */
class Exams
{

    const iblock = EXAMS_HL;
    const types_iblock = EXAM_TYPES_HL;
    const settings_iblock = EXAM_SETTINGS_HL;
    const default_answer_quantity = 1;
    const correct_answers_percent = 40;

    public static function MakeExam($user_id, $conspect_id) {

        $arCards = Cards::GetByConspect($user_id, $conspect_id);

        foreach ($arCards as $arCard) :

            $arExam[$arCard["ID"]]["QUESTION"] = $arCard["UF_QUESTION"];
            $arExam[$arCard["ID"]]["IMAGE"] = $arCard["UF_IMAGE"];
            $arExam[$arCard["ID"]]["ANSWERS"] = self::MakeAnswers($arCards, $arCard["ID"], 5);

        endforeach;

        $return["QUESTIONS"] = $arExam;

        return $return;

    }

    public static function MakeAnswers(&$arCards, $answerQuantity = self::default_answer_quantity) {
        if (!is_array($arCards)) {
            return false;
        } else {
            foreach ($arCards as $cardID => &$arCard) {
                $arCard["CORRECT_ANSWER"] = array(
                    "ID" => $arCard["ID"],
                    "UF_ANSWER" => $arCard["UF_ANSWER"],
                    "UF_ANSWER_IMAGE" => $arCard["UF_ANSWER_IMAGE"]
                );
            }

            if ($answerQuantity == 1){
                foreach ($arCards as $cardID => &$arCard) {
                    $bCorrect = (mt_rand(0, 100) < self::correct_answers_percent) ? true : false;

                    if ($bCorrect) {
                        $arCard["ANSWER"] = $arCard["CORRECT_ANSWER"];
                    } else {
                        $randCard = array_rand($arCards);
                        $arCard["ANSWER"] = array(
                            "ID" => $arCards[$randCard]["ID"],
                            "UF_ANSWER" => $arCards[$randCard]["UF_ANSWER"],
                            "UF_ANSWER_IMAGE" => $arCards[$randCard]["UF_ANSWER_IMAGE"],
                        );
                    }
                }
                unset($arCard);
            } else {
                foreach ($arCards as $cardID => &$arCard) {
                    // TODO - Исключить возможность попадания правильного ответа в список ответов несколько раз
                    $arCard["ANSWERS"] = array();
                    $arCard["ANSWERS"][$arCard["CORRECT_ANSWER"]["ID"]] = $arCard["CORRECT_ANSWER"];
                    while (count($arCard["ANSWERS"]) < $answerQuantity) {
                        $randCard = array_rand($arCards);
                        $test = 1;
                        while (in_array($randCard, array_keys($arCard["ANSWERS"]))) {
                            $randCard = array_rand($arCards);
                            $test++;
                        }

                        $arCard["ANSWERS"][$randCard] = array(
                            "ID" => $arCards[$randCard]["ID"],
                            "UF_ANSWER" => $arCards[$randCard]["UF_ANSWER"],
                            "UF_ANSWER_IMAGE" => $arCards[$randCard]["UF_ANSWER_IMAGE"],
                        );
                    }
                    shuffle($arCard["ANSWERS"]);
                }
                unset($arCard);
            }
        }
    }

    function GetAnswers($user_id, $conspect_id) {

        $arCards = Cards::GetByConspect($user_id, $conspect_id);

        foreach ($arCards as $arCard) {
            $arAnswers[$arCard["ID"]] = $arCard["UF_ANSWER"];
        }

        return $arAnswers;

    }

    public static function Check($user_id, $conspect_id, $arAnswers) {

        $arCorrectAnswers = self::GetAnswers($user_id, $conspect_id);

        foreach ($arCorrectAnswers as $key => $answer) {
            if ($answer == $arAnswers[$key]) {
                $arChecked[$key] = true;
            } else {
                $arChecked[$key] = false;
            }
        }

        return $arChecked;

    }

    public static function SaveResult($user_id, $conspect_id, $arResult, $exam_type) {

        $arConspect = Conspects::GetByID($conspect_id, $user_id);

        if ($arConspect["UF_USER_ID"] != $user_id)
            return false;

        $params = array(
            "UF_USER_ID" => $user_id,
            "UF_CONSPECT" => $conspect_id,
            "UF_RESULT" => serialize($arResult),
            "UF_DATE" => ConvertTimeStamp(time(), "FULL"),
            "UF_EXAM_TYPE" => $exam_type
        );

        $bReturn = LenalHelp::addToHighloadBlock(self::iblock, $params);

        return $bReturn;

    }

    public static function GetExamTypes(){
        $dbExamTypes = LenalHelp::getHighLoadBlockList(
            self::types_iblock,
            array("UF_SORT"=>"asc"),
            array()
        );
        $arExamTypes = array();
        while ($res = $dbExamTypes->Fetch())
            $arExamTypes[] = $res;

        return $arExamTypes;
    }

    public static function GetExamSettings($settings, $turbo = false){
        $dbExamSettingsTypes = CUserFieldEnum::GetList(array(), array("USER_FIELD_NAME"=>"UF_TYPE"));
        while ($res = $dbExamSettingsTypes->Fetch())
            $arExamSettingsTypes[$res["ID"]] = $res["XML_ID"];

        $dbExamSettings = LenalHelp::getHighLoadBlockList(
            self::settings_iblock,
            array(),
            array("ID"=>$settings)
        );
        $arExamSettings = array();
        while ($res = $dbExamSettings->Fetch()) {
            $res["TYPE"] = $arExamSettingsTypes[$res["UF_TYPE"]];
            if (!!$res["UF_TURBO"] == !!$turbo || $res["TYPE"] == "checkbox") {
                $arExamSettings[] = $res;
            }
        }

        $arSettings = array();

        foreach ($arExamSettings as $arExamSetting) {
            if ($arExamSetting["TYPE"] == "select") {
                array_unshift($arSettings, $arExamSetting);
            } else {
                array_push($arSettings, $arExamSetting);
            }
        }

        return $arSettings;
    }

    public static function GetByID($conspect_id, $user_id, $fetch = true) {
        $arFilter = array(
            "ID" => $conspect_id,
            "UF_USER_ID" => $user_id
        );

        $ob = LenalHelp::getHighLoadBlockList(self::iblock, array(), $arFilter);
        $res = $ob->fetch();

        if ($res["UF_USER_ID"]) {
            $res["USER"] = CUser::GetByID($res["UF_USER_ID"])->Fetch();
        }

        if ($res["UF_RESULT"])
            $res["RESULT"] = unserialize($res["UF_RESULT"]);

        if (!$res) {
            return null;
        }

        if ($fetch) {
            return $res;
        } else {
            return $ob;
        }
    }



}