<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 09.06.17
 * Time: 17:16
 */

function printTags($arTags){
    ob_start();
    foreach ($arTags as $tag){
        echo "<a href='/search/?q=$tag' class='conspect-tags__list-item'>$tag</a>";
    }
    $result = ob_get_contents();
    ob_end_clean();
    return $result;
}