<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 21.02.17
 * Time: 16:48
 */

use Bitrix\Main\Mail\Event;

function clearUserUploadedFiles(){
    $files = scandir(USER_UPLOAD_PATH);
    $logName = $_SERVER["DOCUMENT_ROOT"] . "/logs/old_files/" . date("Y") . "/" . date("m") . "/" . date("d") . "/delete.log";

    foreach ($files as $file) {
        if ($file == "." || $file == "..")
            continue;

        $file = USER_UPLOAD_PATH . $file;

        $stat = stat($file);

        if (time() - $stat["mtime"] > 3600) {
            AddMessage2File("Old file deleted - " . $file, $logName);
            unlink($file);
        }
    }

    return "clearUserUploadedFiles();";
}

function resetConspectsMonthlyViewCount(){
    require_once(__DIR__ . "/HL.php");
    $Conspects = new HL("sys_conspects");

    $rsConspects = $Conspects->GetList(
        array(
            "filter" => array(
                ">UF_VIEW_COUNT_MONTH" => 0
            ),
            "select" => array(
                "ID"
            )
        )
    );

    while ($res = $rsConspects->Fetch()){
        $Conspects->Update(
            $res["ID"],
            array(
                "UF_VIEW_COUNT_MONTH" => 0
            )
        );
    }

    return "resetConspectsMonthlyViewCount();";
}

function deactivateTrialConspects(){
    global $USER;
    if (!$USER){
        $USER = new CUser;
    }
    $AvailableConspects = new HL("sys_available_conspects");

    /* DELETE EXPITRED */
    $rsExpired = $AvailableConspects->GetList(
        array(
            "filter" => array(
                "<=UF_EXPIRE" => ConvertTimeStamp(time())
            )
        )
    );

    while ($res = $rsExpired->Fetch()){
        $AvailableConspects->Delete($res["ID"]);
    }

    $rsAlmostExpired = $AvailableConspects->GetList(
        array(
            "filter" => array(
                "<=UF_EXPIRE" => ConvertTimeStamp(time() + (60 * 60 * 24 * 5))
            )
        )
    );
    /* END DELETE EXPIRED */

    /* MAIL ABOUT ALMOST EXPIRED */
    while ($res = $rsAlmostExpired->Fetch()) {
        $arAlmostExpiredIDs[] = $res["UF_CONSPECT"];
        $arConspectToUser[$res["UF_CONSPECT"]] = $res["UF_USER_ID"];
    }
    unset($res);

    $arAlmostExpired = Conspects::GetAll('', array("ID"=>$arAlmostExpiredIDs));

    $rsUsers = $USER->GetList($by = "", $order = "", array("ID"=>$arConspectToUser));
    while ($res = $rsUsers->Fetch()){
        $arUsers[$res["ID"]] = $res;
    }

    foreach ($arAlmostExpired as $arConspect) {
        $arMailFields = array(
            "EMAIL" => $arUsers[$arConspectToUser[$arConspect["ID"]]]["EMAIL"],
            "CONSPECT_NAME" => $arConspect["UF_NAME"],
            "USER_NAME" => $arUsers[$arConspectToUser[$arConspect["ID"]]]["TITLE"],
            "LINK" => "https://" . $_SERVER["HTTP_HOST"] . "/shop/" . $arConspect["UF_CODE"] . "/"
        );

        Event::send(
            array(
                "EVENT_NAME" => "CONSPECT_EXPIRE",
                "LID" => "s1",
                "C_FIELDS" => $arMailFields
            )
        );
    }
    /* END MAIL ABOUT ALMOST EXPIRED */

    return "deactivateTrialConspects();";
}