;
var body = $('body');

$(document).ready(function() {
    /*START modals*/

    /*START iPhone jumping fix*/
    function toggleModal() {
        var offset;
        if (window.innerWidth <= 767) {
            if (document.body.classList.contains('modal--opened')) {
                offset = parseInt(document.body.style.top, 10);
                document.body.classList.remove('modal--opened');
                document.body.scrollTop = (offset * -1);
                document.body.style.top = 0;
            } else {
                offset = document.body.scrollTop;
                document.body.style.top = (offset * -1) + 'px';
                document.body.classList.add('modal--opened');
            }
        }

    }
    /*END iPhone jumping fix*/

    var modalBG = body.find('.modal-bg');
    var allModals = body.find('.modal');

    modalBG.on('click', function() {
        if (this.classList.contains("js-disabled"))
            return false;

        body.removeClass('open-modal-js');

        $(this).fadeOut();
        allModals.fadeOut();

        body.find('.examNav-burger').removeClass('is-active');
        closeModals();
    });
    //open modals
    body.on('click', '.open-modal', function(e) {
        e.preventDefault();

        var modalName = $(this).attr('data-modal');

        toggleModal();

        if (this.dataset.ajax == "Y") {
            callPopupAjax(modalName, this);
        } else {
            callPopup(modalName);
        }
    });

    //close modals on cross
    body.on('click', '.modal__btn-close', function() {
        $('body').find('.exam__navigation').removeClass('js-visibility');
        //

        if (this.classList.contains("js-disabled"))
            return true;

        body.removeClass('open-modal-js');
        modalBG.fadeOut();
        $(this).closest('.modal').fadeOut();
        closeModals();
        toggleModal();
    });

    body.on('click', '.js-modal_close', function() {
        //
        $('body').find('.exam__navigation').removeClass('js-visibility');
        //

        body.removeClass('open-modal-js');
        modalBG.fadeOut();
        $(this).closest('.modal').fadeOut();
        toggleModal();
    });

    //close modals on ESC
    $(this).keydown(function(eventObject) {
        if (eventObject.which == 27) {
            //
            $('body').find('.exam__navigation').removeClass('js-visibility');
            //
            body.removeClass('open-modal-js');
            modalBG.fadeOut();
            allModals.fadeOut();
        }
    });

    //thanks-modal close
    body.on('click', '.thanks-modal__btn', function(e) {
        e.preventDefault();
        body.removeClass('open-modal-js');

        modalBG.fadeOut();
        allModals.fadeOut();
    });
    //new-category-modal close
    body.on('click', '#new-category-modal .new-category-modal__btn--txt', function(e) {
        e.preventDefault();
        body.removeClass('open-modal-js');
        modalBG.fadeOut();
        body.find('#new-category-modal').fadeOut();
    });
    //edit-category-modal close
    body.on('click', '#edit-category-modal .new-category-modal__btn--txt', function(e) {
        e.preventDefault();
        body.removeClass('open-modal-js');
        modalBG.fadeOut();
        body.find('#edit-category-modal').fadeOut();
    });

    //new-pass-modal close
    body.on('click', '.new-pass-modal__btn--txt', function(e) {
        e.preventDefault();
        body.removeClass('open-modal-js');
        modalBG.fadeOut();
        allModals.fadeOut();
    });
    /*END modals*/

    /*START side-menu*/
    body.find(".side-menu__hamburger").on('click', function() {
        //slide for side-menu > ul
        body.find('.side-menu__content').toggleClass('open');
        //animation for burger icon/ slide for burger
        $(this).toggleClass("is-active");
        //
        $(this).toggleClass("open");
    });
    //scroll for side-menu
    var windowWidth = window.innerWidth;
    if (windowWidth <= 1299) {
        body.find(".side-menu__hamburger").on('click', function() {
            body.toggleClass('hamburger');
        });
        //
        var sideMenuHeight = body.find('.side-menu__content').height() - 90;
        body.find('.side-menu__content').css('height', sideMenuHeight + 'px');

    }
    /*END side-menu*/

    /*START header-search*/
    //open search-block
    var headerSBlock = body.find('.search-block');
    body.find('.header__right-search').on('click', function() {
        if (!this.classList.contains("js-header_index_search")){
            headerSBlock.addClass('active');
        }
    });
    //close search-block
    body.find('.search-block__btn-cancel').on('click', function() {
        headerSBlock.removeClass('active');
    });
    //open search-block from tablet

    body.find('.side-menu__search').on('click', function() {
        headerSBlock.addClass('active');
        body.find('.search-block__input').focus();
    });
    /*END header-search*/

    /*START change-photo*/
    body.find('.change-photo__trigger-wrapper').on('click', function() {
        $(this).addClass('triggered');
        body.find('.change-photo__img').css('display', 'inline-block');
    });
    /*END change-photo*/

    /* Carousels START */

    body.find('.main-slider').slick({
        arrows: false,
        dots: true,
        lazyLoad: 'ondemand',
        slidesToShow: 1,
        slidesToScroll: 1
    });

    /* Carousels END */


    /*START main-tabs*/
    // var mtTab = body.find('.main-tabs__link'); // main-tabs tab
    // var mtTabActive = body.find('.main-tabs__link.active'); // main-tabs active tab
    // var mtTabAttr = mtTabActive.attr('data-tabAnchor'); //attr of active tab
    // var mtTabContent = body.find('.main-tabs__content');
    // var mtTabActiveContent = body.find('.main-tabs__content[data-tabAnchor=' + mtTabAttr + ']');
    // mtTabActiveContent.show(); //load active tab(first) content when document is ready
    // mtTab.on('click', function() {
    //     mtTab.removeClass('active'); //removing class 'active' from all tabs
    //     $(this).addClass('active'); //adding class 'active' to current tab
    //     var mtTabAttr = $(this).attr('data-tabAnchor'); //locally define new value
    //     var mtTabActiveContent = body.find('.main-tabs__content[data-tabAnchor=' + mtTabAttr + ']'); //locally define new value
    //     mtTabContent.hide(); //hiding all other tabs
    //     mtTabActiveContent.fadeIn(); //showing current active tab content
    // });
    /*END main-tabs*/


    //Ripple effect ink animation
    // $(function() {
    //     var ink, d, x, y;
    //     $('.ripple').click(function(e) {
    //         // e.preventDefault();
    //         // e.stopPropagation(true);
    //         if ($(this).find('.ripple-ink').length === 0) {
    //             $(this).prepend('<span class="ripple-ink"></span>');
    //         }
    //
    //         ink = $(this).find('.ripple-ink');
    //         ink.removeClass('animate-ripple');
    //
    //         if (!ink.height() && !ink.width()) {
    //             d = Math.max($(this).outerWidth(), $(this).outerHeight());
    //             ink.css({ height: d, width: d });
    //         }
    //
    //         x = e.pageX - $(this).offset().left - ink.width() / 2;
    //         y = e.pageY - $(this).offset().top - ink.height() / 2;
    //
    //         ink.css({ top: y + 'px', left: x + 'px' }).addClass('animate-ripple');
    //     });
    // });

    /*START heapbox */
    // body.find('.selection__item').heapbox();
    /*END heapbox */
    /*scrollbar for heapbox START*/
    // body.find('.heapOptions').mCustomScrollbar({
    //     theme: 'light-3'
    // });

    /*START dragula for conspect-create page*/
    dragula([document.getElementById('creationContent')], {
        moves: function(el, container, handle) {
            return handle.classList.contains('new-card__move');
        }
    });

    var scrollTopInterval, scrollBotInterval;

    $('body').on('mousedown', '.new-card__move', function() {
        var windowHeight = window.innerHeight;
        $('body').on('mousemove', function(e) {
            var mouseY = e.clientY;

            if (mouseY <= 20) { //
                clearInterval(scrollTopInterval);
                scrollTopInterval = setInterval(function(){
                    window.scrollTo(0, window.pageYOffset - 2);
                }, 5);
            } else {
                clearInterval(scrollTopInterval);
            }

            if (mouseY >= windowHeight - 20) { //
                clearInterval(scrollBotInterval);
                scrollBotInterval = setInterval(function(){
                    window.scrollTo(0, window.pageYOffset + 2);
                }, 5);
            } else {
                clearInterval(scrollBotInterval);
            }
        })
    });
    $('body').on('mouseup', function() {
        $('body').off('mousemove');
        clearInterval(scrollBotInterval);
        clearInterval(scrollTopInterval);
    });

    /*END dragula for conspect-create page*/

    /*edit conspect START*/
    body.on('click', '.new-card__btn--edit', function() {
        var cardWrapper = $(this).closest('.new-card'); //global parent of current card
        var cardBody = cardWrapper.find('.new-card__body'); //body of current card
        var cardQuestion = cardWrapper.find('.new-card__text'); //text-wrappers of current card

        cardWrapper.addClass('js-editable');
        cardBody.addClass('active'); //setting borders for q/a
        cardQuestion.attr('contenteditable', 'true'); //making text editable
        cardQuestion.first().focus();
    });
    /*edit conspect END*/

    /*countdown*/
    var curTime = new Date().getTime() + 30000;
    body.find('.exam-start__btn').on('click', function() {
        body.find(".getting-started").countdown(curTime, function(event) {
            $(this).text(event.strftime('%S'));
        });
    });

    /*countdown*/

    /*START studying nav hamburger*/
    body.on('click', '.examNav-burger', function() {
        $(this).toggleClass('is-active');
        switch ($(this).hasClass('is-active')) {
            case false:
                body.find('.modal-bg').fadeOut();
                body.removeClass('open-modal-js');
                break;
            case true:
                body.find('.modal-bg').fadeIn();
                body.addClass('open-modal-js');
                break;
        }
    });
    /*END studying nav hamburger*/


    /*==========================================
    =            exam-settings tabs            =
    ==========================================*/

    if ($('.exam-settings__kind-input:checked')) {
        var kind = $('.exam-settings__kind-input:checked').attr('data-tabName');
        $('.exam-settings__kind-input:checked').next('.exam-settings__kind-label').addClass('checked');
        $('body').find('.exam-settings__drops--' + kind).show();

    }
    $('body').on('change', '.exam-settings__kind-input', function() {
        var self = $(this);
        if (this.checked) {
            var kind = self.attr('data-tabName');
            $('.exam-settings__kind-input ~ .exam-settings__kind-label').removeClass('checked');
            self.parent().find('.exam-settings__kind-label').addClass('checked');
            $('body').find('.exam-settings__drops').hide();
            $('body').find('.exam-settings__drops--' + kind).show();
        }
    });

    /*=====  End of exam-settings tabs  ======*/

    if (window.innerWidth >= 1200) {
        $(window).scroll(function(){
            if ($(this).scrollTop() > 400) {
                $('.js-scrollTop').fadeIn();
            } else {
                $('.js-scrollTop').fadeOut();
            }
        });

        //Click event to scroll to top
        $('.js-scrollTop').click(function(){
            $('html, body').animate({scrollTop : 0},800);
            return false;
        });
    }

    /**/
    $('body').on('click', '.purchases-list__btn-toggle', function(e) {
        e.preventDefault();

        $(this).closest('.purchases-list__item').next('.purchases-list__conspects-wrap').slideToggle();

        if(!$(this).hasClass('js-open')) {
            $(this).addClass('js-open');
            $(this).html('Свернуть');
            $(this).attr('title', 'Скрыть приобретенные конспекты');
        } else {
            $(this).removeClass('js-open');
            $(this).html('Развернуть');
            $(this).attr('title', 'Показать приобретенные конспекты');
        }

    });
    /**/



});

function callPopup(modalName, disableBG){
    if (disableBG === undefined){
        disableBG = false;
    }
    body.find('.modal.active').hide();
    body.find('.examNav-burger').removeClass('is-active');
    body.addClass('open-modal-js');
    body.find('.modal-bg').fadeIn();
    body.find('#' + modalName + '').fadeIn().addClass('active');
    if (disableBG){
        body.find(".modal-bg").addClass("js-disabled");
    }

    //
    $('body').find('.exam__navigation').addClass('js-visibility');
    //
}
function closeModals(){
    body.removeClass('open-modal-js');

    body.find(".modal").fadeOut();
    body.find('.modal-bg').fadeOut();
    body.find('.examNav-burger').removeClass('is-active');
    afterAjaxHandler = function(){

    };
}

function callPopupAjax(modalName, el){
    var ajaxOptions = {
        data: {}
    };

    var dataset = el.dataset;
    for (var i in dataset){
        if (dataset.hasOwnProperty(i)){
            ajaxOptions.data[i] = dataset[i];
        }
    }

    ajaxRequest(ajaxOptions);

    //
    $('body').find('.exam__navigation').addClass('js-visibility');
    //
}

/*debounce function*/
function debounce(func, wait, immediate) {
    var timeout;
    return function() {
        var context = this,
            args = arguments;
        var later = function() {
            timeout = null;
            if (!immediate) func.apply(context, args);
        };
        var callNow = immediate && !timeout;
        clearTimeout(timeout);
        timeout = setTimeout(later, wait);
        if (callNow) func.apply(context, args);
    };
};
var SideMenuScroll = debounce(function() {
    var windowWidth = $(window).width();
    if (windowWidth <= 1299) {
        var sideMenuHeight = $(window).height() - 90;
        body.find('.side-menu__content').css('height', sideMenuHeight + 'px');
    }
}, 250);
//scroll for side-menu
$(window).on('resize', SideMenuScroll);

/*exam-navigation position START*/
var examNav = $('body').find('.exam__navigation'),
    elementPosition = examNav.offset(),
    examNavFixedStyles = {
        'position': 'fixed',
        'top': '0',
        'min-width': '262px',
        'margin-top': '5px',
        'z-index': '1'
    },
    examNavDefaultStyles = {
        'position': 'absolute',
        'min-width': '262px'
    };
if (elementPosition) {
    $(window).on('scroll', function() {
        if ('.conspect_create_form') return ;
        if ($(window).scrollTop() > elementPosition.top - 30) {
            examNav.css(examNavFixedStyles);
        } else {
            examNav.css(examNavDefaultStyles);
        }
    });
}

/*exam-navigaion position END*/