/**
 * Created by razor on 27.10.16.
 */
jQuery.extend(
    jQuery.validator.messages, {
        required: "Заполните это поле.",
        remote: "Please fix this field.",
        email: "Введите правильный email.",
        url: "Please enter a valid URL.",
        date: "Please enter a valid date.",
        dateISO: "Please enter a valid date (ISO).",
        number: "Please enter a valid number.",
        digits: "Please enter only digits.",
        creditcard: "Please enter a valid credit card number.",
        equalTo: "Поля не совпадают.",
        accept: "Please enter a value with a valid extension.",
        maxlength: jQuery.validator.format("Please enter no more than {0} characters."),
        minlength: jQuery.validator.format("Please enter at least {0} characters."),
        rangelength: jQuery.validator.format("Please enter a value between {0} and {1} characters long."),
        range: jQuery.validator.format("Please enter a value between {0} and {1}."),
        max: jQuery.validator.format("Please enter a value less than or equal to {0}."),
        min: jQuery.validator.format("Please enter a value greater than or equal to {0}.")
    }
);

var body = $("body");
var preloader = document.querySelector(".preloader");
var cards = document.querySelectorAll("#creationContent .new-card");
var cardsCount = cards.length;
var Share = {
    instagram: function() {

    },
    vkontakte: function () {
        var url = 'http://vkontakte.ru/share.php?';
        url += 'url=' + encodeURIComponent(window.location.href);
        url += '&title=' + encodeURIComponent(document.title);
        url += '&description=' + encodeURIComponent(get_desription());
        url += '&image=' + (document.getElementById("for_social") ? encodeURIComponent(document.getElementById("for_social").src) : encodeURIComponent('/local/templates/premium/img/ukrsib_logo.jpg'));
        url += '&noparse=true';
        Share.popup(url);
    },
    facebook: function () {
        var url = 'http://www.facebook.com/sharer.php?s=100';
        url += '&p[title]=' + encodeURIComponent(document.title);
        url += '&p[summary]=' + encodeURIComponent(get_desription());
        url += '&p[url]=' + encodeURIComponent(window.location.href);
        url += '&p[picture]=' + (document.getElementById("for_social") ? encodeURIComponent(document.getElementById("for_social").src) : encodeURIComponent('/local/templates/premium/img/ukrsib_logo.jpg'));
        Share.popup(url);
    },
    twitter: function () {
        var url = 'http://twitter.com/share?';
        url += 'text=' + encodeURIComponent(document.title);
        url += '&url=' + encodeURIComponent(window.location.href);
        url += '&image=' + (document.getElementById("for_social") ? encodeURIComponent(document.getElementById("for_social").src) : encodeURIComponent('/local/templates/premium/img/ukrsib_logo.jpg'));
        Share.popup(url);
    },
    google: function () {
        var url = 'https://plus.google.com/share?';
        url += 'text=' + encodeURIComponent(document.title);
        url += '&url=' + encodeURIComponent(window.location.href);
        url += '&image=' + (document.getElementById("for_social") ? encodeURIComponent(document.getElementById("for_social").src) : encodeURIComponent('/local/templates/premium/img/ukrsib_logo.jpg'));
        Share.popup(url);
    },
    linkedin: function () {
        var url = 'http://www.linkedin.com/shareArticle?mini=true';
        url += '&url=' + encodeURIComponent(window.location.href);
        url += '&title=' + encodeURIComponent(document.title);
        url += '&summary=' + encodeURIComponent(get_desription());
// url += '&source=' + encodeURIComponent(document.title);
        Share.popup(url);
    },
    mailru: function () {
        var url = 'http://connect.mail.ru/share?';
        url += '&share_url=' + encodeURIComponent(window.location.href);
        // url += '&title=' + encodeURIComponent(document.title);
        //url += '&summary=' + encodeURIComponent(get_desription());
// url += '&source=' + encodeURIComponent(document.title);
        Share.popup(url);
    },
    mailto: function () {
        var url = 'mailto:?';
        url += '&body=' + encodeURIComponent(window.location.href);
        url += '%20' + encodeURIComponent(document.title);
        url += '%20' + encodeURIComponent(get_desription());
// url += '&source=' + encodeURIComponent(document.title);
        Share.popup(url);
    },
    popup: function (url) {
        window.open(url, '', 'toolbar=0,status=0,width=626,height=436');
    }
};
var conspectErrorColorTimeout;
var conspectSaveTimeout = {};

function setCookie(name, value, options) {
    options = options || {};

    var expires = options.expires;

    if (typeof expires == "number" && expires) {
        var d = new Date();
        d.setTime(d.getTime() + expires * 1000);
        expires = options.expires = d;
    }
    if (expires && expires.toUTCString) {
        options.expires = expires.toUTCString();
    }

    value = encodeURIComponent(value);

    var updatedCookie = name + "=" + value;

    for (var propName in options) {
        updatedCookie += "; " + propName;
        var propValue = options[propName];
        if (propValue !== true) {
            updatedCookie += "=" + propValue;
        }
    }

    document.cookie = updatedCookie;
}
function getCookie(name) {
    var matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
}
function deleteCookie(name) {
    document.cookie = name + '=;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
};
function conspectDownloadCategoryAddHandler(params){
    var select = document.getElementById("dCategorySelect");
    var option = document.createElement("option");
    option.text = params.UF_NAME;
    option.value = params.ID;
    select.add(option, select[1]);
    option.selected = "selected";

    initHeapbox();
    body.find('#heapbox_dCategorySelect .holder').text(params.UF_NAME);
    body.find('#heapbox_dCategorySelect .holder').attr("rel", params.ID);
    closePopups();
    callPopup("download-conspect-modal");
}
function conspectCategoryAddHandler(params){
    var select = document.getElementById("creationSelect");
    var option = document.createElement("option");
    option.text = params.UF_NAME;
    option.value = params.ID;
    select.add(option, select[1]);
    option.selected = "selected";

    initHeapbox();
    body.find('#heapbox_creationSelect .holder').text(params.UF_NAME);
    body.find('#heapbox_creationSelect .holder').attr("rel", params.ID);
    closePopups();
}
function closePopups(){
    body.find(".modal.active").fadeOut();
    body.find(".modal-bg").fadeOut();
    body.removeClass('open-modal-js');
}
function conspectValidationErrors(form, conspect){
    if (conspect.status != "error"){
        return true;
    }

    var i, item, card, question, answer;
    var scroll = true;

    for (i in conspect.errors){
        if (conspect.errors.hasOwnProperty(i)){
            item = conspect.errors[i];
            card = form.querySelector(".new-card[data-number='"+item.number+"']");
            answer = card.querySelector(".js-answer_wrap");
            question = card.querySelector(".js-question_wrap");

            if (question) {
                if (item.question) {
                    question.classList.add("error");
                    if (scroll){
                        body.animate({scrollTop: $(question).offset().top}, 600);
                        scroll = false;
                    }
                } else {
                    question.classList.remove("error");
                }
            }

            if (answer) {
                if (item.answer) {
                    answer.classList.add("error");
                    if (scroll){
                        body.animate({scrollTop: $(answer).offset().top}, 600);
                        scroll = false;
                    }
                } else {
                    answer.classList.remove("error");
                }
            }
        }
    }
}
function initNotificationPopup(params){
    if (params === undefined){
        params = {};
    }
    /*START heapbox */
    body.find('.selection__item').heapbox();
    /*END heapbox */
    /*scrollbar for heapbox START*/
    body.find('.heapOptions').mCustomScrollbar({
        theme: 'light-3'
    });
    /*scrollbar for heapbox END*/
    /*datetimepicker*/
    $('#notificationDatePicker').datetimepicker({
        locale: 'ru',
        useCurrent: false,
        minDate: new Date()
    });

    /**/
    $('body').on('click', '#notificationDatePicker', function() {
        $('.bootstrap-datetimepicker-widget .picker-switch a[data-action]')
            .find('.glyphicon-time')
                .removeClass('glyphicon-time')
                    .addClass('icon icon-wall-clock')
                        .text('Указать время');


        $('body').find('.bootstrap-datetimepicker-widget .picker-switch a[data-action]')
            .find('.icon-wall-clock')
                .on('click', function() {
                    $(this).toggleClass('icon icon-wall-clock');

                    if ($(this).text() == "Указать время")
                        $(this).text("Указать дату");
                    else
                        $(this).text("Указать время");
                });
    });

    $('body').find("#notificationDatePicker").on('focus', function() {
        $('.bootstrap-datetimepicker-widget .picker-switch a[data-action]')
            .find('.glyphicon-time')
            .removeClass('glyphicon-time')
            .addClass('icon icon-wall-clock')
            .text('Указать время');


        $('body').find('.bootstrap-datetimepicker-widget .picker-switch a[data-action]')
            .find('.icon-wall-clock')
            .on('click', function() {
                $(this).toggleClass('icon icon-wall-clock');

                if ($(this).text() == "Указать время")
                    $(this).text("Указать дату");
                else
                    $(this).text("Указать время");
            });
    });

    var availableTags = params.autocomplete_data || {};
    $("#conspect_autocomplete").autocomplete({
        data: availableTags
    });

    /**/
    /*datetimepicker*/
}
function get_desription() {
    var metas = document.getElementsByTagName('meta');
    for (var i = 0; i < metas.length; i++) {
        if (metas[i].getAttribute("name") == "Description") {
            return crop_str(metas[i].getAttribute("content"));
        }
    }
    return "";
}
function toFormData(obj, form, namespace) {
    var fd = form || new FormData();
    var formKey;

    for(var property in obj) {
        if(obj.hasOwnProperty(property) && obj[property]) {
            if (namespace) {
                formKey = namespace + '[' + property + ']';
            } else {
                formKey = property;
            }

            // if the property is an object, but not a File, use recursivity.
            if (obj[property] instanceof Date) {
                fd.append(formKey, obj[property].toISOString());
            }
            else if (typeof obj[property] === 'object' && !(obj[property] instanceof File)) {
                toFormData(obj[property], fd, formKey);
            } else { // if it's a string or a File object
                fd.append(formKey, obj[property]);
            }
        }
    }

    return fd;
}
function ajaxRequest(options){
    var url = options.url || globals.ajaxFile,
        method = (options.method == "GET") ? "GET" : 'POST',
        callback = options.callback || ajaxHandler,
        data = options.data || {},
        xmlHttp = new  XMLHttpRequest();

    var body = '';

    xmlHttp.open(method,url);

    if (options.body) {
        body = options.body;
    } else {
        xmlHttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        var i = 0;

        for (var key in data) {
            if (data.hasOwnProperty(key)) {
                body += (i == 0) ? '' : '&';
                body += key + '=' + encodeURIComponent(data[key]);
                i++;
            }
        }
    }

    xmlHttp.send(body);

    xmlHttp.onreadystatechange = function(){
        if(xmlHttp.status === 200 && xmlHttp.readyState ===4){
            callback(xmlHttp.responseText);
        }
    };
}
function ajaxHandler(res){
    var result = JSON.parse(res);
    var locationTimeout = 500;
    if (result.form_id && result.form_id.length > 0)
        var form = document.querySelector("#" + result.form_id);

    if (result.status === "error") {
        if (result.message) {
            toastr.error(result.message);
        }
    } else if(result.no_action === true){
        return true;
    } else if(result.status === "html") {
        document.getElementById(result.container).innerHTML = result.html;
        callPopup(result.container);
        initFormsValidation();
        initHeapbox();
    } else {
        if (form && form.classList.contains("js-skip_reload")) {

        } else {
            if (result.message) {
                toastr.success(result.message);
            }
        }
    }
    if (result.location !== undefined) {
        setTimeout(function(){
            location.href = result.location;
        }, locationTimeout);
    } else if (result.callbackFunction !== undefined){
        if (result.callbackMethod !== undefined) {
            window[result.callbackFunction][result.callbackMethod](result.callbackParams);
        } else {
            window[result.callbackFunction](result.callbackParams);
        }
    } else if (result.callback !== undefined){
        setTimeout(function(){
            callPopup(result.callback);
        }, locationTimeout);
    } else if (result.reload === "Y") {
        if (form && form.classList.contains("js-skip_reload")) {

        } else {
            setTimeout(function(){
                location.reload();
            }, locationTimeout);
        }
    }

    if (result.handler === true && afterAjaxHandler !== undefined){
        afterAjaxHandler(result);
    }
}
function ajaxSubmit(form) {
    // var buttons
    var formData = new FormData(form);
    var ajaxOptions = {
        callback: function(res){
            ajaxHandler(res);
        }
    };
    if (form.url !== undefined)
        ajaxOptions.url = form.url.value;
    if (form.parse_conspect !== undefined){
        if (form.parse_conspect.value == "Y") {
            var conspect = parseConspectForm(form);
            if (!conspectValidationErrors(form, conspect)){
                return false;
            }
            var conspectData = {
                conspect: conspect
            };
            formData = toFormData(conspectData, formData);
        }
    }
    formData.append("form_id", form.getAttribute('id'));
    ajaxOptions.body = formData;
    ajaxRequest(ajaxOptions);
}
function handleFileSelect(evt, callback) {
    var files = evt.target.files; // FileList object

    // Loop through the FileList and render image files as thumbnails.
    for (var i = 0, f; f = files[i]; i++) {

        // Only process image files.
        if (!f.type.match('image.*')) {
            continue;
        }

        var reader = new FileReader();

        // Closure to capture the file information.
        reader.onload = (function(theFile) {
            return function(e) {
                callback(e);
            };
        })(f);

        // Read in the image file as a data URL.
        reader.readAsDataURL(f);
    }
}
function sendFile(el, success) {

    var file = $(el),
        files = el.files;

    if (file.val().length > 0) {
        var data = new FormData();

        $.each( files, function( key, value ){
            data.append( key, value );
        });

        var parent = file.closest("form");
        var submit = parent.find("button");
        var progressBar = parent.find(".progress_bar");

        if (submit) {
            submit.disabled = true;
        }
        if (progressBar) {
            progressBar.css("backgroundColor", "red");
            progressBar.removeClass("hidden");
        }

        $.ajax({
            url: globals.ajax + "load_file.php",
            data: data,
            type: "post",
            cache: false,
            dataType: "json",
            processData: false,
            contentType: false,
            xhr: function() {
                var xhr = new window.XMLHttpRequest();
                // Upload progress
                xhr.upload.addEventListener("progress", function(evt){
                    if (evt.lengthComputable) {
                        var percentComplete = evt.loaded / evt.total;

                        if (progressBar) {
                            progressBar.css("width", ((percentComplete * 100) + "%"));
                        }

                        if (percentComplete == 1) {
                            if (progressBar) {
                                progressBar.css("backgroundColor", "green");
                            }
                        }
                        // Do something with upload progress
                    }
                }, false);

                return xhr;
            },
            success: function(result) {
                if (submit){
                    submit.attr("disabled", false);
                }
                if (result.file.length > 0){
                    success(result);
                }
            }
        });
    }
}
function blockNonAuthorized(e, node) {
    e.preventDefault();
    e.stopImmediatePropagation();
    $('body').find('.modal.active').hide();
    $('body').addClass('open-modal-js');
    $('body').find('.modal-bg').fadeIn();
    $('body').find('#reg-modal').fadeIn().addClass('active');
}
function initFormsValidation() {
    $("body").find(".ajax_submit").each(function(){
        var formOb = $(this);
        formOb.validate({
            submitHandler: function(form, e){
                e.preventDefault();
                ajaxSubmit(form);
            },
            ignore: []
        });
    });
}
function checkSubs(newSubscribes){
    var subButtons = document.querySelectorAll(".js-sub");
    var key, button;

    if (newSubscribes != undefined){
        globals.subscribed = newSubscribes;
    }

    for (key in subButtons){
        if (subButtons.hasOwnProperty(key)){
            button = subButtons[key];
            if (globals.subscribed === null) {
                button.style.display = "none";
            } else if (globals.subscribed == []){
                if (button.classList.contains("js-unsubscribe")) {
                    button.style.display = "none";
                }
                if (button.classList.contains("js-subscribe")) {
                    button.style.display = "block";
                }
            } else {
                if (globals.subscribed.indexOf(parseInt(button.dataset.user_id)) === -1) {
                    if (button.classList.contains("js-unsubscribe")) {
                        button.style.display = "none";
                    }
                    if (button.classList.contains("js-subscribe")) {
                        button.style.display = "block";
                    }
                } else {
                    if (button.classList.contains("js-subscribe")) {
                        button.style.display = "none";
                    }
                    if (button.classList.contains("js-unsubscribe")) {
                        button.style.display = "block";
                    }
                }
            }
        }
    }
}
function hideLoader(){
    $(preloader).fadeOut(500);
}
function showLoader(){
    $(preloader).fadeIn();
}
function initHeapbox(){
    /*START heapbox */
    body.find('.selection__item').heapbox({
        "onChange": function(val, el){
            if (val === "create_category"){
                setTimeout(function(){
                    callPopup("conspect_add-new-category-modal");
                }, 500);
            }
            if (val === "create_category_modal"){
                setTimeout(function(){
                    callPopup("conspect_download-new-category-modal");
                }, 500);
            }
        }
    });
    body.find('.selection__item').heapbox("update");
    /*END heapbox */
    /*scrollbar for heapbox START*/
    body.find('.heapOptions').mCustomScrollbar({
        theme: 'light-3'
    });
}
function syncName(nameInput){
    var name = nameInput.value;
    var target = document.getElementById("conspect_card-name");

    if (target.innerHTML != name)
        target.innerHTML = name;
}
function parseConspectForm(form) {
    var cards = form.querySelectorAll(".new-card");
    var card;
    var arCards = [];
    var i = 0;
    var item = {};
    var errors = [];
    var error;
    var status = true;

    for (i in cards){
        if (cards.hasOwnProperty(i)){
            card = cards[i];
            item = {
                id: "",
                question : {
                    text : "",
                    image : ""
                },
                answer : {
                    text : "",
                    image : ""
                }
            };

            if (card.dataset.id > 0){
                item.id = card.dataset.id;
            }
            item.question.text = card.querySelector(".js-question_text").innerHTML;
            item.question.image = $(card).find(".js-question_image").attr("src");
            // item.question.image = card.querySelector(".js-question_image").src;
            item.question.image_name = card.querySelector(".js-question_image-input").value;
            item.answer.text = card.querySelector(".js-answer_text").innerHTML;
            item.answer.image = $(card).find(".js-answer_image").attr("src");
            // item.answer.image = card.querySelector(".js-answer_image").src;
            item.answer.image_name = card.querySelector(".js-answer_image-input").value;

            error = {
                number: card.dataset.number,
                question: false,
                answer: false
            };

            if ((item.question.text == "" && item.question.image_name == "" && !item.question.image)
                || (item.answer.text == "" && item.answer.image_name == "" && item.answer.image == "")){

                if (item.question.text == "" && item.question.image_name == "" && !item.question.image){
                    error.question = true;
                }
                if (item.answer.text == "" && item.answer.image_name == "" && item.answer.image == ""){
                    error.answer = true;
                }
                status = false;
            }

            if (!form.classList.contains("js-skip_reload")) {
                errors.push(error);
            }
            arCards.push(item);
        }
    }

    if (form.classList.contains("js-skip_reload"))
        status = true;

    if (!status) {
        return {
            status: 'error',
            errors: errors
        }
    } else {
        return arCards;
    }
}
function checkNew(){
    var newCountContainer = document.querySelector(".js-new_count");
    if (!newCountContainer){
        return false;
    }

    var viewedConspects;
    if (localStorage.viewedConspects !== undefined){
        viewedConspects = JSON.parse(localStorage.viewedConspects);
    } else {
        viewedConspects = {};
    }

    var data = new FormData;
    data.append("action", "new");
    toFormData(viewedConspects, data, "viewed_conspects");
    toFormData(globals.subscribed, data, "subscribes");
    var ajaxOptions = {
        body: data,
        callback: function(res){
            var result = JSON.parse(res);

            if (result.count > 0){
                newCountContainer.classList.remove("hidden");
                newCountContainer.innerHTML = result.count;
            } else {
                newCountContainer.classList.add("hidden");
                newCountContainer.innerHTML = result.count;
            }
        }
    };
    ajaxRequest(ajaxOptions);
}
function checkNotifications(){
    var notificationCountContainer = document.querySelector('.js-notifications_count');
    if (!notificationCountContainer){
        return false;
    }

    var data = new FormData;
    data.append("action", "notifications");
    var ajaxOptions = {
        body: data,
        callback: function(res){
            var result = JSON.parse(res);

            if (result.status === "success"){
                if (result.count > 0){
                    notificationCountContainer.classList.remove("hidden");
                    notificationCountContainer.innerHTML = result.count;
                } else {
                    notificationCountContainer.classList.add("hidden");
                    notificationCountContainer.innerHTML = result.count;
                }
            }
        }
    };

    ajaxRequest(ajaxOptions);
}

function shop_buy_lots(ids, trial){
    var ajaxOptions = {
        data: {
            action: "shop_buy_lots",
            ids: JSON.stringify(ids),
            trial: trial
        },
        callback: function(res){
            ajaxHandler(res);
            var result = JSON.parse(res);
            if (result.status === "success") {
                var basketContainer = document.querySelector(".js-header_basket");
                if (basketContainer) {
                    basketContainer.innerHTML = result.header_basket;
                }
            }
            document.querySelector("#cart-modal").innerHTML = result.popup;
            callPopup("cart-modal");
        }
    };
    ajaxRequest(ajaxOptions);
}

$("document").ready(function(){
    $("body#block-links").on("click", ".js-auth", function(e){
        var self = this;
        blockNonAuthorized(e, self)
    });

    // Input Mask
    body.find('.js-phone_mask').inputmask("+38 (999) 999 99 99");

    // TextFill in Question and Answer
    $('.new-card__text').textfill({
        minFontPixels: 8,
        maxFontPixels: 26,
        explicitHeight: 140
    });

    setTimeout(function(){
        hideLoader();
    }, 1500);
    initFormsValidation();
    checkSubs();
    initHeapbox();

    //Check new interval
    setInterval(function(){
        checkNew();
        checkNotifications();
    }, 30000);
    checkNew();
    checkNotifications();

    /* HANDLE CREATE CATEGORY LINK */
    body.on("click", "a", function(e){
        var href = this.href;
        if (href == globals.create_category) {
            e.preventDefault();
            callPopup("new-category-modal");
        }
    });

    body.on("change", ".js-answer_image-input", function(e){
        var input = $(this);
        var card = input.closest(".new-card");
        // handleFileSelect(e, function (result) {
        //     card.find(".js-answer_image").prop('src', result.target.result);
        // });
        card.find(".js-answer_image_add").addClass("hidden");
        card.find(".js-answer_image_remove").removeClass("hidden");
        card.removeClass("error");
    });
    body.on("change", ".js-question_image-input", function(e){
        var input = $(this);
        var card = input.closest(".new-card");
        // handleFileSelect(e, function (result) {
        //     card.find(".js-question_image").prop('src', result.target.result);
        // });
        card.find(".js-question_image_add").addClass("hidden");
        card.find(".js-question_image_remove").removeClass("hidden");
        card.removeClass("error");
    });
    body.on("click", ".js-answer-image-remove-button", function(e){
        e.preventDefault();
        var card = $(this).closest(".new-card");
        if (!card.hasClass("js-editable")){
            return false;
        }
        card.find(".js-answer_image_add").removeClass("hidden");
        card.find(".js-answer_image_remove").addClass("hidden");
        card.find(".js-answer_image-input").val("");
        card.find(".js-answer_image").prop("src", "");
    });
    body.on("click", ".js-question-image-remove-button", function(e){
        e.preventDefault();
        var card = $(this).closest(".new-card");
        if (!card.hasClass("js-editable")){
            return false;
        }
        card.find(".js-question_image_add").removeClass("hidden");
        card.find(".js-question_image_remove").addClass("hidden");
        card.find(".js-question_image-input").val("");
        card.find(".js-question_image").prop("src", "");
    });
    body.on("click", ".js-go_back", function(e){
        e.preventDefault();
        window.history.back();
    });
    body.on("click", ".js-share-fb", function(e){
        e.preventDefault();
        Share.facebook();
    });
    body.on("click", ".js-share-vk", function(e){
        e.preventDefault();
        Share.vkontakte();
    });
    body.on("click", ".js-share-gp", function(e){
        e.preventDefault();
        Share.google();
    });
    body.on("change", "#conspect_name", function(e){
        syncName(this);
    });
    body.on("keyup", "#conspect_name", function(e){
        syncName(this);
    });
    // body.on("change", "#conspect_image", function(e){
    //     handleFileSelect(e, function (result) {
    //         document.getElementById('conspect_img_src').src = result.target.result;
    //         document.getElementById('conspect_image-src').value = result.target.result;
    //     });
    // });
    body.on("click", "#delete_image", function(e){
        e.preventDefault();

        document.getElementById("conspect_image").value = "";
        document.getElementById("conspect_img_src").src = globals.default_image;
        document.getElementById("conspect_img-src").value = "";
    });
    body.on("click", "#card_add-button", function(e){
        e.preventDefault();

        if (body.find(".new-card").length > 50){
            this.disabled = true;
            toastr.warning("В конспект нельзя добавить более 50 карточек.");
            return false;
        }

        var defaultCard = document.getElementById("defaultCard");
        var defaultCardClone = defaultCard.cloneNode(true);
        var cardCloneAnswerImageFile = defaultCardClone.querySelector(".js-answer_image-input");
        var cardCloneAnswerImageFileLabel = defaultCardClone.querySelector(".js-answer_image-label");
        var cardCloneAnswerImage = defaultCardClone.querySelector(".js-answer_image");
        var cardCloneQuestionImageFile = defaultCardClone.querySelector(".js-question_image-input");
        var cardCloneQuestionImageFileLabel = defaultCardClone.querySelector(".js-question_image-label");
        var cardCloneQuestionImage = defaultCardClone.querySelector(".js-question_image");
        defaultCardClone.id = "";
        defaultCardClone.dataset.number = cardsCount;
        cardCloneAnswerImageFile.id = "answer_image-input-" + cardsCount;
        cardCloneAnswerImageFile.name = "answer_file-" + cardsCount;
        cardCloneAnswerImageFileLabel.id = "answer_image-label-" + cardsCount;
        cardCloneAnswerImageFileLabel.setAttribute("for", "answer_image-input-" + cardsCount);
        cardCloneAnswerImage.id = "answer_image-" + cardsCount;
        cardCloneQuestionImageFile.id = "question_image-input-" + cardsCount;
        cardCloneQuestionImageFile.name = "question_file-" + cardsCount;
        cardCloneQuestionImageFileLabel.id = "question_image-label-" + cardsCount;
        cardCloneQuestionImageFileLabel.setAttribute("for", "question_image-input-" + cardsCount);
        cardCloneQuestionImage.id = "question_image-" + cardsCount;

        cardCloneAnswerImageFile.addEventListener("change", function(e){
            handleFileSelect(e, function (result) {
                cardCloneAnswerImage.src = result.target.result;
            });
        });
        cardCloneQuestionImageFile.addEventListener("change", function(e){
            handleFileSelect(e, function (result) {
                cardCloneQuestionImage.src = result.target.result;
            });
        });

        var cardsContainer = document.getElementById("creationContent");
        cardsContainer.appendChild(defaultCardClone);
        var params;

        params = {
            input_file: document.querySelector("#question_image-input-" + cardsCount),
            input_label: document.querySelector("#question_image-label-" + cardsCount),
            image: document.querySelector("#question_image-" + cardsCount)
        };

        this["question" + cardsCount + "Cropper"] = new imageCropper(params);
        this["question" + cardsCount + "Cropper"].Init();

        params = {
            input_file: document.querySelector("#answer_image-input-" + cardsCount),
            input_label: document.querySelector("#answer_image-label-" + cardsCount),
            image: document.querySelector("#answer_image-" + cardsCount)
        };

        this["answer" + cardsCount + "Cropper"] = new imageCropper(params);
        this["answer" + cardsCount + "Cropper"].Init();

        cardsCount++;
    });
    body.on("click", ".js-delete_card", function(e){
        e.preventDefault();

        document.querySelector("#card_add-button").disabled = false;

        var target = $(this);
        var card = target.closest(".new-card");
        var card_id = card.data("id");
        var conspect_id = target.closest("#conspect_create_form").find("input[name='conspect_id']").val();
        var user_id = target.closest("#conspect_create_form").find("input[name='user_id']").val();

        if (card_id != undefined){
            var ajaxOptions = {
                data: {
                    card_id: card_id,
                    conspect_id: conspect_id,
                    user_id: user_id,
                    action: "delete_card"
                },
                callback: function(res){
                    ajaxHandler(res);
                    var result = JSON.parse(res);
                    if (result.status == "success") {
                        card.fadeOut(400);

                        setTimeout(function(){
                            card.remove();
                        }, 450);
                    }
                }
            };

            ajaxRequest(ajaxOptions);
        } else {
            card.fadeOut(400);

            setTimeout(function(){
                card.remove();
            }, 450);
        }
    });
    // body.on("change", "#personal_image_cropped", function(e){
    //     var image = document.querySelector(".js-cropper-image");
    //     sendFile(this, function (result) {
    //         if (result.status === true) {
    //             image.src = result.file;
    //             initCropper();
    //         }
    //     })
    // });
    body.on("click", ".js-sub", function(e){
        e.preventDefault();
        e.stopPropagation(true);
        var ajaxOptions = {
            data:{
                action: 'subscribe',
                user_id: this.dataset.user_id
            }
        };

        ajaxRequest(ajaxOptions);
    });
    body.on("click", ".js-conspect_image-changer", function(e){
        e.preventDefault();
        e.stopPropagation();

        body.find(".js-conspect_image-input").click();
    });
    body.on("click", ".js-set_study", function(e){
        if (this.dataset.conspect_id == undefined){
            toastr.error("no conspect id");
            return;
        }
        if (this.dataset.user_id == undefined){
            toastr.error("no user id");
            return;
        }

        var self = this;
        self.classList.add("js-loading");
        var ajaxOptions = {
            data: {
                action: "conspect_study",
                conspect_id: this.dataset.conspect_id,
                user_id: this.dataset.user_id
            },
            callback: function(res){
                var result = JSON.parse(res);
                var icon = self.querySelector(".icon");
                var conspect = $(self).closest(".js-conspect");
                icon.classList.toggle("icon-done");
                icon.classList.toggle("icon-studying");
                self.classList.remove("js-loading");
                var active_tab = document.querySelector(".js-my_conspects-tab.active");
                if (active_tab) {
                    var active_study = active_tab.dataset.study;

                    if (active_study !== undefined){
                        conspect.addClass("hidden");
                    }
                }
                if (conspect.attr("data-study") == 0){
                    self.title = "Конспект выучен";
                    conspect.attr("data-study", 1);
                } else {
                    self.title = "Конспект на изучении";
                    conspect.attr("data-study", 0);
                }

                ajaxHandler(res);
            }
        };

        ajaxRequest(ajaxOptions);
    });
    body.on("click", ".js-pagination_more", function(e){
        e.preventDefault();

        var self = this;

        var ajaxOptions = {
            url: this.href,
            data: {
                ajax: "Y"
            },
            callback: function(res){
                body.find(".js-pagination_delete").remove();
                var container = body.find(".js-pagination_container");

                container.append(res);

                checkSubs();

                self.dataset.disabled = 0;
            }
        };

        ajaxRequest(ajaxOptions);
    });
    body.on("keyup", ".js-answer_wrap.error .js-answer_text, .js-question_wrap.error .js-question_text", function(e){
        $(this).closest(".error").removeClass("error");
    });
    body.on("keypress change focus focusout", ".js-answer_text, .js-question_text", function(e){
        var self = this;
        var allowed = [8, 46, 37, 38, 39, 40];
        if (allowed.indexOf(e.keyCode) === -1){
            console.log(self.innerText.length);
            if (self.innerText.length >= 140){
                e.preventDefault();
                clearTimeout(conspectErrorColorTimeout);
                self.classList.add("js-conspect_error_color");
                conspectErrorColorTimeout = setTimeout(function(){
                    self.classList.remove("js-conspect_error_color");
                }, 200);
            }
            if (self.innerText.length > 140){
                self.innerText = self.innerText.slice(0, 140);
            }
        }
    });
    body.on("click", ".js-reload", function(e){
        e.preventDefault();
        location.reload();
    });
    body.on("click", ".js-save_conspect", function(e){
        var self = this;

        var ajaxOptions = {
            data: {
                action: "conspect_save",
                conspect_id: self.dataset.conspect_id,
                user_id: self.dataset.user_id,
                modal: "download-conspect-modal"
            }
        };

        ajaxRequest(ajaxOptions);
    });
    body.on("click", ".js-header_index_search", function(e){
        e.preventDefault();
        e.stopPropagation();
        $('body').find(".js-index_search").focus();
    });
    body.on("click", ".js-close_modals", function(e){
        e.preventDefault();
        closePopups();
    });
    body.on("click", ".js-shop_select", function(e){
        e.preventDefault();
        var ajaxOptions = {
            data: {
                action: "shop_buy-select",
                conspect_id: this.dataset.conspect_id,
            },
            callback: function(res){
                ajaxHandler(res);

                var result = JSON.parse(res);
                if (result.status === "success") {
                    document.querySelector("#cart-choice-modal").innerHTML = result.popup;
                    callPopup("cart-choice-modal");
                }
            }
        };
        ajaxRequest(ajaxOptions);
    });
    body.on("click", ".js-shop_buy", function(e){
        e.preventDefault();
        var ajaxOptions = {
            data: {
                action: "shop_buy",
                conspect_id: this.dataset.conspect_id,
                shop_id: this.dataset.shop_conspect
            },
            callback: function(res){
                ajaxHandler(res);

                var result = JSON.parse(res);
                if (result.status === "success") {
                    var basketContainer = document.querySelector(".js-header_basket");
                    if (basketContainer) {
                        basketContainer.innerHTML = result.header_basket;
                    }
                    document.querySelector("#cart-modal").innerHTML = result.popup;
                    callPopup("cart-modal");
                }
            }
        };
        ajaxRequest(ajaxOptions);
    });
    body.on("click", ".js-shop_delete", function(e){
        var self = this;
        e.preventDefault();
        var ajaxOptions = {
            data: {
                action: "shop_delete",
                id: this.dataset.id
            },
            callback: function(res){
                ajaxHandler(res);
                var result = JSON.parse(res);
                if (result.status === "success") {
                    body.find(".js-header_basket").html(result.header_basket);

                    $(self).closest(".js-conspect").fadeOut(400);
                    setTimeout(function(){
                        $(self).remove();
                    }, 450);
                }
                var ajaxOptions1 = {
                    url: window.location,
                    data: {
                        ajax: "Y"
                    },
                    callback: function(res){
                        document.querySelector(".js-basket_info").innerHTML = res;
                    }
                };
                ajaxRequest(ajaxOptions1);
            }
        };
        ajaxRequest(ajaxOptions);
    });
    body.on("focus", ".js-question_text, .js-answer_text", function(e){
        var self = $(this);
        $('html, body').animate({scrollTop:self.offset().top - (window.innerHeight/2) + (self.closest(".new-card").innerHeight() / 2)}, 200);
    });
    body.on("change", ".js-add_to_library", function(e){
         var self = this;
         var image_id = document.querySelector("#js-conspect_image");
         if (self.checked){
             if (image_id.value <= 0){
                 var params = {
                     show_preview: true,
                     cropCallback: function(result){
                         console.log(result);
                         var ajaxOptions = {
                             data:{
                                 action: "edit_conspect",
                                 conspect_id: document.querySelector(".js-conspect_id").value,
                                 skip: "Y",
                                 conspect_image: result.image.id
                             },
                             callback: function(res){
                                 var callbackResult = JSON.parse(res);
                                 if (callbackResult.status === "success") {
                                     ajaxHandler(res);
                                     closeModals();
                                     self.checked = true;
                                     image_id.value = callbackResult.conspect.UF_IMAGE;
                                 }
                             }
                         };
                         ajaxRequest(ajaxOptions);
                     }
                 };
                 var conspectImageCrop = new imageCropper(params);
                 conspectImageCrop.Init();
                 conspectImageCrop.ShowModal();
                 self.checked = false;
             } else {
                 toastr.success("Конспект добавлен в общую библиотеку.");
             }
         } else {
             toastr.warning("Конспект удален из общей библиотеки.");
         }
    });
    body.on("click", ".js-delete_notifications", function(e){
        var self = this;
        e.preventDefault();
        var ajaxOptions = {
            data: {
                action: "notifications_done"
            },
            callback: function(res){
                ajaxHandler(res);
                var result = JSON.parse(res);

            }
        };
        ajaxRequest(ajaxOptions);
    });
    body.on("click", ".js-publish", function(e){
        e.preventDefault();

        if (this.dataset.modal !== undefined && this.dataset.conspect !== undefined){
            var conspectInput = document.querySelector("#" +
                this.dataset.modal +
                " input[name='conspect_id']");

            if (conspectInput !== undefined){
                conspectInput.value = this.dataset.conspect;
            }
        }
    });
    body.on("click", ".js-final_publish", function(e){
        var self = this;

        var ajaxOptions = {
            data: {
                action: "conspect_publish_popup",
                conspect_id: self.dataset.conspect,
                modal: "author-final-publish"
            }
        };

        ajaxRequest(ajaxOptions);
    });
    body.on("change", ".js-publish_action", function(e){
        if (this.value === "return"){
            body.find(".js-comment").removeClass("hidden");
        } else {
            body.find(".js-comment").addClass("hidden");
        }
    });
    body.on("click", ".js-close_modal", function(e){
        $(this).closest(".modal.active").fadeOut();
        body.find(".modal-bg").fadeOut();
        body.removeClass('open-modal-js');
    });
    body.on("click", ".js-aside_collapse", function(e){
        e.preventDefault();
        this.classList.toggle("to-small");
        var view = getCookie("cards_view");
        if (view === undefined){
            view = "big";
        }
        view = (view === "big") ? "small" : "big";
        console.log(view);
        setCookie("cards_view", view, {path:"/"});
        if (view === "small"){
            document.body.classList.remove("hamburger");
            document.body.classList.add("tiny");
        } else {
            document.body.classList.remove("hamburger");
            document.body.classList.remove("tiny");
        }
    });
    body.on("click", ".js-card_study", function(e){
         e.preventDefault();

        if (this.dataset.id === undefined){
            toastr.error("no conspect id");
            return;
        }

        var self = this;
        self.classList.add("js-loading");
        var jCard = $(self).closest(".js-card");
        if (jCard){
            var card = jCard[0];
        }

        var ajaxOptions = {
            data: {
                action: "card_study",
                card_id: this.dataset.id
            },
            callback: function(res){
                ajaxHandler(res);

                var result = JSON.parse(res);
                var icon = self.querySelector(".icon");
                icon.classList.toggle("icon-done");
                icon.classList.toggle("icon-studying");
                self.classList.remove("js-loading");
                if (self.dataset.study == 0){
                    self.title = "Вопрос выучен";
                    self.dataset.study = 1;
                    if (card) {
                        card.dataset.study = 1;
                    }

                } else {
                    self.title = "Вопрос на изучении";
                    self.dataset.study = 0;
                    if (card) {
                        card.dataset.study = 0;
                    }
                }
            }
        };

        ajaxRequest(ajaxOptions);
    });


    // CREATE NEW Q/A ON TAB CLICK
    $(window).keyup(function (e) {
        var code = (e.keyCode ? e.keyCode : e.which);
        if (code === 9 && $('.js-card_add-button:focus').length) {
            $('.js-card_add-button:focus').click();
            var cards = body.find(".js-all_cards .new-card");
            var lastCard = cards[cards.length - 1];
            $(lastCard).find(".js-question_text").focus();
        }
    });

    var tagsList = $('body').find('.conspect-tags__list');
    var tagsListHeight = tagsList.height();

    if (tagsListHeight) {
        if (!(tagsListHeight >= 140)) {
            $('body').find('.conspect-tags__list-more').hide();
        } else {
            tagsList.css('height', '140px');

            $('body').on('click', '.conspect-tags__list-more', function(e) {
                e.preventDefault();

                $(this).prev('.conspect-tags__list').toggleClass('js-opened');

                if (tagsList.hasClass('js-opened')) {
                    $(this).text('Показать меньше');
                } else {
                    $(this).text('Показать больше');
                }
            });
        }
    }

    body.find('.conspect-card__body').magnificPopup({
        delegate: 'a',
        type: 'image',
        closeOnContentClick: false,
        closeBtnInside: false,
        mainClass: 'mfp-with-zoom mfp-img-mobile',
        image: {
            verticalFit: true
        },
        gallery: {
            enabled: false
        }
    });
    body.find('.flipper__img-box').magnificPopup({
        delegate: 'a',
        type: 'image',
        closeOnContentClick: false,
        closeBtnInside: false,
        mainClass: 'mfp-with-zoom mfp-img-mobile',
        image: {
            verticalFit: true
        },
        gallery: {
            enabled: false
        }
    });
});

/* IMAGE CROPPER */
var cropper_count = 0;
var imageCropper = function(params){
    if ((!params.image || !params.input_file || !params.input_label) && (params.cropCallback !== undefined && typeof params.cropCallback !== "function")){
        console.error("Wrong imageCropper params");
        return false;
    }
    this.params = params;
    this.input_file = params.input_file;
    this.input_label = params.input_label;
    this.image = params.image;
    this.show_preview = params.show_preview;
    this.modal = document.querySelector("#change-image-modal");
    this.popupParams = {
        modal_id: "change-image-modal",
        file: this.modal.querySelector("#image_cropped"),
        image: this.modal.querySelector(".js-cropper-image")
    };
    this.cropperParams = {
        image_id: "cropper-image",
        reset: document.querySelector("#cropper-reset"),
        apply: document.querySelector("#cropper-apply")
    };
    this.cropper_image = $("body").find("#" + this.cropperParams.image_id);
    this.cropper_number = cropper_count;
    cropper_count++;
};
imageCropper.prototype.Init = function(){
    this.AddHandlers();
};
imageCropper.prototype.InitCrop = function(){
    this.cropper_image.cropper({
        minContainerWidth: 300,
        minContainerHeight: 300,
        aspectRatio: 1 / 1,
        preview: "#cropper-preview",
        movable: false,
        zoomOnWheel: false,
        zoomOnTouch: false,
        scalable: false,
        viewMode: 1,
        zoomable: false,
        toggleDragModeOnDblclick: false,
        dragMode: 'move',
        crop: function(e) {
            // Output the result data for cropping image.
        }
    });
    if (this.show_preview === true){
        document.querySelector("#cropper-preview").style.display = "block";
    }
};
imageCropper.prototype.Check = function(){
    var self = this;
    return (self.modal.dataset.cropper_number && parseInt(self.modal.dataset.cropper_number) === parseInt(self.cropper_number));
};
imageCropper.prototype.ShowModal = function(){
    var self = this;
    self.Refresh();
    self.modal.dataset.cropper_number = self.cropper_number;
    callPopup(self.popupParams.modal_id);
};
imageCropper.prototype.AddHandlers = function(){
    var self = this;

    if (self.image) {
        self.input_labels = document.querySelectorAll("label[for='" + self.input_file.id + "']");

        for (var label, i = 0, n = self.input_labels.length; i < n; i++) {
            if (self.input_labels.hasOwnProperty(i)) {
                label = self.input_labels[i];
                label.addEventListener("click", function (e) {
                    e.preventDefault();
                    self.ShowModal();
                });
            }
        }

        // self.input_label.addEventListener("click", function(e){
        //     e.preventDefault();
        //     self.Refresh();
        //     callPopup(self.popupParams.modal_id);
        //     self.modal.dataset.cropper_number = self.cropper_number;
        // });
    }
    self.popupParams.file.addEventListener("change", function(e){
        if (!self.Check()){
            return false;
        }
        sendFile(this, function (result) {
            if (result.status === true) {
                self.popupParams.image.src = result.file;
                self.InitCrop();
                self.modal.querySelector("#cropper-reset").classList.remove("hidden");
            }
        })
    });
    self.cropperParams.reset.addEventListener('click', function(e){
        if (!self.Check()){
            return false;
        }
        e.preventDefault();
        self.cropper_image.cropper('crop');
    });
    self.cropperParams.apply.addEventListener('click', function(e){
        if (!self.Check()){
            return false;
        }
        e.preventDefault();
        self.Crop();
    });
};
imageCropper.prototype.Refresh = function(){
    this.modal.querySelector("#cropper-reset").classList.add("hidden");
    this.modal.querySelector(".progress_bar").classList.add("hidden");
    this.cropper_image.cropper('clear');
    this.cropper_image.cropper('destroy');
    this.cropper_image.attr("src", '');
    this.popupParams.file.value = "";
};
imageCropper.prototype.Crop = function(){
    var self = this;
    var cropped_data = self.cropper_image.cropper('getData', 'rounded');
    var data = new FormData();

    data.append("action", "crop_image");
    data.append("image", self.popupParams.image.src);
    toFormData(cropped_data, data, "crop_data");

    var ajaxOptions = {
        body: data,
        callback: function(res){
            ajaxHandler(res);

            var result = JSON.parse(res);
            if (result.status === "success") {
                if (self.image !== undefined) {
                    if (self.image.tagName === "IMG") {
                        self.image.src = result.image.image;
                    } else {
                        self.image.style.backgroundImage = 'url("' + result.image.image + '")';
                    }
                    closeModals();
                    self.input_file.value = result.image.image;
                    $(self.input_file).change();

                    if (self.params.personal === true) {
                        $("body").find("#personal_form").submit();
                    }
                }
            }

            if (self.params.cropCallback !== undefined && typeof self.params.cropCallback === "function") {
                self.params.cropCallback(result);
            }
        }
    };

    ajaxRequest(ajaxOptions);
};

/* MAKE MATERIAL CHIPS BETTER */
var Chips = function(target, params){
    Chips.counter++;
    var self = this;
    self.main = $(target);
    self.main.material_chip(params);
    self.inputs = {};

    self.Init(params);
    if (params.data !== undefined && params.data.length > 0) {
        self.LoadData(params.data);
    }
};
Chips.counter = 0;
Chips.prototype.CreateInputsHolder = function(){
    var self = this;

    var inputsWrap = document.createElement("div");
    inputsWrap.id = "chips-input_holder-" + Chips.counter;
    inputsWrap.classList.add("hidden");

    self.main.parent().append(inputsWrap);

    self.inputsWrap = inputsWrap;
};
Chips.prototype.Init = function(params){
    var self = this;

    if (params.placeholder) {
        self.main.find("input").attr("placeholder", params.placeholder);
    }

    self.CreateInputsHolder();

    self.main.on("chip.add", function(e, chip){
        self.AddChip(chip.tag);
    });
    self.main.on("chip.delete", function(e, chip){
        self.DeleteChip(chip.tag);
    });
};
Chips.prototype.LoadData = function(data){
    var self = this;

    data.forEach(function(item){
        self.AddChip(item.tag);
    })
};
Chips.prototype.DeleteChip = function(tag){
    var self = this;

    if (self.inputs[tag] !== undefined) {
        self.inputsWrap.removeChild(self.inputs[tag]);
    }
};
Chips.prototype.AddChip = function(tag){
    var self = this;

    var newChipInput = document.createElement("input");
    newChipInput.type = "hidden";
    newChipInput.name = "tags[]";
    newChipInput.value = tag;

    self.inputs[tag] = newChipInput;

    self.inputsWrap.append(newChipInput);
};
Chips.prototype.GetData = function(){
    return this.main.material_chip('data');
};