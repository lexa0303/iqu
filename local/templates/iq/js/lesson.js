//***LESSON START!***

;
var body = $("body");
var lessonSliderObject = body.find('.lesson-slider');

function initSlider() {
    var item;

    //Init Slick carousel into .lesson-slider
    lessonSliderObject.slick({
        arrows: true,
        dots: false,
        infinite: false,
        draggable: false,
        touchMove: false,
        easing: 'ease-in',
        prevArrow: '<button type="button" class="slick-prev"><span class="lesson-slider__prev-img"></span><span class="lesson-prev-txt">Предыдущий вопрос</span></button>',
        nextArrow: '<button type="button" class="slick-next"><span class="lesson-next-txt">Следующий вопрос</span><span class="lesson-slider__next-img"></span></button>',
        appendArrows: '.flipper__lower'
    });

        var lessonSlider = document.querySelector('.lesson-slider');
        var $lessonSlider = $('body').find('.lesson-slider');
        var lessonSlidesAmount = document.getElementsByClassName('flip-container').length;
        var progressCurrentOutput = document.getElementsByClassName('lesson-tooltip-current');
        var progressTotalOutput = document.getElementsByClassName('lesson-tooltip-total');
        var lessonProgressBar = document.getElementsByClassName('lesson-progress-bar');

        //Set number of total fcards in lesson
        for (item in progressTotalOutput) {
            if (progressTotalOutput.hasOwnProperty(item)) {
                progressTotalOutput[item].innerHTML = lessonSlidesAmount;
            }
        };


        //Set width of lesson progressbar
        for (item in lessonProgressBar) {
            if (lessonProgressBar.hasOwnProperty(item)) {
                lessonProgressBar[item].style.width = ((1 / lessonSlidesAmount) * 100) + '%';
            }
        };

    // spacebar flip function
    document.body.onkeyup = function (e) {
        var focus = document.querySelector(":focus");

        if (
            focus !== null &&
            (
                focus.classList.contains("search-block__input") ||
                focus.classList.contains("bug-notification__textarea") ||
                focus.classList.contains("modal__input")
            )

        ){
            return false;
        }

        console.log(e.keyCode);

        //Space
        if (e.keyCode == 32 || e.keyCode == 87 || e.keyCode == 38 || e.keyCode == 83 || e.keyCode == 40) {
            e.preventDefault();
            var lessonSlide = document.querySelector('.slick-active.flip-container');
            lessonSlide.classList.toggle('flip');

        } else if (e.keyCode == 37 || e.keyCode == 65) {
            //Left
            e.preventDefault();
            body.find('.lesson-slider').slick('slickPrev');
        } else if (e.keyCode == 39 || e.keyCode == 68) {
            //Right
            e.preventDefault();
            body.find('.lesson-slider').slick('slickNext');
        }
    };
    // prevent scrolling down on the lesson page by pressing space
    // document.body.onkeydown = function (e) {
        // if (e.keyCode == 32) {
        //     e.preventDefault();
        //
        //     var lessonSlide = document.querySelector('.slick-active.flip-container');
            // lessonSlide.focus();
        // }
    // };

    //Flipper behavior for lesson cards
    body.find('.flip-slide').on('click', function (e) {
        e.preventDefault();
        $(this).closest('.flip-container').toggleClass('flip');
    });

    body.find('.lesson-slider .slick-prev').on('click', function (e) {
        e.preventDefault();
        $lessonSlider.slick('slickPrev');
    });

    body.find('.lesson-slider .slick-next').on('click', function (e) {
        e.preventDefault();
        $lessonSlider.slick('slickNext');
    });

        //Lesson progressbar behavior
        //Lesson progress tooltip behavior
    $lessonSlider.on('beforeChange', function(event, slick, currentSlide, nextSlide) {
        var item;
        for (item in progressCurrentOutput) {
            if (progressCurrentOutput.hasOwnProperty(item)) {
                progressCurrentOutput[item].innerHTML = (nextSlide + 1);
            }
        };

        var percentProgress = ((nextSlide + 1) / lessonSlidesAmount) * 100;
        for (item in lessonProgressBar) {
            if (lessonProgressBar.hasOwnProperty(item)) {
                lessonProgressBar[item].style.width = percentProgress + '%';
            }
        };
    });
}

$(document).ready(function() {
    if (document.querySelector('.lesson-slider')) {
        initSlider();
    }

    body.on("click", ".js-delete_card-lesson", function(e){
        e.preventDefault();

        var target = $(this);
        var card = target.closest(".js-lesson_card");
        var card_id = card.data("id");
        var conspect_id = card.data("conspect_id");
        var user_id = card.data("user_id");

        if (card_id != undefined){
            var ajaxOptions = {
                data: {
                    card_id: card_id,
                    conspect_id: conspect_id,
                    user_id: user_id,
                    action: "delete_card"
                },
                callback: function(res){
                    ajaxHandler(res);
                    var result = JSON.parse(res);
                    var index;
                    if (result.status == "success") {
                        card.fadeOut(400);

                        setTimeout(function(){
                            index = card.data('slickIndex');
                            lessonSliderObject.slick("slickRemove", index);// card.remove();
                        }, 420);

                        setTimeout(function(){
                            lessonSliderObject.slick("unslick");
                            initSlider();
                            lessonSliderObject.slick("slickGoTo", index);
                        }, 450);
                    }
                }
            };

            ajaxRequest(ajaxOptions);
        }
    });

    // TextFill in Question and Answer
    $('.flipper__txt-box').textfill({
        minFontPixels: 8,
        maxFontPixels: 24,
        explicitHeight: 150
    });

});


//***LESSOND END!!!***
