<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<? use Bitrix\Main\Page\Asset; ?>
<? use Bitrix\Main\Localization\Loc; ?>

<footer class="footer">
    <div class="container">
        <?$APPLICATION->IncludeComponent(
            "bitrix:main.include",
            "",
            Array(
                "AREA_FILE_SHOW" => "file",
                "AREA_FILE_SUFFIX" => "inc",
                "EDIT_TEMPLATE" => "",
                "PATH" => "/inc/footer_socials.php"
            )
        );?>
    </div>
</footer>

<?if ($USER->IsAuthorized()) : ?>
    <a href="javascript:void(0);" class="bug-basket__link-wrap open-modal" data-modal="bug-notification">
        <span class="bug-basket__link">!</span>
    </a>
    <a href="<?=SITE_DIR;?>conspects/Help/help/" class="bug-basket__link-wrap bug-low">
        <span class="bug-basket__link">?</span>
    </a>
<?endif;?>

<!--<a href="javascript:void(0);" class="js-scrollTop" title="Scroll to the top">-->
<!--    <span class="icon icon-arrow-Scrolltop"></span>-->
<!--</a>-->

<div class="modal-bg"></div>

<div class="cart-modal modal" id="cart-modal"></div>
<div class="cart-choice-modal modal" id="cart-choice-modal"></div>

<div class="modal" id="author-publish">
    <a class="modal__btn-close icon icon-cross" href="javascript:void(0)"></a>
    <form class="new-category-modal__body ajax_submit">
        <input type="hidden" name="conspect_id">
        <?=bitrix_sessid_post();?>
        <input type="hidden" name="user_id" value="<?=$arUser["ID"];?>">
        <input type="hidden" name="action" value="conspect_publish">
        <span class="new-category-modal__heading"><?=Loc::getMessage("CONSPECT_PUBLISH_HEADING");?></span>
        <div class="modal__fieldset">
            <label class="modal__label" for="conspect_publish_price">Укажите желаемую цену вашего конспекта</label>
            <input class="modal__input" type="text" name="price" id="conspect_publish_price">
        </div>
        <div class="modal__fieldset conspect_publish__textarea-wrap">
            <label class="modal__label" for="conspect_publish_text">Комментарий</label>
            <textarea required
                      class="modal__textarea conspect_publish__textarea"
                      name="text"
                      id="conspect_publish_text"
                      type="text"></textarea>
        </div>
        <button class="button button--rounded new-category-modal__btn" href="javascript:void(0)"><?=Loc::getMessage("SEND");?></button>
        <a class="new-category-modal__btn--txt js-close_modals" href="javascript:void(0)"><?=Loc::getMessage("CANCEL");?></a>
    </form>
</div>
<div class="modal" id="author-final-publish"></div>
<div class="modal" id="bug-notification">
    <a class="modal__btn-close icon icon-cross" href="javascript:void(0)"></a>
    <form class="new-category-modal__body ajax_submit">
        <input type="hidden" name="user_id" value="<?=$arUser["ID"];?>">
        <input type="hidden" name="action" value="bug_notification">
        <span class="new-category-modal__heading"><?=Loc::getMessage("BUG_NOTIFICATION_HEADING");?></span>
        <span class="new-category-modal__text"><?=Loc::getMessage("BUG_NOTIFICATION_TEXT");?></span>
        <div class="modal__fieldset">
<!--            <label class="modal__label" for="category_add_name">--><?//=Loc::getMessage("INPUT_HEADING_CATEGORY_NAME");?><!--</label>-->
            <textarea required
                      class="modal__textarea bug-notification__textarea"
                      name="text"
                      id="category_add_name"
                      type="text"></textarea>
        </div>
        <button class="button button--rounded new-category-modal__btn" href="javascript:void(0)"><?=Loc::getMessage("SEND");?></button>
        <a class="new-category-modal__btn--txt js-close_modals" href="javascript:void(0)"><?=Loc::getMessage("CANCEL");?></a>
    </form>
</div>
<div class="modal" id="delete-conspect-radio-modal">
    <a class="modal__btn-close icon icon-cross" href="javascript:void(0)"></a>
    <form class="delete-conspect-radio__body" action=""><span class="delete-conspect-radio__heading">Удаление конспекта</span><span class="delete-conspect-radio__text">Вы можете удалить конспект из своего профиля и библиотеки.</span>
        <div class="delete-conspect-radio__kinds">
            <div class="delete-conspect-radio__kind-wrapper">
                <input class="delete-conspect-radio__kind-input" type="radio" id="fromprofile" checked name="deleteConspectRadio">
                <label class="delete-conspect-radio__kind-label" for="fromprofile">Удалить из своего профиля</label>
            </div>
            <div class="delete-conspect-radio__kind-wrapper">
                <input class="delete-conspect-radio__kind-input" type="radio" id="fromLibrary" checked name="deleteConspectRadio">
                <label class="delete-conspect-radio__kind-label" for="fromLibrary">Удалить из библиотеки </label>
            </div>
            <div class="delete-conspect-radio__kind-wrapper">
                <input class="delete-conspect-radio__kind-input" type="radio" id="fromBoth" checked name="deleteConspectRadio">
                <label class="delete-conspect-radio__kind-label" for="fromBoth">Удалить из своего профиля и библиотеки</label>
            </div>
        </div>
        <input class="delete-conspect-radio__submit button" type="submit" value="Создать напоминание">
        <a class="delete-conspect-radio__cancel button" href="javascript:void(0);">Отмена</a>
    </form>
</div>
<div class="modal" id="change-image-modal">
    <a class="modal__btn-close icon icon-cross" href="javascript:void(0)"></a>
    <form class="new-pass-modal__body ajax_submit">
        <span class="new-pass-modal__heading js-image_crop-heading">Красивый конспект приятно учить!</span>
        <span class="new-modal__subheading js-image_crop-description">Выбери иллюстрацию, которая точно передаёт содержание и легко запоминается</span>
        <!-- Cropper start -->
        <div id="cropper-wrap" class="change-image-wrap">
            <img class="js-cropper-image" id="cropper-image" src="" alt="">
        </div>
        <div id="cropper-preview"></div>
        <div id="cropper-cropped"></div>
        <button id="cropper-reset" title="Сбросить">
            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" viewBox="0 0 44 44" enable-background="new 0 0 44 44" width="24px" height="24px">
                <path d="m22,0c-12.2,0-22,9.8-22,22s9.8,22 22,22 22-9.8 22-22-9.8-22-22-22zm3.3,33.5c-0.6,0.2-1.3-0.3-1.3-1v-2.1c0-0.4 0.2-0.8 0.6-0.9 3.1-1.1 5.4-4.1 5.4-7.5 0-4.4-3.6-8-8-8s-8,3.6-8,8c0,1.4 0.4,2.8 1.1,4 0.1,0.2 0.4,0.3 0.7,0.2 0.2-0.1 0.2-0.3 0.2-0.4v-0.8c0-0.6 0.4-1 1-1h2c0.6,0 1,0.4 1,1v8c0,0.6-0.4,1-1,1h-8c-0.6,0-1-0.4-1-1v-2c0-0.6 0.4-1 1-1h1c0.3,0 0.5-0.2 0.5-0.5 0-0.1 0-0.2-0.1-0.3-1.6-2-2.4-4.5-2.4-7.2 0-6.6 5.4-12 12-12s12,5.4 12,12c0,5.5-3.7,10.1-8.7,11.5z"/>
            </svg>
        </button>
        <label class="change-img-input-label">
            <input type="file"
                   accept="image/jpeg, image/png, image/jpg, image/gif"
                   name="image_cropped"
                   class="change-img-input"
                   id="image_cropped">
            <span class="change-img-input-span">Загрузить фото</span>
        </label>
        <!--        <button id="cropper-crop">Crop</button>-->
        <!-- Cropper end -->

        <button id="cropper-apply" class="button button--rounded new-pass-modal__btn" href="javascript:void(0)"><?=Loc::getMessage("SAVE");?></button>
        <a class="new-pass-modal__btn--txt" href="javascript:void(0)"><?=Loc::getMessage("CANCEL");?></a>
        <div class="progress_bar hidden"></div>
    </form>
</div>
<div class="modal" id="blocked-alerts-modal">
    <a class="modal__btn-close icon icon-cross" href="javascript:void(0)"></a>
    <div class="blocked-alerts-modal__body">
        <div class="blocked-alerts-modal__heading">Разрешить оповещения в браузере</div>
        <div class="blocked-alerts-modal__advices">
            <ul class="blocked-alerts-modal__advices-list">
                <li class="blocked-alerts-modal__advices-item">
                    <img src="<?=SITE_TEMPLATE_PATH;?>/images/blocked-1.png" alt="">
                </li>
                <li class="blocked-alerts-modal__advices-item">
                    <img src="<?=SITE_TEMPLATE_PATH;?>/images/blocked-2.png" alt="">
                </li>
                <li class="blocked-alerts-modal__advices-item">
                    <img src="<?=SITE_TEMPLATE_PATH;?>/images/blocked-3.png" alt="">
                </li>
            </ul>
        </div>
    </div>
</div>
<div class="modal" id="auth-modal">
    <a class="modal__btn-close icon icon-cross" href="javascript:void(0)"></a>
    <form class="auth-modal__body ajax_submit">
        <input type="hidden" name="action" value="authorize">
        <span class="auth-modal__heading"><?=Loc::getMessage("MODAL_AUTH_HEADING");?></span>
        <div class="auth-modal__content">
            <div class="auth-modal__content--left">
                <div class="modal__fieldset">
                    <label class="modal__label" for="auth_email"><?=Loc::getMessage("INPUT_HEADING_EMAIL");?>
                        <input required name="email" id="auth_email" class="modal__input" type="email" placeholder="<?=Loc::getMessage("PLACEHOLDER_EMAIL");?>">
                    </label>
                </div>
                <div class="modal__fieldset">
                    <label class="modal__label" for="auth_password"><?=Loc::getMessage("INPUT_HEADING_PASSWORD");?>
                        <input required name="password" id="auth_password" class="modal__input" type="password" placeholder="<?=Loc::getMessage("PLACEHOLDER_PASSWORD");?>">
                    </label>
                </div>
                <button class="button button--rounded auth-modal__btn-login"><?=Loc::getMessage("MODAL_AUTH_BUTTON");?></button>
                <a class="auth-modal__forgot-pass open-modal" href="javascript:void(0);" data-modal="reset-pass-modal">
                    <?=Loc::getMessage("MODAL_FORGOT_PASSWORD_BUTTON");?></a>
                <a class="auth-modal__reg open-modal" href="javascript:void(0);" data-modal="reg-modal">
                    <?=Loc::getMessage("MODAL_TO_REG_BUTTON");?></a>
            </div>
            <div class="auth-modal__content--right">
                <? $APPLICATION->IncludeComponent(
                    "bitrix:system.auth.form",
                    "",
                    Array(
                        "REGISTER_URL" => "",
                        "FORGOT_PASSWORD_URL" => "",
                        "PROFILE_URL" => REGION_DIR . "personal/",
                        "SHOW_ERRORS" => "Y"
                    )
                ); ?>
            </div>
        </div>
    </form>
</div>
<div class="modal" id="reg-modal">
    <a class="modal__btn-close icon icon-cross" href="javascript:void(0)"></a>
    <form class="reg-modal__body ajax_submit">
        <span class="reg-modal__heading"><?=Loc::getMessage("MODAL_REG_HEADING");?></span>
        <input type="hidden" name="action" value="register">
        <div class="reg-modal__content">
            <div class="reg-modal__content--left">
                <div class="modal__fieldset">
                    <label class="modal__label" for="reg_email"><?=Loc::getMessage("INPUT_HEADING_EMAIL");?>
                        <input required class="modal__input" name="email" id="reg_email" type="email" placeholder="<?=Loc::getMessage("PLACEHOLDER_EMAIL");?>">
                    </label>
                </div>
                <div class="modal__fieldset">
                    <label class="modal__label" for=""><?=Loc::getMessage("INPUT_HEADING_PASSWORD");?>
                        <input required class="modal__input" name="password" id="reg_password" type="password" placeholder="<?=Loc::getMessage("PLACEHOLDER_PASSWORD");?>">
                    </label>
                </div>
                <div class="modal__fieldset">
                    <label class="modal__label" for="reg_password_repeat"><?=Loc::getMessage("INPUT_HEADING_PASSWORD_REPEAT");?>
                        <input required class="modal__input" name="password_repeat" id="reg_password_repeat" type="password" placeholder="<?=Loc::getMessage("PLACEHOLDER_PASSWORD");?>">
                    </label>
                </div>
                <div class="modal__fieldset">
                    <label class="modal__label" for="reg_title"><?=Loc::getMessage("INPUT_HEADING_TITLE");?>
                        <input required class="modal__input" name="title" id="reg_title" type="text" placeholder="<?=Loc::getMessage("PLACEHOLDER_TITLE");?>">
                    </label>
                </div>
                <button type="submit" class="button button--rounded reg-modal__btn-login" href="javascript:void(0)"><?=Loc::getMessage("MODAL_REG_BUTTON");?></button>

            </div>
            <div class="reg-modal__content--right">
                <? $APPLICATION->IncludeComponent(
                    "bitrix:system.auth.form",
                    "",
                    Array(
                        "REGISTER_URL" => "",
                        "FORGOT_PASSWORD_URL" => "",
                        "PROFILE_URL" => REGION_DIR . "personal/",
                        "SHOW_ERRORS" => "Y"
                    )
                ); ?>
                <div class="reg-modal__auth-box">
                    <span class="reg-modal__auth-misc">Есть аккаунт?</span>
                    <a class="button button--rounded reg-modal__auth open-modal" href="javascript:void(0);" data-modal="auth-modal"><?=Loc::getMessage("MODAL_TO_AUTH_BUTTON");?></a>
                </div>

            </div>
        </div>
    </form>
</div>
<div class="modal" id="thanks-modal">
    <a class="modal__btn-close icon icon-cross" href="javascript:void(0)"></a>
    <form class="thanks-modal__body" action="">
        <span class="thanks-modal__heading"><?=Loc::getMessage("MODAL_THANKS_HEADING");?></span>
        <span class="thanks-modal__text"><?=Loc::getMessage("MODAL_THANKS_TEXT");?></span>
        <img class="thanks-modal__img" src="<?=SITE_TEMPLATE_PATH;?>/images/letter-img.png" alt="">
        <a class="button button--rounded thanks-modal__btn" href="javascript:void(0)"><?=Loc::getMessage("CLOSE");?></a>
    </form>
</div>
<div class="modal" id="edit-category-modal"></div>
<div class="modal" id="new-category-modal">
    <a class="modal__btn-close icon icon-cross" href="javascript:void(0)"></a>
    <form class="new-category-modal__body ajax_submit">
        <input type="hidden" name="user_id" value="<?=$arUser["ID"];?>">
        <input type="hidden" name="action" value="category_add">
        <span class="new-category-modal__heading"><?=Loc::getMessage("MODAL_ADD_CATEGORY_HEADING");?></span>
        <span class="new-category-modal__text"><?=Loc::getMessage("MODAL_ADD_CATEGORY_TEXT");?></span>
        <div class="modal__fieldset">
            <label class="modal__label" for="category_add_name"><?=Loc::getMessage("INPUT_HEADING_CATEGORY_NAME");?></label>
            <input required class="modal__input" name="category_name" id="category_add_name" type="text" placeholder="<?=Loc::getMessage("PLACEHOLDER_CATEGORY_NAME");?>">
        </div>
        <button class="button button--rounded new-category-modal__btn" href="javascript:void(0)"><?=Loc::getMessage("SAVE");?></button>
        <a class="new-category-modal__btn--txt" href="javascript:void(0)"><?=Loc::getMessage("CANCEL");?></a>
    </form>
</div>
<div class="modal" id="conspect_add-new-category-modal">
    <a class="modal__btn-close icon icon-cross" href="javascript:void(0)"></a>
    <form class="new-category-modal__body ajax_submit">
        <input type="hidden" name="user_id" value="<?=$arUser["ID"];?>">
        <input type="hidden" name="action" value="category_add">
        <input type="hidden" name="conspect" value="Y">
        <span class="new-category-modal__heading"><?=Loc::getMessage("MODAL_ADD_CATEGORY_HEADING");?></span>
        <span class="new-category-modal__text"><?=Loc::getMessage("MODAL_ADD_CATEGORY_TEXT");?></span>
        <div class="modal__fieldset">
            <label class="modal__label" for="category_add_name"><?=Loc::getMessage("INPUT_HEADING_CATEGORY_NAME");?></label>
            <input required class="modal__input" name="category_name" id="category_add_name" type="text" placeholder="<?=Loc::getMessage("PLACEHOLDER_CATEGORY_NAME");?>">
        </div>
        <button class="button button--rounded new-category-modal__btn" href="javascript:void(0)"><?=Loc::getMessage("SAVE");?></button>
        <a class="new-category-modal__btn--txt" href="javascript:void(0)"><?=Loc::getMessage("CANCEL");?></a>
    </form>
</div>
<div class="modal" id="conspect_download-new-category-modal">
    <a class="modal__btn-close icon icon-cross" href="javascript:void(0)"></a>
    <form class="new-category-modal__body ajax_submit">
        <input type="hidden" name="user_id" value="<?=$arUser["ID"];?>">
        <input type="hidden" name="action" value="category_add">
        <input type="hidden" name="conspect_download" value="Y">
        <span class="new-category-modal__heading"><?=Loc::getMessage("MODAL_ADD_CATEGORY_HEADING");?></span>
        <span class="new-category-modal__text"><?=Loc::getMessage("MODAL_ADD_CATEGORY_TEXT");?></span>
        <div class="modal__fieldset">
            <label class="modal__label" for="category_add_name"><?=Loc::getMessage("INPUT_HEADING_CATEGORY_NAME");?></label>
            <input required class="modal__input" name="category_name" id="category_add_name" type="text" placeholder="<?=Loc::getMessage("PLACEHOLDER_CATEGORY_NAME");?>">
        </div>
        <button class="button button--rounded new-category-modal__btn" href="javascript:void(0)"><?=Loc::getMessage("SAVE");?></button>
        <a class="new-category-modal__btn--txt" href="javascript:void(0)"><?=Loc::getMessage("CANCEL");?></a>
    </form>
</div>
<div class="modal" id="reset-pass-modal">
    <a class="modal__btn-close icon icon-cross" href="javascript:void(0)"></a>
    <form class="reset-pass-modal__body ajax_submit">
        <input type="hidden" name="action" value="restore_password_request">
        <span class="reset-pass-modal__heading"><?=Loc::getMessage("MODAL_RESTORE_PASSWORD_HEADING");?></span>
        <span class="reset-pass-modal__text"><?=Loc::getMessage("MODAL_RESTORE_PASSWORD_TEXT");?></span>
        <div class="modal__fieldset">
            <label class="modal__label" for="restore_pass_email"><?=Loc::getMessage("INPUT_HEADING_EMAIL");?></label>
            <input required class="modal__input"
                   name="email"
                   id="restore_pass_email"
                   type="email"
                   placeholder="<?=Loc::getMessage("PLACEHOLDER_EMAIL");?>">
        </div>
        <button class="button button--rounded reset-pass-modal__btn"
                href="javascript:void(0)"><?=Loc::getMessage("SEND");?></button>
        <a class="reset-pass-modal__btn--txt open-modal"
           href="javascript:void(0);"
           data-modal="auth-modal"><?=Loc::getMessage("MODAL_TO_AUTH_BUTTON");?></a>
    </form>
</div>
<?$APPLICATION->IncludeComponent(
    "bitrix:system.auth.changepasswd",
    "popup",
    Array(
        "SHOW_ERRORS" => "Y"
    ),
    false
);?>
<div class="modal" id="delete-conspect-modal"></div>
<div class="modal" id="delete-category-modal"></div>
<div class="modal" id="download-conspect-modal"></div>
<div class="modal" id="exam-results-modal">
    <a class="modal__btn-close icon icon-cross js-disabled js-exam_modal_cross" href="javascript:void(0)"></a>
    <div class="exam-results__wrap">
        <div class="exam-results__content">
            <div class="exam-results__content">
                <div class="exam-results__title"><?=Loc::getMessage("CONSPECT_EXAM_RESULT_TITLE");?></div>
                <div class="exam-results__answers">
                    <span class="exam-results__answers-num js-result">
                        <span class="js-correct_answers">0</span>
                        <?=Loc::getMessage("OUT_OF");?>
                        <span class="js-total_answers">0</span>
                    </span>
                    <span class="exam-results__answers-desc"><?=Loc::getMessage("CONSPECT_EXAM_RESULT_DESCRIPTION");?></span>
                </div>
                <div class="exam-results__btn-wrap">
                    <a class="exam-results__btn exam-results__btn--repeat button button--rounded js-reload" href="javascript:void(0);"><?=Loc::getMessage("CONSPECT_EXAM_RESTART");?></a>
                    <a class="exam-results__btn exam-results__btn--back button button--rounded js-back_link" href="javascript:void(0);"><?=Loc::getMessage("CONSPECT_EXAM_RESULT_BACK");?></a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal" id="create-notification-modal"></div>
<div class="search-block">
    <form class="search-block__body" action="<?= SITE_DIR; ?>search/">
        <input name="q" class="search-block__input" type="text"
               placeholder="<?= Loc::getMessage("SEARCH_PLACEHOLDER"); ?>">
        <label class="icon icon-search search-block__btn-submit" for="searchTopBtn">
            <input type="submit" value="" id="searchTopBtn">
        </label>
        <a class="icon icon-cross search-block__btn-cancel js-skip-disabled" href="javascript:void(0)"></a>
    </form>
</div>

<?if ($_SESSION["SHOW_LOADER"] != "Y") : ?>
    <?$_SESSION["SHOW_LOADER"] = "Y";?>
    <div class="preloader">
        <div class="preloader-outer">
            <div class="preloader-inner">
                <img src="<?=SITE_TEMPLATE_PATH;?>/images/logo.svg"/>
            </div>
        </div>
    </div>
<?endif;?>

<?if ($_GET["change_password"] == "yes" && isset($_GET["USER_CHECKWORD"]) && isset($_GET["USER_LOGIN"]) && !$USER->IsAuthorized()) : ?>
    <script>
            callPopup("new-pass-modal");
    </script>
<?endif;?>
<script>
    (function(){
        if (globals.page != undefined) {
            var menu_item = document.querySelector(".js-aside_" + globals.page);
            if (menu_item != undefined)
                menu_item.classList.add("active");
        }
    })()
</script>
</body>
</html>