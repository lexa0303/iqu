<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;

/** @var array $arParams */
/** @var array $arResult */
/** @var CBitrixComponentTemplate $this */

$this->setFrameMode(true);

$strNavQueryString = ($arResult["NavQueryString"] != "" ? $arResult["NavQueryString"] . "&amp;" : "");
$strNavQueryStringFull = ($arResult["NavQueryString"] != "" ? "?" . $arResult["NavQueryString"] : "");

?>
<? if ($arResult["NavPageCount"] > 1) : ?>
    <? if ($arResult["NavPageNomer"] < $arResult["NavPageCount"]): ?>
        <div class="section__more-btn-wrap js-pagination_delete">
            <a class="section__more-btn js-pagination_more" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]+1)?>">
                <?= Loc::getMessage("MORE_BUTTON"); ?>
            </a>
        </div>
    <? endif;?>
    <section class="library__pagination js-pagination_delete">
        <div class="bottom-pagination">
            <? if ($arResult["NavPageNomer"] > 1): ?>
                <? if ($arResult["bSavePage"]): ?>
                    <a class="bottom-pagination__link"
                       href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=1">
                        <span>1</span>
                    </a>
                <? else: ?>
                    <? if ($arResult["NavPageNomer"] > 2): ?>
                    <? else: ?>
                    <? endif ?>
                    <a class="bottom-pagination__link" href="<?= $arResult["sUrlPath"] ?><?= $strNavQueryStringFull ?>">
                        <span>1</span>
                    </a>
                <? endif ?>
            <? else: ?>
                <span class="bottom-pagination__link active">1</span>
            <? endif ?>
            <? if ($arResult["NavPageNomer"] > 3) : ?>
                <span class="bottom-pagination__link">...</span>
            <? endif; ?>
            <?
            $arResult["nStartPage"]++;
            while ($arResult["nStartPage"] <= $arResult["nEndPage"] - 1):?>
                <? if ($arResult["nStartPage"] == $arResult["NavPageNomer"]): ?>
                    <span class="bottom-pagination__link active"><?= $arResult["nStartPage"] ?></span>
                <? else: ?>
                    <a class="bottom-pagination__link"
                       href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= $arResult["nStartPage"] ?>">
                        <span><?= $arResult["nStartPage"] ?></span>
                    </a>
                <? endif ?>
                <? $arResult["nStartPage"]++ ?>
            <? endwhile ?>
            <? if ($arResult["NavPageNomer"] < $arResult["NavPageCount"]): ?>
                <? if ($arResult["NavPageCount"] - $arResult["NavPageNomer"] > 2) : ?>
                    <span class="bottom-pagination__link">...</span>
                <? endif; ?>
                <? if ($arResult["NavPageCount"] > 1): ?>
                    <a class="bottom-pagination__link"
                       href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= $arResult["NavPageCount"] ?>">
                        <span><?= $arResult["NavPageCount"] ?></span>
                    </a>
                <? endif ?>
            <? else: ?>
                <? if ($arResult["NavPageCount"] > 1): ?>
                    <span class="bottom-pagination__link active"><?= $arResult["NavPageCount"] ?></span>
                <? endif ?>
            <? endif ?>
        </div>
    </section>
<? endif; ?>
<script>
    function checkVisible(elm) {
        var rect = elm.getBoundingClientRect();
        var viewHeight = Math.max(document.documentElement.clientHeight, window.innerHeight);
        return !(rect.bottom < 0 || rect.top - viewHeight >= 0);
    }

    document.addEventListener("scroll", function(e){
        var moreButton = document.querySelector(".js-pagination_more");
        if (moreButton) {
            if (checkVisible(moreButton) && !moreButton.dataset.disabled) {
                moreButton.dataset.disabled = 1;
//                moreButton.click();
            }
        }
    });
</script>
