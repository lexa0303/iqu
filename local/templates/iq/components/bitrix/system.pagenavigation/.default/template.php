<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

/** @var array $arParams */
/** @var array $arResult */
/** @var CBitrixComponentTemplate $this */

$this->setFrameMode(true);

$strNavQueryString = ($arResult["NavQueryString"] != "" ? $arResult["NavQueryString"]."&amp;" : "");
$strNavQueryStringFull = ($arResult["NavQueryString"] != "" ? "?".$arResult["NavQueryString"] : "");
?>
<?if ($arResult["NavPageCount"] > 1) : ?>
    <div class="bottom-pagination">
        <?if ($arResult["NavPageNomer"] > 1):?>
            <?if($arResult["bSavePage"]):?>
<!--                    <div class="page__left">-->
<!--                        <a class="pagi__arrow pagi__back" href="--><?//=$arResult["sUrlPath"]?><!--?--><?//=$strNavQueryString?><!--PAGEN_--><?//=$arResult["NavNum"]?><!--=--><?//=($arResult["NavPageNomer"]-1)?><!--">-->
<!--                            <i class="material-icons">arrow_back</i>-->
<!--                        </a>-->
<!--                    </div>-->
                <a class="bottom-pagination__link" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=1">
                    <span>1</span>
                </a>
            <?else:?>
                <?if ($arResult["NavPageNomer"] > 2):?>
<!--                        <div class="page__left">-->
<!--                            <a class="pagi__arrow pagi__back" href="--><?//=$arResult["sUrlPath"]?><!--?--><?//=$strNavQueryString?><!--PAGEN_--><?//=$arResult["NavNum"]?><!--=--><?//=($arResult["NavPageNomer"]-1)?><!--">-->
<!--                                <i class="material-icons">arrow_back</i>-->
<!--                            </a>-->
<!--                        </div>-->
                <?else:?>
<!--                        <div class="page__left">-->
<!--                            <a class="pagi__arrow pagi__back" href="--><?//=$arResult["sUrlPath"]?><!----><?//=$strNavQueryStringFull?><!--">-->
<!--                                <i class="material-icons">arrow_back</i>-->
<!--                            </a>-->
<!--                        </div>-->
                <?endif?>
                <a class="bottom-pagination__link" href="<?=$arResult["sUrlPath"]?><?=$strNavQueryStringFull?>">
                    <span>1</span>
                </a>
            <?endif?>
        <?else:?>
<!--                <div class="page__left">-->
<!--                    <a class="pagi__arrow pagi__back" href="javascript:void(0);">-->
<!--                        <i class="material-icons">arrow_back</i>-->
<!--                    </a>-->
<!--                </div>-->
            <span class="bottom-pagination__link active">1</span>
        <?endif?>
        <?if ($arResult["NavPageNomer"] > 3) : ?>
            <span class="bottom-pagination__link">...</span>
        <?endif;?>
        <?
        $arResult["nStartPage"]++;
        while($arResult["nStartPage"] <= $arResult["nEndPage"]-1):?>
            <?if ($arResult["nStartPage"] == $arResult["NavPageNomer"]):?>
                <span class="bottom-pagination__link active"><?=$arResult["nStartPage"]?></span>
            <?else:?>
                <a class="bottom-pagination__link" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=$arResult["nStartPage"]?>">
                    <span><?=$arResult["nStartPage"]?></span>
                </a>
            <?endif?>
            <?$arResult["nStartPage"]++?>
        <?endwhile?>
        <?if($arResult["NavPageNomer"] < $arResult["NavPageCount"]):?>
            <?if ($arResult["NavPageCount"] - $arResult["NavPageNomer"] > 2) : ?>
                <span class="bottom-pagination__link">...</span>
            <?endif;?>
            <?if($arResult["NavPageCount"] > 1):?>
                <a class="bottom-pagination__link" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=$arResult["NavPageCount"]?>">
                    <span><?=$arResult["NavPageCount"]?></span>
                </a>
            <?endif?>
<!--                <div class="page__right">-->
<!--                    <a class="pagi__arrow pagi__forward" href="--><?//=$arResult["sUrlPath"]?><!--?--><?//=$strNavQueryString?><!--PAGEN_--><?//=$arResult["NavNum"]?><!--=--><?//=($arResult["NavPageNomer"]+1)?><!--">-->
<!--                        <i class="material-icons">arrow_forward</i>-->
<!--                    </a>-->
<!--                </div>-->
        <?else:?>
            <?if($arResult["NavPageCount"] > 1):?>
                <span class="bottom-pagination__link active"><?=$arResult["NavPageCount"]?></span>
            <?endif?>
<!--                <div class="page__right">-->
<!--                    <span class="pagi__arrow pagi__forward">-->
<!--                        <i class="material-icons">arrow_forward</i>-->
<!--                    </span>-->
<!--                </div>-->
        <?endif?>
    </div>
<?endif;?>

