<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$frame = $this->createFrame()->begin("");
$arResult["BANNER"] = str_replace("<br>", "", $arResult["BANNER"]);
$arResult["BANNER"] = str_replace("</br>", "", $arResult["BANNER"]);
$arResult["BANNER"] = str_replace("<br />", "", $arResult["BANNER"]);
$arResult["BANNER"] = str_replace("<a ", "<a class='main-slider__item' ", $arResult["BANNER"]);
$arResult["BANNER"] = preg_replace("/width=\"([0-9]*)\"/", "", $arResult["BANNER"]);
$arResult["BANNER"] = preg_replace("/height=\"([0-9]*)\"/", "", $arResult["BANNER"]);
echo $arResult["BANNER"];
$frame->end();