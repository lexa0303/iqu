<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

use Bitrix\Main\Localization\Loc;
use \Bitrix\Main\Application;

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 */

?>
<div class="modal" id="new-pass-modal">
    <a class="modal__btn-close icon icon-cross" href="javascript:void(0)"></a>
    <form class="new-pass-modal__body ajax_submit">
        <input type="hidden" name="url" value="<?= $arResult["AUTH_URL"] ?>">
        <span class="new-pass-modal__heading"><?=Loc::getMessage("MODAL_NEW_PASSWORD_HEADING");?></span>
        <span class="new-pass-modal__text"><?=Loc::getMessage("MODAL_NEW_PASSWORD_TEXT");?></span>
        <input type="hidden" name="USER_LOGIN" value="<?=$arResult["LAST_LOGIN"];?>">
        <input type="hidden" name="AUTH_FORM" value="Y">
        <input type="hidden" name="TYPE" value="CHANGE_PWD">
            <input value="<?=$arResult["USER_CHECKWORD"];?>" name="USER_CHECKWORD" type="hidden">
        <fieldset class="modal__fieldset">
            <label class="modal__label" for="new_pass_password"><?=Loc::getMessage("INPUT_HEADING_NEW_PASSWORD");?></label>
            <input required
                   class="modal__input"
                   name="USER_PASSWORD"
                   id="new_pass_password"
                   type="password"
                   placeholder="<?=Loc::getMessage("PLACEHOLDER_PASSWORD");?>">
        </fieldset>
        <fieldset class="modal__fieldset">
            <label class="modal__label" for="new_pass_password_repeat"><?=Loc::getMessage("INPUT_HEADING_PASSWORD_REPEAT");?></label>
            <input required
                   equalTo="#new_pass_password"
                   class="modal__input"
                   name="USER_CONFIRM_PASSWORD"
                   id="new_pass_password_repeat"
                   type="password"
                   placeholder="<?=Loc::getMessage("PLACEHOLDER_PASSWORD");?>">
        </fieldset>
        <button class="button button--rounded new-pass-modal__btn" href="javascript:void(0)"><?=Loc::getMessage("SAVE");?></button>
        <a class="new-pass-modal__btn--txt" href="javascript:void(0)"><?=Loc::getMessage("CANCEL");?></a>
    </form>
</div>
<?if ($_SERVER["REQUEST_METHOD"] == "POST")  :
    $request_list = Application::getInstance()->getContext()->getRequest()->toArray();
    foreach ($request_list as $key => $item) {
        $request[$key] = CUtil::JSEscape($item);
    }
    if ($request["TYPE"] == "CHANGE_PWD" && $request["AUTH_FORM"] == "Y") :
        $APPLICATION->RestartBuffer();
            $return = array(
                "status" => "success",
                "message" => Loc::getMessage("MODAL_CHANGE_PASSWORD_SUCCESS"),
                "callback" => "auth-modal"
            );
        die(Bitrix\Main\Web\Json::encode($return));
    endif;
endif;?>