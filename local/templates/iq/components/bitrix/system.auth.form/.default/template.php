<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
\Bitrix\Main\Localization\Loc::loadLanguageFile(__FILE__, LANG_ID, true); ?>

<? if ($arResult["FORM_TYPE"] == "login"): ?>
    <?$APPLICATION->IncludeComponent("bitrix:socserv.auth.form", "",
        array(
            "AUTH_SERVICES" => $arResult["AUTH_SERVICES"],
            "AUTH_URL" => $arResult["AUTH_URL"],
            "POST" => $arResult["POST"],
            "POPUP" => "N",
            "SUFFIX" => "form",
        ),
        $component,
        array("HIDE_ICONS" => "Y")
    );?>
<? endif ?>
