<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 31.03.17
 * Time: 15:16
 */


foreach ($arResult["AUTH_SERVICES_ICONS"] as &$arIcon) :
    switch ($arIcon["ID"]) :
        case "GooglePlusOAuth":
            $arIcon["ICON_CLASS"] = "gg";
            break;
        case "VKontakte":
            $arIcon["ICON_CLASS"] = "vk";
            break;
        case "Facebook":
            $arIcon["ICON_CLASS"] = "fb";
            break;
    endswitch;
endforeach;