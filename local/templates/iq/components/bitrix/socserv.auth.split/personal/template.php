<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

use Bitrix\Main\Localization\Loc;

?>
<?
if ($arResult['ERROR_MESSAGE'])
    ShowMessage($arResult['ERROR_MESSAGE']);
?>
<div class="link-socials__wrapper">
    <?
    $arServices = $arResult["AUTH_SERVICES_ICONS"];
    if (!empty($arResult["AUTH_SERVICES"])) {
        ?>

        <?
        $APPLICATION->IncludeComponent("bitrix:socserv.auth.form", "personal",
            array(
                "AUTH_SERVICES" => $arResult["AUTH_SERVICES"],
                "CURRENT_SERVICE" => $arResult["CURRENT_SERVICE"],
                "AUTH_URL" => $arResult['CURRENTURL'],
                "POST" => $arResult["POST"],
                "SHOW_TITLES" => 'N',
                "FOR_SPLIT" => 'Y',
                "AUTH_LINE" => 'N',
            ),
            $component,
            array("HIDE_ICONS" => "Y")
        );
        ?>
        <?
    }

    if (isset($arResult["DB_SOCSERV_USER"]) && $arParams["SHOW_PROFILES"] != 'N') { ?>
        <? foreach ($arResult["DB_SOCSERV_USER"] as $key => $arUser) {
            if (!$icon = htmlspecialcharsbx($arResult["AUTH_SERVICES_ICONS"][$arUser["EXTERNAL_AUTH_ID"]]["ICON_CLASS"]))
                $icon = 'openid';
            $authID = ($arServices[$arUser["EXTERNAL_AUTH_ID"]]["NAME"]) ? $arServices[$arUser["EXTERNAL_AUTH_ID"]]["NAME"] : $arUser["EXTERNAL_AUTH_ID"];
            ?>
            <div class="link-socials active">
                <span class="link-socials__icon icon icon-<?= $icon; ?>"></span>
                <div class="link-socials__text">
                    <span class="link-socials__name"><?= $arUser["VIEW_NAME"] ?></span>
                    <? if (in_array($arUser["ID"], $arResult["ALLOW_DELETE_ID"])): ?>
                        <a class="link-socials__turn-off"
                           href="<?= htmlspecialcharsbx($arUser["DELETE_LINK"]) ?>"
                           onclick="return confirm('<?= GetMessage("SS_PROFILE_DELETE_CONFIRM") ?>')"><?= Loc::getMessage("DISABLE"); ?></a>
                    <? else : ?>
                        <a class="link-socials__turn-off disabled" disabled
                           href="javascript:void(0)"><?= Loc::getMessage("CANT_DISABLE"); ?></a>
                    <? endif; ?>
                </div>
            </div>
        <? } ?>
    <? } ?>
</div>
