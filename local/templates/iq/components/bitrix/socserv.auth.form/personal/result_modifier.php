<?
    foreach($arParams["~AUTH_SERVICES"] as $key=>$service):
        switch($service['ID']):
            case 'Facebook':
                $arParams["~AUTH_SERVICES"][$key]['BUTTON-CLASS'] = 'fb';
                $arParams["~AUTH_SERVICES"][$key]['ICON-CLASS'] = 'fb';
                $arParams["~AUTH_SERVICES"][$key]['LANG'] = 'FACEBOOK';
                break;
            case 'GooglePlusOAuth':
                $arParams["~AUTH_SERVICES"][$key]['BUTTON-CLASS'] = 'gg';
                $arParams["~AUTH_SERVICES"][$key]['ICON-CLASS'] = 'gp';
                $arParams["~AUTH_SERVICES"][$key]['LANG'] = 'GOOGLE';
                break;
            case 'VKontakte':
                $arParams["~AUTH_SERVICES"][$key]['BUTTON-CLASS'] = 'vk';
                $arParams["~AUTH_SERVICES"][$key]['ICON-CLASS'] = 'vk';
                $arParams["~AUTH_SERVICES"][$key]['LANG'] = 'VK';
                break;
            case 'INSTAGRAM':
                $arParams["~AUTH_SERVICES"][$key]['BUTTON-CLASS'] = 'insta';
                $arParams["~AUTH_SERVICES"][$key]['ICON-CLASS'] = 'insta';
                $arParams["~AUTH_SERVICES"][$key]['LANG'] = 'INSTA';
                break;
        endswitch;
    endforeach;
?>