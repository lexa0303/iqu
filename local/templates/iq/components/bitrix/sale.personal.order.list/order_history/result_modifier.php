<?php
$el = new CIBlockElement;
foreach ($arResult["ORDERS"] as &$arOrder){
    $arConspectIDs = array();

    foreach ($arOrder["BASKET_ITEMS"] as $arItem){
        $arConspectIDs[] = $arItem["PRODUCT_ID"];
    }
    
    $rsConspects = $el->GetList(
        array(),
        array("ID"=>$arConspectIDs, "IBLOCK_ID"=>CONSPECTS_SHOP_IBLOCK),
        false,
        false,
        array("ID", "IBLOCK_ID", "PROPERTY_CONSPECT_ID")
    );
    while ($res = $rsConspects->Fetch()){
        $arConspects[$res["ID"]] = $res;
    }
    
    foreach ($arOrder["BASKET_ITEMS"] as &$arItem){
        $arItem["CONSPECT"] = Conspects::GetByID($arConspects[$arItem["PRODUCT_ID"]]["PROPERTY_CONSPECT_ID_VALUE"]);
        unset($arItem);
    }
    unset($arOrder);
}