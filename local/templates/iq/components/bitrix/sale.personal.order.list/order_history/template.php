<?

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main,
    Bitrix\Main\Localization\Loc,
    Bitrix\Main\Page\Asset;

CJSCore::Init(array('clipboard'));

global $APPLICATION;

Loc::loadMessages(__FILE__);

if (!empty($arResult['ERRORS']['FATAL'])) {
    foreach ($arResult['ERRORS']['FATAL'] as $error) {
        ShowError($error);
    }
    $component = $this->__component;
    if ($arParams['AUTH_FORM_IN_TEMPLATE'] && isset($arResult['ERRORS']['FATAL'][$component::E_NOT_AUTHORIZED])) {
        $APPLICATION->AuthForm('', false, false, 'N', false);
    }

} else {
    if (!empty($arResult['ERRORS']['NONFATAL'])) {
        foreach ($arResult['ERRORS']['NONFATAL'] as $error) {
            ShowError($error);
        }
    }
    if (!count($arResult['ORDERS'])) {
        if ($_REQUEST["filter_history"] == 'Y') {
            if ($_REQUEST["show_canceled"] == 'Y') {
                ?>
                <h3><?= Loc::getMessage('SPOL_TPL_EMPTY_CANCELED_ORDER') ?></h3>
                <?
            } else {
                ?>
                <h3><?= Loc::getMessage('SPOL_TPL_EMPTY_HISTORY_ORDER_LIST') ?></h3>
                <?
            }
        } else {
            ?>
            <h3><?= Loc::getMessage('SPOL_TPL_EMPTY_ORDER_LIST') ?></h3>
            <?
        }
    }
    ?>
    <?
    if (!count($arResult['ORDERS'])) {
        ?>
        <div class="row col-md-12 col-sm-12">
            <a href="<?= htmlspecialcharsbx($arParams['PATH_TO_CATALOG']) ?>" class="sale-order-history-link">
                <?= Loc::getMessage('SPOL_TPL_LINK_TO_CATALOG') ?>
            </a>
        </div>
        <?
    }
    ?>
    <div class="purchases-list">
        <? foreach ($arResult['ORDERS'] as $key => $arOrder) { ?>
            <div class="purchases-list__item-wrap">
                <div class="purchases-list__item">
                    <div class="purchases-list__main purchases-list__width">
                        <div class="purchases-list__number">Заказ #<?= $arOrder["ORDER"]["ACCOUNT_NUMBER"]; ?></div>
                        <a href="javascript:void(0);" class="purchases-list__btn-toggle"
                           title="Показать приобретенные конспекты">Развернуть</a>
                    </div>
                    <div class="purchases-list__date purchases-list__width"><?= $arOrder["ORDER"]["DATE_INSERT"]->toString(); ?></div>
                    <div class="purchases-list__conspects purchases-list__width"><?= count($arOrder["BASKET_ITEMS"]); ?> конспектов</div>
                    <div class="purchases-list__money purchases-list__width">
                        <span class="purchases-list__money-title">на сумму</span>
                        <span class="purchases-list__money-num"><?= $arOrder["ORDER"]["FORMATED_PRICE"]; ?></span>
                    </div>
                    <div class="purchases-list__status purchases-list__width">
                        <? if ($arOrder["ORDER"]["PAYED"] == "Y") : ?>
                            <span class="purchases-list__status-paid">Оплачено</span>
                        <? else : ?>
                            <?$paymentDescription = "Оплата заказа №" . $arOrder["ORDER"]["ACCOUNT_NUMBER"] . ". IQU";?>
                            <?$paymentData = array(
                                "version" => 3,
                                "sandbox" => 1,
                                "amount" => "0.01",
                                "order_id" => $arOrder["ORDER"]["ACCOUNT_NUMBER"],
                                "currency" => $arOrder["ORDER"]["CURRENCY"],
                                "public_key" => "i48999756024",
                                "action" => "pay",
                                "description" => $paymentDescription,
                                "return_url" => "http://" . $_SERVER["SERVER_NAME"] . "/shop/history/",
                                "server_url" => "http://" . $_SERVER["SERVER_NAME"] . "/shop/payment_receive.php",
                            );
                            $jsonData = Bitrix\Main\Web\Json::encode($paymentData);
                            $data = base64_encode($jsonData);
                            $private_key = "fDqc9fdC6C5PfmeAkgvxpkMmeKF3t42CFujSZQYK";
                            ?>
                            <form id="order-<?=$arOrder["ORDER"]["ID"];?>" method="POST" action="https://www.liqpay.com/api/3/checkout" target="_blank" style="display:inline;">
                                <input type="hidden" name="data" value="<?=$data;?>"/>
                                <input type="hidden" name="signature" value="<?=base64_encode( sha1( $private_key . $data . $private_key, 1 ));?>"/>
                                <input style="margin: 0;" type="submit" class="purchases-list__status-pay" value="Оплатить"/>
                            </form>
                        <? endif; ?>
                    </div>
                    <a href="javascript:void(0);"
                       class="purchases-list__btn-toggle purchases-list__btn-toggle--adaptive"
                       title="Показать приобретенные конспекты">Развернуть</a>
                </div>
                <div class="purchases-list__conspects-wrap">
                    <div class="purchases-list__conspects-title">Приобретенные конспекты:</div>
                    <div class="row">
                        <? foreach ($arOrder["BASKET_ITEMS"] as $arItem) : ?>
                            <? if ($arItem["CONSPECT"]["ID"]) : ?>
                                <? $APPLICATION->IncludeComponent(
                                    "custom:conspect_card_shop",
                                    ($arOrder["ORDER"]["PAYED"] == "Y") ? "history_payed" : "history",
                                    Array(
                                        "CACHE_TIME" => $arParams["CACHE_TIME"],
                                        "CACHE_TYPE" => $arParams["CACHE_TYPE"],
                                        "CARDS_HLBLOCK" => "4",
                                        "CATEGORIES_HLBLOCK" => "3",
                                        "CONSPECTS_HLIBLOCK" => "6",
                                        "CONSPECT_ID" => $arItem["CONSPECT"]["ID"],
                                        "SEF_MODE" => $arParams["SEF_URL_TEMPLATES"],
                                        "MASTER" => $arParams["MASTER"],
                                        "SEF_FOLDER" => $arParams["SEF_FOLDER"],
                                        "CART_ITEM_ID" => $arItem["ID"]
                                    )
                                ); ?>
                            <? endif; ?>
                        <? endforeach; ?>
                    </div>
                </div>
            </div>
        <? } ?>
    </div>
    <? echo $arResult["NAV_STRING"];
}
?>
<script>
    $(document).ready(function(){
        var orderId = '<?=$_GET["order_id"];?>';

        if (orderId) {
            var orderForm = document.querySelector('#order-' + orderId);
            if (orderForm) {
                var data = $(orderForm).serialize();
                window.history.pushState({cart: true}, window.title, '<?=$APPLICATION->GetCurPage(false);?>');
                window.history.pushState({cart: true}, window.title, '<?=$APPLICATION->GetCurPage(false);?>');
                location.href = orderForm.action + "?" + data;
            }
        }
    });
</script>