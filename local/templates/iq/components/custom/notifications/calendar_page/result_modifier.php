<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 04.07.17
 * Time: 17:09
 */

function SortItems($a, $b){
    if (strtolower(substr($a["name"], 0, 1)) < "а" && strtolower(substr($b["name"], 0, 1)) > "а"){
        return 1;
    }
    if (strtolower(substr($a["name"], 0, 1)) > "а" && strtolower(substr($b["name"], 0, 1)) < "а"){
        return -1;
    }
    return (strtolower($a["name"]) < strtolower($b["name"])) ? -1 : 1;
}

$arJsItems = array();
foreach ($arResult as $arItem){
    $arJsItem = array();
    $arJsItem["id"] = $arItem["id"];
    $arJsItem["conspect_id"] = $arItem["conspect"]["ID"];
    $arJsItem["name"] = $arItem["conspect"]["UF_NAME"];
    $arJsItem["time"] = $arItem["when"] * 1000;
    $arJsItem["period"] = $arItem["period"];
    $arJsItem["editable"] = true;
    if ($arJsItem["period"] == "day") {
        $arJsItem["period_time"] = 86400 * 1000;
    } elseif ($arJsItem["period"] == "week") {
        $arJsItem["period_time"] = 604800 * 1000;
    } elseif ($arJsItem["period"] == "month") {
        $arJsItem["period_time"] = 2592000 * 1000;
    } else {
        $arJsItem["period_time"] = 2592000 * 1000;
    }
    $arJsItems[] = $arJsItem;
}
unlink($arItem);
$arAdditionalItems = array();
foreach ($arJsItems as $arItem){
    $next = $arItem["time"];
    while (($next + $arItem["period_time"]) < ((time() * 1000) + (2592000 * 1000 * 6))){
        $next += $arItem["period_time"];

        $arAdditionalItem = $arItem;
        $arAdditionalItem["time"] = $next;
        $arAdditionalItems[] = $arAdditionalItem;
    }
}
usort($arJsItems, "SortItems");
$arResult["JS_ITEMS"] = $arJsItems;
$arResult["JS_ITEMS_CLONES"] = $arAdditionalItems;