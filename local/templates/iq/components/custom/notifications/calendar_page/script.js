/**
 * Created by alex on 04.07.17.
 */

var afterAjaxHandler;

var NotificationCalendar = function(params){
    this.calendar = document.querySelector("#calendar");
    this.notificationsList = document.querySelector(".js-notifications_list");
    this.addNotificationButton = document.querySelector(".js-notification_add");
    this.selectedDate = document.querySelector(".js-selected_date");
    this.notifications = [];
    this.edit_popup = document.querySelector("#create-notification-modal");
    var item, date;
    this.user = params.user;
    if (params.items !== undefined) {
        for (var i in params.items) {
            if (params.items.hasOwnProperty(i)) {
                item = params.items[i];
                date = new Date(item.time);
                this.notifications.push(
                    {
                        title: item.name,
                        start: date,
                        id: item.id,
                        conspect_id: item.conspect_id,
                        period: item.period,
                        node: document.querySelector(".js-notification[data-id='" + item.id +"']"),
                        editable: !!item.editable
                    }
                );
            }
        }
    }

    this.Init(params);
};
NotificationCalendar.prototype.Init = function(params){
    var self = this;

    /* initialize the calendar
     -----------------------------------------------------------------*/

    $(self.calendar).fullCalendar({
        header: {
            left: 'prev, today',
            center: 'title',
            right: 'today, next'
        },
        navLinks: true,
        events: self.notifications || {},
        editable: true,
        droppable: true, // this allows things to be dropped onto the calendar
        eventDurationEditable: false,
        eventLimit: true,
        showMoreRender: function(events){
            events.forEach(function(item){
                $(item.el).addClass(item.event.period);
                if (!item.event.editable){
                    $(item.el).addClass("notification-opacity");
                }
            });
        },
        navLinkDayClick: function(date, e){
            if (self.selected_day_node !== undefined) {
                self.selected_day_node.classList.remove("selected");
            }
            var start = date.unix() * 1000;
            var end = start + 86400 * 1000 - 1;
            self.notifications.forEach(function(item){
                if (item.start.getTime() <= start || item.start.getTime() >= end){
                    item.node.classList.add("hidden");
                }
            });
            self.notifications.forEach(function(item){
                if (item.start.getTime() >= start && item.start.getTime() <= end){
                    item.node.classList.remove("hidden");
                }
            });
            self.selected_day_node = e.target;
            self.selected_day_node.classList.add("selected");

            self.selectedDate.innerText = date.format();
        },
        eventClick: function (item, e, view) {
            self.EditNotification(item.id, item);
        },
        eventDrop: function(item, e, view){
            var ajaxOptions = {
                data: {
                    action: "edit_notification",
                    user_id: self.user,
                    notification_date: self.GetEventById(item.id).start.format(),
                    id: item.id
                }
            };
            ajaxRequest(ajaxOptions);
        },
        eventAfterRender: function(item, el){
            self.GetNotification(item.id).calendar_node = el[0];
            if (!item.editable){
                $(el).addClass("notification-opacity");
            }
            item.node.classList.remove("day");
            item.node.classList.remove("week");
            item.node.classList.remove("month");
            item.node.classList.add(item.period);
            $(el).removeClass("day");
            $(el).removeClass("week");
            $(el).removeClass("month");
            $(el).addClass(item.period);
            $(el).attr("data-id", item.id);
        }
    });

    this.InitHandlers();
};
NotificationCalendar.prototype.InitHandlers = function(){
    var self = this;

    var notification;
    for (var i in self.notifications){
        if (self.notifications.hasOwnProperty(i)){
            notification = self.notifications[i];
            self.AddNodeHandlers(notification);
        }
    }

    self.addNotificationButton.addEventListener("click", function(e){
        e.preventDefault();

        self.ShowAddPopup();
    });
};
NotificationCalendar.prototype.AddNodeHandlers = function(notification){
    var self = this;
    if (notification.node.dataset.block !== "Y") {
        notification.node.querySelector(".js-notification_delete").addEventListener("click", function (e) {
            e.preventDefault();

            $(self.calendar).fullCalendar("removeEvents", this.dataset.id);
            $(this).closest(".js-notification").fadeOut();

            var ajaxOptions = {
                data: {
                    action: "delete_notification",
                    id: this.dataset.id,
                    user_id: self.user
                },
                callback: function(res){
                    ajaxHandler(res);
                    notification.blocked = false;
                }
            };

            ajaxRequest(ajaxOptions);
        });
        notification.node.addEventListener("click", function (e) {
            if (!e.target.classList.contains("js-notification_delete")) {
                self.EditNotification(this.dataset.id, self.GetEventById(this.dataset.id));
            }
        });
        notification.node.dataset.block = "Y";
    }
};
/**
 *
 * @param id
 * @returns {undefined|object}
 */
NotificationCalendar.prototype.GetNotification = function(id){
    var result = undefined;
    this.notifications.forEach(function(item, i){
        if (item.id === id){
            result = item;
        }
    });
    return result;
};
NotificationCalendar.prototype.AddNotification = function(result){
    var self = this;

    var newNotification = document.createElement("li");
    newNotification.classList.add("calendar__notif-item");
    newNotification.classList.add("js-notification");
    newNotification.dataset.id = result.id;
    var newNotificationName = document.createElement("span");
    newNotificationName.classList.add("calendar__notif-name");
    newNotificationName.innerText = result.params.name;
    var newNotificationDelete = document.createElement("button");
    newNotificationDelete.classList.add("calendar__notif-delete");
    newNotificationDelete.classList.add("js-notification_delete");
    newNotificationDelete.dataset.id = result.id;
    newNotificationDelete.dataset.title = "Удалить напоминание";
    newNotificationDelete.innerText = "✕";
    newNotification.appendChild(newNotificationName);
    newNotification.appendChild(newNotificationDelete);

    self.notificationsList.appendChild(newNotification);
    var newNode = {
        title: result.params.name,
        start: new Date(result.params.time * 1000),
        id: result.id,
        conspect_id: result.params.conspect_id,
        period: result.params.sPeriod,
        node: newNotification
    };
    self.notifications.push(newNode);
    $(self.calendar).fullCalendar("addEventSource", [newNode]);
    self.AddNodeHandlers(newNode);
    afterAjaxHandler = undefined;
};
/**
 *
 * @param id
 * @param item
 * @returns {boolean}
 * @constructor
 */
NotificationCalendar.prototype.EditNotification = function(id, item){
    var notification = this.GetNotification(id);
    if (notification === undefined){
        return false;
    }
    this.ShowEditPopup(id, item);
};
NotificationCalendar.prototype.ShowAddPopup = function(id, item){
    var self = this;
    var ajaxOptions = {
        data: {
            action: "notification_add",
            id: id,
            user_id: this.user,
            modal: "create-notification-modal"
        },
        callback: function(res){
            ajaxHandler(res);
            var result = JSON.parse(res);
            self.SetAddAjaxHandler(item, "add");
        }
    };

    ajaxRequest(ajaxOptions);
};
NotificationCalendar.prototype.ShowEditPopup = function(id, item){
    var self = this;
    var ajaxOptions = {
        data: {
            action: "notification_edit",
            id: id,
            user_id: this.user,
            modal: "create-notification-modal"
        },
        callback: function(res){
            ajaxHandler(res);
            var result = JSON.parse(res);
            self.SetUpdateAjaxHandler(item);
            document.getElementById(ajaxOptions.data.modal).querySelector(".js-delete_notification").addEventListener("click", function(e){
                e.preventDefault();
                var that = this;

                var id = $(this).closest("form").find("input[name='id']").val();

                var ajaxOptions = {
                    data: {
                        action: "delete_notification",
                        id: id,
                        user_id: self.user
                    },
                    callback: function(res){
                        ajaxHandler(res);

                        $(self.GetEventById(id).node).fadeOut();
                        $(self.calendar).fullCalendar("removeEvents", id);
                        closeModals();
                    }
                };

                ajaxRequest(ajaxOptions);
            });
        }
    };

    ajaxRequest(ajaxOptions);
};
NotificationCalendar.prototype.UpdateNotification = function(result, item){
    var masterItem = this.GetEventById(item.id);
    if (masterItem.period !== result.params.sPeriod){
        location.reload();
    }
    if (result.params.sPeriod){
        masterItem.period = result.params.sPeriod;
    }
    masterItem.start = new Date(masterItem.start.unix() + (result.params.time * 1000) - item.start.unix());
    $(this.calendar).fullCalendar("updateEvent", masterItem);
    afterAjaxHandler = undefined;
};
NotificationCalendar.prototype.SetAddAjaxHandler = function(){
    var self = this;
    afterAjaxHandler = function(result){

        self.AddNotification(result);
        closeModals();
        location.reload();
    };
};
NotificationCalendar.prototype.SetUpdateAjaxHandler = function(item){
    var self = this;
    afterAjaxHandler = function(result){
        self.UpdateNotification(result, item);
        closeModals();
    };
};
NotificationCalendar.prototype.GetEventById = function(id){
    var self = this;
    var result = undefined;
    var eventSources = $(calendar.calendar).fullCalendar("getEventSources");
    eventSources.every(function(arEvents){
        arEvents.events.every(function(event){
            if (parseInt(event.id) === parseInt(id)){
                result = event;
                return false;
            }
            return true;
        });
        if (result !== undefined){
            return false;
        }
    });
    return result;
};