<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 14.06.17
 * Time: 10:30
 */

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use Bitrix\Main\Localization\Loc;

global $USER;

$this->setFrameMode(true);
?>
<div class="section__heading">
    <span class="section__heading-txt"><? $APPLICATION->ShowTitle(); ?></span>
</div>
<div class="calendar__wrap">
    <div class="calendar__aside">
        <button class="calendar-page__add-event js-notification_add"
                data-user_id="<?= $USER->GetID(); ?>"
                data-action="notification_add"
                data-modal="create-notification-modal"
                data-ajax="Y">Создать напоминание
        </button>
        <div class="calendar-legend">
            <div class="calendar-legend__item">
                <span class="calendar-legend__color daily"></span>
                <span class="calendar-legend__txt">Ежедневные</span>
            </div>
            <div class="calendar-legend__item">
                <span class="calendar-legend__color weekly"></span>
                <span class="calendar-legend__txt">Еженедельные</span>
            </div>
            <div class="calendar-legend__item">
                <span class="calendar-legend__color monthly"></span>
                <span class="calendar-legend__txt">Ежемесячные</span>
            </div>
        </div>
        <h2 class="calendar__aside-header">Список напоминаний</h2>
        <span class="calendar__aside-date js-selected_date"></span>
        <ul class="calendar__notif-list js-notifications_list">
            <? foreach ($arResult["JS_ITEMS"] as $arItem) : ?>
                <li class="calendar__notif-item js-notification <?= $arItem["period"]; ?>"
                    data-id="<?= $arItem["id"]; ?>">
                    <span class="calendar__notif-name"><?= $arItem["name"]; ?></span>
                    <button class="calendar__notif-delete js-notification_delete"
                            data-id="<?= $arItem["id"]; ?>"
                            title="Удалить напоминание">&#10005;</button>
                </li>
            <? endforeach; ?>
        </ul>
    </div>
    <div id='calendar'></div>
    <div style='clear:both'></div>
</div>
<?$arItems = array_merge($arResult["JS_ITEMS"], $arResult["JS_ITEMS_CLONES"]);?>
<script>
    globals.page = "notifications";
    var calendar = new NotificationCalendar(
        {
            items: <?=Bitrix\Main\Web\Json::encode($arItems);?>,
            user: '<?=$USER->GetID();?>'
        }
    );
</script>
