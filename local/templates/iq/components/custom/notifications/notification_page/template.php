<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 14.06.17
 * Time: 10:30
 */

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use Bitrix\Main\Localization\Loc;

$this->setFrameMode(true);
?>
<div class="section__heading">
    <span class="section__heading-txt">
        <? if (count($arResult) > 0) : ?>
            <?= Loc::getMessage("NOTIFICATIONS_HEADING"); ?>
        <? else : ?>
            <?= Loc::getMessage("NOTIFICATIONS_HEADING_NONE"); ?>
        <? endif; ?>
    </span>
    <? if (count($arResult) > 0) : ?>
        <div class="notification-clear_button-wrap">
            <a class="button notification-clear_button js-delete_notifications" href="javascript:void(0);">
                <span class="notification-clear_button-text">Удалить все напоминания</span>
            </a>
        </div>
    <? endif; ?>
</div>
<?$arShowedNotifications = array();?>
<section class="my-cards__content ">
    <? if (count($arResult) > 0) : ?>
        <div class="fcards__wrapper row">
            <? foreach ($arResult as $arNotification) :
                if ($arNotification["when"] <= time()):?>
                    <? if (!in_array($arNotification["conspect"]["ID"], $arShowedNotifications)) : ?>
                        <? $APPLICATION->IncludeComponent(
                            "custom:conspect_card",
                            "",
                            Array(
                                "CACHE_TIME" => "3600",
                                "CACHE_TYPE" => "N",
                                "CARDS_HLBLOCK" => "4",
                                "CATEGORIES_HLBLOCK" => "3",
                                "CONSPECTS_HLIBLOCK" => "6",
                                "CONSPECT_ID" => $arNotification["conspect"]["ID"],
                                "SEF_URL_TEMPLATES" => array(
                                    "main_page" => "",
                                    "conspect_add" => "conspects/add/",
                                    "conspect_detail" => "conspects/#USER_TITLE#/#CONSPECT_ID#/lesson/",
                                    "conspect_edit" => "conspects/#USER_TITLE#/#CONSPECT_ID#/edit/",
                                    "conspect_list" => "conspects/#USER_TITLE#/",
                                    "category_detail" => "category/#USER_TITLE#/#CATEGORY_ID#/",
                                    "categories_list" => "categories/#USER_TITLE#/",
                                    "exam" => "conspects/#USER_TITLE#/#CONSPECT_ID#/exam/",
                                    "lesson" => "conspects/#USER_TITLE#/#CONSPECT_ID#/lesson/",
                                    "search" => "search/",
                                    "new" => "new/",
                                    "exam_result" => "conspects/#USER_TITLE#/#CONSPECT_ID#/exam/#RESULT_ID#/",
                                ),
                                "MASTER" => "N",
                                "SHOW_STUDY" => "N"
                            )
                        ); ?>
                        <?$arShowedNotifications[] = $arNotification["conspect"]["ID"];?>
                    <?endif;?>
                <?endif;
            endforeach; ?>
        </div>
    <? else : ?>
        <div class="notifications_empty-text">Новых напоминаний нет.</div>
    <? endif; ?>
    <div class="notif-page__to-calendar">
        <a href="calendar" class="button button--rounded notif-page__to-calendar-btn">Перейти на календарь</a>
    </div>
<script>
    globals.page = "notifications";
</script>
