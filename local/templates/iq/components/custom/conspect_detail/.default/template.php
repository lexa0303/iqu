<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;

$this->setFrameMode(true);
global $arUser;
global $USER;
global $APPLICATION;
?>
    <meta property="og:url" content="https://<?= $_SERVER["SERVER_NAME"]; ?><?= $_SERVER["REQUEST_URI"]; ?>"/>
    <meta property="og:type" content="article"/>
    <meta property="og:title" content="<?= $arResult["UF_NAME"]; ?>"/>
    <meta property="og:description" content=""/>
    <meta property="og:image" content="https://<?= $_SERVER["SERVER_NAME"]; ?><?= $arResult["UF_IMAGE"]["SRC"]; ?>"/>
    <section class="section-exam">
        <div class="container">
            <? if (empty($arResult["ERRORS"])) : ?>
                <div class="exam__wrapper">
                    <div class="row">
                        <div class="col-xs-12 col-lg-3 col-lg-push-9">
                            <section class="exam__navigation">
                                <div class="examNav-burger">
                                    <img class="examNav-burger__icon"
                                         src="<?= SITE_TEMPLATE_PATH; ?>/images/settings.svg"
                                         alt="">
                                    <span class="examNav-burger__item"></span>
                                    <span class="examNav-burger__item"></span>
                                </div>
                                <nav class="studying-nav">
                                    <div class="studying-nav__controls">
                                        <? if ($arResult["MASTER"] == "Y" && !$arResult["FROM_LIBRARY"] && !($arUser["IS_AUTHOR"] == "Y")) : ?>
                                            <a class="studying-nav__control-item"
                                               href="<?= $arResult["LESSON_PAGE_URL"]; ?>">
                                                <span class="studying-nav__control-icon icon icon-lesson"></span>
                                                <span class="studying-nav__control-text"><?= Loc::getMessage("LESSON"); ?></span>
                                            </a>
                                            <a class="studying-nav__control-item"
                                               href="<?= $arResult["EXAM_PAGE_URL"]; ?>">
                                                <span class="studying-nav__control-icon icon icon-exam"></span>
                                                <span class="studying-nav__control-text"><?= Loc::getMessage("EXAM"); ?></span>
                                            </a>
                                            <a class="studying-nav__control-item open-modal"
                                               data-modal="create-notification-modal"
                                               data-ajax="Y"
                                               data-action="notification_create"
                                               data-conspect_id="<?= $arResult["ID"]; ?>"
                                               data-user_id="<?= $USER->GetID(); ?>"
                                               href="javascript:void(0);">
                                                <span class="studying-nav__control-icon icon icon-alert"></span>
                                                <span class="studying-nav__control-text"><?= Loc::getMessage("NOTIFICATION"); ?></span>
                                            </a>
                                        <? elseif (($arResult["MASTER"] != "Y" || (!$arResult["UF_ACTIVE"] && !$arUser["IS_AUTHOR"]))) : ?>
                                            <a class="studying-nav__control-item js-save_conspect js-auth"
                                               data-conspect_id="<?= $arResult["ID"]; ?>"
                                               data-user_id="<?= $arResult["USER"]["ID"]; ?>"
                                               href="javascript:void(0);">
                                                <span class="studying-nav__control-icon icon icon-download2"></span>
                                                <span class="studying-nav__control-text"><?= Loc::getMessage("TOOLTIP_CONSPECT_SAVE"); ?></span>
                                            </a>
                                        <? endif; ?>
                                        <? /*
                                <a class="studying-nav__control-item" href="<?=$arResult["LIST_PAGE_URL"];?>">
                                    <span class="studying-nav__control-icon icon icon-arrow-back"></span>
                                    <span class="studying-nav__control-text"><?=Loc::getMessage("BACK");?></span>
                                </a>
                                */ ?>
                                    </div>
                                    <div class="studying-nav__conspect-info">
                                        <div class="studying-nav__user-photo"
                                             style="background-image: url('<?= ($arResult["UF_ORIGIN_USER"]) ? $arResult["ORIGIN_USER"]["IMAGE"]["RESIZED"] : $arResult["USER"]["IMAGE"]["RESIZED"]; ?>');"></div>
                                        <div class="studying-nav__user-info">
                                            <a href="<?= $arResult["USER"]["DETAIL_PAGE_URL"]; ?>"
                                               class="studying-nav__username"><?= ($arResult["UF_ORIGIN_USER"]) ? $arResult["ORIGIN_USER"]["TITLE"] : $arResult["USER"]["TITLE"]; ?></a>
                                            <? if (!$arResult["UF_ORIGIN_USER"] && $arResult["USER"]["TITLE"] != "Help") : ?>
                                                <div class="studying-nav__user-info">
                                                    <div class="studying-nav__user-watches">
                                                        <span class="icon icon-views"></span>
                                                        <span><?= (int)$arResult["UF_VIEW_COUNT"]; ?></span>
                                                    </div>
                                                    <div class="studying-nav__user-loads">
                                                        <span class="icon icon-download2"></span>
                                                        <span><?= (int)$arResult["UF_SAVES_COUNT"]; ?></span>
                                                    </div>
                                                </div>
                                            <? endif; ?>
                                            <? if ($arResult["MASTER"] != "Y" && $arResult["USER"]["TITLE"] != "Help") : ?>
                                                <a class="studying-nav__btn-subscr js-unsubscribe js-sub"
                                                   data-user_id="<?= $arResult["USER"]["ID"]; ?>"
                                                   href="javascript:void(0);">
                                                    <span class="icon icon-unsubscribe"></span><?= Loc::getMessage("UNSUBSCRIBE"); ?>
                                                </a>
                                                <a class="studying-nav__btn-subscr js-subscribe js-sub"
                                                   data-user_id="<?= $arResult["USER"]["ID"]; ?>"
                                                   href="javascript:void(0);">
                                                    <span class="icon icon-subscribe"></span><?= Loc::getMessage("SUBSCRIBE"); ?>
                                                </a>
                                            <? endif; ?>
                                        </div>
                                    </div>
                                    <div class="studying-nav__settings">
                                        <? if ($arResult["MASTER"] == "Y" && !$arResult["UF_ORIGIN_USER"] && !$arResult["FROM_LIBRARY"]) : ?>
                                            <? if (!($arUser["IS_AUTHOR"]) || ($arUser["IS_AUTHOR"] && $arResult["UF_PUBLISH_STATUS"] == 14)) : ?>
                                                <a class="studying-nav__settings-link"
                                                   href="<?= $arResult["EDIT_PAGE_URL"]; ?>">
                                                    <span class="studying-nav__settings-icon icon icon-settings"></span>
                                                    <span class="studying-nav__settings-text"><?= Loc::getMessage("CONSPECT_DETAIL_EDIT"); ?></span>
                                                </a>
                                            <? endif; ?>
                                        <? endif; ?>
                                    </div>
                                    <? if ($arResult["MASTER"] == "Y" && !$arResult["UF_ORIGIN_USER"] && !$arResult["FROM_LIBRARY"] && !($arUser["IS_AUTHOR"] == "Y")) : ?>
                                        <input type="hidden" id="js-conspect_image"
                                               value="<?= $arResult["UF_IMAGE"]["ID"]; ?>">
                                        <form class="studying-nav__addToLibrary" id="add_to_library-form">
                                            <input type="hidden" name="action" value="edit_conspect">
                                            <input class="js-conspect_id" type="hidden" name="conspect_id"
                                                   value="<?= $arResult["ID"]; ?>">
                                            <?= bitrix_sessid_post(); ?>
                                            <input type="hidden" name="skip" value="Y">
                                            <input name="library"
                                                   type="checkbox"
                                                <?= ($arResult["UF_SHOW_IN_LIBRARY"]) ? "checked" : ""; ?>
                                                   id="addToLibrary"
                                                   class="studying-nav__addToLibrary-input js-add_to_library">
                                            <label for="addToLibrary" class="studying-nav__addToLibrary-btn button">Добавить
                                                в общую библиотеки</label>
                                            <label for="addToLibrary"
                                                   class="studying-nav__removeFromLibrary-btn button">Удалить
                                                из общей библиотеки</label>
                                        </form>
                                    <? endif; ?>
                                    <?php if ($arUser["IS_AUTHOR"] && $arResult["MASTER"] == "Y" && !$arResult["UF_ORIGIN"] && $arResult["UF_PUBLISH_STATUS"] == 14) : ?>
                                        <div class="studying-nav__publish-wrap">
                                            <button class="button studying-nav__publish-button open-modal js-publish"
                                                    data-conspect="<?= $arResult["ID"]; ?>"
                                                    data-modal="author-publish">Опубликовать
                                            </button>
                                        </div>
                                    <?php endif; ?>
                                    <?php if ($arUser["IS_AUTHOR"] && $arResult["MASTER"] == "Y" && !$arResult["UF_ORIGIN"] && $arResult["UF_PUBLISH_STATUS"] == 18) : ?>
                                        <div class="studying-nav__publish-wrap">
                                            <button class="button studying-nav__publish-button open-modal js-final_publish"
                                                    data-conspect="<?= $arResult["ID"]; ?>">Опубликовать
                                            </button>
                                        </div>
                                    <?php endif; ?>
                                    <div class="studying-nav__share-wrap">
                                        <span class="studying-nav__share-title"><?= Loc::getMessage("SHARE"); ?></span>
                                        <a class="studying-nav__share-item js-share-fb" href="javascript:void(0);">
                                            <span class="icon icon-fb"></span>
                                        </a>
                                        <? /*
                                <a class="studying-nav__share-item js-share-vk" href="javascript:void(0);">
                                    <span class="icon icon-vk"></span>
                                </a>
*/ ?>
                                        <a class="studying-nav__share-item js-share-gp" href="javascript:void(0);">
                                            <span class="icon icon-gp"></span>
                                        </a>
                                    </div>
                                    <div class="conspect-tags__list-wrap">
                                        <div class="conspect-tags__list-title">Теги:</div>
                                        <div class="conspect-tags__list">
                                            <?= printTags($arResult["UF_TAGS"]); ?>
                                        </div>
                                        <a href="javascript:void(0);" class="conspect-tags__list-more">Показать
                                            больше</a>
                                    </div>
                                </nav>
                            </section>
                        </div>
                        <div class="col-xs-12 col-lg-9 col-lg-pull-3">
                            <div class="conspect-cards__wrapper">
                                <div class="section__heading section__heading--with-controls">
                                    <span class="section__heading-txt"><?= $APPLICATION->ShowTitle(); ?></span>
                                </div>
                                <article class="conspect-cards__content">
                                    <? foreach ($arResult["CARDS"] as $arCard) : ?>
                                        <div class="conspect-cards__item js-card" data-id="<?= $arCard["ID"];?>" data-study="<?= $arCard["UF_STUDIED"];?>">
                                            <div class="row conspect-cards__holder">
                                                <? if ($arResult["MASTER"] == "Y") : ?>
                                                    <div class="conspect-card__sticker js-card_study"
                                                         data-id="<?= $arCard["ID"]; ?>"
                                                         data-study="<?= $arCard["UF_STUDIED"]; ?>"
                                                        <? if ($arCard["UF_STUDIED"]) : ?>
                                                            title="Вопрос выучен"
                                                        <? else : ?>
                                                            title="Вопрос не выучен"
                                                        <? endif; ?>>
                                                        <span class="icon <?= ($arCard["UF_STUDIED"]) ? "icon-done" : "icon-studying"; ?>"></span>
                                                    </div>
                                                <? endif; ?>
                                                <div class="col-md-6">
                                                    <div class="conspect-card__wrapper question">
                                                        <div class="conspect-card__title"><?= Loc::getMessage("QUESTION"); ?></div>
                                                        <div class="conspect-card__body">
                                                            <div class="conspect-card__img">
                                                                <a class="conspect-card__img-link"
                                                                   href="<?= $arCard["UF_QUESTION_IMAGE"]["SRC"]; ?>">
                                                                    <img src="<?= $arCard["UF_QUESTION_IMAGE"]["RESIZED"]; ?>"
                                                                         alt="">
                                                                </a>
                                                            </div>
                                                            <div class="conspect-card__text">
                                                                <span><?= $arCard["UF_QUESTION"]; ?></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="conspect-card__wrapper answer">
                                                        <div class="conspect-card__title"><?= Loc::getMessage("ANSWER"); ?></div>
                                                        <div class="conspect-card__body">
                                                            <div class="conspect-card__img">
                                                                <a class="conspect-card__img-link"
                                                                   href="<?= $arCard["UF_ANSWER_IMAGE"]["SRC"]; ?>">
                                                                    <img src="<?= $arCard["UF_ANSWER_IMAGE"]["RESIZED"]; ?>"
                                                                         alt="">
                                                                </a>
                                                            </div>
                                                            <div class="conspect-card__text">
                                                                <span><?= $arCard["UF_ANSWER"]; ?></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <? endforeach; ?>
                                </article>
                            </div>
                        </div>
                    </div>
                </div>
                <? if ($USER->IsAuthorized() && $arResult["MASTER"] == "Y") : ?>
                    <section class="main-bot-btns__section">
                        <div class="row">
                            <div class="col-md-9">
                                <div class="main-bot-btns__wrap">
                                    <a class="ripple button conspect-card__bottom-btn--lesson"
                                       href="<?= $arResult["LESSON_PAGE_URL"]; ?>"><?= Loc::getMessage("CONSPECT_DETAIL_TO_LESSON"); ?></a>
                                    <a class="ripple button conspect-card__bottom-btn--exam"
                                       href="<?= $arResult["EXAM_PAGE_URL"]; ?>"><?= Loc::getMessage("CONSPECT_DETAIL_TO_EXAM"); ?></a>
                                </div>
                            </div>
                        </div>
                    </section>
                <? endif; ?>
            <? else : ?>
                <div style="padding-top: 20px;">
                    <? foreach ($arResult["ERRORS"] as $error) : ?>
                        <div style="font-size: 24px; padding: 20px; text-align: center;">
                            <?= $error; ?>
                        </div>
                    <? endforeach; ?>
                </div>
            <? endif; ?>
        </div>
    </section>

<? if ($arResult["MASTER"] == "Y" && !$arResult["UF_ORIGIN_USER"] && !$arResult["FROM_LIBRARY"] && !($arUser["IS_AUTHOR"] == "Y")) : ?>
    <script>
        (function () {
            var libraryInput = document.querySelector("#addToLibrary");
            var status = libraryInput.checked;

            window.onbeforeunload = function (e) {
                if (status !== libraryInput.checked) {
                    var form = document.getElementById("add_to_library-form");
                    var formData = new FormData(form);
                    var ajaxOptions = {
                        body: formData
                    };

                    ajaxRequest(ajaxOptions);
                }
            }
        })();
    </script>
<? endif; ?>