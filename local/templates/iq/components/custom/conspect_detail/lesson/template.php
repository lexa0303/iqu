<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Page\Asset;

$this->setFrameMode(true);
?>
<? Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/lesson.js"); ?>
<section class="main-lesson__section">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-lg-3 col-lg-push-9">
                <section class="exam__navigation">
                    <div class="examNav-burger">
                        <img class="examNav-burger__icon" src="<?=SITE_TEMPLATE_PATH;?>/images/settings.svg" alt="">
                        <span class="examNav-burger__item"></span>
                        <span class="examNav-burger__item"></span>
                    </div>
                    <nav class="studying-nav">
                        <div class="studying-nav__controls">
                            <a class="studying-nav__control-item active" href="javascript:void(0);">
                                <span class="studying-nav__control-icon icon icon-lesson"></span>
                                <span class="studying-nav__control-text"><?=Loc::getMessage("LESSON");?></span>
                            </a>
                            <?if ($arResult["MASTER"] == "Y") : ?>
                                <a class="studying-nav__control-item" href="<?=$arResult["EXAM_PAGE_URL"];?>">
                                    <span class="studying-nav__control-icon icon icon-exam"></span>
                                    <span class="studying-nav__control-text"><?=Loc::getMessage("EXAM");?></span>
                                </a>
                                <?/*
                                <a class="studying-nav__control-item"
                                   data-modal="create-notification-modal"
                                   data-ajax="Y"
                                   data-action="notification_create"
                                   data-conspect_id="<?=$arResult["ID"];?>"
                                   data-user_id="<?=$USER->GetID();?>"
                                   href="javascript:void(0);">
                                    <span class="studying-nav__control-icon icon icon-alert"></span>
                                    <span class="studying-nav__control-text"><?=Loc::getMessage("NOTIFICATION");?></span>
                                </a>
 */?>
                            <?endif;?>
<!--                            <a class="studying-nav__control-item" href="--><?//=$arResult["LIST_PAGE_URL"];?><!--">-->
<!--                                <span class="studying-nav__control-icon icon icon-arrow-back"></span>-->
<!--                                <span class="studying-nav__control-text">--><?//=Loc::getMessage("BACK");?><!--</span>-->
<!--                            </a>-->
                        </div>
                        <div class="studying-nav__conspect-info">
                            <div class="studying-nav__user-photo"
                                 style="background-image: url('<?=($arResult["UF_ORIGIN_USER"]) ? $arResult["ORIGIN_USER"]["IMAGE"]["RESIZED"] : $arResult["USER"]["IMAGE"]["RESIZED"];?>');"></div>
                            <div class="studying-nav__user-info">
                                <a href="<?=$arResult["USER"]["DETAIL_PAGE_URL"];?>" class="studying-nav__username"><?=($arResult["UF_ORIGIN_USER"]) ? $arResult["ORIGIN_USER"]["TITLE"] : $arResult["USER"]["TITLE"];?></a>
                                <?if (!$arResult["UF_ORIGIN_USER"]) : ?>
                                    <div class="studying-nav__user-info">
                                        <div class="studying-nav__user-watches">
                                            <span class="icon icon-views"></span>
                                            <span><?=(int)$arResult["UF_VIEW_COUNT"];?></span>
                                        </div>
                                        <div class="studying-nav__user-loads">
                                            <span class="icon icon-download2"></span>
                                            <span><?=(int)$arResult["UF_SAVES_COUNT"];?></span>
                                        </div>
                                    </div>
                                <?endif;?>
                                <?if ($arResult["MASTER"] != "Y") :?>
                                    <a class="studying-nav__btn-subscr js-unsubscribe js-sub" data-user_id="<?=$arResult["USER"]["ID"];?>" href="javascript:void(0);">
                                        <span class="icon icon-unsubscribe"></span><?=Loc::getMessage("UNSUBSCRIBE");?>
                                    </a>
                                    <a class="studying-nav__btn-subscr js-subscribe js-sub" data-user_id="<?=$arResult["USER"]["ID"];?>" href="javascript:void(0);">
                                        <span class="icon icon-subscribe"></span><?=Loc::getMessage("SUBSCRIBE");?>
                                    </a>
                                <?endif;?>
                            </div>
                        </div>
                        <section class="lesson-progress">
                            <span class="lesson-progress__title"><?=Loc::getMessage("PROGRESS");?>:</span>
                            <div class="lesson-progress__box">
                                <div class="lesson-progress-bar" style="width: 1%;">
                                    <div class="lesson-progress-tooltip">
                                        <span class="lesson-tooltip-current">1</span><?=Loc::getMessage("OUT_OF");?>
                                        <span class="lesson-tooltip-total">0</span>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <div class="studying-nav__share-wrap">
                            <span class="studying-nav__share-title"><?=Loc::getMessage("SHARE");?></span>
                            <a class="studying-nav__share-item js-share-fb" href="javascript:void(0);">
                                <span class="icon icon-fb"></span>
                            </a>
                            <?/*
                            <a class="studying-nav__share-item js-share-vk" href="javascript:void(0);">
                                <span class="icon icon-vk"></span>
                            </a>
*/?>
                            <a class="studying-nav__share-item js-share-gp" href="javascript:void(0);">
                                <span class="icon icon-gp"></span>
                            </a>
                        </div>
                    </nav>
                </section>
            </div>
            <div class="col-xs-12 col-lg-9 col-lg-pull-3">
                <h1 class="section__heading">
                    <span class="section__heading-txt"><?= $arResult["UF_NAME"]; ?></span>
                </h1>
                <div class="row">
                    <div class="col-lg-11">
                        <div class="lesson-slider">
                            <?php foreach ($arResult["CARDS"] as $arCard) : ?>
                                <div class="flip-container vertical js-lesson_card"
                                    data-id="<?=$arCard["ID"];?>"
                                    data-conspect_id="<?=$arResult["ID"];?>"
                                    data-user_id="<?=$arResult["USER"]["ID"];?>">
                                    <div class="flipper">
                                        <div class="flipper-question">
                                            <div class="flipper-upper clearfix">
                                                <div class="flipper-upper__options">
                                                    <?/*if ($arResult["MASTER"] == "Y") :?>
                                                        <?if (!$arResult["UF_ORIGIN_USER"]) : ?>
                                                            <a class="flipper-upper__edit"
                                                               href="<?=$arResult["EDIT_PAGE_URL"];?>#card-<?=$arCard["ID"];?>"
                                                               title="<?= Loc::getMessage("CREATE_CONSPECT_EDIT_CARD"); ?>">
                                                                <span class="icon icon-edit"></span>
                                                            </a>
                                                        <?endif;?>
                                                        <a class="flipper-upper__del js-delete_card-lesson"
                                                           href="javascript:void(0);"
                                                           title="<?= Loc::getMessage("CREATE_CONSPECT_DELETE_CARD"); ?>">
                                                            <span class="icon icon-trashbox"></span>
                                                        </a>
                                                    <?endif;*/?>
                                                </div>
                                                <div class="flipper-upper__descr">
<!--                                                    <span class="flipper__descr-icon">?</span>-->
                                                    <?= Loc::getMessage("QUESTION"); ?>
                                                </div>
                                            </div>
                                            <div class="flipper__body clearfix">
                                                <? if ($arCard["UF_QUESTION_IMAGE"]["ID"] > 0) :?>
                                                    <div class="flipper__img-box col-xs-4">
                                                        <a class="flipper__img-link" href="<?=$arCard["UF_QUESTION_IMAGE"]["SRC"];?>">
                                                            <img class="flippper__img" src="<?=$arCard["UF_QUESTION_IMAGE"]["RESIZED"];?>" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="flipper__txt-box col-xs-8"><?=$arCard["UF_QUESTION"];?></div>
                                                <? else : ?>
                                                    <div class="flipper__txt-box col-xs-12"><span><?=$arCard["UF_QUESTION"];?></span></div>
                                                <?endif;?>
                                            </div>
                                            <div class="flipper__lower">
                                                <button class="flip-slide">
                                                    <span class="flip-btn__txt-first flip-btn__with-txt"><?= Loc::getMessage("CONSPECT_LESSON_TURN_AROUND"); ?></span>
                                                    <span class="flipper__flip-icon icon icon-lesson-space"></span>
                                                    <span class="flip-btn__txt-second flip-btn__with-txt"><?= Loc::getMessage("CONSPECT_LESSON_SPACE"); ?></span>
                                                </button>
                                            </div>
                                        </div>
                                        <div class="flipper-answer">
                                            <div class="flipper-upper clearfix">
                                                <?/*php if ($arResult["MASTER"] == "Y") : ?>
                                                <div class="flipper-upper__options">
                                                    <?if (!$arResult["UF_ORIGIN_USER"]) : ?>
                                                        <a class="flipper-upper__edit"
                                                           href="<?=$arResult["EDIT_PAGE_URL"];?>#card-<?=$arCard["ID"];?>"
                                                           title="<?= Loc::getMessage("CREATE_CONSPECT_EDIT_CARD"); ?>">
                                                            <span class="icon icon-edit"></span>
                                                        </a>
                                                    <?endif;?>
                                                    <a class="flipper-upper__del js-delete_card-lesson"
                                                       href="javascript:void(0);"
                                                       title="<?= Loc::getMessage("CREATE_CONSPECT_DELETE_CARD"); ?>">
                                                        <span class="icon icon-trashbox"></span>
                                                    </a>
                                                </div>
                                                <?php endif;*/?>
                                                <div class="flipper-upper__descr">
<!--                                                    <span class="flipper__descr-icon">!</span>-->
                                                    <?= Loc::getMessage("ANSWER"); ?>
                                                </div>
                                            </div>
                                            <div class="flipper__body clearfix">
                                                <? if ($arCard["UF_ANSWER_IMAGE"]["ID"] > 0) :?>
                                                    <div class="flipper__img-box col-xs-4">
                                                        <a class="flipper__img-link" href="<?=$arCard["UF_ANSWER_IMAGE"]["SRC"];?>">
                                                            <img class="flippper__img" src="<?=$arCard["UF_ANSWER_IMAGE"]["RESIZED"];?>" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="flipper__txt-box col-xs-8"><span><?=$arCard["UF_ANSWER"];?></span></div>
                                                <? else :?>
                                                    <div class="flipper__txt-box col-xs-12"><?=$arCard["UF_ANSWER"];?></div>
                                                <?endif;?>
                                            </div>
                                            <div class="flipper__lower">
                                                <button class="flip-slide">
                                                    <span class="flip-btn__txt-first flip-btn__with-txt"><?= Loc::getMessage("CONSPECT_LESSON_TURN_AROUND"); ?></span>
                                                    <span class="flipper__flip-icon icon icon-lesson-space"></span>
                                                    <span class="flip-btn__txt-second flip-btn__with-txt"><?= Loc::getMessage("CONSPECT_LESSON_SPACE"); ?></span>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <? endforeach; ?>
                        </div>
                        <section class="lesson-progress lesson-progress--adaptive">
                            <span class="lesson-progress__title"><?=Loc::getMessage("PROGRESS");?>:</span>
                            <div class="lesson-progress__box">
                                <div class="lesson-progress-bar" style="width: 1%;">
                                    <div class="lesson-progress-tooltip">
                                        <span class="lesson-tooltip-current">1</span><?=Loc::getMessage("OUT_OF");?>
                                        <span class="lesson-tooltip-total">0</span>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>