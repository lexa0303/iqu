<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 15.02.17
 * Time: 15:53
 */

/* CHECK IF CONSPECT WAS PURCHASED AND EXPIRED */
if ($arResult["UF_ORIGIN_CONSPECT"]) {
    $arOriginConspect = Conspects::GetByID($arResult["UF_ORIGIN_CONSPECT"]);
    if ($arOriginConspect["UF_SHOP"]){
        $AvailableConspects = new HL("sys_available_conspects");

        $arAvailable = $AvailableConspects->GetList(
            array(
                "filter" => array(
                    "UF_USER_ID" => $arResult["UF_USER_ID"],
                    "UF_CONSPECT" => $arResult["UF_ORIGIN_CONSPECT"]
                )
            )
        )->Fetch();


        if (!$arAvailable) {
            LocalRedirect($arResult["DETAIL_PAGE_URL"]);
        }
    }
}

foreach ($arResult["CARDS"] as &$arCard) :
    if ($arCard["UF_ANSWER_IMAGE"]["ID"] > 0) {
        $arCard["UF_ANSWER_IMAGE"]["RESIZED"] = LenalHelp::img($arCard["UF_ANSWER_IMAGE"]["ID"], 200, 200);
    }
    if ($arCard["UF_QUESTION_IMAGE"]["ID"] > 0) {
        $arCard["UF_QUESTION_IMAGE"]["RESIZED"] = LenalHelp::img($arCard["UF_QUESTION_IMAGE"]["ID"], 200, 200);
    }
endforeach;