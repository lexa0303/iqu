<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 15.02.17
 * Time: 15:53
 */

if ($arResult["UF_IMAGE"]["ID"] > 0){
    $image = CFile::ResizeImageGet($arResult["UF_IMAGE"]["ID"], array("height"=>300, "width"=>300), BX_RESIZE_IMAGE_PROPORTIONAL);
    $arResult["UF_IMAGE"]["RESIZED"] = $image["src"];
}

foreach ($arResult["CARDS"] as &$arCard) :
    if ($arCard["UF_ANSWER_IMAGE"]["ID"] > 0) {
        $arCard["UF_ANSWER_IMAGE"]["RESIZED"] = LenalHelp::img($arCard["UF_ANSWER_IMAGE"]["ID"], 250, 250);
    }
    if ($arCard["UF_QUESTION_IMAGE"]["ID"] > 0) {
        $arCard["UF_QUESTION_IMAGE"]["RESIZED"] = LenalHelp::img($arCard["UF_QUESTION_IMAGE"]["ID"], 250, 250);
    }
endforeach;

/* CHECK IF USER CAME FROM LIBRARY */
$referer = explode("?", $_SERVER["HTTP_REFERER"]);
$refererUrl = $referer[0];
if (strpos($refererUrl, SERVER_URL . "/library/") !== false){
    $arResult["FROM_LIBRARY"] = true;
} else {
    $arResult["FROM_LIBRARY"] = false;
}
