<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;

$this->setFrameMode(true);
global $USER;
$currency = Bitrix\Currency\CurrencyManager::getBaseCurrency();
?>
<? if (empty($arResult["ERRORS"])) : ?>
    <div class="conspect-preview__wrap">
        <div class="container">
            <div class="conspect-cards__wrapper row">
                <div class="col-sm-4 col-md-4 col-lg-3">
                    <div class="fcard__box-wrap fcard__box-wrap-shop js-conspect">
                        <div class="fcard__box fcard__box--large ripple rounded">
                            <a class="fcard__img-wrap" href="#"
                               style="background-image: url(<?= $arResult["UF_IMAGE"]["RESIZED"]; ?>);">
                            </a>
                            <div class="fcard__lower">
                                <div class="fcard__heading-wrap">
                                    <a class="fcard__heading js-conspect_name"
                                       href="javascript:void(0);"><?= $arResult["UF_NAME"]; ?></a>
                                </div>
                                <div class="fcard__author-wrap">
                                    <a class="fcard__author"
                                       href="<?= $arResult["USER"]["DETAIL_PAGE_URL"]; ?>"><?= $arResult["USER"]["TITLE"]; ?></a>
                                </div>
                                <div class="fcard__price"><?= CurrencyFormat($arResult["UF_SHOP_PRICE"], $currency); ?></div>
                                <div class="fcard__extra-info">
                                    <span class="fcard__extra-date"><?= $arResult["UF_UPDATE_TIME"]->toString(); ?></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-8 col-md-7 col-lg-7">
                    <div class="conspect-preview__info shop-conspect_preview">
                        <div class="user-conspects__title"><?= $arResult["UF_NAME"]; ?></div>
                        <div class="fcard__author-wrap shop">
                            <a class="fcard__author"
                               href="<?= $arResult["USER"]["DETAIL_PAGE_URL"]; ?>"><?= $arResult["USER"]["TITLE"]; ?></a>
                            <a class="fcard__unsubscribe js-unsubscribe js-sub"
                               href="javascript:void(0);" data-user_id="<?= $arResult["USER"]["ID"]; ?>">
                                <span class="icon icon-unsubscribe"></span>
                                <span>Отписаться</span>
                            </a>
                            <a class="fcard__unsubscribe js-subscribe js-sub"
                               href="javascript:void(0);" data-user_id="<?= $arResult["USER"]["ID"]; ?>">
                                <span class="icon icon-subscribe"></span>
                                <span>Подписаться</span>
                            </a>
                        </div>
                        <div class="conspect-preview__buy-buttons">
                            <div class="courses__button-holder">
                                <div class="courses-conspects__title order"><?= $arResult["TRIAL"]["PRICE_FORMATTED"]; ?></div>
                                <a href="javascript:void(0);"
                                   data-shop_conspect="<?= $arResult["TRIAL"]["ID"]; ?>"
                                   data-conspect_id="<?= $arResult["ID"]; ?>"
                                   class="button button--roundedbot-btn-one categories-listing__btn--create-conspect js-shop_buy">взять
                                    в аренду</a>
                            </div>
                            <div class="courses__button-holder">
                                <div class="courses-conspects__title buy"><?= $arResult["BUY"]["PRICE_FORMATTED"]; ?></div>
                                <a href="javascript:void(0);"
                                   data-shop_conspect="<?= $arResult["BUY"]["ID"]; ?>"
                                   data-conspect_id="<?= $arResult["ID"]; ?>"
                                   class="categories-listing__btn--create-category button js-shop_buy">выкупить навсегда</a>
                            </div>


                        </div>
                        <? if ($arResult["UF_DESCRIPTION"]) : ?>
                            <div class="shop-conspect__subtitle">О конспекте:</div>
                            <div class="shop-conspect__desc"><?= $arResult["UF_DESCRIPTION"]; ?></div>
                        <? endif; ?>
                    </div>
                </div>
            </div>
            <article class="conspect-cards__content">
                <? foreach ($arResult["CARDS"] as $arCard) : ?>
                    <div class="conspect-cards__item">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="conspect-card__wrapper">
                                    <div class="conspect-card__title"><?= Loc::getMessage("QUESTION"); ?></div>
                                    <div class="conspect-card__body">
                                        <div class="conspect-card__img">
                                            <img src="<?= $arCard["UF_QUESTION_IMAGE"]["RESIZED"]; ?>" alt="">
                                        </div>
                                        <div class="conspect-card__text">
                                            <span><?= $arCard["UF_QUESTION"]; ?></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="conspect-card__wrapper">
                                    <div class="conspect-card__title"><?= Loc::getMessage("ANSWER"); ?></div>
                                    <div class="conspect-card__body">
                                        <div class="conspect-card__img">
                                            <img src="<?= $arCard["UF_ANSWER_IMAGE"]["RESIZED"]; ?>" alt="">
                                        </div>
                                        <div class="conspect-card__text">
                                            <span><?= $arCard["UF_ANSWER"]; ?></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <? endforeach; ?>
            </article>
            <div class="conspect-preview__tip">
                Что бы увидеть весь конспект, его нужно приобрести.
            </div>
            <? if (count($arResult["CATEGORY_CONSPECTS"]) > 0) : ?>
                <div class="user-conspects__title">Другие конспекты в этой категории</div>
                <section class="main-tabs__content shop">
                    <div class="fcards__wrapper row">
                        <? foreach ($arResult["CATEGORY_CONSPECTS"] as $conspect) : ?>
                            <? $APPLICATION->IncludeComponent(
                                "custom:conspect_card_shop",
                                "shop",
                                Array(
                                    "CACHE_TIME"         => $arParams["CACHE_TIME"],
                                    "CACHE_TYPE"         => $arParams["CACHE_TYPE"],
                                    "CARDS_HLBLOCK"      => "4",
                                    "CATEGORIES_HLBLOCK" => "3",
                                    "CONSPECTS_HLIBLOCK" => "6",
                                    "CONSPECT_ID"        => $conspect,
                                    "SEF_MODE"           => $arParams["SEF_URL_TEMPLATES"],
                                    "MASTER"             => $arParams["MASTER"],
                                    "SEF_FOLDER"         => $arParams["SEF_FOLDER"],
                                )
                            ); ?>
                        <? endforeach; ?>
                        <div class="clearfix"></div>
                    </div>
                </section>
            <? endif; ?>
        </div>
        <section class="main-bot-btns__section">
            <div class="container">
                <div class="main-bot-btns__wrap">
                    <a class="ripple button button--rounded main-bot-btns__link bot-btn-one js-auth" href="/shop/">Магазин</a>
                    <a class="ripple button button--rounded main-bot-btns__link bot-btn-two" href="/library/">Бесплатная
                        библиотека</a>
                </div>
            </div>
        </section>
    </div>
<? else : ?>
    <div class="conspect-preview__wrap">
        <div class="container">
            <? foreach ($arResult["ERRORS"] as $error) : ?>
                <div style="font-size: 24px; padding: 20px; text-align: center;">
                    <?= $error; ?>
                </div>
            <? endforeach; ?>
        </div>
    </div>
    <? require($_SERVER["DOCUMENT_ROOT"] . "/include/shop_library_buttons.php"); ?>
<? endif; ?>
