/**
 * Created by alex on 27.02.17.
 */
var items = document.querySelectorAll(".js-conspect");
var tabButtons = document.querySelectorAll(".js-my_conspects-tab");
var noSearchResult = document.querySelector(".js-conspect_no-search");
var searchInput = document.getElementById("conspects_search");
var activeTab;

(function(){
    for (var i in tabButtons){
        if (tabButtons.hasOwnProperty(i)){
            var tabButton = tabButtons[i];
            tabButton.addEventListener("click", function(e){
                if (activeTab !== undefined) {
                    activeTab.classList.remove("active");
                }
                this.classList.add("active");
                changeItems(this.dataset.status);
                activeTab = this;
            });
        }
    }

    if (searchInput) {
        searchInput.addEventListener("search", function (e) {
            searchItems(e);
        });
        searchInput.addEventListener("keyup", function (e) {
            searchItems(e);
        });
        searchInput.addEventListener("keydown", function (e) {
            searchItems(e);
        });
    }
})();

function searchItems(e){
    var item, i, name, search, found;
    if (activeTab !== undefined){
        activeTab.classList.remove("active");
    }

    found = false;
    if (typeof e === "object") {
        search = e.target.value;
    } else {
        search = e;
    }

    search = search.toLowerCase();

    for (i in items){
        if (items.hasOwnProperty(i)){
            item = items[i];
            name = item.querySelector(".js-conspect_name").innerHTML.toLowerCase();
            if (name.indexOf(search) === -1) {
                item.classList.add("hidden");
            } else {
                item.classList.remove("hidden");
                found = true;
            }
        }
    }

    if (found){
        noSearchResult.classList.add("hidden");
    } else {
        noSearchResult.classList.remove("hidden");
    }
}

function changeItems(status){
    var allowedStatuses = ['14', '18', '19'];
    var item, i;
    noSearchResult.classList.add("hidden");

    if (allowedStatuses.indexOf(status) !== -1){
        for (i in items){
            if (items.hasOwnProperty(i)){
                item = items[i];

                if (item.dataset.status === status){
                    item.classList.remove("hidden");
                } else {
                    item.classList.add("hidden");
                }
            }
        }
    } else {
        for (i in items){
            if (items.hasOwnProperty(i)){
                item = items[i];
                if (allowedStatuses.indexOf(item.dataset.status) === -1){
                    item.classList.remove("hidden");
                } else {
                    item.classList.add("hidden");
                }
            }
        }
    }
}