<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use Bitrix\Main\Localization\Loc;
$this->setFrameMode(true);
?>
<section>
    <div class="container">
        <section class="my-cards__header">
            <div class="my-cards__tabs-wrapper">
                <ul class="main-tabs">
                    <li class="main-tabs__item">
                        <a class="main-tabs__link js-my_conspects-tab" data-status="14" href="javascript:void(0)">
                            <span class="main-tabs__icon icon icon-studying"></span>
                            Не опубликованные
                        </a>
                    </li>
                    <li class="main-tabs__item">
                        <a class="main-tabs__link js-my_conspects-tab" data-status="0" href="javascript:void(0)">
                            <span class="main-tabs__icon icon icon-studying"></span>
                            На проверке
                        </a>
                    </li>
                    <li class="main-tabs__item">
                        <a class="main-tabs__link js-my_conspects-tab" data-status="18" href="javascript:void(0)">
                            <span class="main-tabs__icon icon icon-studying"></span>
                            Ожидают подтверждения
                        </a>
                    </li>
                    <li class="main-tabs__item">
                        <a class="main-tabs__link js-my_conspects-tab" data-status="19" href="javascript:void(0)">
                            <span class="main-tabs__icon icon icon-done"></span>
                            Опубликованные
                        </a>
                    </li>
                </ul>
            </div>
            <form class="my-cards__search-wrapper" id="conspect_search-form">
                <label class="my-cards__search-descr" for="myCardsSearch">
                    <button class="my-cards__search-icon icon icon-search"></button>
                </label>
                <input class="my-cards__search" type="search"
                       name="q" id="conspects_search"
                       value="<?=($_GET["q"]) ? $_GET["q"] : "";?>"
                       placeholder="<?=Loc::getMessage("CONSPECT_LIST_SEARCH");?>">
            </form>
            <?if (strlen($_GET["q"]) > 0) : ?>
                <script>
                    searchItems('<?=$_GET["q"];?>');
                </script>
            <?endif;?>
        </section>
        <section class="my-cards__content">
            <article class="main-tabs__content">
                <div class="fcards__wrapper row">
                    <? foreach ($arResult["ITEMS"] as $arConspect) : ?>
                        <?$APPLICATION->IncludeComponent(
                            "custom:conspect_card",
                            "",
                            Array(
                                "CACHE_TIME" => $arParams["CACHE_TIME"],
                                "CACHE_TYPE" => $arParams["CACHE_TYPE"],
                                "CARDS_HLBLOCK" => "4",
                                "CATEGORIES_HLBLOCK" => "3",
                                "CONSPECTS_HLIBLOCK" => "6",
                                "CONSPECT_ID" => $arConspect["ID"],
                                "SEF_MODE" => $arParams["SEF_URL_TEMPLATES"],
                                "MASTER" => $arParams["MASTER"],
                                "SHOW_STUDY" => "Y",
                                "IS_AUTHOR" => $arParams["IS_AUTHOR"]
                            )
                        );?>
                    <?endforeach;?>
                    <div class="clearfix"></div>
                    <div class="fcard__no-search-result js-conspect_no-search hidden"><?=Loc::getMessage("NO_SEARCH_RESULT");?></div>
                </div>
            </article>
        </section>
    </div>
    <? require($_SERVER["DOCUMENT_ROOT"] . "/include/shop_library_buttons.php"); ?>
</section>
<script>
    globals.page = "conspect";

    document.querySelector(".js-my_conspects-tab").click();
</script>