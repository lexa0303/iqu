<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use Bitrix\Main\Localization\Loc;
$this->setFrameMode(true);
?>
<section class="choosen-category__heading">
    <div class="container">
        <div class="section__heading">
            <span class="section__heading-txt"><?=$arResult["CATEGORY"]["UF_NAME"];?></span>
        </div>
        <?if ($arResult["MASTER"] == "Y") :?>
            <div class="choosen-category__controls">
                <div class="my-category__controls">
                    <a class="my-category__btn my-category__btn--conspect-create" href="<?=$arResult["CATEGORY"]["ADD_CONSPECT_URL"];?>">
                        <span class="icon icon-new-note"></span>
                    </a>
                    <a class="my-category__btn my-category__btn--edit open-modal"
                       data-modal="edit-category-modal"
                       data-ajax="Y"
                       data-action="category_edit"
                       data-category_id="<?=$arResult["CATEGORY"]["ID"];?>"
                       data-user_id="<?=$arResult["CATEGORY"]["USER"]["ID"];?>">
                        <span class="icon icon-edit"></span>
                    </a>
                    <a class="my-category__btn my-category__btn--delete open-modal"
                       data-modal="delete-category-modal"
                       data-ajax="Y"
                       data-action="category_delete"
                       data-category_id="<?=$arCategory["ID"];?>"
                       data-user_id="<?=$arCategory["USER"]["ID"];?>">
                        <span class="icon icon-trashbox"></span>
                    </a>
                </div>
            </div>
        <?endif;?>
    </div>
</section>
<article class="choosen-category__content">
    <div class="container">
        <div class="fcards__wrapper row">
            <? foreach ($arResult["ITEMS"] as $arConspect) : ?>
                <?$APPLICATION->IncludeComponent(
                    "custom:conspect_card",
                    "",
                    Array(
                        "CACHE_TIME" => $arParams["CACHE_TIME"],
                        "CACHE_TYPE" => $arParams["CACHE_TYPE"],
                        "CARDS_HLBLOCK" => "4",
                        "CATEGORIES_HLBLOCK" => "3",
                        "CONSPECTS_HLIBLOCK" => "6",
                        "CONSPECT_ID" => $arConspect["ID"],
                        "SEF_MODE" => $arParams["SEF_URL_TEMPLATES"],
                        "MASTER" => $arParams["MASTER"],
                        "SHOW_STUDY" => "Y"
                    )
                );?>
            <?endforeach;?>
        </div>
    </div>
</article>