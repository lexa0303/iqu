<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use Bitrix\Main\Localization\Loc;
$this->setFrameMode(true);
?>
<section class="user-conspects__head">
    <div class="container">
        <div class="user-conspects__head-wrap">
            <div class="user-conspects__photo-wrap" style="background-image: url(<?=$arResult["USER"]["IMAGE"];?>)"></div>
            <div class="user-conspects__info-wrap">
                <div class="user-conspects__title">
                    <?=Loc::getMessage("CONSPECT_DETAIL_ANOTHER_HEADING");?> <span><?=$arResult["USER"]["TITLE"];?></span>
                </div>
                <a class="user-conspects__btn--back js-go_back" href="javascript:void(0);">
                    <span class="user-conspects__icon-back"></span>
                    <span><?=Loc::getMessage("GO_BACK");?></span>
                </a>
                <div class="user-conspects__info">
                    <div class="user-conspects__info-numbers">
                        <div class="user-conspects__info-title"><?=Loc::getMessage("CONSPECT_DETAIL_ANOTHER_CONSPECT_COUNT");?></div>
                        <span class="user-conspects__info-icon icon icon-conspect"></span>
                        <span class="user-conspects__ammount"><?=$arResult["COUNT_CONSPECTS"];?></span>
                    </div>
                    <div class="user-conspects__info-numbers">
                        <div class="user-conspects__info-title"><?=Loc::getMessage("CONSPECT_DETAIL_ANOTHER_DOWNLOADS_COUNT");?></div>
                        <span class="user-conspects__info-icon icon icon-download2"></span>
                        <span class="user-conspects__ammount"><?=$arResult["COUNT_DOWNLOADS"];?></span>
                    </div>
                    <div class="user-conspects__info-numbers">
                        <div class="user-conspects__info-title"><?=Loc::getMessage("CONSPECT_DETAIL_ANOTHER_SUBSCRIBERS_COUNT");?></div>
                        <span class="user-conspects__info-icon icon icon-users"></span>
                        <span class="user-conspects__ammount"><?=$arResult["USER"]["COUNT_SUBSCRIBERS"];?></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="container">
        <section class="my-cards__content">
            <article class="main-tabs__content">
                <div class="fcards__wrapper row">
                    <? foreach ($arResult["ITEMS"] as $arConspect) : ?>
                        <?$APPLICATION->IncludeComponent(
                            "custom:conspect_card",
                            "",
                            Array(
                                "CACHE_TIME" => $arParams["CACHE_TIME"],
                                "CACHE_TYPE" => $arParams["CACHE_TYPE"],
                                "CARDS_HLBLOCK" => "4",
                                "CATEGORIES_HLBLOCK" => "3",
                                "CONSPECTS_HLIBLOCK" => "6",
                                "CONSPECT_ID" => $arConspect["ID"],
                                "SEF_MODE" => $arParams["SEF_URL_TEMPLATES"],
                                "MASTER" => $arParams["MASTER"]
                            )
                        );?>
                    <?endforeach;?>
                    <div class="clearfix"></div>
                    <div class="fcard__no-search-result js-conspect_no-search hidden"><?=Loc::getMessage("NO_SEARCH_RESULT");?></div>
                </div>
<!--                <div class="section__more-btn-wrap">-->
<!--                    <a class="section__more-btn" href="javascript:void(0);">--><?//=Loc::getMessage("MORE_BUTTON");?><!--</a>-->
<!--                </div>-->
            </article>
        </section>
    </div>
</section>
<? require($_SERVER["DOCUMENT_ROOT"] . "/include/shop_library_buttons.php"); ?>