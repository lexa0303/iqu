<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use Bitrix\Main\Localization\Loc;
$this->setFrameMode(true);
?>
<section>
    <div class="container">
        <section class="my-cards__header">
            <div class="my-cards__tabs-wrapper">
                <ul class="main-tabs">
                    <li class="main-tabs__item">
                        <a class="main-tabs__link js-my_conspects-tab" data-study="0" data-sort="sort" href="javascript:void(0)">
                            <span class="main-tabs__icon icon icon-studying"></span>
                            <?=Loc::getMessage("CONSPECT_LIST_LEARNING_BUTTON");?>
                        </a>
                    </li>
                    <li class="main-tabs__item">
                        <a class="main-tabs__link js-my_conspects-tab" data-study="1" data-sort="name" href="javascript:void(0)">
                            <span class="main-tabs__icon icon icon-done"></span>
                            <?=Loc::getMessage("CONSPECT_LIST_LEARNED_BUTTON");?>
                        </a>
                    </li>
                </ul>
            </div>
            <form class="my-cards__search-wrapper" id="conspect_search-form">
                <label class="my-cards__search-descr" for="myCardsSearch">
                    <button class="my-cards__search-icon icon icon-search"></button>
                </label>
                <input class="my-cards__search" type="search"
                       name="q" id="conspects_search"
                       value="<?=($_GET["q"]) ? $_GET["q"] : "";?>"
                       placeholder="<?=Loc::getMessage("CONSPECT_LIST_SEARCH");?>">
            </form>
            <?if (strlen($_GET["q"]) > 0) : ?>
                <script>
                    searchItems('<?=$_GET["q"];?>');
                </script>
            <?endif;?>
        </section>
        <section class="my-cards__content">
            <article class="main-tabs__content">
                <div class="fcards__wrapper row">
                    <? foreach ($arResult["ITEMS"] as $key => $arConspect) : ?>
                        <?$APPLICATION->IncludeComponent(
                            "custom:conspect_card",
                            "",
                            Array(
                                "CACHE_TIME" => $arParams["CACHE_TIME"],
                                "CACHE_TYPE" => $arParams["CACHE_TYPE"],
                                "CARDS_HLBLOCK" => "4",
                                "CATEGORIES_HLBLOCK" => "3",
                                "CONSPECTS_HLIBLOCK" => "6",
                                "CONSPECT_ID" => $arConspect["ID"],
                                "SEF_MODE" => $arParams["SEF_URL_TEMPLATES"],
                                "MASTER" => $arParams["MASTER"],
                                "SHOW_STUDY" => "Y",
                                "INDEX" => ($key + 1),

                            )
                        );?>
                    <?endforeach;?>
                    <div class="clearfix"></div>
                    <div class="fcard__no-search-result js-conspect_no-search hidden"><?=Loc::getMessage("NO_SEARCH_RESULT");?></div>
                </div>
            </article>
        </section>
    </div>
    <? require($_SERVER["DOCUMENT_ROOT"] . "/include/shop_library_buttons.php"); ?>
</section>
<script>
    globals.page = "conspect";

    document.querySelector(".js-my_conspects-tab").click();
</script>