/**
 * Created by alex on 27.02.17.
 */
var items = document.querySelectorAll(".js-conspect");
var tabButtons = document.querySelectorAll(".js-my_conspects-tab");
var noSearchResult = document.querySelector(".js-conspect_no-search");
var searchInput = document.getElementById("conspects_search");
var activeTab;

(function(){
    for (var i in tabButtons){
        if (tabButtons.hasOwnProperty(i)){
            var tabButton = tabButtons[i];
            tabButton.addEventListener("click", function(e){
                if (activeTab !== undefined) {
                    activeTab.classList.remove("active");
                }
                this.classList.add("active");
                sortItems(this.dataset.sort);
                changeItems(this.dataset.study);
                activeTab = this;
            });
        }
    }

    if (searchInput) {
        searchInput.addEventListener("search", function (e) {
            searchItems(e);
        });
        searchInput.addEventListener("keyup", function (e) {
            searchItems(e);
        });
        searchInput.addEventListener("keydown", function (e) {
            searchItems(e);
        });
    }
})();

function searchItems(e){
    var item, i, name, search, found;
    if (activeTab !== undefined){
        activeTab.classList.remove("active");
    }

    found = false;
    if (typeof e === "object") {
        search = e.target.value;
    } else {
        search = e;
    }

    search = search.toLowerCase();

    for (i in items){
        if (items.hasOwnProperty(i)){
            item = items[i];
            name = item.querySelector(".js-conspect_name").innerHTML.toLowerCase();
            if (name.indexOf(search) === -1) {
                item.classList.add("hidden");
            } else {
                item.classList.remove("hidden");
                found = true;
            }
        }
    }

    if (found){
        noSearchResult.classList.add("hidden");
    } else {
        noSearchResult.classList.remove("hidden");
    }
}

function changeItems(studied){
    var item, i;
    noSearchResult.classList.add("hidden");

    switch (studied){
        case undefined:
            for (i in items){
                if (items.hasOwnProperty(i)){
                    item = items[i];
                    item.classList.remove("hidden");
                }
            }
            break;
        default:
            for (i in items){
                if (items.hasOwnProperty(i)){
                    item = items[i];

                    if (item.dataset.study === studied){
                        item.classList.remove("hidden");
                    } else {
                        item.classList.add("hidden");
                    }
                }
            }
            break;
    }
}

var parent = document.querySelector(".fcards__wrapper");

function sortItems(order){
    var item;
    var newItems = [];
    items.forEach(function(el, i){
        newItems.push(el);
    });
    if (order === "name") {
        newItems.sort(compareItemsName);
    } else {
        newItems.sort(compareItemsIndex);
    }

    newItems.forEach(function(el, i){
        parent.appendChild(el);
    });
}

function compareItemsName(a, b){
    var aName = a.querySelector(".js-conspect_name").innerText.toLowerCase();
    var bName = b.querySelector(".js-conspect_name").innerText.toLowerCase();


    var aRus = true;
    var bRus = true;

    if (aName[0] < "а"){
        aRus = false;
    }
    if (bName[0] < "а"){
        bRus = false;
    }

    if ((aRus && bRus) || (!aRus && !bRus)){
        if (aName < bName){
            return -1;
        } else {
            return 1;
        }
    }
    if (aRus && !bRus){
        return -1;
    }
    if (!aRus && bRus){
        return 1;
    }
}
function compareItemsIndex(a, b){
    var aName = parseInt(a.dataset.sort_index);
    var bName = parseInt(b.dataset.sort_index);

    if (aName < bName){
        return -1;
    } else {
        return 1;
    }
}