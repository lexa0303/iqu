<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$this->setFrameMode(true);
use \Bitrix\Main\Localization\Loc;

global $APPLICATION;
?>
<section class="categories-listing">
    <div class="container">
        <div class="section__heading">
            <span class="section__heading-txt"><? $APPLICATION->ShowTitle(); ?></span>
        </div>
        <? if (count($arResult["CATEGORIES"]) > 0) : ?>
            <? foreach ($arResult["CATEGORIES"] as $arCategory) : ?>
                <article class="my-category">
                    <div class="my-category__heading">
                        <span class="my-category__title"><?= $arCategory["UF_NAME"]; ?></span>
                        <? if ($arParams["MASTER"] == "Y") : ?>
                            <div class="my-category__controls">
                                <a class="my-category__btn my-category__btn--conspect-create"
                                   title="<?= Loc::getMessage("TOOLTIP_ADD_CONSPECT"); ?>"
                                   href="<?= $arCategory["ADD_CONSPECT_URL"] ?>">
                                    <span class="icon icon-new-note"></span>
                                </a>
                                <a class="my-category__btn my-category__btn--edit open-modal"
                                   data-modal="edit-category-modal"
                                   data-ajax="Y"
                                   data-action="category_edit"
                                   data-category_id="<?= $arCategory["ID"]; ?>"
                                   data-user_id="<?= $arCategory["USER"]["ID"]; ?>">
                                    <span class="icon icon-edit"
                                          title="<?= Loc::getMessage("TOOLTIP_EDIT_CATEGORY"); ?>"></span>
                                </a>
                                <a class="my-category__btn my-category__btn--delete open-modal"
                                   data-modal="delete-category-modal"
                                   data-ajax="Y"
                                   data-action="category_delete"
                                   data-category_id="<?= $arCategory["ID"]; ?>"
                                   data-user_id="<?= $arCategory["USER"]["ID"]; ?>">
                                    <span class="icon icon-trashbox"
                                          title="<?= Loc::getMessage("TOOLTIP_DELETE_CATEGORY"); ?>"></span>
                                </a>
                            </div>
                        <? endif; ?>
                    </div>
                    <div class="my-category__content">
                        <? if (count($arCategory["CONSPECTS"]) > 0) : ?>
                            <div class="fcards__wrapper">
                                <? foreach ($arCategory["CONSPECTS"] as $arConspect) : ?>
                                    <? $APPLICATION->IncludeComponent(
                                        "custom:conspect_card",
                                        "",
                                        Array(
                                            "CACHE_TIME" => $arParams["CACHE_TIME"],
                                            "CACHE_TYPE" => $arParams["CACHE_TYPE"],
                                            "CARDS_HLBLOCK" => "4",
                                            "CATEGORIES_HLBLOCK" => "3",
                                            "CONSPECTS_HLIBLOCK" => "6",
                                            "CONSPECT_ID" => $arConspect["ID"],
                                            "SEF_MODE" => $arParams["SEF_URL_TEMPLATES"],
                                            "MASTER" => $arParams["MASTER"],
                                            "SHOW_STUDY" => $arParams["MASTER"]
                                        )
                                    ); ?>
                                <? endforeach; ?>
                            </div>
                            <? if (count($arCategory["UF_CONSPECTS"]) > 4) : ?>
                                <div class="section__more-btn-wrap">
                                    <a class="section__more-btn"
                                       href="<?= $arCategory["DETAIL_PAGE_URL"]; ?>"><?= Loc::getMessage("MORE_BUTTON"); ?></a>
                                </div>
                            <? else : ?>
                                <div class="clearfix"></div>
                            <? endif; ?>
                        <? else : ?>
                            <span class="my-category__warning"><?= Loc::getMessage("CONSPECTS_EMPTY"); ?></span>
                        <? endif; ?>
                    </div>
                </article>
            <? endforeach; ?>
        <? else : ?>
            <article class="my-category">
                <?= $arResult["ERROR"]; ?>
            </article>
        <? endif; ?>
        <article class="categories-listing__bottom">
            <? if ($arParams["MASTER"] == "Y") : ?>
                <a class="categories-listing__btn--create-conspect button" href="<?= getLink("CREATE_CONSPECT"); ?>">
                    <span class="icon icon-new-note"></span>
                    <span><?= Loc::getMessage("ASIDE_MENU_CREATE_CONSPECT"); ?></span>
                </a>
            <? endif; ?>
            <?= $arResult["NAV_STRING"]; ?>
            <? if ($arParams["MASTER"] == "Y") : ?>
                <a class="categories-listing__btn--create-category button" href="<?= getLink("CREATE_CATEGORY"); ?>">
                    <span class="icon icon-new-category"></span>
                    <span><?= Loc::getMessage("ASIDE_MENU_CREATE_CATEGORY"); ?></span>
                </a>
            <? endif; ?>
        </article>
    </div>
</section>
<? if ($arParams["MASTER"] == "Y") : ?>
    <script>
        globals.page = "category";
    </script>
<? endif; ?>
