<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$this->setFrameMode(true);
use \Bitrix\Main\Localization\Loc;
?>
<?foreach ($arResult["CATEGORIES"] as $arCategory) : ?>
    <section class="main-section popular-shop__section">
        <div class="container">
            <h2 class="section__heading">
                <span class="section__heading-txt"><?=$arCategory["UF_NAME"];?></span>
            </h2>
            <div class="fcards__wrapper">
                <?foreach ($arCategory["CONSPECTS"] as $arConspect) : ?>
                    <?if ($arConspect["USER"]["ID"] == $USER->GetID())
                        $master = "Y";
                    else
                        $master = "N";?>
                    <?$APPLICATION->IncludeComponent(
                        "custom:conspect_card",
                        "",
                        Array(
                            "CACHE_TIME" => $arParams["CACHE_TIME"],
                            "CACHE_TYPE" => $arParams["CACHE_TYPE"],
                            "CARDS_HLBLOCK" => "4",
                            "CATEGORIES_HLBLOCK" => "3",
                            "CONSPECTS_HLIBLOCK" => "6",
                            "CONSPECT_ID" => $arConspect["ID"],
                            "SEF_MODE" => $arParams["SEF_URL_TEMPLATES"],
                            "MASTER" => $master
                        )
                    );?>
                <?endforeach;?>
            </div>
            <div class="section__more-btn-wrap">
                <a class="section__more-btn" href="<?=$arCategory["DETAIL_PAGE_URL"];?>"><?=Loc::getMessage("MORE_BUTTON");?></a>
            </div>
        </div>
    </section>
<?endforeach;?>