<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use Bitrix\Main\Localization\Loc;
$this->setFrameMode(true);
?>
<section class="library__wrapper">
    <div class="container">
        <div class="section__heading">
            <span class="section__heading-txt"><?=Loc::getMessage("NEW_HEADING");?></span>
        </div>
        <section class="my-cards__content">
            <?if (count($arResult["ITEMS"]) > 0) : ?>
                <article class="main-tabs__content js-pagination_container">
                    <?if ($_POST["ajax"] == "Y")
                        $APPLICATION->RestartBuffer();?>
                    <div class="fcards__wrapper row">
                        <? foreach ($arResult["ITEMS"] as $arConspect) : ?>
                            <?$APPLICATION->IncludeComponent(
                                "custom:conspect_card",
                                "",
                                Array(
                                    "CACHE_TIME" => $arParams["CACHE_TIME"],
                                    "CACHE_TYPE" => $arParams["CACHE_TYPE"],
                                    "CARDS_HLBLOCK" => "4",
                                    "CATEGORIES_HLBLOCK" => "3",
                                    "CONSPECTS_HLIBLOCK" => "6",
                                    "CONSPECT_ID" => $arConspect["ID"],
                                    "SEF_MODE" => $arParams["SEF_URL_TEMPLATES"],
                                    "MASTER" => $arParams["MASTER"]
                                )
                            );?>
                        <?endforeach;?>
                    </div>
                    <?=$arResult["NAV_STRING"];?>
                    <?if ($_POST["ajax"] == "Y")
                        die();?>
                </article>
            <?else : ?>
                Новинок нет.
            <?endif;?>
        </section>
    </div>
</section>
<script>
    globals.page = "new";
</script>