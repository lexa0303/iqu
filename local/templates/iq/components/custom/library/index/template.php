<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use Bitrix\Main\Localization\Loc;
$this->setFrameMode(true);
?>
<div class="fcards__wrapper">
    <?foreach ($arResult["ITEMS"] as $arConspect) : ?>
        <?$APPLICATION->IncludeComponent(
            "custom:conspect_card",
            "",
            Array(
                "CACHE_TIME" => $arParams["CACHE_TIME"],
                "CACHE_TYPE" => $arParams["CACHE_TYPE"],
                "CARDS_HLBLOCK" => "4",
                "CATEGORIES_HLBLOCK" => "3",
                "CONSPECTS_HLIBLOCK" => "6",
                "CONSPECT_ID" => $arConspect["ID"],
                "SEF_MODE" => $arParams["SEF_URL_TEMPLATES"],
                "MASTER" => "N"
            )
        );?>
    <?endforeach;?>
</div>