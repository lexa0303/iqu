<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 15.02.17
 * Time: 15:53
 */

if ($arResult["UF_IMAGE"] == 0)
    $arResult["UF_IMAGE"] = null;

if ($arResult["UF_IMAGE"]["ID"])
    $arResult["UF_IMAGE"]["RESIZED"] = LenalHelp::img($arResult["UF_IMAGE"]["ID"], 500, 500);

foreach ($arResult["CARDS"] as &$arCard) :
    if ($arCard["UF_ANSWER_IMAGE"]["ID"] > 0) {
        $arCard["UF_ANSWER_IMAGE"]["RESIZED"] = LenalHelp::img($arCard["UF_ANSWER_IMAGE"]["ID"], 200, 200);
    }
    if ($arCard["UF_QUESTION_IMAGE"]["ID"] > 0) {
        $arCard["UF_QUESTION_IMAGE"]["RESIZED"] = LenalHelp::img($arCard["UF_QUESTION_IMAGE"]["ID"], 200, 200);
    }
endforeach;