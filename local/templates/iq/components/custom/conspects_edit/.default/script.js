/**
 * Created by alex on 05.04.17.
 */

var body = $("body");

function ConspectEditor(params){
    for (var i in params){
        if (params.hasOwnProperty(i)){
            if (typeof params[i] === 'string' && params[i].substr(0, 1) !== ".")
                params[i] = "." + params[i];
        }
    }
    this.params = params;
    this.Init();
}

ConspectEditor.prototype.Init = function(){
    var editor = this;
    var allElementsSelector = editor.params.question + ", " +
        editor.params.answer + ", " +
        editor.params.question_image + ", " +
        editor.params.question_image_delete + ", " +
        editor.params.answer_image + ", " +
        editor.params.answer_image_delete;

    editor.saveTimeouts = {};

    body.on("focusout", allElementsSelector, function(e){
        var self = this;
        var card = $(self).closest(editor.params.card);
        if (!card){
            return false;
        }

        editor.CheckCard(card);
    });

    body.on("focus", allElementsSelector, function(e){
        var self = this;
        var card = $(self).closest(editor.params.card);
        if (!card){
            return false;
        }

        var card_id = card.data('id');

        if (editor.saveTimeouts[card_id] !== undefined){
            clearTimeout(editor.saveTimeouts[card_id]);
            console.log("stop - " + card_id);
        }
    });
};

ConspectEditor.prototype.CheckCard = function(card){
    var editor = this;
    var card_id = card.data('id');

    if (editor.saveTimeouts[card_id] !== undefined){
        clearTimeout(editor.saveTimeouts[card_id]);
    }
    editor.saveTimeouts[card_id] = setTimeout(function(){
        editor.CardSaver(card);
        delete(editor.saveTimeouts[card_id]);
    }, 500);
};

/**
 * Attempts to save the card
* @return {boolean} - card save status
* */
ConspectEditor.prototype.CardSaver = function (card) {
    var editor = this;
    var card_id = card.data('id');
    var item = {
        question: {},
        answer: {}
    };
    var error;
    var status = true;

    item.question.text = card.find(".js-question_text").text();
    item.question.image = card.find(".js-question_image").attr("src");
    item.question.image_name = card.find(".js-question_image-input").val();
    item.answer.text = card.find(".js-answer_text").text();
    item.answer.image = card.find(".js-answer_image").attr("src");
    item.answer.image_name = card.find(".js-answer_image-input").val();

    error = {
        number: card.data('number'),
        question: false,
        answer: false
    };

    if ((item.question.text === "" && item.question.image_name === "" && !item.question.image)
        || (item.answer.text === "" && item.answer.image_name === "" && item.answer.image === "")){
        if (item.question.text === "" && item.question.image_name === "" && !item.question.image){
            error.question = true;
        }
        if (item.answer.text === "" && item.answer.image_name === "" && item.answer.image === ""){
            error.answer = true;
        }
        status = false;
    }

    if (status === false){
        return false;
    }

    editor.SaveCard(item);
};

ConspectEditor.prototype.SaveCard = function(item){
    var editor = this;
    var ajaxOptions = {
        callback: ajaxHandler
    };
    var formData = new FormData();
    var conspectData = {
        conspect: [item]
    };
    formData = toFormData(conspectData, formData);
    formData.append("action", "save_card");
    ajaxOptions.body = formData;
    // ajaxRequest(ajaxOptions);
};
