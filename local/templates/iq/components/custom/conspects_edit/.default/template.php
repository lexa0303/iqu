<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use \Bitrix\Main\Localization\Loc;
$this->setFrameMode(true);
?>
<div class="section__heading creation__heading">
    <span class="section__heading-txt"><?=Loc::getMessage("EDIT_CONSPECT_HEADING");?></span>
</div>
<section class="creation">
    <form id="conspect_create_form" class="ajax_submit js-save_form">
        <input type="hidden" name="conspect_id" value="<?=$arResult["ID"];?>">
        <input type="hidden" name="action" value="edit_conspect">
        <input type="hidden" name="parse_conspect" value="Y">
        <input type="hidden" name="user_id" value="<?=$arResult["USER"]["ID"];?>">
        <input type="hidden" name="conspect_image_src" id="conspect_image-src" value="<?=$arResult["UF_IMAGE"]["SRC"];?>">

        <article class="creation__header-wrap">
            <div class="row">
                <?/*
                <div class="col-xs-12 col-lg-3 col-lg-push-9">
                    <section class="exam__navigation exam__navigation--editConspect">
                        <div class="examNav-burger">
                            <img class="examNav-burger__icon" src="<?=SITE_TEMPLATE_PATH;?>/images/settings.svg" alt="">
                            <span class="examNav-burger__item"></span>
                            <span class="examNav-burger__item"></span>
                        </div>
                        <nav class="studying-nav">
                            <div class="studying-nav__controls">
                                <a class="studying-nav__control-item" href="<?=$arResult["LESSON_PAGE_URL"];?>">
                                    <span class="studying-nav__control-icon icon icon-lesson"></span>
                                    <span class="studying-nav__control-text"><?=Loc::getMessage("LESSON");?></span>
                                </a>
                                <a class="studying-nav__control-item" href="<?=$arResult["EXAM_PAGE_URL"];?>">
                                    <span class="studying-nav__control-icon icon icon-exam"></span>
                                    <span class="studying-nav__control-text"><?=Loc::getMessage("EXAM");?></span>
                                </a>
                                <a class="studying-nav__control-item open-modal"
                                   data-modal="create-notification-modal"
                                   data-ajax="Y"
                                   data-action="notification_create"
                                   data-conspect_id="<?=$arResult["ID"];?>"
                                   data-user_id="<?=$USER->GetID();?>"
                                   href="javascript:void(0);">
                                    <span class="studying-nav__control-icon icon icon-alert"></span>
                                    <span class="studying-nav__control-text"><?=Loc::getMessage("NOTIFICATION");?></span>
                                </a>
<!--                                <a class="studying-nav__control-item" href="--><?//=$arResult["LIST_PAGE_URL"];?><!--">-->
<!--                                    <span class="studying-nav__control-icon icon icon-arrow-back"></span>-->
<!--                                    <span class="studying-nav__control-text">--><?//=Loc::getMessage("BACK");?><!--</span>-->
<!--                                </a>-->
                            </div>
                            <div class="studying-nav__conspect-info">
                                <div class="studying-nav__user-photo"
                                     style="background-image: url('<?=$arResult["USER"]["IMAGE"]["RESIZED"];?>');"></div>
                                <div class="studying-nav__user-info">
                                    <span class="studying-nav__username"><?=$arResult["USER"]["TITLE"];?></span>
                                    <div class="studying-nav__user-info">
                                        <div class="studying-nav__user-watches">
                                            <span class="icon icon-views"></span>
                                            <span><?=(int)$arResult["UF_VIEW_COUNT"];?></span>
                                        </div>
                                        <div class="studying-nav__user-loads">
                                            <span class="icon icon-download2"></span>
                                            <span><?=(int)$arResult["UF_SAVES_COUNT"];?></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <fieldset class="creation-data__group">
                                <input style="display: none"
                                    <?=($arResult["UF_SHOW_IN_LIBRARY"]) ? "checked" : "";?>
                                       class="big-conspect-checkbox__input"
                                       type="checkbox"
                                       name="library"
                                       id="show_in_library">
                                <label for="show_in_library" class="big-conspect-checkbox__label">
                                    <span class="big-conspect-checkbox__icon"></span>
                                    <span class="big-conspect-checkbox__txt"><?=Loc::getMessage("CREATE_CONSPECT_ADD_TO_LIBRARY");?></span>
                                </label>
                            </fieldset>
                            <div class="studying-nav__share-wrap">
                                <span class="studying-nav__share-title"><?=Loc::getMessage("SHARE");?></span>
                                <a class="studying-nav__share-item js-share-fb" href="javascript:void(0);">
                                    <span class="icon icon-fb"></span>
                                </a>
                                <a class="studying-nav__share-item js-share-vk" href="javascript:void(0);">
                                    <span class="icon icon-vk"></span>
                                </a>
                                <a class="studying-nav__share-item js-share-gp" href="javascript:void(0);">
                                    <span class="icon icon-gp"></span>
                                </a>
                            </div>
                        </nav>
                    </section>
                </div>
*/?>
                <div class="col-xs-12 col-lg-9">
                    <div class="creation__header">
                        <div class="creation-data-preview fcard__box fcard__box--small ripple">
                            <div class="my-category__stickers">
                                <label class="creation-data__img-text" for="conspect_image">
                                    <a class="fcard__category-sticker" href="#" title="Редактировать конспект">
                                        <span class="icon icon-edit"></span>
                                    </a>
                                </label>
                                <?/*
                                <input name="conspect_image"
                                       accept="image/jpeg, image/png, image/jpg, image/gif"
                                       class="creation-data__img-change js-conspect_image-input"
                                       type="text" id="conspect_image">
                                <a class="fcard__category-sticker open-modal" id="delete_image" href="">
                                    <span class="icon icon-trashbox"></span>
                                </a>
                                */?>
                            </div>
                            <label for="conspect_image" class="fcard__img-wrap js-conspect_image-changer" href="javascript:void(0);">
                                <img class="fcard__img" id="conspect_img_src" src="<?=$arResult["UF_IMAGE"]["RESIZED"];?>" alt="">
                            </label>
                            <div class="fcard__lower">
                                <div class="fcard__heading-wrap">
                                    <a class="fcard__heading" id="conspect_card-name" href="javascript:void(0);"><?=$arResult["UF_NAME"];?></a>
                                </div>
                                <div class="fcard__author-wrap fcard__author-wrap--name-only">
                                    <a class="fcard__author" href="javascript:void(0);"><?=$arResult["USER"]["TITLE"];?></a>
                                </div>
                            </div>
                        </div>
                        <div class="creation-data-wrapper">
                        <fieldset class="creation-data__group">
                            <label class="creation-data__descr" for="conspect_name">
                                <?=Loc::getMessage("CREATE_CONSPECT_NAME_LABEL");?>
                                <span class="creation-data__descr-star">*</span>
                            </label>
                            <input class="creation-data__change"
                                   required
                                   value="<?=$arResult["UF_NAME"];?>"
                                   type="text"
                                   id="conspect_name"
                                   name="name"
                                   placeholder="<?=Loc::getMessage("CREATE_CONSPECT_NAME_PLACEHOLDER");?>">
                        </fieldset>
                        <fieldset class="creation-data__group selection-group">
                            <label class="creation-data__descr" for="creationSelect">
                                <?=Loc::getMessage("CATEGORY");?>
                                <span class="creation-data__descr-star">*</span>
                            </label>
                            <select name="category_id" required class="creation-data__select selection__item" id="creationSelect">
                                <option selected value=""><?=Loc::getMessage("CREATE_CONSPECT_NONE_CATEGORY");?></option>
                                <?foreach ($arResult["CATEGORIES"] as $arCategory) : ?>
                                    <option
                                        <? if ($arCategory["ID"] == $arResult["CATEGORY"]["ID"]) : ?>
                                            selected
                                        <?endif;?>
                                            value="<?=$arCategory["ID"];?>"><?=$arCategory["UF_NAME"];?></option>
                                <?endforeach;?>
                                <option value="create_category"><?=Loc::getMessage("CREATE_CONSPECT_CREATE_CATEGORY");?></option>
                            </select>
                        </fieldset>
                        <fieldset class="creation-data__group creation-data__group--img">
                            <div class="creation-data__img-config">
                                <div>
                                    <label class="creation-data__img-descr" for="conspect_image">
                                        <span class="icon icon-add-photo"></span>
                                    </label>
                                </div>
                                <div>
                                    <label class="creation-data__img-text" for="conspect_image">
                                        <span class="creation-data__img-txt"><?=Loc::getMessage("CREATE_CONSPECT_CHANGE_IMAGE");?></span>
                                    </label>
                                    <input name="conspect_image"
                                           accept="image/jpeg, image/png, image/jpg, image/gif"
                                           class="creation-data__img-change js-conspect_image-input"
                                           type="text"
                                           id="conspect_image">
                                    <a class="creation-data__img-delete" id="delete_image" href=""><?=Loc::getMessage("CREATE_CONSPECT_DELETE_IMAGE");?></a>
                                </div>
                            </div>
                        </fieldset>
                            <script>
                                (function() {
                                    var params = {
                                        input_file: document.querySelector("#conspect_image"),
                                        input_label: document.querySelector(".creation-data__img-text"),
                                        image: document.querySelector("#conspect_img_src")
                                    };

                                    var conspectImageCropper = new imageCropper(params);
                                    conspectImageCropper.Init();
                                })();
                            </script>

<!--                        <div class="creation-data__submit">-->
<!--                            <button class="creation-data__btn--submit js-block_submit">-->
<!--                                <span class="icon icon-done"></span>-->
<!--                                <span>--><?//=Loc::getMessage("SAVE");?><!--</span>-->
<!--                            </button>-->
<!--                        </div>-->
                    </div>
                        <div class="conspect-tags__add-wrap">
                            <div class="conspect-tags__label">Добавить теги:</div>
                            <div class="chips conspect-tags__input-add js-tags"></div>
                            <div class="conspect-tags__add-tip">Что бы добавить тег введите нужное слово или словосочетание и нажмите «Enter»</div>
                        </div>
                        <script>
                            var arTags = <?=Bitrix\Main\Web\Json::encode($arResult["UF_TAGS"]);?>;
                            var arObjTags = [];
                            if (arTags) {
                                arTags.forEach(function (tag) {
                                    if (tag) {
                                        arObjTags.push({tag: tag});
                                    }
                                });
                            }
                            var Tags = new Chips(".js-tags", {
                                data: arObjTags,
                                limit: Infinity,
                                placeholder: '+ Добавить тег'
                            })
                        </script>
                    </div>
                </div>
            </div>
        </article>
        <article class="creation__content js-all_cards" id="creationContent">
            <?foreach ($arResult["CARDS"] as $key => $arCard) : ?>
                <div id="card-<?=$arCard["ID"];?>" class="new-card js-editable" data-number="<?=$key;?>" data-id="<?=$arCard["ID"];?>">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="new-card__wrapper js-question_wrap">
                                <div class="new-card__title"><?=Loc::getMessage("QUESTION");?></div>
                                <div class="new-card__body">
                                    <div class="new-card__img new-card__img-width">
                                        <img id="question_image-<?=$key;?>" class="js-question_image" src="<?=$arCard["UF_QUESTION_IMAGE"]["RESIZED"];?>" alt=""></div>
                                    <div class="new-card__text js-question_text" contenteditable="true"><?=$arCard["UF_QUESTION"];?></div>
                                    <div class="new-card__btn new-card__btn--add js-question_image_add <?=($arCard["UF_QUESTION_IMAGE"]["ID"] > 0) ? "hidden" : "";?>">
                                        <label id="question_image-label-<?=$key;?>" for="question_image-input-<?=$key;?>" class="new-card__btn-descr js-question_image-label"
                                               title="<?=Loc::getMessage("CREATE_CONSPECT_ADD_CARD_IMAGE");?>">
                                            <span class="icon icon-add-photo"></span>
                                        </label>
                                        <input name="question_file-<?=$key;?>"
                                               accept="image/jpeg, image/png, image/jpg, image/gif"
                                               id="question_image-input-<?=$key;?>"
                                               class="new-card__add-img js-question_image-input"
                                               type="text">
                                    </div>
                                    <div class="new-card__btn new-card__btn--remove js-question_image_remove <?=($arCard["UF_QUESTION_IMAGE"]["ID"] > 0) ? "" : "hidden";?>" title="<?=Loc::getMessage("CREATE_CONSPECT_DELETE_CARD_IMAGE");?>">
                                        <button class="new-card__remove-photo js-question-image-remove-button">
                                            <span class="icon icon-remove-photo"></span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <script>
                            (function() {
                                var params = {
                                    input_file: document.querySelector("#question_image-input-<?=$key;?>"),
                                    input_label: document.querySelector("#question_image-label-<?=$key;?>"),
                                    image: document.querySelector("#question_image-<?=$key;?>")
                                };

                                var question<?=$key;?>Cropper = new imageCropper(params);
                                question<?=$key;?>Cropper.Init();
                            })();
                        </script>
                        <div class="col-md-6">
                            <div class="new-card__wrapper js-answer_wrap">
                                <div class="new-card__title"><?=Loc::getMessage("ANSWER");?></div>
                                <div class="new-card__body">
                                    <div class="new-card__controls">
<!--                                        <div class="new-card__btn--edit" title="--><?//=Loc::getMessage("CREATE_CONSPECT_EDIT_CARD");?><!--">-->
<!--                                            <div class="icon icon-edit"></div>-->
<!--                                        </div>-->
                                        <div class="new-card__btn--delete js-delete_card" title="<?=Loc::getMessage("CREATE_CONSPECT_DELETE_CARD");?>">
                                            <div class="icon icon-trashbox"></div>
                                        </div>
                                        <div class="new-card__btn--move" title="<?=Loc::getMessage("CREATE_CONSPECT_MOVE_CARD");?>">
                                            <div class="new-card__move icon icon-move"></div>
                                        </div>
                                    </div>
                                    <div class="new-card__img new-card__img-width">
                                        <img id="answer_image-<?=$key;?>" class="js-answer_image" src="<?=$arCard["UF_ANSWER_IMAGE"]["RESIZED"];?>" alt="">
                                    </div>
                                    <div class="new-card__text js-answer_text" contenteditable="true"><?=$arCard["UF_ANSWER"];?></div>
                                    <div class="new-card__btn new-card__btn--add js-answer_image_add <?=($arCard["UF_ANSWER_IMAGE"]["ID"] > 0) ? "hidden" : "";?>">
                                        <label id="answer_image-label-<?=$key;?>" for="answer_image-input-<?=$key;?>" class="new-card__btn-descr js-answer_image-label"
                                               title="<?=Loc::getMessage("CREATE_CONSPECT_ADD_CARD_IMAGE");?>">
                                            <span class="icon icon-add-photo"></span>
                                        </label>
                                        <input name="answer_file-<?=$key;?>"
                                               accept="image/jpeg, image/png, image/jpg, image/gif"
                                               id="answer_image-input-<?=$key;?>"
                                               class="new-card__add-img js-answer_image-input"
                                               type="text">
                                    </div>
                                    <div class="new-card__btn new-card__btn--remove js-answer_image_remove <?=($arCard["UF_ANSWER_IMAGE"]["ID"] > 0) ? "" : "hidden";?>"
                                         title="<?=Loc::getMessage("CREATE_CONSPECT_DELETE_CARD_IMAGE");?>">
                                        <button class="new-card__remove-photo js-answer-image-remove-button">
                                            <span class="icon icon-remove-photo"></span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <script>
                            (function() {
                                var params = {
                                    input_file: document.querySelector("#answer_image-input-<?=$key;?>"),
                                    input_label: document.querySelector("#answer_image-label-<?=$key;?>"),
                                    image: document.querySelector("#answer_image-<?=$key;?>")
                                };

                                var answer<?=$key;?>Cropper = new imageCropper(params);
                                answer<?=$key;?>Cropper.Init();
                            })();
                        </script>
                    </div>
                </div>
            <?endforeach;?>
        </article>
        <article class="creation__footer">
<!--            <a class="creation__dismiss button" href="--><?//=$arResult["LINKS"]["CANCEL"];?><!--">-->
<!--                <span class="icon icon-cross"></span>-->
<!--                <span>--><?//=Loc::getMessage("CANCEL");?><!--</span>-->
<!--            </a>-->
            <button class="new-card__btn--create button js-card_add-button" id="card_add-button">
                <span class="icon icon-fat-cross"></span>
                <span><?=Loc::getMessage("CREATE_CONSPECT_ADD_CARD");?></span>
            </button>
            <button class="creation__submit button js-block_submit" type="submit">
                <span class="icon icon-done"></span>
                <span><?=Loc::getMessage("SAVE");?></span>
            </button>
        </article>
    </form>
</section>

<?/* DEFAULT CARD TO BE COPIED ON ADD CLICK */?>
<div class="hidden">
    <div class="new-card js-editable" id="defaultCard">
        <div class="row">
            <div class="col-md-6">
                <div class="new-card__wrapper js-question_wrap">
                    <div class="new-card__title"><?=Loc::getMessage("QUESTION");?></div>
                    <div class="new-card__body">
                        <div class="new-card__img new-card__img-width">
                            <img id="answer_image-1" class="js-question_image" src="" alt=""></div>
                        <div class="new-card__text js-question_text" contenteditable="true"></div>
                        <div class="new-card__btn new-card__btn--add js-question_image_add">
                            <label id="question_image-label-1" for="question_image-input-1" class="new-card__btn-descr js-question_image-label"
                                   title="<?=Loc::getMessage("CREATE_CONSPECT_ADD_CARD_IMAGE");?>">
                                <span class="icon icon-add-photo"></span>
                            </label>
                            <input name="question_file"
                                   accept="image/jpeg, image/png, image/jpg, image/gif"
                                   id="question_image-input-1"
                                   class="new-card__add-img js-question_image-input"
                                   type="text">
                        </div>
                        <div class="new-card__btn new-card__btn--remove hidden js-question_image_remove" title="<?=Loc::getMessage("CREATE_CONSPECT_DELETE_CARD_IMAGE");?>">
                            <button class="new-card__remove-photo js-question-image-remove-button">
                                <span class="icon icon-remove-photo"></span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="new-card__wrapper js-answer_wrap">
                    <div class="new-card__title"><?=Loc::getMessage("ANSWER");?></div>
                    <div class="new-card__body">
                        <div class="new-card__controls">
                            <div class="new-card__btn--edit" title="<?=Loc::getMessage("CREATE_CONSPECT_EDIT_CARD");?>">
                                <div class="icon icon-edit"></div>
                            </div>
                            <div class="new-card__btn--delete js-delete_card" title="<?=Loc::getMessage("CREATE_CONSPECT_DELETE_CARD");?>">
                                <div class="icon icon-trashbox"></div>
                            </div>
                            <div class="new-card__btn--move" title="<?=Loc::getMessage("CREATE_CONSPECT_MOVE_CARD");?>">
                                <div class="new-card__move icon icon-move"></div>
                            </div>
                        </div>
                        <div class="new-card__img new-card__img-width">
                            <img id="answer_image-1" class="js-answer_image" src="" alt="">
                        </div>
                        <div class="new-card__text js-answer_text" contenteditable="true"></div>
                        <div class="new-card__btn new-card__btn--add js-answer_image_add">
                            <label id="answer_image-label-1" for="answer_image-input-1" class="new-card__btn-descr js-answer_image-label"
                                   title="<?=Loc::getMessage("CREATE_CONSPECT_ADD_CARD_IMAGE");?>">
                                <span class="icon icon-add-photo"></span>
                            </label>
                            <input name="answer_file"
                                   accept="image/jpeg, image/png, image/jpg, image/gif"
                                   id="answer_image-input-1"
                                   class="new-card__add-img js-answer_image-input"
                                   type="text">
                        </div>
                        <div class="new-card__btn new-card__btn--remove hidden js-answer_image_remove" title="<?=Loc::getMessage("CREATE_CONSPECT_DELETE_CARD_IMAGE");?>">
                            <button class="new-card__remove-photo js-answer-image-remove-button">
                                <span class="icon icon-remove-photo"></span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?/* DEFAULT CARD TO BE COPIED ON ADD CLICK */?>

<script>
    var allow_submit = true;
    body.on("click", ".js-block_submit", function(e){
        allow_submit = false;
        setTimeout(function(){
            allow_submit = true;
        }, 5000);
    });
    window.onbeforeunload = function(e){
        if (allow_submit) {
            body.find(".js-save_form").addClass("js-skip_reload").submit();
        }
    }
</script>
