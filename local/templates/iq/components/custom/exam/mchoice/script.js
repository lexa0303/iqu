/**
 * Created by alex on 23.03.17.
 */

var body = $("body");

function ExamMchoice(params){
    params.count_questions = parseInt(params.count_questions);
    params.question = parseFloat(params.question) * 1000;
    this.params = params;
    this.Init();
}

ExamMchoice.prototype.Init = function(){
    var self = this;
    var i;
    self.answers = {};
    self.examForm = document.querySelector("."+self.params.form);
    self.timer = document.getElementsByClassName(self.params.timer);
    self.nextQuestionButtons = document.getElementsByClassName(self.params.next);
    self.answerRadios = document.getElementsByClassName(self.params.radio);
    self.progressBar = document.getElementsByClassName(self.params.progress_bar);
    self.progressTotal = document.getElementsByClassName(self.params.progress_total);
    self.progressCurrent = document.getElementsByClassName(self.params.progress_current);
    self.cards = document.getElementsByClassName(self.params.cards);
    self.cardsCount = self.cards.length;
    self.lastActiveCard = 0;
    self.activeCards = [];
    self.checkedAnswers = {};
    self.examActive = true;

    //Set number of total fcards in lesson
    for (i in self.progressTotal) {
        if (self.progressTotal.hasOwnProperty(i)) {
            self.progressTotal[i].innerHTML = self.cardsCount;
        }
    }

    for (i = 0; i < self.nextQuestionButtons.length; i++){
        if (self.nextQuestionButtons.hasOwnProperty(i)){
            self.nextQuestionButtons[i].addEventListener("click", function(e){
                e.preventDefault();
                if (this.classList.contains("disabled")){
                    return false;
                }
                self.NextQuestion();
            });
        }
    }

    for (i = 0; i < self.answerRadios.length; i++){
        if (self.answerRadios.hasOwnProperty(i)){
            self.answerRadios[i].addEventListener("change", function(e){
                var target = this;
                self.RadioHandler(target, e);
            });
        }
    }
    this.StartExam();
};

ExamMchoice.prototype.RadioHandler = function(target, e){
    var self = this;
    var i, j, card, radios, radio, bFlag, flag;
    self.checkedAnswers[target.dataset.card_id] = "Y";

    flag = true;
    for (i in self.activeCards){
        if (self.activeCards.hasOwnProperty(i)){
            bFlag = false;
            card = self.cards[self.activeCards[i]];
            radios = card.getElementsByClassName(self.params.radio);
            for (j in radios){
                if (radios.hasOwnProperty(j)){
                    radio = radios[j];
                    if (radio.checked === true){
                        bFlag = true;
                    }
                }
            }
            if (!bFlag){
                flag = false;
            }
        }
    }

    self.UpdateProgressbar();

    if (flag){
        self.DisableNextQuestion(true);
    }
};

ExamMchoice.prototype.StopExam = function(){
    var i, item, countAnswers;
    var self = this;
    clearTimeout(self.examTimeout);

    self.UpdateProgressbar();
    self.examActive = false;

    var formData = new FormData(self.examForm);

    formData.append("action", "save_exam");
    formData.append("conspect_id", self.params.conspect_id);
    formData.append("exam_type", "mchoice");
    toFormData(self.params.exam_cards, formData, "exam_cards");

    var ajaxOptions = {
        body: formData,
        callback: function(res){
            ajaxHandler(res);
            var result = JSON.parse(res);
            location.href = self.params.redirect + result.exam_id + "/";
            // callPopup("exam-results-modal", true);
            document.querySelector(".js-correct_answers").innerHTML = result.correct_count;
            document.querySelector(".js-total_answers").innerHTML = self.cardsCount;
            document.querySelector(".js-back_link").href = self.params.back_link;
            document.querySelector(".js-exam_modal_cross").href = self.params.back_link;
        }
    };
    ajaxRequest(ajaxOptions);
};

ExamMchoice.prototype.CheckExam = function(){
    var flag = true;
    if ((this.activeSlideNumber + 1) >= this.slideCount){
        this.StopExam();
        flag = false;
    }
    return flag;
};

ExamMchoice.prototype.HideAllCards = function(){
    var self = this;
    var i, card;

    for (i in self.cards){
        if (self.cards.hasOwnProperty(i)){
            card = self.cards[i];
            card.style.display = "none";
        }
    }
};

ExamMchoice.prototype.SetActiveCards = function(){
    var self = this;
    var i, card;

    for (i = self.activeCards[0]; i < self.activeCards[0] + self.params.count_questions; i++){
        if (self.cards.hasOwnProperty(i)){
            self.checkedAnswers[self.cards[i].dataset.card_id] = "Y";
        }
    }

    self.activeCards = [];

    for (i = self.lastActiveCard; i < self.lastActiveCard + self.params.count_questions; i++){
        if (self.cards.hasOwnProperty(i)){
            card = self.cards[i];
            card.style.display = "block";
            self.activeCards.push(i);
        }
    }

    self.lastActiveCard = i;
};

ExamMchoice.prototype.DisableNextQuestion = function(flag){
    var self = this;
    var i;
    var removeDisabled = !(flag === undefined);

    for (i in self.nextQuestionButtons){
        if (self.nextQuestionButtons.hasOwnProperty(i)){
            if (removeDisabled) {
                self.nextQuestionButtons[i].classList.remove("disabled");
            } else {
                self.nextQuestionButtons[i].classList.add("disabled");
            }
        }
    }
};

ExamMchoice.prototype.NextQuestion = function(){
    var self = this;
    self.HideAllCards();
    self.SetActiveCards();
    self.DisableNextQuestion();
    self.UpdateProgressbar();
    if (self.examActive) {
        self.SetQuestionTimeout();
        self.ScrollToFirstCard();
    } else {
        self.StopExam();
    }
    if (self.lastActiveCard >= self.cardsCount){
        self.examActive = false;
    }
};

ExamMchoice.prototype.ScrollToFirstCard = function(){
    var self = this;
    var firstCard = $(self.cards[self.activeCards[0]]);
    $('html, body').animate({scrollTop: firstCard.offset().top}, 300);
};

ExamMchoice.prototype.UpdateProgressbar = function(){
    var self = this;
    var i;
    for (i in self.progressBar) {
        if (self.progressBar.hasOwnProperty(i)) {
            self.progressBar[i].style.width = ((Object.keys(self.checkedAnswers).length / self.cardsCount) * 100) + '%';
        }
    }
    for (i in self.progressCurrent) {
        if (self.progressCurrent.hasOwnProperty(i)) {
            self.progressCurrent[i].innerHTML = Object.keys(self.checkedAnswers).length;
        }
    }
};

ExamMchoice.prototype.SetQuestionTimeout = function(){
    var self = this;
    clearTimeout(self.examTimeout);
    self.SetTimer(self.params.question * self.activeCards.length);
    self.examTimeout = setTimeout(function(){
        self.NextQuestion();
    }, self.params.question * self.activeCards.length);
};

ExamMchoice.prototype.StartExam = function(){
    this.NextQuestion();
};

ExamMchoice.prototype.SetTimer = function(time){
    var currentTime = new Date();
    var timerTime = currentTime.getTime() + parseInt(time);
    $(this.timer).countdown(timerTime, function(event) {
        $(this).text(event.strftime('%M:%S'));
    });
};