<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use Bitrix\Main\Localization\Loc;
$this->setFrameMode(true);
?>
<section>
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-lg-3 col-lg-push-9">
                <section class="exam__navigation">
                    <div class="examNav-burger">
                        <img class="examNav-burger__icon" src="<?=SITE_TEMPLATE_PATH;?>/images/settings.svg" alt="">
                        <span class="examNav-burger__item"></span>
                        <span class="examNav-burger__item"></span>
                    </div>
                    <nav class="studying-nav">
                        <div class="studying-nav__controls">
                            <a class="studying-nav__control-item" href="<?=$arResult["LESSON_PAGE_URL"];?>">
                                <span class="studying-nav__control-icon icon icon-lesson"></span>
                                <span class="studying-nav__control-text"><?=Loc::getMessage("LESSON");?></span>
                            </a>
                            <a class="studying-nav__control-item active" href="<?=$arResult["EXAM_PAGE_URL"];?>">
                                <span class="studying-nav__control-icon icon icon-exam"></span>
                                <span class="studying-nav__control-text"><?=Loc::getMessage("EXAM");?></span>
                            </a>
                            <a class="studying-nav__control-item open-modal"
                               data-modal="create-notification-modal"
                               data-ajax="Y"
                               data-action="notification_create"
                               data-conspect_id="<?=$arResult["ID"];?>"
                               data-user_id="<?=$USER->GetID();?>"
                               href="javascript:void(0);">
                                <span class="studying-nav__control-icon icon icon-alert"></span>
                                <span class="studying-nav__control-text"><?=Loc::getMessage("NOTIFICATION");?></span>
                            </a>
<!--                            <a class="studying-nav__control-item" href="--><?//=$arResult["LIST_PAGE_URL"];?><!--">-->
<!--                                <span class="studying-nav__control-icon icon icon-arrow-back"></span>-->
<!--                                <span class="studying-nav__control-text">--><?//=Loc::getMessage("BACK");?><!--</span>-->
<!--                            </a>-->
                        </div>
                        <div class="studying-nav__settings">
                            <a class="studying-nav__settings-link" href="<?=$arResult["EXAM_PAGE_URL"];?>">
                                <span class="studying-nav__settings-icon icon icon-settings"></span>
                                <span class="studying-nav__settings-text"><?=Loc::getMessage("CONSPECT_EXAM_SETTINGS");?></span>
                            </a>
                        </div>
                        <div class="studying-nav__conspect-info">
                            <div class="studying-nav__user-photo"
                                 style="background-image: url('<?=($arResult["UF_ORIGIN_USER"]) ? $arResult["ORIGIN_USER"]["IMAGE"]["RESIZED"] : $arResult["USER"]["IMAGE"]["RESIZED"];?>');"></div>
                            <div class="studying-nav__user-info">
                                <a href="<?=$arResult["USER"]["DETAIL_PAGE_URL"];?>" class="studying-nav__username"><?=($arResult["UF_ORIGIN_USER"]) ? $arResult["ORIGIN_USER"]["TITLE"] : $arResult["USER"]["TITLE"];?></a>
                                <?if (!$arResult["UF_ORIGIN_USER"]) : ?>
                                    <div class="studying-nav__user-info">
                                        <div class="studying-nav__user-watches">
                                            <span class="icon icon-views"></span>
                                            <span><?=(int)$arResult["UF_VIEW_COUNT"];?></span>
                                        </div>
                                        <div class="studying-nav__user-loads">
                                            <span class="icon icon-download2"></span>
                                            <span><?=(int)$arResult["UF_SAVES_COUNT"];?></span>
                                        </div>
                                    </div>
                                <?endif;?>
                            </div>
                        </div>
                        <section class="exam-timer__wrap">
<!--                            <div class="exam-timer__title">--><?//=Loc::getMessage("CONSPECT_EXAM_TIME");?><!--:</div>-->
                            <div class="exam-timer">
                                <span class="exam-timer__icon icon icon-clock"></span>
                                <div class="exam-timer__time js-exam_timer">00</div>
                                <span class="exam-timer__txt"><?=Loc::getMessage("CONSPECT_EXAM_SEC");?></span>
                            </div><a class="exam-timer__extend" href="javascript:void(0);"><?=Loc::getMessage("CONSPECT_EXAM_EXTEND");?></a>
                        </section>
                        <section class="lesson-progress">
                            <span class="lesson-progress__title"><?=Loc::getMessage("PROGRESS");?></span>
                            <div class="lesson-progress__box">
                                <div class="lesson-progress-bar" style="width: 1%;">
                                    <div class="lesson-progress-tooltip">
                                        <span class="lesson-tooltip-current">0</span><?=Loc::getMessage("OUT_OF");?>
                                        <span class="lesson-tooltip-total">0</span>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <div class="exam-start__btn-wrap">
                            <a class="exam-start__btn button button--rounded js-reload" href=""><?=Loc::getMessage("CONSPECT_EXAM_RESTART");?></a>
                        </div>
                    </nav>
                </section>
            </div>
            <form class="col-xs-12 col-lg-9 col-lg-pull-3 js-exam_form">
                <div class="section__heading exam-results__heading">
                    <span class="section__heading-txt"><?=Loc::getMessage("CONSPECT_EXAM_CHOICE");?></span>
                    <span class="exam-results__conspect"><?= $arResult["UF_NAME"]; ?></span>
                </div>
                <div class="exam-timer exam-timer--fixed">
                    <span class="exam-timer__icon icon icon-clock"></span>
                    <div class="exam-timer__time js-exam_timer">00</div>
                    <span class="exam-timer__txt"><?=Loc::getMessage("CONSPECT_EXAM_SEC");?></span>
                </div>
                <?$questionNumber = 1;?>
                <article class="exam-mchoice__wrap">
                    <?foreach ($arResult["CARDS"] as $arCard) : ?>
                        <div class="js-card" data-card_id="<?=$arCard["ID"];?>">
                            <div class="exam-mchoice__question">
                                <div class="exam-mchoice__title"><?=$questionNumber;?> <?=Loc::getMessage("QUESTION");?></div>
                                <div class="exam-mchoice__block-wrap">
                                    <div class="conspect-card__body">
                                        <div class="conspect-card__img">
                                            <img src="<?=$arCard["UF_QUESTION_IMAGE"]["SRC"];?>" alt="">
                                        </div>
                                        <div class="conspect-card__text">
                                            <span><?=$arCard["UF_QUESTION"];?></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <?$answerNumber = 1;?>
                                <input type="radio"
                                       checked
                                       class="hidden"
                                       name="answers[<?= $arCard["ID"]; ?>][answer_text]"
                                       value="wrong_answer"
                                       id="mch-answer_<?= $arCard["ID"]; ?>_0">
                                <?foreach ($arCard["ANSWERS"] as $arAnswer) : ?>
                                    <div class="col-md-6">
                                        <div class="exam-mchoice__answers-wrap">
                                            <div class="exam-mchoice__answer">
                                                <div class="exam-mchoice__title"><?=Loc::getMessage("ANSWER");?> <?=$answerNumber;?></div>
                                                <div class="exam-mchoice__block-wrap">
                                                    <input name="answers[<?=$arCard["ID"];?>][answer_text]"
                                                           class="exam-mchoice__var-input js-answer_radio"
                                                           type="radio"
                                                           value="<?=$arAnswer["ID"];?>"
                                                           data-answer_id="<?=$arAnswer["ID"];?>"
                                                           data-card_id="<?=$arCard["ID"];?>"
                                                           id="mch-answer_<?=$arCard["ID"];?>_<?=$arAnswer["ID"];?>">
                                                    <label class="exam-mchoice__block conspect-card__body" for="mch-answer_<?=$arCard["ID"];?>_<?=$arAnswer["ID"];?>">
                                                        <span class="conspect-card__img">
                                                            <img src="<?=$arAnswer["UF_ANSWER_IMAGE"]["SRC"];?>" alt="">
                                                        </span>
                                                        <span class="conspect-card__text">
                                                            <span><?=$arAnswer["UF_ANSWER"];?></span>
                                                        </span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?$answerNumber++;?>
                                <?endforeach;?>
                            </div>
                        </div>
                        <?$questionNumber++;?>
                    <?endforeach;?>
                </article>
                <div class="exam-mchoice__footer">
                    <a class="exam-mchoice__next-q button js-next_question disabled" href=""><?=Loc::getMessage("CONSPECT_EXAM_NEXT_QUESTION");?></a>
                </div>
            </form>
        </div>
    </div>
</section>
<script>
    (function(){
        var params = {
            conspect_id: '<?=(int)$arResult["ID"];?>',
            question: '<?=(int)$_GET["question_speed"];?>',
            count_questions: '<?=(int)$_GET["count_questions"];?>',
            progress_bar: "lesson-progress-bar",
            progress_total: 'lesson-tooltip-total',
            progress_current: 'lesson-tooltip-current',
            cards: 'js-card',
            timer: "js-exam_timer",
            next: "js-next_question",
            radio: "js-answer_radio",
            back_link: '<?=$arResult["DETAIL_PAGE_URL"];?>',
            form: 'js-exam_form',
            exam_cards: <?=Bitrix\Main\Web\Json::encode($arResult["CARDS"]);?>,
            redirect: '<?=$arResult["EXAM_PAGE_URL"];?>'
        };

        var exam = new ExamMchoice(params);
    })();
</script>