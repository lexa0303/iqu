<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 15.02.17
 * Time: 15:53
 */

foreach ($arResult["CARDS"] as &$arCard) :
    if ($arCard["ANSWER"]["UF_ANSWER_IMAGE"]["ID"] > 0) {
        $arCard["ANSWER"]["UF_ANSWER_IMAGE"]["RESIZED"] = LenalHelp::img($arCard["ANSWER"]["UF_ANSWER_IMAGE"]["ID"], 250, 250);
    }
    if ($arCard["UF_QUESTION_IMAGE"]["ID"] > 0) {
        $arCard["UF_QUESTION_IMAGE"]["RESIZED"] = LenalHelp::img($arCard["UF_QUESTION_IMAGE"]["ID"], 250, 250);
    }
endforeach;