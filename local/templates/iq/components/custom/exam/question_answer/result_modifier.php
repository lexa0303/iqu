<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 15.02.17
 * Time: 15:53
 */

foreach ($arResult["CARDS"] as &$arCard) :
    if ($arCard["UF_ANSWER_IMAGE"]["ID"] > 0) {
        $arCard["UF_ANSWER_IMAGE"]["RESIZED"] = LenalHelp::img($arCard["UF_ANSWER_IMAGE"]["ID"], 230, 230);
    }
    if ($arCard["UF_QUESTION_IMAGE"]["ID"] > 0) {
        $arCard["UF_QUESTION_IMAGE"]["RESIZED"] = LenalHelp::img($arCard["UF_QUESTION_IMAGE"]["ID"], 230, 230);
    }
endforeach;