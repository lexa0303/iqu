/**
 * Created by alex on 23.03.17.
 */

var body = $("body");

function ExamQuestionAnswer(container, params) {
    params.answer = parseFloat(params.answer) * 1000;
    params.question = parseFloat(params.question) * 1000;
    this.slider = body.find(container);
    this.bAnswer = (params.answer_first === "answer");
    this.params = params;
    this.Init();
}

ExamQuestionAnswer.prototype.Init = function () {
    var self = this;
    var i;
    this.answers = {};
    this.answerButton = document.getElementsByClassName(self.params.flip);
    this.timer = document.getElementsByClassName(self.params.timer);
    this.progressBar = document.getElementsByClassName(self.params.progress_bar);
    this.progressTotal = document.getElementsByClassName(self.params.progress_total);
    this.progressCurrent = document.getElementsByClassName(self.params.progress_current);
    this.slider.slick({
        arrows: true,
        dots: false,
        infinite: false,
        draggable: false,
        touchMove: false,
        easing: 'ease-in',
        prevArrow: false,
        nextArrow: false
    });
    this.slideCount = this.slider.slick("getSlick").slideCount;

    //Set number of total fcards in lesson
    for (i in self.progressTotal) {
        if (self.progressTotal.hasOwnProperty(i)) {
            self.progressTotal[i].innerHTML = this.slideCount;
        }
    }

    //Set width of lesson progressbar
    for (i in self.progressTotal) {
        if (self.progressTotal.hasOwnProperty(i)) {
            self.progressTotal[i].style.width = ((1 / this.slideCount) * 100) + '%';
        }
    }

    this.slider.on("afterChange", function (e, slick, currentSlide) {
        self.RefreshActive();
    });
    this.slider.on('beforeChange', function (event, slick, currentSlide, nextSlide) {
        var i;
        for (i in self.progressCurrent) {
            if (self.progressCurrent.hasOwnProperty(i)) {
                self.progressCurrent[i].innerHTML = (nextSlide + 1);
            }
        }
        var percentProgress = ((nextSlide + 1) / self.slideCount) * 100;
        for (i in self.progressBar) {
            if (self.progressBar.hasOwnProperty(i)) {
                self.progressBar[i].style.width = percentProgress + '%';
            }
        }
    });

    $(this.answerButton).on("click", function (e) {
        self.SetCorrectAnswer();
    });

    document.body.onkeyup = function (e) {
        //Space
        if (e.keyCode == 32) {
            e.preventDefault();

            if (!self.activeSlide.classList.contains("flip") || (self.bAnswer && self.activeSlide.classList.contains("flip"))) {
                self.SetCorrectAnswer();
            }
        } else if (e.keyCode == 39 || e.keyCode == 68) {
            //Right
            e.preventDefault();

            if (self.activeSlide.classList.contains("flip")) {
                self.NextAnswer();
            } else {
                return false;
            }
        }
    };

    this.RefreshActive();
    this.StartExam();
};

ExamQuestionAnswer.prototype.SetCorrectAnswer = function () {
    this.activeSlide.classList.add("js-exam_correct");
    this.Flip("Y");
};

ExamQuestionAnswer.prototype.StopExam = function () {
    var i, item, countAnswers;
    var self = this;
    clearTimeout(self.examTimeout);

    countAnswers = 0;

    for (i in self.answers) {
        if (self.answers.hasOwnProperty(i)) {
            item = self.answers[i];

            console.log(item);

            countAnswers += parseInt(item);
        }
    }

    var ajaxOptions = {
        data: {
            action: "save_exam",
            conspect_id: self.params.conspect_id,
            count_answers: countAnswers,
            result: JSON.stringify(self.answers),
            exam_type: "question_answer"
        },
        callback: function (res) {
            ajaxHandler(res);
            callPopup("exam-results-modal", true);
            // document.querySelector(".js-correct_answers").innerHTML = countAnswers;
            document.querySelector(".exam-results__answers").style.opacity = 0;
            document.querySelector(".exam-results__answers").style.cursor = "default";
            // document.querySelector(".js-total_answers").innerHTML = self.slideCount;
            document.querySelector(".js-back_link").href = self.params.back_link;
            document.querySelector(".js-exam_modal_cross").href = self.params.back_link;
        }
    };
    ajaxRequest(ajaxOptions);
};

ExamQuestionAnswer.prototype.CheckExam = function () {
    var flag = true;
    if ((this.activeSlideNumber + 1) >= this.slideCount) {
        this.StopExam();
        flag = false;
    }
    return flag;
};

ExamQuestionAnswer.prototype.RefreshActive = function () {
    this.activeSlide = document.querySelector('.slick-active.flip-container');
    this.activeSlideNumber = parseInt(this.activeSlide.dataset.slickIndex);
};

ExamQuestionAnswer.prototype.NextSlide = function () {
    this.slider.slick('slickNext');
    this.SetTimer(this.params.answer);
    this.SetAnswerTimeout();
};

ExamQuestionAnswer.prototype.PrevSlide = function () {
    this.slider.slick('slickPrev');
};

ExamQuestionAnswer.prototype.Flip = function (flag) {
    var self = this;
    if (flag === "Y") {
        self.answers[self.activeSlide.dataset.id] = 1;
    } else {
        self.answers[self.activeSlide.dataset.id] = 0;
    }
    self.activeSlide.classList.toggle('flip');
    if (self.CheckExam()) {
        self.SetQuestionTimeout();
    }
};

ExamQuestionAnswer.prototype.NextAnswer = function () {
    var self = this;

    self.SetAnswerTimeout();
    self.NextSlide();
};

ExamQuestionAnswer.prototype.SetQuestionTimeout = function () {
    var self = this;
    clearTimeout(self.examTimeout);
    self.SetTimer(self.params.question);

    self.examTimeout = setTimeout(function () {
        self.NextSlide();
    }, self.params.question);
};

ExamQuestionAnswer.prototype.SetAnswerTimeout = function () {
    var self = this;
    clearTimeout(self.examTimeout);
    self.SetTimer(self.params.answer);

    self.examTimeout = setTimeout(function () {
        self.Flip();
    }, self.params.answer);
};

ExamQuestionAnswer.prototype.StartExam = function () {
    this.SetAnswerTimeout();
};

ExamQuestionAnswer.prototype.SetTimer = function (time) {
    var currentTime = new Date();
    var timerTime = currentTime.getTime() + parseInt(time);
    $(this.timer).countdown(timerTime, function (event) {
        $(this).text(event.strftime('%M:%S'));
    });
};