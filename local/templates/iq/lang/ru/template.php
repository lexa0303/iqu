<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 13.02.17
 * Time: 16:28
 */

/**
 * @var $MESS array
 */

$MESS["ERROR_TOAST"] = "Не заполнено поле \"#FIELD#\"";
$MESS["ERROR_PASSWORD_CHECK"] = "Пароли не совпадают";
$MESS["ERROR_EMAIL_NOT_FOUND"] = "Пользователь с указанным email не найден";
$MESS["ERROR_NO_AUTH"] = "Вы не авторизованы";
$MESS["ERROR_WRONG_USER"] = "Неправильный ID пользователя";
$MESS["ERROR_WRONG_PASSWORD"] = "Неправильный пароль";
$MESS["ERROR_CATEGORY_EXISTS"] = "Категория с таким названием уже существует";
$MESS["ERROR_EMPTY_CONSPECT"] = "Нету заполненных карточек";

$MESS["AJAX_AUTH_SUCCESS"] = "Вы успешно авторизованы";

$MESS["HEADER_COUNT_CONSPECTS"] = "Кол-во конспектов";
$MESS["HEADER_COUNT_USERS"] = "Кол-во пользователей";
$MESS["HEADER_DOWNLOAD_APP"] = "Скачать мобильное приложение";
$MESS["HEADER_MAIN_PAGE"] = "Перейти на главную страницу";
$MESS["HEADER_STATUS"] = "Статус PRO";

$MESS["MAIN_PAGE_SEARCH"] = "Поиск нужного конспекта";
$MESS["MAIN_PAGE_MY_CONSPECTS"] = "Мои<br>конспекты";
$MESS["MAIN_PAGE_CREATE_CONSPECT"] = "Создать<br>конспект";
$MESS["MAIN_PAGE_LIBRARY"] = "Библиотека";
$MESS["MAIN_PAGE_FREE_LIBRARY"] = "Бесплатная библиотека";
$MESS["MAIN_PAGE_SHOP"] = "Магазин";

$MESS["LOGIN_AUTH"] = "Присоединиться";
$MESS["LOGIN_ENTER"] = "Войти";
$MESS["LOGIN_EXIT"] = "Выйти";
$MESS["LOGIN_EXIT_ACCOUNT"] = "Выйти из акканута";

$MESS["ASIDE_MENU_OPEN"] = "Открыть меню";
$MESS["ASIDE_MENU_HEADING"] = "Online learning tool";
$MESS["ASIDE_MENU_MY_CONSPECTS"] = "Мои конспекты";
$MESS["ASIDE_MENU_CREATE_CONSPECT"] = "Создать конспект";
$MESS["ASIDE_MENU_MY_CATEGORIES"] = "Мои категории";
$MESS["ASIDE_MENU_CREATE_CATEGORY"] = "Создать категорию";
$MESS["ASIDE_MENU_LIBRARY"] = "Библиотека";
$MESS["ASIDE_MENU_SHOP"] = "Магазин";

$MESS["OUT_OF"] = " из";
$MESS["PROGRESS"] = "Прогресс";
$MESS["ENABLE"] = "Подключить";
$MESS["DISABLE"] = "Отключить";
$MESS["CANT_DISABLE"] = "Этот аккаунт отключить нельзя: на него зарегистрирован ваш профиль";
$MESS["LINK_TO"] = "Привязать";
$MESS["PASSWORD"] = "Пароль";
$MESS["CHANGE_PASSWORD"] = "Изменить пароль";
$MESS["SEARCH"] = "Поиск";
$MESS["CLOSE"] = "Закрыть";
$MESS["SAVE"] = "Сохранить";
$MESS["SAVE_CHANGES"] = "Сохранить изменения";
$MESS["CANCEL"] = "Отмена";
$MESS["SEND"] = "Отправить";
$MESS["YES"] = "Да";
$MESS["NO"] = "Нет";
$MESS["CATEGORY"] = "Категория";
$MESS["QUESTION"] = "Вопрос";
$MESS["ANSWER"] = "Ответ";
$MESS["SUBSCRIBE"] = "Подписаться";
$MESS["UNSUBSCRIBE"] = "Отписаться";
$MESS["NO_SEARCH_RESULT"] = "По данному запросу ничего не найдено";
$MESS["LESSON"] = "Урок";
$MESS["EXAM"] = "Экзамен";
$MESS["NOTIFICATION"] = "Напоминание";
$MESS["BACK"] = "Назад";
$MESS["SHARE"] = "Поделиться";
$MESS["GO_BACK"] = "Вернуться назад";
$MESS["ACCOUNT"] = "Аккаунт";
$MESS["PRO_ACCOUNT_ENABLED"] = "PRO аккаунт оформлен";
$MESS["PRO_ACCOUNT_DISABLED"] = "PRO аккаунт не оформлен";
$MESS["VERSION"] = "Версия";
$MESS["VERSION_OT"] = "Версия от";

$MESS["MORE_BUTTON"] = "Ещё...";
$MESS["VIEW_COUNT"] = "Количество просмотров";
$MESS["SAVES_COUNT"] = "Количество скачиваний";

$MESS["FOOTER_FACEBOOK"] = "IQU в Facebook";
$MESS["FOOTER_VK"] = "IQU вконтакте";
$MESS["FOOTER_ABOUT"] = "О проекте IQU";

$MESS["INPUT_HEADING_EMAIL"] = "Ваш E-mail";
$MESS["INPUT_HEADING_PASSWORD"] = "Ваш Пароль";
$MESS["INPUT_HEADING_OLD_PASSWORD"] = "Старый пароль";
$MESS["INPUT_HEADING_NEW_PASSWORD"] = "Новый пароль";
$MESS["INPUT_HEADING_PASSWORD_REPEAT"] = "Повторите ваш пароль";
$MESS["INPUT_HEADING_TITLE"] = "Имя/Никнейм";
$MESS["INPUT_HEADING_CATEGORY_NAME"] = "Название категории";
$MESS["INPUT_HEADING_CHOOSE_CATEGORY"] = "Выберите категорию";
$MESS["INPUT_HEADING_CONTROL_STRING"] = "Контрольная строка";

$MESS["PLACEHOLDER_SEARCH"] = "Введите ваш запрос";
$MESS["PLACEHOLDER_EMAIL"] = "Ivan@gmail.com";
$MESS["PLACEHOLDER_PASSWORD"] = "***************";
$MESS["PLACEHOLDER_TITLE"] = "IVANK";
$MESS["PLACEHOLDER_CATEGORY_NAME"] = "IVANK";
$MESS["PLACEHOLDER_CONTROL_STRING"] = "Контрольная строка из письма";
$MESS["PLACEHOLDER_PHONE"] = "+38 (___) ___ __ __";

$MESS["TOOLTIP_EDIT_CATEGORY"] = "Редактировать категорию";
$MESS["TOOLTIP_DELETE_CATEGORY"] = "Удалить категорию";
$MESS["TOOLTIP_ADD_CONSPECT"] = "Добавить конспект";
$MESS["TOOLTIP_EDIT_CONSPECT"] = "Редактировать конспект";
$MESS["TOOLTIP_DELETE_CONSPECT"] = "Удалить конспект";
$MESS["TOOLTIP_CONSPECT_CARD_COUNT"] = "Кол-во карточек в конспекте";
$MESS["TOOLTIP_CONSPECT_CREATE_TIME"] = "Дата создания конспекта";
$MESS["TOOLTIP_CONSPECT_DOWNLOAD"] = "Скачать конспект";
$MESS["TOOLTIP_CONSPECT_SAVE"] = "Сохранить конспект";

$MESS["MODAL_AUTH_HEADING"] = "Добро пожаловать!";
$MESS["MODAL_AUTH_VK"] = "Авторизироваться Вконтакте";
$MESS["MODAL_AUTH_FACEBOOK"] = "Авторизироваться Facebook";
$MESS["MODAL_AUTH_GOOGLE"] = "Авторизироваться Google plus";
$MESS["MODAL_AUTH_BUTTON"] = "Авторизироваться";
$MESS["MODAL_FORGOT_PASSWORD_BUTTON"] = "Забыли пароль?";
$MESS["MODAL_TO_REG_BUTTON"] = "Нет аккаунта? Зарегистрироваться!";
$MESS["MODAL_TO_AUTH_BUTTON"] = "Авторизироваться";
$MESS["MODAL_REG_HEADING"] = "Зарегистрируйтесь";
$MESS["MODAL_REG_SUCCESS"] = "Вы успешно зарегистрированы";
$MESS["MODAL_REG_BUTTON"] = "Зарегистрироваться";
$MESS["MODAL_THANKS_HEADING"] = "Проверьте ваш Email";
$MESS["MODAL_THANKS_TEXT"] = "На указанный вами ящик отправлено письмо с инструкцией по восстановлению пароля";
$MESS["MODAL_EDIT_CATEGORY_HEADING"] = "Редактировать категорию";
$MESS["MODAL_EDIT_CATEGORY_TEXT"] = "В этом поп-ап окне вы можете редактировать название категории. вот и все. Больше ничего.";
$MESS["MODAL_ADD_CATEGORY_HEADING"] = "Новая категория";
$MESS["MODAL_ADD_CATEGORY_TEXT"] = "Создай новую группу конспектов, объединенных одной темой.";
$MESS["MODAL_RESTORE_PASSWORD_HEADING"] = "Восcтановление пароля";
$MESS["MODAL_RESTORE_PASSWORD_TEXT"] = "Для восстановления пароля необходимо ввести в полениже email который вы указывали при регистрации";
$MESS["MODAL_NEW_PASSWORD_HEADING"] = "Создайте новый пароль";
$MESS["MODAL_NEW_PASSWORD_TEXT"] = "Выбор в качестве кодового слова последовательности цифр (самый «мертвый» вариант – 12345) или даты рождения - исключено";
$MESS["MODAL_DELETE_CONSPECT_HEADING"] = "Удалить конспект";
$MESS["MODAL_DELETE_CATEGORY_HEADING"] = "Удалить категорию";
$MESS["MODAL_DELETE_CONSPECT_TEXT"] = "Вы действительно хотите удалить конспект?";
$MESS["MODAL_DELETE_CATEGORY_TEXT"] = "Вы действительно хотите удалить категорию, и ВСЕ конспекты в ней?";
$MESS["MODAL_DOWNLOAD_CONSPECT_HEADING"] = "Скачать конспект";
$MESS["MODAL_DOWNLOAD_CONSPECT_TEXT"] = "Добавь конспект в личную библиотеку!";
$MESS["MODAL_CREATE_NOTIFICATION_HEADING"] = "Создать напоминание";
$MESS["MODAL_EDIT_NOTIFICATION_HEADING"] = "Изменить напоминание";
$MESS["MODAL_CREATE_NOTIFICATION_CALENDAR"] = "Перейти на календарь напоминаний";
$MESS["MODAL_CREATE_NOTIFICATION_TEXT"] = "Хочешь напомнить себе о том, что нужно выучить конспект?<br>Заполни поля:";
$MESS["MODAL_CREATE_NOTIFICATION_WHEN"] = "Когда";
$MESS["MODAL_CREATE_NOTIFICATION_PERIOD"] = "Периодичность";
$MESS["MODAL_CREATE_NOTIFICATION_SUBMIT"] = "Создать напоминание";
$MESS["MODAL_EDIT_NOTIFICATION_SUBMIT"] = "Изменить напоминание";
$MESS["MODAL_PASSWORD_RESET_REQUEST_SENT"] = "Контрольная строка для смены пароля выслана";
$MESS["MODAL_CHANGE_PASSWORD_SUCCESS"] = "Пароль успешно изменен";

$MESS["CONSPECT_CARD_AMOUNT_1"] = "#COUNT# карточка";
$MESS["CONSPECT_AMOUNT_1"] = "#COUNT# конспект";
$MESS["CONSPECT_CARD_AMOUNT_2"] = "#COUNT# карточки";
$MESS["CONSPECT_AMOUNT_2"] = "#COUNT# конспекта";
$MESS["CONSPECT_CARD_AMOUNT_5"] = "#COUNT# карточек";
$MESS["CONSPECT_AMOUNT_5"] = "#COUNT# конспектов";
$MESS["CONSPECTS_EMPTY"] = "Нет конспектов";

$MESS["EDIT_CONSPECT_HEADING"] = "Редактировать конспект";
$MESS["CREATE_CONSPECT_HEADING"] = "Создать конспект";
$MESS["CREATE_CONSPECT_CREATE_CATEGORY"] = "Создать категорию";
$MESS["CREATE_CONSPECT_NAME_LABEL"] = "Введите название конспекта";
$MESS["CREATE_CONSPECT_NAME_PLACEHOLDER"] = "Минимум 1 символ";
$MESS["CREATE_CONSPECT_NONE_CATEGORY"] = "Не выбрано";
$MESS["CREATE_CONSPECT_CHANGE_IMAGE"] = "Изменить обложку";
$MESS["CREATE_CONSPECT_DELETE_IMAGE"] = "Удалить обложку";
$MESS["CREATE_CONSPECT_ADD_CARD_IMAGE"] = "Добавить фото к карточке";
$MESS["CREATE_CONSPECT_DELETE_CARD_IMAGE"] = "Удалить фото карточки";
$MESS["CREATE_CONSPECT_EDIT_CARD"] = "Редактировать карточку";
$MESS["CREATE_CONSPECT_DELETE_CARD"] = "Удалить карточку";
$MESS["CREATE_CONSPECT_MOVE_CARD"] = "Перетащите карточку";
$MESS["CREATE_CONSPECT_ADD_CARD"] = "Добавить вопрос / ответ";
$MESS["CREATE_CONSPECT_ADD_TO_LIBRARY"] = "Добавить в общую Библиотеку";
$MESS["CREATE_CONSPECT_EMPTY_NOTIFICATION"] = "Чтобы сохранить конспект введите хотя бы один вопрос/ответ";

$MESS["CONSPECT_LIST_LEARNING_BUTTON"] = "Учу сейчас";
$MESS["CONSPECT_LIST_LEARNED_BUTTON"] = "Выученные";
$MESS["CONSPECT_LIST_SEARCH"] = "Поиск по конспектам";
$MESS["CONSPECT_LIST_LEARNING_STICKER_TITLE"] = "Конспект на изучении";
$MESS["CONSPECT_LIST_LEARNED_STICKER_TITLE"] = "Конспект изучен";
$MESS["CONSPECT_LIST_CARD_COUNT_TITLE"] = "Кол-во карточек в конспекте";
$MESS["CONSPECT_LIST_DATE_TITLE"] = "Кол-во карточек в конспекте";

$MESS["CONSPECT_DETAIL_TO_LESSON"] = "Пройти урок";
$MESS["CONSPECT_DETAIL_TO_EXAM"] = "Пройти экзамен";
$MESS["CONSPECT_DETAIL_EDIT"] = "Редактировать конспект";
$MESS["CONSPECT_DETAIL_ANOTHER_HEADING"] = "Конспекты пользователя:";
$MESS["CONSPECT_DETAIL_ANOTHER_CONSPECT_COUNT"] = "Кол-во конспектов";
$MESS["CONSPECT_DETAIL_ANOTHER_DOWNLOADS_COUNT"] = "Кол-во скачиваний";
$MESS["CONSPECT_DETAIL_ANOTHER_SUBSCRIBERS_COUNT"] = "Кол-во подписчиков";
$MESS["CONSPECT_LESSON_TURN_AROUND"] = "Повернуть";
$MESS["CONSPECT_LESSON_SPACE"] = "SPACE";
$MESS["CONSPECT_EXAM_SETTINGS"] = "Настройки экзамена";
$MESS["CONSPECT_EXAM_CONSPECT_CHOICE_HEADING"] = "Конспект:";
$MESS["CONSPECT_EXAM_SETTINGS_HEADING"] = "Настройка экзамена";
$MESS["CONSPECT_EXAM_SETTINGS_DESCRIPTION"] = "Настройте экзамен перед тем как начать. Дополнительные параметры и настройки вы можете приобрести в нашем магазине";
$MESS["CONSPECT_EXAM_QA"] = "Вопрос\\Ответ";
$MESS["CONSPECT_EXAM_YN"] = "Да\\Нет";
$MESS["CONSPECT_EXAM_CHOICE"] = "Множественный выбор";
$MESS["CONSPECT_EXAM_TIME"] = "Время";
$MESS["CONSPECT_EXAM_SEC"] = "сек";
$MESS["CONSPECT_EXAM_EXTEND"] = "Продлить";
$MESS["CONSPECT_EXAM_RESTART"] = "Начать заново";
$MESS["CONSPECT_EXAM_RESULT_TITLE"] = "Поздравляем!<br>Вы сдали экзамен.";
$MESS["CONSPECT_EXAM_RESULT_BACK"] = "Вернуться назад";
$MESS["CONSPECT_EXAM_RESULT_DESCRIPTION"] = "Правильных ответов";
$MESS["CONSPECT_EXAM_RESULT_CORRECT_ANSWER"] = "Правильных ответов";
$MESS["CONSPECT_EXAM_RESULT_WRONG_ANSWER"] = "Не правильных ответов";
$MESS["CONSPECT_EXAM_NEXT_QUESTION"] = "Следующий вопрос";
$MESS["CONSPECT_EXAM_NEXT_QUESTIONS"] = "Следующие вопросы";
$MESS["CONSPECT_SAVE_CHOOSE_CATEGORY"] = "Выберите категорию";
$MESS["CONSPECT_EXAM_TO_CONSPECT"] = "Вернутся к конспекту";
$MESS["CONSPECT_DELETE_PROFILE"] = "Удалить из своего профиля";
$MESS["CONSPECT_DELETE_LIBRARY"] = "Удалить из библиотеки";
$MESS["CONSPECT_DELETE_BOTH"] = "Удалить из своего профиля и библиотеки";

$MESS["CATEGORY_CREATED_SUCCESS"] = "Категория успешно создана";
$MESS["CATEGORY_CREATED_ERROR"] = "Ошибка при создании категории";

$MESS["PERSONAL_HEADING"] = "Личные данные";
$MESS["PERSONAL_ADD_PHOTO"] = "Добавить фото";
$MESS["PERSONAL_CHANGE_PHOTO"] = "Изменить фото";
$MESS["PERSONAL_LOGIN_HEADING"] = "Логин (для входа на сайт)";
$MESS["PERSONAL_TITLE_HEADING"] = "Имя на сайте (никнейм)";
$MESS["PERSONAL_CITY_HEADING"] = "Город в котором вы проживаете";
$MESS["PERSONAL_EMAIL_HEADING"] = "E-mail";
$MESS["PERSONAL_PHONE_HEADING"] = "Номер телефона";
$MESS["PERSONAL_ACCOUNT_INFO"] = "Администрация Сайта при обработке персональных данных принимает все необходимые организационные и технические меры для защиты персональных данных.";
$MESS["PERSONAL_SOCIALS_HEADING"] = "Привязать социальные сети";
$MESS["PERSONAL_STAT_HEADING"] = "Личная статистика";
$MESS["PERSONAL_CONSPECT_COUNT"] = "Кол-во моих конспектов";
$MESS["PERSONAL_CATEGORY_COUNT"] = "Кол-во категорий";
$MESS["PERSONAL_SAVES_COUNT"] = "Кол-во скачиваний";
$MESS["PERSONAL_SUBSCRIBERS_COUNT"] = "Кол-во подписчиков";

$MESS["LIBRARY_HEADING"] = "Библиотека";
$MESS["LIBRARY_TAB_ALL_CONSPECTS"] = "Все конспекты";
$MESS["LIBRARY_TAB_NEW"] = "Новые";
$MESS["LIBRARY_TAB_POPULAR"] = "Популярное за этот месяц";
$MESS["LIBRARY_TAB_SUBSCRIBED"] = "Я подписан";
$MESS["LIBRARY_SEARCH"] = "Поиск по конспектам";

$MESS["NEW_HEADING"] = "Новое у избранных пользователей";

$MESS["BUG_NOTIFICATION_HEADING"] = "Нашли ошибку?";
$MESS["BUG_NOTIFICATION_TEXT"] = "Такое бывает, это тестовая версия сайта. Помоги нам ее исправить, коротко описав суть.";

$MESS["SEARCH_HEADING"] = "Поиск";
$MESS["SEARCH_PLACEHOLDER"] = "Поиск нужного конспекта";
$MESS["SEARCH_RESULTS_HEADING"] = "Результаты поиска:";
$MESS["SEARCH_RESULTS_EMPTY"] = "По данному запросу ничего не найдено.";

$MESS["NOTIFICATIONS_HEADING"] = "Пора учить эти конспекты";
$MESS["NOTIFICATIONS_HEADING_NONE"] = "Напоминания";

$MESS["CONSPECT_PUBLISH_HEADING"] = "Опубликуйте конспект";
$MESS["CONSPECT_PUBLISH_TEXT"] = "Тут может быть еще какой-то текст";

$MESS["META_SELF_CATEGORIES_LIST"] = "Мои категории";
$MESS["META_USER_CATEGORIES_LIST"] = "Категории пользователя #USER_TITLE#";