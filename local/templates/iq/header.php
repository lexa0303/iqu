<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<? use Bitrix\Main\Page\Asset; ?>
<? use Bitrix\Main\Localization\Loc; ?>
<?
Loc::loadLanguageFile($_SERVER["DOCUMENT_ROOT"] . "/bitrix/templates/" . SITE_TEMPLATE_ID . "/header.php");
Loc::loadLanguageFile($_SERVER["DOCUMENT_ROOT"] . "/local/templates/" . SITE_TEMPLATE_ID . "/template.php");
CJSCore::Init(array("fx"));
$curPage = $APPLICATION->GetCurPage(true);
?>
<!DOCTYPE html>
<html xml:lang="<?= LANGUAGE_ID ?>" lang="<?= LANGUAGE_ID ?>">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0, width=device-width">
    <link rel="shortcut icon" type="image/x-icon" href="<?= SITE_DIR ?>favicon.ico"/>
    <? $APPLICATION->ShowHead(); ?>
    <? # STYLES ?>
    <? Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/external/materialize/materialize.min.css"); ?>
    <? Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/fonts/icomoon/style.css"); ?>
    <? Asset::getInstance()->addCss("https://fonts.googleapis.com/css?family=PT+Sans:400,700|Roboto:400,700&amp;amp;subset=cyrillic"); ?>
    <? Asset::getInstance()->addCss("https://fonts.googleapis.com/icon?family=Material+Icons"); ?>
    <? Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/external/bootstrap/css/bootstrap.min.css"); ?>
    <? Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/fonts/icomoon/style.css"); ?>
    <? Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/external/animate/animate.min.css"); ?>
    <? Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/external/slick1.6.0/slick/slick-with-theme.css"); ?>
    <? Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/external/heapbox/asbestos/css/asbestos.css"); ?>
    <? Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/external/dragula/dragula.min.css"); ?>
    <? Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/external/mcustomscroll/jquery.mCustomScrollbar.min.css"); ?>
    <? Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/external/datepicker/bootstrap-datetimepicker.min.css"); ?>
    <? Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/external/toast/toastr.min.css"); ?>
    <? Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/external/cropper/cropper.min.css"); ?>
    <? Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/external/fullcalendar-3.4.0/fullcalendar.min.css"); ?>
    <? Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/external/magnific/magnific-popup.css"); ?>
    <? Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/style.css"); ?>
    <? Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/common.css"); ?>
    <? # END STYLES ?>

    <? # SCRIPTS ?>
    <? Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/external/jquery/jquery-2.2.4.min.js"); ?>
    <? Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/external/materialize/materialize.min.js"); ?>
    <? Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/external/bootstrap/js/bootstrap.min.js"); ?>
    <? Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/external/slick1.6.0/slick/slick.min.js"); ?>
    <? Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/external/heapbox/jquery.heapbox-0.9.4.min.js"); ?>
    <? Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/external/dragula/dragula.min.js"); ?>
    <? Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/external/mcustomscroll/jquery.mCustomScrollbar.concat.min.js"); ?>
    <? Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/external/datepicker/moment-with-locales.min.js"); ?>
    <? Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/external/datepicker/bootstrap-datetimepicker.min.js"); ?>
    <? Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/external/toast/toastr.min.js"); ?>
    <? Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/external/inputMask/jquery.inputmask.bundle.min.js"); ?>
    <? Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/external/TextFill/jquery.textfill.min.js"); ?>
    <? Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/external/countdown/jquery.countdown.min.js"); ?>
    <? Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/jquery.validate_1_14.min.js"); ?>
    <? Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/additional-methods_1_14.min.js"); ?>
    <? Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/external/cropper/cropper.min.js"); ?>
    <? Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/external/fullcalendar-3.4.0/lib/jquery-ui.min.js"); ?>
    <? Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/external/fullcalendar-3.4.0/fullcalendar.js"); ?>
    <? Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/external/fullcalendar-3.4.0/locale/ru.js"); ?>
    <? Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/external/magnific/jquery.magnific-popup.min.js"); ?>
    <? Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/common.js"); ?>
    <? Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/application.js"); ?>
    <? Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/script.js"); ?>
    <? # END SCRIPTS ?>

    <title><? $APPLICATION->ShowTitle() ?></title>
</head>
<? global $USER; ?>
<body <?= (!$USER->IsAuthorized()) ? "id='block-links'" : ""; ?>
        class="<?= ($_COOKIE["cards_view"] == "small") ? "tiny" : ""; ?>">
<?
$userID = $USER->GetID();
if ($userID > 0) {
    $dbUser = CUser::GetList(
        $by = "sort",
        $order = "asc",
        array("ID" => $userID),
        array(
            "SELECT" => array(
                "UF_*"
            )
        )
    );
    if ($res = $dbUser->Fetch()) :
        global $arUser;
        $arUser = $res;
        $arUser["PHOTO_SMALL"] = CFile::GetPath($arUser["PERSONAL_PHOTO"]);
    endif;

    if (CSite::InGroup(array(AUTHORS_GROUP_ID))) {
        $arUser["IS_AUTHOR"] = "Y";
    }
    if (CSite::InGroup(array(TURBO_GROUP_ID))) {
        $arUser["TURBO"] = "Y";
    }
}

$dbUsers = CUser::GetList($by = "sort", $order = "asc", array("ACTIVE" => "Y"));
while ($res = $dbUsers->Fetch())
    $arAllUsers[] = $res["ID"];

global $arTotal;
$arTotal = array(
    "USERS" => count($arAllUsers),
    "CONSPECTS" => Conspects::GetCount()
);
?>
<script>
    var sub = <?=Bitrix\Main\Web\Json::encode($arUser["UF_SUBSCRIBED"]);?>;
    var globals = {
        ajax: '<?=AJAX;?>',
        ajaxFile: '<?=AJAX_FILE;?>',
        site_id: '<?=SITE_ID;?>',
        lang_id: '<?=LANGUAGE_ID;?>',
        user_id: '<?=$userID;?>',
        create_category: 'https://<?=$_SERVER["SERVER_NAME"];?><?=getLink("CREATE_CATEGORY");?>',
        default_image: '<?=DEFAULT_IMAGE;?>',
        subscribed: sub,
    };
</script>
<? /*
#TODO REMOVE PANEL STYLES
*/ ?>
<div style="z-index:0; position:absolute; width:100%;z-index:100;" id="panel"><? $APPLICATION->ShowPanel(); ?></div>
<header class="header">
    <div class="container">
        <div class="header__content">
            <div class="header__left">
                <div class="ammount__wrapper" title="<?= Loc::getMessage("HEADER_COUNT_CONSPECTS"); ?>">
                    <span class="icon icon-all-cards ammount__icon"></span>
                    <span class="ammount__number"><?= $arTotal["CONSPECTS"]; ?></span>
                </div>
                <div class="ammount__wrapper" title="<?= Loc::getMessage("HEADER_COUNT_USERS"); ?>">
                    <span class="icon icon-users ammount__icon"></span>
                    <span class="ammount__number"><?= $arTotal["USERS"]; ?></span>
                </div>
            </div>
            <div class="header__middle">
                <? /*
                <a class="header__middle-img--app" href="#" title="<?=Loc::getMessage("HEADER_DOWNLOAD_APP");?> ">
                    <img src="<?=SITE_TEMPLATE_PATH;?>/images/app-link-img.png" alt="">
                </a>
                */ ?>
                <a class="header__middle-img--logo js-skip-disabled" href="<?= SITE_DIR; ?>"
                   title="<?= Loc::getMessage("HEADER_MAIN_PAGE"); ?>">
                    <img src="<?= SITE_TEMPLATE_PATH; ?>/images/logo.svg" alt="">
                </a>
                <? /*
                <a class="pro-btn active" href="#" title="<?=Loc::getMessage("HEADER_STATUS");?>">pro</a>
                */ ?>
            </div>
            <div class="header__right">
                <!--                <a class="header__right-search js-skip-disabled -->
                <? //=($APPLICATION->GetCurPage() == "/") ? "js-header_index_search" : "";?><!--" href="javascript:void(0)">-->
                <!--                    <span class="icon icon-search"></span>-->
                <!--                    <span class="header__right-text">-->
                <? //=Loc::getMessage("SEARCH");?><!--</span>-->
                <!--                </a>-->
                <? if (CSite::InDir("/shop/")) : ?>
                    <div class="js-header_basket">
                        <? $APPLICATION->IncludeComponent(
                            "bitrix:sale.basket.basket.line",
                            "header_cart",
                            Array(
                                "HIDE_ON_BASKET_PAGES" => "Y",
                                "PATH_TO_BASKET" => SITE_DIR . "shop/cart/",
                                "PATH_TO_ORDER" => SITE_DIR . "shop/order/make/",
                                "PATH_TO_PERSONAL" => SITE_DIR . "personal/",
                                "PATH_TO_PROFILE" => SITE_DIR . "personal/",
                                "PATH_TO_REGISTER" => SITE_DIR . "login/",
                                "POSITION_FIXED" => "N",
                                "SHOW_AUTHOR" => "N",
                                "SHOW_EMPTY_VALUES" => "Y",
                                "SHOW_NUM_PRODUCTS" => "Y",
                                "SHOW_PERSONAL_LINK" => "Y",
                                "SHOW_PRODUCTS" => "N",
                                "SHOW_TOTAL_PRICE" => "Y"
                            )
                        ); ?>
                    </div>
                <? endif; ?>
                <? if ($USER->IsAuthorized()) : ?>
                    <div class="header__right-auth logged-in">
                        <a class="header__right-user-link" href="<?= SITE_DIR; ?>personal/">
                            <div class="header__right-user-photo"
                                 style="background-image: url(<?= $arUser["PHOTO_SMALL"]; ?>);"></div>
                            <div class="header__right-username-wrapper"><?= ($arUser["TITLE"]) ? $arUser["TITLE"] : $arUser["LOGIN"]; ?></div>
                        </a>
                        <div class="header__right-logout-wrapper">
                            <a class="header__right-logout" href="<?= SITE_DIR; ?>?logout=yes">
                                <span class="icon icon-cross"></span>
                                <span class="header__right-logout-txt"><?= Loc::getMessage("LOGIN_EXIT"); ?></span>
                            </a>
                        </div>
                    </div>
                <? else : ?>
                    <div class="header__right-auth not-logged-in">
                        <span class="icon icon-key"></span>
                        <a class="open-modal header__right-reg" href="javascript:void(0)"
                           data-modal="reg-modal"><?= Loc::getMessage("LOGIN_AUTH"); ?></a>
                        <span>/</span>
                        <a class="open-modal header__right-login" href="javascript:void(0)"
                           data-modal="auth-modal"><?= Loc::getMessage("LOGIN_ENTER"); ?></a>
                    </div>
                <? endif; ?>
            </div>
        </div>
    </div>
</header>
<nav class="side-menu__wrapper <? //=($USER->IsAuthorized()) ? "authorized" : "hidden-lg";?>">
    <div class="side-menu__hamburger" title="<?= Loc::getMessage("ASIDE_MENU_OPEN"); ?>">
        <span class="side-menu__hamburger-wrapper">
            <span class="side-menu__hamburger-line"></span>
            <span class="side-menu__hamburger-line"></span>
            <span class="side-menu__hamburger-line"></span>
        </span>
        <div class="side-menu__txt"><?= Loc::getMessage("ASIDE_MENU_HEADING"); ?></div>
    </div>
    <div class="side-menu__content">
        <? if ($USER->IsAuthorized()) : ?>
            <a class="side-menu__item js-aside_conspect" href="<?= getLink("MY_CONSPECTS"); ?>"
               title="<?= Loc::getMessage("ASIDE_MENU_MY_CONSPECTS"); ?>">
                <span class="icon side-menu__icon icon-notes side-menu__icon"></span>
                <div class="side-menu__txt"><?= Loc::getMessage("ASIDE_MENU_MY_CONSPECTS"); ?></div>
            </a>
            <a class="side-menu__item js-aside_add_conspect" href="<?= getLink("CREATE_CONSPECT"); ?>"
               title="<?= Loc::getMessage("ASIDE_MENU_CREATE_CONSPECT"); ?>">
                <span class="icon side-menu__icon icon-new-note side-menu__icon"></span>
                <div class="side-menu__txt"><?= Loc::getMessage("ASIDE_MENU_CREATE_CONSPECT"); ?></div>
            </a>
            <a class="side-menu__item js-aside_category" href="<?= getLink("MY_CATEGORIES"); ?>"
               title="<?= Loc::getMessage("ASIDE_MENU_MY_CATEGORIES"); ?>">
                <span class="icon side-menu__icon icon-layers side-menu__icon"></span>
                <div class="side-menu__txt"><?= Loc::getMessage("ASIDE_MENU_MY_CATEGORIES"); ?></div>
            </a>
            <a class="side-menu__item" href="<?= getLink("CREATE_CATEGORY"); ?>"
               title="<?= Loc::getMessage("ASIDE_MENU_CREATE_CATEGORY"); ?>">
                <span class="icon side-menu__icon icon-new-category side-menu__icon"></span>
                <div class="side-menu__txt"><?= Loc::getMessage("ASIDE_MENU_CREATE_CATEGORY"); ?></div>
            </a>
            <? if ($arUser["IS_AUTHOR"] != "Y") : ?>
                <a class="side-menu__item js-aside_library" href="<?= getLink("LIBRARY"); ?>"
                   title="<?= Loc::getMessage("ASIDE_MENU_LIBRARY"); ?>">
                    <span class="icon side-menu__icon icon-new-library side-menu__icon"></span>
                    <div class="side-menu__txt"><?= Loc::getMessage("ASIDE_MENU_LIBRARY"); ?></div>
                </a>
                <!--            -->
                <a class="side-menu__item js-aside_new" href="<?= getLink("NEW"); ?>" title="Обновления в библиотеке">
                    <span class="icon side-menu__icon icon-library-updates side-menu__icon">
                        <span class="side-menu__icon-notification js-new_count hidden">0</span>
                    </span>
                    <div class="side-menu__txt">Обновления в библиотеке</div>
                </a>
                <a class="side-menu__item js-aside_notifications" href="<?= getLink("NOTIFICATIONS"); ?>"
                   title="Напоминания">
                    <span class="icon side-menu__icon icon-lamp side-menu__icon">
                        <span class="side-menu__icon-notification js-notifications_count hidden">0</span>
                    </span>
                    <div class="side-menu__txt">Напоминания</div>
                </a>
                <!--            -->
                <a class="side-menu__item js-aside_shop js-auth <?/*link-disabled*/?>" href="<?= getLink("SHOP"); ?>"
                   title="<?= Loc::getMessage("ASIDE_MENU_SHOP"); ?>">
                    <span class="icon side-menu__icon icon-coin side-menu__icon"></span>
                    <div class="side-menu__txt"><?= Loc::getMessage("ASIDE_MENU_SHOP"); ?></div>
                </a>
            <? endif; ?>
            <a class="side-menu__item side-menu__item-bottom js-aside_collapse <?= ($_COOKIE["cards_view"] == "small") ? "to-small" : ""; ?>"
               href="#" title="Переключить вид карточек">
                <span class="icon side-menu__icon side-menu__icon">
                    <span class="glyphicon glyphicon-th-large side-menu__collapse-small"></span>
                    <span class="glyphicon glyphicon-th side-menu__collapse-big"></span>
                </span>
                <div class="side-menu__txt">Переключить вид</div>
            </a>
        <? else : ?>
            <a class="side-menu__item js-aside_library" href="<?= getLink("LIBRARY"); ?>"
               title="<?= Loc::getMessage("ASIDE_MENU_LIBRARY"); ?>">
                <span class="icon side-menu__icon icon-new-library side-menu__icon"></span>
                <div class="side-menu__txt"><?= Loc::getMessage("ASIDE_MENU_LIBRARY"); ?></div>
            </a>
            <a class="side-menu__item js-aside_shop js-auth <?/*link-disabled*/?>" href="<?= getLink("SHOP"); ?>"
               title="<?= Loc::getMessage("ASIDE_MENU_SHOP"); ?>">
                <span class="icon side-menu__icon icon-coin side-menu__icon"></span>
                <div class="side-menu__txt"><?= Loc::getMessage("ASIDE_MENU_SHOP"); ?></div>
            </a>
            <a class="side-menu__item js-aside_add_conspect js-auth" href="<?= getLink("CREATE_CONSPECT"); ?>"
               title="<?= Loc::getMessage("ASIDE_MENU_CREATE_CONSPECT"); ?>">
                <span class="icon side-menu__icon icon-new-note side-menu__icon"></span>
                <div class="side-menu__txt"><?= Loc::getMessage("ASIDE_MENU_CREATE_CONSPECT"); ?></div>
            </a>
        <? endif; ?>
        <div class="side-menu__item--ammount first-item">
            <span class="icon side-menu__icon icon-all-cards side-menu__icon"></span>
            <div class="side-menu__txt"><?= $arTotal["CONSPECTS"]; ?></div>
        </div>
        <div class="side-menu__item--ammount">
            <span class="icon side-menu__icon icon-users side-menu__icon"></span>
            <div class="side-menu__txt"><?= $arTotal["USERS"]; ?></div>
        </div>
        <div class="side-menu__item--search">
            <a class="side-menu__search" href="<?= SITE_DIR; ?>search/">
                <span class="icon side-menu__icon icon-search"></span>
                <span class="side-menu__txt"><?= Loc::getMessage("SEARCH"); ?></span>
            </a>
        </div>
        <div class="side-menu__item--cart">
            <? if (CSite::InDir("/shop/")) : ?>
                <div class="js-header_basket">
                    <? $APPLICATION->IncludeComponent(
                        "bitrix:sale.basket.basket.line",
                        "header_cart",
                        Array(
                            "HIDE_ON_BASKET_PAGES" => "Y",
                            "PATH_TO_BASKET" => SITE_DIR . "shop/cart/",
                            "PATH_TO_ORDER" => SITE_DIR . "shop/order/make/",
                            "PATH_TO_PERSONAL" => SITE_DIR . "personal/",
                            "PATH_TO_PROFILE" => SITE_DIR . "personal/",
                            "PATH_TO_REGISTER" => SITE_DIR . "login/",
                            "POSITION_FIXED" => "N",
                            "SHOW_AUTHOR" => "N",
                            "SHOW_EMPTY_VALUES" => "Y",
                            "SHOW_NUM_PRODUCTS" => "Y",
                            "SHOW_PERSONAL_LINK" => "Y",
                            "SHOW_PRODUCTS" => "N",
                            "SHOW_TOTAL_PRICE" => "Y"
                        )
                    ); ?>
                </div>
            <? endif; ?>
        </div>
        <? if (is_array($arUser)) : ?>
            <div class="side-menu__item--user">
                <a class="side-menu__user" href="<?= SITE_DIR; ?>personal/">
                    <div class="side-menu__user-photo"
                         style="background-image: url(<?= $arUser["PHOTO_SMALL"]; ?>);"></div>
                    <span class="side-menu__txt"><?= $arUser["LOGIN"]; ?></span>
                </a>
            </div>
        <? else : ?>
            <div class="side-menu__item--auth">
                <span class="side-menu__icon icon icon-key"></span>
                <a class="open-modal side-menu__reg side-menu__txt" href="javascript:void(0)"
                   data-modal="reg-modal"><?= Loc::getMessage("LOGIN_AUTH"); ?></a>
                <span>/</span>
                <a class="open-modal side-menu__auth" href="javascript:void(0)"
                   data-modal="auth-modal"><?= Loc::getMessage("LOGIN_ENTER"); ?></a>
            </div>
        <? endif; ?>
        <div class="side-menu__item--links">
            <a class="side-menu__item--link first-item" href="javascript:void(0);">
                <img src="<?= SITE_TEMPLATE_PATH; ?>/images/app-link-img.png" alt="">
            </a>
            <a class="side-menu__item--link--pro" href="javascript:void(0);">pro</a>
        </div>
    </div>
</nav>