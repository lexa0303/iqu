<?php
/**
 * Created by PhpStorm.
 * User: al
 * Date: 10.11.2016
 * Time: 16:58
 */

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
use \Bitrix\Main\Application;

global $APPLICATION;
$doc_root = Application::getDocumentRoot();

$request_list = Application::getInstance()->getContext()->getRequest()->toArray();
foreach ($request_list as $key => $item) {
    $request_list[$key] = CUtil::JSEscape($item);
}

$user_id = $request_list["user_id"];
$conspect_id = $request_list["conspect_id"];
$arAnswers = $request_list["answers"];

$result = Exams::Check($user_id, $conspect_id, $arAnswers);

Exams::SaveResult($user_id, $conspect_id, $result);

echo json_encode($result);