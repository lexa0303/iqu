<?php
/**
 * Created by PhpStorm.
 * User: al
 * Date: 03.11.2016
 * Time: 11:20
 */

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
use \Bitrix\Main\Application;

global $APPLICATION;
$doc_root = Application::getDocumentRoot();


$request_list = Application::getInstance()->getContext()->getRequest()->toArray();
foreach ($request_list as $key => $item) {
    $request_list[$key] = CUtil::JSEscape($item);
}

$action = $request_list["action"];
$request_list["action"] = null;
unset($request_list["action"]);

$arParams = $request_list;

switch ($action) {

    case "add" :

        $return = Categories::Add($arParams);

        if ((is_array($return)) && (!empty($return["errors"]))) {
            echo json_encode($return);
        } else {
            echo $return;
        }

        break;

    case "update" :

        $category_id = $arParams["categories_id"];
        $arParams["categories_id"] = null;
        unset($arParams["categories_id"]);

        $return = Categories::Update($category_id, $arParams);

        if ((is_array($return)) && (!empty($return["errors"]))) {
            echo json_encode($return);
        } else {
            echo $return;
        }

        break;

    case "delete" :

        $return = Categories::Delete($arParams["category_id"], $arParams["user_id"]);

        if ((is_array($return)) && (!empty($return["errors"]))) {
            echo json_encode($return);
        } else {
            echo $return;
        }


        break;

}
