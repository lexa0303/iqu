<?php
/**
 * Created by PhpStorm.
 * User: al
 * Date: 27.10.2016
 * Time: 11:22
 */

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
use \Bitrix\Main\Application;

global $APPLICATION;
$doc_root = Application::getDocumentRoot();


$request_list = Application::getInstance()->getContext()->getRequest()->toArray();
foreach ($request_list as $key => $item) {
    $request_list[$key] = CUtil::JSEscape($item);
}

$action = $request_list["action"];
$request_list["action"] = null;
unset($request_list["action"]);

$arParams = $request_list;

switch ($action) {

    case "add" :

        $return = Cards::Add($arParams);

        if ((is_array($return)) && (!empty($return["errors"]))) {
            echo json_encode($return);
        } else {
            echo true;
        }

        break;

    case "update" :

        $card_id = $arParams["card_id"];
        $arParams["card_id"] = null;
        unset($arParams["card_id"]);

        $return = Cards::Update($card_id, $arParams);

        if ((is_array($return)) && (!empty($return["errors"]))) {
            echo json_encode($return);
        } else {
            echo true;
        }

        break;

    case "delete" :

        $return = Cards::Delete($arParams["card_id"], $arParams["user_id"]);

        if ((is_array($return)) && (!empty($return["errors"]))) {
            echo json_encode($return);
        } else {
            echo $return;
        }


        break;

    case "get_by_conspect" :

        ob_start();

        $APPLICATION->IncludeComponent(
            "custom:cards_list",
            ".default",
            array(
                "IBLOCK_ID" => "4",
                "IBLOCK_TYPE" => "conspect",
                "CONSPECT_ID" => $arParams["conspect_id"],
                "USER_ID" => $arParams["user_id"],
                "COMPONENT_TEMPLATE" => ".default",
                "CACHE_TYPE" => "A",
                "CACHE_TIME" => "36000"
            ),
            false
        );

        $res["html"] = ob_get_contents();
        ob_end_clean();

        $return = json_encode($res);
        echo $return;

        break;

}



