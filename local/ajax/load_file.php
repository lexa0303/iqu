<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
use \Bitrix\Main\Application;
use \Bitrix\Main\Localization\Loc;

global $APPLICATION;
$doc_root = Application::getDocumentRoot();


$request_list = Application::getInstance()->getContext()->getRequest()->toArray();
foreach ($request_list as $key => $item) {
    $request_list[$key] = CUtil::JSEscape($item);
}

$data = array();

//if( $request_list['uploadfiles'] == "y" ) {
    $error = false;
    $files = array();
    $request_files = Application::getInstance()->getContext()->getRequest()->getFileList()->toArray();

    $uploaddir = USER_UPLOAD_PATH_RELATIVE; // - directory where we will load files

    // If directory does not exist
    if (!is_dir($uploaddir)) mkdir($uploaddir, 0777);

    // replace files from temporary folder
    foreach ($request_files as $file) {
        $rand = rand(1,9999);
        if (move_uploaded_file($file['tmp_name'], $doc_root . $uploaddir . basename($rand.$file['name']))) {
            $file = $uploaddir . $rand.$file['name'];
        } else {
            $error = true;
        }
    }

    if ($error) {
        $res_mas = array(
            "status" => false,
            "title" => "Ошибка",//Loc::getMessage("SEND_ERROR_TITLE"),
            "message" => "Ошибка при загрузке файла",//Loc::getMessage("SEND_ERROR")
        );
    } else {
        $res_mas = array(
            "status" => true,
            "file" => $file
        );
    }

    $GLOBALS["APPLICATION"]->RestartBuffer();
    header('Content-Type: application/json');
    echo json_encode($res_mas);
//}
?>