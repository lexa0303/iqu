<?php
/**
 * Created by PhpStorm.
 * User: al
 * Date: 27.10.2016
 * Time: 15:25
 */

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
use \Bitrix\Main\Application;

global $APPLICATION;
$doc_root = Application::getDocumentRoot();


$request_list = Application::getInstance()->getContext()->getRequest()->toArray();
foreach ($request_list as $key => $item) {
    $request_list[$key] = CUtil::JSEscape($item);
}


$action = $request_list["action"];
$request_list["action"] = null;
unset($request_list["action"]);

$arParams = $request_list;

switch ($action) {

    case "add" :

        $return = Conspects::Add($arParams);

        if ((is_array($return)) && (!empty($return["errors"]))) {
            echo json_encode($return);
        } else {
            echo $return;
        }

        break;

    case "update" :

        $conspectID = $arParams["conspect_id"];
        $arParams["conspect_id"] = null;
        unset($arParams["conspect_id"]);

        $return = Conspects::Update($conspectID, $arParams);

        if ((is_array($return)) && (!empty($return["errors"]))) {
            echo json_encode($return);
        } else {
            echo $return;
        }

        break;

    case "delete":

        $return = Conspects::Delete($arParams['conspect_id'],$arParams['user_id']);

        if ((is_array($return)) && (!empty($return["errors"]))) {
            echo json_encode($return);
        } else {
            echo $return;
        }

        break;
}