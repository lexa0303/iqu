<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 16.02.17
 * Time: 16:02
 */

if ($_SERVER["REQUEST_METHOD"] != "POST")
    die();

function dropError($text){
    $return = array(
        "status" => "error",
        "message" => $text
    );

    die(Bitrix\Main\Web\Json::encode($return));
}

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
use \Bitrix\Main\Application;
use \Bitrix\Main\Loader;
use \Bitrix\Main\Localization\Loc;
use \Bitrix\Sale;
use Bitrix\Sale\Delivery;
use Bitrix\Sale\PaySystem;
use Bitrix\Main\Config\Option;
use Bitrix\Sale\DiscountCouponsManager;
use Bitrix\Main\Context;

Loc::loadLanguageFile($_SERVER["DOCUMENT_ROOT"]."/local/templates/".SITE_TEMPLATE_ID."/template.php");

global $APPLICATION;
$doc_root = Application::getDocumentRoot();

$request = array();

$request_list = Application::getInstance()->getContext()->getRequest()->toArray();
foreach ($request_list as $key => $item) {
    $request[$key] = CUtil::JSEscape($item);
}

if (isset($request["action"])) {
    $action = $request["action"];
    $request["action"] = null;
    unset($request["action"]);
} else {
    $return = array(
        "status" => "error",
        "message" => "Неверный action"
    );
    echo Bitrix\Main\Web\Json::encode($return);
    die();
}

global $USER;

$return = array();

switch ($action) {

    case "authorize":
        if (!$request["email"])
            dropError(Loc::getMessage("ERROR_TOAST", array("#FIELD#"=>"email")));
        if (!$request["password"])
            dropError(Loc::getMessage("ERROR_TOAST", array("#FIELD#"=>"пароль")));

        $cUser = new CUser;

        $auth = $cUser->Login($request["email"], $request["password"]);

        $return = array(
            "status" => ($auth["TYPE"] == "ERROR") ? strtolower($auth["TYPE"]) : "success",
            "message" => ($auth["TYPE"] == "ERROR") ? $auth["MESSAGE"] : Loc::getMessage("AJAX_AUTH_SUCCESS"),
            "reload" => ($auth["TYPE"] == "ERROR") ? "N" : "Y",
            "auth" => $auth
        );
        break;
    case "register":
        if (!$request["email"])
            dropError(Loc::getMessage("ERROR_TOAST", array("#FIELD#"=>"email")));
        if (!$request["password"])
            dropError(Loc::getMessage("ERROR_TOAST", array("#FIELD#"=>"пароль")));
        if (!$request["password_repeat"])
            dropError(Loc::getMessage("ERROR_TOAST", array("#FIELD#"=>"повтор пароля")));
        if ($request["password"] != $request["password_repeat"])
            dropError(Loc::getMessage("ERROR_PASSWORD_CHECK"));
        if (!$request["title"])
            dropError(Loc::getMessage("ERROR_TOAST", array("#FIELD#"=>"никнейм")));

        $arNewUser= array(
            "LOGIN" => $request["email"],
            "EMAIL" => $request["email"],
            "PASSWORD" => $request["password"],
            "CONFIRM_PASSWORD" => $request["password_repeat"],
            "LID" => SITE_ID,
            "TITLE" => $request["title"]
        );

        $cUser = new CUser;

        $dbEmailUser = $cUser->GetList($by = "", $order = "", array("EMAIL"=>$arNewUser["EMAIL"]));
        if ($res = $dbEmailUser->Fetch()){
            $return = array(
                "status" => "error",
                "message" => "Пользователь с таким email уже существует"
            );
            break;
        }
        $dbLoginUser = $cUser->GetList($by = "", $order = "", array("LOGIN"=>$arNewUser["LOGIN"]));
        if ($res = $dbLoginUser->Fetch()){
            $return = array(
                "status" => "error",
                "message" => "Пользователь с таким email уже существует"
            );
            break;
        }
        $dbTitleUser = $cUser->GetList($by = "", $order = "", array("TITLE"=>$arNewUser["TITLE"]));
        if ($res = $dbTitleUser->Fetch()){
            $return = array(
                "status" => "error",
                "message" => "Пользователь с таким никнеймом уже существует"
            );
            break;
        }

        if ($userID = $cUser->Add($arNewUser))
            $cUser->Authorize($userID);

        $return = array(
            "status" => "success",
            "message" => Loc::getMessage("MODAL_REG_SUCCESS"),
            "reload" => "Y"
        );
        break;
    case "restore_password_request":
        if (!$request["email"])
            dropError(Loc::getMessage("ERROR_TOAST", array("#FIELD#"=>"email")));

        global $USER;
        $pass = $USER->SendPassword($request["email"], $request["email"]);
        if($pass["TYPE"] == "OK")
            $return = array(
                "status" => "success",
                "message" => Loc::getMessage("MODAL_PASSWORD_RESET_REQUEST_SENT")
            );
        else
            $return = array(
                "status" => "error",
                "message" => Loc::getMessage("ERROR_EMAIL_NOT_FOUND")
            );
        break;
    case "category_add":
        if (!$request["user_id"])
            dropError(Loc::getMessage("ERROR_NO_AUTH"));
        //        if ($request["user_id"] != $USER->GetID())
//            dropError(Loc::getMessage("ERROR_WRONG_USER"));
        if (!$request["category_name"])
            dropError(Loc::getMessage("ERROR_TOAST", array("#FIELD#"=>"название категории")));

        $arCategories = Categories::GetAll(
            $request["user_id"],
            array(
                "UF_NAME" => $request["category_name"]
            )
        );

        if (is_array($arCategories))
            dropError(Loc::getMessage("ERROR_CATEGORY_EXISTS"));

        $arNewCategory = array(
            "UF_NAME" => $request["category_name"],
            "UF_USER_ID" => $request["user_id"]
        );

        $newCategory = Categories::Add($arNewCategory);

        //Check if was called from conspect add/edit page
        if ($request["conspect"] === "Y"){
            if ($newCategory) {
                $arCategory = Categories::GetByID($newCategory);
                $return = array(
                    "status" => "success",
                    "message" => Loc::getMessage("CATEGORY_CREATED_SUCCESS"),
                    "callbackFunction" => "conspectCategoryAddHandler",
                    "callbackParams" => $arCategory
                );
            } else {
                $return = array(
                    "status" => "error",
                    "message" => Loc::getMessage("CATEGORY_CREATED_ERROR")
                );
            }
        } else if ($request["conspect_download"] === "Y"){
            if ($newCategory) {
                $arCategory = Categories::GetByID($newCategory);
                $return = array(
                    "status" => "success",
                    "message" => Loc::getMessage("CATEGORY_CREATED_SUCCESS"),
                    "callbackFunction" => "conspectDownloadCategoryAddHandler",
                    "callbackParams" => $arCategory
                );
            } else {
                $return = array(
                    "status" => "error",
                    "message" => Loc::getMessage("CATEGORY_CREATED_ERROR")
                );
            }
        } else {
            if ($newCategory)
                $return = array(
                    "status" => "success",
                    "message" => Loc::getMessage("CATEGORY_CREATED_SUCCESS"),
                    "location" => getLink("MY_CATEGORIES", $request["user_id"])
                );
            else
                $return = array(
                    "status" => "error",
                    "message" => Loc::getMessage("CATEGORY_CREATED_ERROR")
                );
        }

        break;
    case "category_edit":
        if (!$request["category_id"])
            dropError(Loc::getMessage("ERROR_TOAST", array("#FIELD#"=>"Не выбрана категория")));
        if (!$request["user_id"])
            dropError(Loc::getMessage("ERROR_NO_AUTH"));
        //        if ($request["user_id"] != $USER->GetID())
//            dropError(Loc::getMessage("ERROR_WRONG_USER"));

        ob_start();
        $APPLICATION->IncludeComponent(
            "custom:categories_edit",
            ".default",
            array(
                "CATEGORY_ID" => $request["category_id"],
                "USER_ID" => $request["user_id"],
                "COMPONENT_TEMPLATE" => ".default",
                "CACHE_TYPE" => "A",
                "CACHE_TIME" => "36000"
            ),
            false
        );
        $html = ob_get_contents();
        ob_end_clean();
        $return = array(
            "status" => "html",
            "container" => "edit-category-modal",
            "html" => $html
        );
        break;
    case "edit_category":
        if (!$request["category_id"])
            dropError(Loc::getMessage("ERROR_TOAST", array("#FIELD#"=>"категория")));
        if (!$request["user_id"])
            dropError(Loc::getMessage("ERROR_NO_AUTH"));
        if (!$request["name"])
            dropError(Loc::getMessage("ERROR_TOAST", array("#FIELD#"=>"имя")));
        //        if ($request["user_id"] != $USER->GetID())
//            dropError(Loc::getMessage("ERROR_WRONG_USER"));

        $arCategory = Categories::GetByID($request["category_id"]);

        if (!is_array($arCategory) || $arCategory["UF_USER_ID"] != $USER->GetID()) {
            $return = array(
                "status" => "error",
                "message" => "Категория не найдена"
            );
            break;
        }
        if ($arCategory["UF_NAME"] != $request["name"]) {
            $arNewCategory = array(
                "UF_NAME" => $request["name"]
            );
            Categories::Update($arCategory["ID"], $arNewCategory);
        }

        $return = array(
            "status" => "success",
            "message" => "Название категории успешно изменено",
            "reload" => "Y"
        );

        break;
    case "category_delete":
        if (!$request["category_id"])
            dropError(Loc::getMessage("ERROR_TOAST", array("#FIELD#"=>"Не выбрана категория")));
        if (!$request["user_id"])
            dropError(Loc::getMessage("ERROR_NO_AUTH"));
        //        if ($request["user_id"] != $USER->GetID())
//            dropError(Loc::getMessage("ERROR_WRONG_USER"));

        ob_start();
        $APPLICATION->IncludeComponent(
            "custom:categories_delete",
            ".default",
            array(
                "CATEGORY_ID" => $request["category_id"],
                "USER_ID" => $request["user_id"],
                "COMPONENT_TEMPLATE" => ".default",
                "CACHE_TYPE" => "A",
                "CACHE_TIME" => "36000"
            ),
            false
        );
        $html = ob_get_contents();
        ob_end_clean();
        $return = array(
            "status" => "html",
            "container" => $request["modal"],
            "html" => $html
        );
        break;
    case "delete_category":
        if (!$request["category_id"])
            dropError(Loc::getMessage("ERROR_TOAST", array("#FIELD#"=>"категория")));
        if (!$request["user_id"])
            dropError(Loc::getMessage("ERROR_NO_AUTH"));
        //        if ($request["user_id"] != $USER->GetID())
//            dropError(Loc::getMessage("ERROR_WRONG_USER"));

        $result = Categories::Delete($request["category_id"], $request["user_id"]);

        if ($result === true){
            $return = array(
                "status" => "success",
                "message" => "Категория успешно удалена",
                "reload" => "Y"
            );
        } else {
            $return = array(
                "status" => "error",
                "message" => $result["errors"]
            );
        }
        break;
    case "add_conspect":
        if (!$request["category_id"])
            dropError(Loc::getMessage("ERROR_TOAST", array("#FIELD#"=>"категория")));
        if (!$request["user_id"])
            dropError(Loc::getMessage("ERROR_NO_AUTH"));
        if (!$request["name"])
            dropError(Loc::getMessage("ERROR_TOAST", array("#FIELD#"=>"название конспекта")));
        //if ($request["user_id"] != $USER->GetID())
//            dropError(Loc::getMessage("ERROR_WRONG_USER"));
        if (!$request["conspect"] || !is_array($request["conspect"]))
            dropError(Loc::getMessage("ERROR_EMPTY_CONSPECT"));

        $arNewConspect = array(
            "UF_NAME" => $request["name"],
            "UF_USER_ID" => $request["user_id"],
//            "UF_IMAGE" => ($_FILES["conspect_image"]["name"])
//                ? saveBase64Image($_FILES["conspect_image"]["name"], $request["conspect_image_src"])
//                : ""
        );

        if (isset($request["tags"]) && is_array($request["tags"])){
            $arNewConspect["UF_TAGS"] = $request["tags"];
        }

        if ($request["conspect_image"])
            $arNewConspect["UF_IMAGE"] = $request["conspect_image"];

        if (isset($request["library"]))
            $arNewConspect["UF_SHOW_IN_LIBRARY"] = $request["library"];

        foreach ($request["conspect"] as $arCard) :
            $arNewCard = array(
                "UF_QUESTION" => $arCard["question"]["text"],
                "UF_ANSWER" => $arCard["answer"]["text"],
                "UF_USER_ID" => $request["user_id"]
            );

            if ($arCard["question"]["image_name"]) {
//                $questionImagePath = saveBase64Image($arCard["question"]["image_name"], $arCard["question"]["image"]);
//                $arNewCard["UF_QUESTION_IMAGE"] = $questionImagePath;
                $arNewCard["UF_QUESTION_IMAGE"] = $arCard["question"]["image_name"];
            }
            if ($arCard["answer"]["image_name"]) {
//                $answerImagePath = saveBase64Image($arCard["answer"]["image_name"], $arCard["answer"]["image"]);
//                $arNewCard["UF_ANSWER_IMAGE"] = $answerImagePath;
                $arNewCard["UF_ANSWER_IMAGE"] = $arCard["answer"]["image_name"];
            }

            $arNewConspect["CARDS"][] = $arNewCard;
        endforeach;

        $conspectID = Conspects::Add($arNewConspect, $request["category_id"]);

        if (is_array($conspectID) && $conspectID["error"]){
            $return = array(
                "status" => "error",
                "message" => $conspectID["error"]
            );
        } else {
            $return = array(
                "status" => "success",
                "conspect_id" => $conspectID,
                "message" => "Конспект успешно добавлен",
                "location" => getLink("MY_CONSPECTS", $request["user_id"])
            );
        }
        break;
    case "edit_conspect":
        if (!$request["conspect_id"])
            dropError(Loc::getMessage("ERROR_TOAST", array("#FIELD#"=>"id конспекта")));
        if ($request["skip"] != "Y") {
            if (!$request["category_id"])
                dropError(Loc::getMessage("ERROR_TOAST", array("#FIELD#" => "категория")));
            if (!$request["user_id"])
                dropError(Loc::getMessage("ERROR_NO_AUTH"));
            if (!$request["name"])
                dropError(Loc::getMessage("ERROR_TOAST", array("#FIELD#" => "название конспекта")));
//            if ($request["user_id"] != $USER->GetID())
//                dropError(Loc::getMessage("ERROR_WRONG_USER"));
            if (!$request["conspect"] || !is_array($request["conspect"]))
                dropError(Loc::getMessage("ERROR_EMPTY_CONSPECT"));
        } else {
            check_bitrix_sessid();
        }

        $conspectID = $request["conspect_id"];
        $categoryID = $request["category_id"];

        $arNewConspect = array(
            "UF_NAME" => $request["name"],
            "UF_USER_ID" => $request["user_id"],
            "CATEGORY_ID" => $categoryID,
        );

        if (isset($request["tags"]) && is_array($request["tags"])){
            $arNewConspect["UF_TAGS"] = $request["tags"];
        }
        if (isset($request["library"]))
            $arNewConspect["UF_SHOW_IN_LIBRARY"] = ($request["library"] == "on") ? '1' : '0';
        else
            $arNewConspect["UF_SHOW_IN_LIBRARY"] = '0';

        if ($_FILES["conspect_image"]["name"])
            $arNewConspect["UF_IMAGE"] = saveBase64Image($_FILES["conspect_image"]["name"], $request["conspect_image_src"]);

        if ($request["conspect_image"])
            $arNewConspect["UF_IMAGE"] = $request["conspect_image"];

        foreach ($request["conspect"] as $arCard) :
            $arNewCard = array(
                "UF_QUESTION" => $arCard["question"]["text"],
                "UF_ANSWER" => $arCard["answer"]["text"],
                "UF_USER_ID" => $request["user_id"],
                "ID" => ($arCard["id"]) ? $arCard["id"] : false
            );

            if ($arCard["question"]["image_name"]) {
//                $questionImagePath = saveBase64Image($arCard["question"]["image_name"], $arCard["question"]["image"]);
//                $arNewCard["UF_QUESTION_IMAGE"] = $questionImagePath;
                $arNewCard["UF_QUESTION_IMAGE"] = $arCard["question"]["image_name"];
            }
            if ($arCard["answer"]["image_name"]) {
//                $answerImagePath = saveBase64Image($arCard["answer"]["image_name"], $arCard["answer"]["image"]);
//                $arNewCard["UF_ANSWER_IMAGE"] = $answerImagePath;
                $arNewCard["UF_ANSWER_IMAGE"] = $arCard["answer"]["image_name"];
            }

            $arNewConspect["CARDS"][] = $arNewCard;
        endforeach;

        global $USER;
        $userTitle = $USER->GetParam("TITLE");

        Conspects::Update($conspectID, $arNewConspect);

        $return = array(
            "status" => "success",
            "message" => "Конспект успешно обновлен",
            "conspect" => $arNewConspect,
//            "reload" => "Y"
//            "location" => "/conspects/" . $userTitle . "/"
        );
        if ($request["skip"] == "Y"){
            $return["reload"] = "Y";
            $return["no_action"] = true;
        } else {
            $return["location"] = "/conspects/" . $userTitle . "/";
        }
        break;
    case "delete_card":
        if (!$request["user_id"])
            dropError(Loc::getMessage("ERROR_NO_AUTH"));
        //if ($request["user_id"] != $USER->GetID())
//            dropError(Loc::getMessage("ERROR_WRONG_USER"));
        if (!$request["card_id"])
            dropError(Loc::getMessage("ERROR_TOAST", array("#FIELD#"=>"ID карточки")));
        if (!$request["conspect_id"])
            dropError(Loc::getMessage("ERROR_TOAST", array("#FIELD#"=>"ID конспекта")));

        $bDelete = Cards::Delete($request["card_id"], $request["user_id"]);
        $bDelete = true;

        if ($bDelete === true){
            $return = array(
                "status" => "success",
                "message" => "Карточка удалена"
            );
        } else {
            $return = array(
                "status" => "error",
                "message" => "Ошибка при удалении",
                "error" => $bDelete
            );
        }

        break;
    case "personal_edit":
        check_bitrix_sessid();
        if (!$request["user_id"])
            dropError(Loc::getMessage("ERROR_NO_AUTH"));
        //if ($request["user_id"] != $USER->GetID())
//            dropError(Loc::getMessage("ERROR_WRONG_USER"));

        $arUpdate = array(
            "request" => $_FILES
        );

        if (isset($request["email"]) && isEmail($request["email"]))
            $arUpdate["EMAIL"] = $request["email"];
        if (isset($request["city"]))
            $arUpdate["PERSONAL_CITY"] = $request["city"];
        if (isset($request["phone"]))
            $arUpdate["PERSONAL_PHONE"] = $request["phone"];
        if (isset($request["title"]))
            $arUpdate["TITLE"] = $request["title"];
        if (isset($request["photo"])) {
//            $exp = explode("/", $_FILES["photo"]["type"]);
//            $saveTo = USER_UPLOAD_PATH . $_FILES["photo"]["name"];
//            copy($_FILES["photo"]["tmp_name"], $saveTo);
            $arUpdate["PERSONAL_PHOTO"] = CFile::MakeFileArray($request["photo"]);
        }

        $result = $USER->Update($request["user_id"], $arUpdate);

        $return = array(
            "status" => "success",
            "message" => "Личные данные успешно изменены",
            "reload" => "Y",
            "result" => $result
        );

        break;
    case "change_password":
        if (!$request["old_password"])
            dropError(Loc::getMessage("ERROR_TOAST", array("#FIELD#"=>"текущий пароль")));
        if (!$request["password"])
            dropError(Loc::getMessage("ERROR_TOAST", array("#FIELD#"=>"новый пароль")));
        if (!$request["password_repeat"])
            dropError(Loc::getMessage("ERROR_TOAST", array("#FIELD#"=>"новый пароль")));
        if ($request["password"] != $request["password_repeat"])
            dropError(Loc::getMessage("ERROR_PASSWORD_CHECK"));
        if (!$request["user_id"])
            dropError(Loc::getMessage("ERROR_NO_AUTH"));
        //if ($request["user_id"] != $USER->GetID())
//            dropError(Loc::getMessage("ERROR_WRONG_USER"));

        if (checkPassword($request["user_id"], $request["old_password"])){
            $arUpdate = array(
                "PASSWORD" => $request["password"],
                "CONFIRM_PASSWORD" => $request["password_repeat"]
            );
            $USER->Update($request["user_id"], $arUpdate);

            $return = array(
                "status" => "success",
                "message" => "Пароль успешно изменен",
                "reload" => "Y"
            );
        } else {
            dropError(Loc::getMessage("ERROR_WRONG_PASSWORD"));
        }
        break;
    case "subscribe":
        if (!$request["user_id"])
            dropError(Loc::getMessage("ERROR_TOAST", array("#FIELD#"=>"ID пользователя")));
        if (!$USER->IsAuthorized())
            dropError(Loc::getMessage("ERROR_NO_AUTH"));

        $arUser = $USER->GetList(
            $by="sort",
            $order="asc",
            array("ID"=>$USER->GetID()),
            array(
                "SELECT" => array(
                    "UF_SUBSCRIBED"
                )
            )
        )->Fetch();

        $arSubscribes = $arUser["UF_SUBSCRIBED"];

        $return = array();

        if (!in_array($request["user_id"], $arSubscribes)){
            $arSubscribes[] = (int)$request["user_id"];
        } else {
            foreach ($arSubscribes as $i => $id){
                if ($id == $request["user_id"]){
                    $arSubscribes[$i] = null;
                    unset($arSubscribes[$i]);
                }
            }
            $action = "delete";
        }

        $USER->Update(
            $USER->GetID(),
            array(
                "UF_SUBSCRIBED" => $arSubscribes
            )
        );

        $return = array(
            "status" => "success",
            "message" => ($action == "delete") ? "Вы больше не подписаны на этого пользователя" : "Вы успешно подписаны",
            "callbackFunction" => "checkSubs",
            "callbackParams" => $arSubscribes
        );

        break;
    case "conspect_delete":
        if (!$request["conspect_id"])
            dropError(Loc::getMessage("ERROR_TOAST", array("#FIELD#"=>"конспект")));
        if (!$request["user_id"])
            dropError(Loc::getMessage("ERROR_NO_AUTH"));
        //if ($request["user_id"] != $USER->GetID())
//            dropError(Loc::getMessage("ERROR_WRONG_USER"));

        ob_start();
        $APPLICATION->IncludeComponent(
            "custom:conspect_delete",
            ".default",
            array(
                "CONSPECT_ID" => $request["conspect_id"],
                "USER_ID" => $request["user_id"],
                "COMPONENT_TEMPLATE" => ".default",
                "CACHE_TYPE" => "A",
                "CACHE_TIME" => "36000"
            ),
            false
        );
        $html = ob_get_contents();
        ob_end_clean();
        $return = array(
            "status" => "html",
            "container" => $request["modal"],
            "html" => $html
        );
        break;
    case "delete_conspect":
        if (!$request["conspect_id"])
            dropError(Loc::getMessage("ERROR_TOAST", array("#FIELD#"=>"конспект")));
        if (!$request["user_id"])
            dropError(Loc::getMessage("ERROR_NO_AUTH"));
        //if ($request["user_id"] != $USER->GetID())
//            dropError(Loc::getMessage("ERROR_WRONG_USER"));

        switch ($request["delete_from"]) {
            case "full":
                $result = Conspects::Delete($request["conspect_id"], $request["user_id"]);
                break;
            case "profile":
                $arParams = array(
                    "UF_ACTIVE" => 0
                );
                $result = Conspects::Update($request["conspect_id"], $arParams);
                break;
            case "library":
                $arParams["UF_SHOW_IN_LIBRARY"] = '0';
                $result = Conspects::Update($request["conspect_id"], $arParams);
                break;
            default:
                $result = array(
                    "errors" => "Wrong action"
                );
                break;
        }

        if ($result === true){
            $return = array(
                "status" => "success",
                "message" => "Конспект успешно удален",
                "reload" => "Y",
            );
        } else {
            $return = array(
                "status" => "error",
                "message" => $result["errors"]
            );
        }
        break;
    case "notification_create":
        if (!$request["conspect_id"])
            dropError(Loc::getMessage("ERROR_TOAST", array("#FIELD#"=>"конспект")));
        if (!$request["user_id"])
            dropError(Loc::getMessage("ERROR_NO_AUTH"));
        //if ($request["user_id"] != $USER->GetID())
//            dropError(Loc::getMessage("ERROR_WRONG_USER"));

        ob_start();
        $APPLICATION->IncludeComponent(
            "custom:conspect_notification_create",
            ".default",
            array(
                "CONSPECT_ID" => $request["conspect_id"],
                "USER_ID" => $request["user_id"],
                "COMPONENT_TEMPLATE" => ".default",
                "CACHE_TYPE" => "A",
                "CACHE_TIME" => "36000"
            ),
            false
        );
        $html = ob_get_contents();
        ob_end_clean();
        $return = array(
            "status" => "html",
            "container" => $request["modal"],
            "html" => $html,
            "callbackFunction" => "initNotificationPopup"
        );
        break;
    case "notification_add":
        if (!$request["user_id"])
            dropError(Loc::getMessage("ERROR_NO_AUTH"));
        //if ($request["user_id"] != $USER->GetID())
//            dropError(Loc::getMessage("ERROR_WRONG_USER"));

        ob_start();
        $APPLICATION->IncludeComponent(
            "custom:conspect_notification_create",
            "conspect_select",
            array(
                "CONSPECT_ID" => "",
                "USER_ID" => $request["user_id"],
                "COMPONENT_TEMPLATE" => ".default",
                "CACHE_TYPE" => "A",
                "CACHE_TIME" => "36000"
            ),
            false
        );
        $html = ob_get_contents();
        ob_end_clean();
        global $conspects;
        $return = array(
            "status" => "html",
            "container" => $request["modal"],
            "html" => $html,
            "callbackFunction" => "initNotificationPopup",
            "callbackParams" => array(
                "autocomplete_data" => $conspects
            )
        );
        break;
    case "notification_edit":
        if (!$request["id"])
            dropError(Loc::getMessage("ERROR_TOAST", array("#FIELD#"=>"notification id")));
        if (!$request["user_id"])
            dropError(Loc::getMessage("ERROR_NO_AUTH"));
        //if ($request["user_id"] != $USER->GetID())
//            dropError(Loc::getMessage("ERROR_WRONG_USER"));

        ob_start();
        $APPLICATION->IncludeComponent(
            "custom:conspect_notification_create",
            "edit",
            array(
                "NOTIFICATION_ID" => $request["id"],
                "USER_ID" => $request["user_id"],
                "COMPONENT_TEMPLATE" => "edit",
                "CACHE_TYPE" => "A",
                "CACHE_TIME" => "36000"
            ),
            false
        );
        $html = ob_get_contents();
        ob_end_clean();
        $return = array(
            "status" => "html",
            "container" => $request["modal"],
            "html" => $html,
            "callbackFunction" => "initNotificationPopup"
        );
        break;
    case "create_notification":
        if (!$request["conspect_id"])
            dropError(Loc::getMessage("ERROR_TOAST", array("#FIELD#"=>"конспект")));
        if (!$request["notification_date"])
            dropError(Loc::getMessage("ERROR_TOAST", array("#FIELD#"=>"notification_date")));
        if (!$request["notification_period"])
            dropError(Loc::getMessage("ERROR_TOAST", array("#FIELD#"=>"notification_period")));
        if (!$request["user_id"])
            dropError(Loc::getMessage("ERROR_NO_AUTH"));
        //if ($request["user_id"] != $USER->GetID())
//            dropError(Loc::getMessage("ERROR_WRONG_USER"));

        $dbPeriods = CUserfieldEnum::GetList(array(), array("USER_FIELD_NAME"=>"UF_PERIOD"));

        while ($res = $dbPeriods->Fetch())
            $arPeriods[$res["XML_ID"]] = $res["ID"];

        $arParams = array(
            "UF_CONSPECT" => $request["conspect_id"],
            "UF_NEXT_DATE" => $request["notification_date"],
            "UF_USER_ID" => $request["user_id"],
            "UF_PERIOD" => $arPeriods[$request["notification_period"]]
        );

        $notificationID = Notifications::Add($arParams);

        $return = array(
            "status" => "success",
            "message" => "Напоминание успешно создано",
            "reload" => "Y"
        );
        break;
    case "add_notification":
        if (!$request["conspect_name"])
            dropError(Loc::getMessage("ERROR_TOAST", array("#FIELD#"=>"конспект")));
        if (!$request["notification_date"])
            dropError(Loc::getMessage("ERROR_TOAST", array("#FIELD#"=>"notification_date")));
        if (!$request["notification_period"])
            dropError(Loc::getMessage("ERROR_TOAST", array("#FIELD#"=>"notification_period")));
        if (!$request["user_id"])
            dropError(Loc::getMessage("ERROR_NO_AUTH"));
        //if ($request["user_id"] != $USER->GetID())
//            dropError(Loc::getMessage("ERROR_WRONG_USER"));

        $arConspects = Conspects::GetAll($request["user_id"], array("UF_NAME"=>$request["conspect_name"]));
        $arConspect = array_shift($arConspects);
        if (!is_array($arConspect)){
            dropError("Конспект с таким названием не найден");
        }

        $dbPeriods = CUserfieldEnum::GetList(array(), array("USER_FIELD_NAME"=>"UF_PERIOD"));

        while ($res = $dbPeriods->Fetch())
            $arPeriods[$res["XML_ID"]] = $res["ID"];

        $arParams = array(
            "UF_CONSPECT" => $arConspect["ID"],
            "UF_NEXT_DATE" => $request["notification_date"],
            "UF_USER_ID" => $request["user_id"],
            "UF_PERIOD" => $arPeriods[$request["notification_period"]]
        );

        $notificationID = Notifications::Add($arParams);

        $return = array(
            "status" => "success",
            "message" => "Напоминание успешно создано",
            "id" => $notificationID,
            "params" => array(
                "time" => strtotime($arParams["UF_NEXT_DATE"]),
                "period" => $arParams["UF_PERIOD"],
                "sPeriod" => $request["notification_period"],
                "name" => $arConspect["UF_NAME"],
                "conspect_id" => $arConspect["ID"]
            ),
            "handler" => true
        );
        break;
    case "edit_notification":
        if (!$request["id"])
            dropError(Loc::getMessage("ERROR_TOAST", array("#FIELD#"=>"notification id")));
        if (!$request["notification_date"])
            dropError(Loc::getMessage("ERROR_TOAST", array("#FIELD#"=>"notification_date")));
        if (!$request["user_id"])
            dropError(Loc::getMessage("ERROR_NO_AUTH"));
        //if ($request["user_id"] != $USER->GetID())
//            dropError(Loc::getMessage("ERROR_WRONG_USER"));

        $arNotification = Notifications::GetByID($request["id"]);
        if ($request["user_id"] != $arNotification["UF_USER_ID"])
            dropError(Loc::getMessage("ERROR_WRONG_USER"));

        $dbPeriods = CUserfieldEnum::GetList(array(), array("USER_FIELD_NAME"=>"UF_PERIOD"));

        while ($res = $dbPeriods->Fetch())
            $arPeriods[$res["XML_ID"]] = $res["ID"];

        $date = strtotime($request["notification_date"]);
        $request["notification_date"] = date("d.m.Y H:i", $date);

        $arParams = array(
            "UF_NEXT_DATE" => $request["notification_date"],
            "UF_USER_ID" => $request["user_id"],
        );
        if ($request["notification_period"] && $arPeriods[$request["notification_period"]]){
            $arParams["UF_PERIOD"] = $arPeriods[$request["notification_period"]];
        }

        $notificationID = Notifications::Update($request["id"], $arParams);

        $return = array(
            "status" => "success",
            "message" => "Напоминание успешно изменено",
            "id" => $request["id"],
            "params" => array(
                "time" => strtotime($arParams["UF_NEXT_DATE"]),
                "period" => $arParams["UF_PERIOD"],
                "sPeriod" => $request["notification_period"]
            ),
            "handler" => true
        );
        break;
    case "delete_notification":
        if (!$request["id"])
            dropError(Loc::getMessage("ERROR_TOAST", array("#FIELD#"=>"notification id")));
        if (!$request["user_id"])
            dropError(Loc::getMessage("ERROR_NO_AUTH"));
        //if ($request["user_id"] != $USER->GetID())
//            dropError(Loc::getMessage("ERROR_WRONG_USER"));

        $arNotification = Notifications::GetByID($request["id"]);
        if ($request["user_id"] != $arNotification["UF_USER_ID"])
            dropError(Loc::getMessage("ERROR_WRONG_USER"));

        $notificationID = Notifications::Delete($request["id"]);

        $return = array(
            "status" => "success",
            "message" => "Напоминание успешно удалено",
            "id" => $request["id"],
        );
        break;
    case "notifications_done":
        if (!$USER->IsAuthorized())
            dropError(Loc::getMessage("ERROR_NO_AUTH"));

        $arNotifications = Notifications::GetAll(array(
            "UF_USER_ID" => $USER->GetID(),
            "<=UF_NEXT_DATE"=>ConvertTimeStamp(time(), "FULL")
        ));
        foreach ($arNotifications as $arNotification)
            Notifications::UpdateDate($arNotification["ID"]);

        $return = array(
            "status" => "success",
            "message" => "Все уведомления удалены",
            "reload" => "Y"
        );
        break;
    case "notifications":
        $userID = $USER->GetID();

        if (!$userID){
            $return = array(
                "status" => "error",
                "message" => "No Auth",
            );
        } else {
            $arNotifications = Notifications::GetAll(
                array(
                    "UF_USER_ID" => $userID,
                    "<=UF_NEXT_DATE"=>ConvertTimeStamp(time(), "FULL")
                )
            );

            foreach ($arNotifications as $arNotification){
                $arUniqueNotifications[$arNotification["UF_CONSPECT"]] = $arNotification["UF_CONSPECT"];
            }

            $return = array(
                "status" => "success",
                "count" => count($arUniqueNotifications)
            );
        }
        break;
    case "conspect_study":
        if (!$request["user_id"])
            dropError(Loc::getMessage("ERROR_NO_AUTH"));
        if (!$request["conspect_id"])
            dropError(Loc::getMessage("ERROR_TOAST", array("#FIELD#"=>"id конспекта")));
        //if ($request["user_id"] != $USER->GetID())
//            dropError(Loc::getMessage("ERROR_WRONG_USER"));

        $status = Conspects::SwapStudy($request["conspect_id"], $request["user_id"]);

        if ($status === false){
            $return = array(
                "status" => "error",
                "message" => "Ошибка при изменении статуса",
            );
        } elseif ($status === 1) {
            $return = array(
                "status" => "success",
                "message" => "Конспект отмечен как выученый",
                "result" => $status
            );
        } elseif ($status === 0) {
            $return = array(
                "status" => "success",
                "message" => "Конспект отмечен как не выученый",
                "result" => $status
            );
        } else {
            $return = array(
                "status" => "warning",
                "message" => "Неизвестная ошибка",
                "result" => $status
            );
        }


        break;
    case "exam_settings":
        if (!$request["user_id"])
            dropError(Loc::getMessage("ERROR_NO_AUTH"));
        if (!$request["conspect_id"])
            dropError(Loc::getMessage("ERROR_TOAST", array("#FIELD#"=>"id конспекта")));
        //if ($request["user_id"] != $USER->GetID())
//            dropError(Loc::getMessage("ERROR_WRONG_USER"));
        if (!$request["exam_type"])
            dropError(Loc::getMessage("ERROR_TOAST", array("#FIELD#"=>"тип экзамена")));

        switch ($request["exam_type"]) :
            case "qa":
                if (!$request["qa_answer_speed"] || !$request["qa_question_speed"]){
                    dropError("wrong params");
                }

                $query = array(
                    "answer_speed" => (float)$request["qa_answer_speed"],
                    "question_speed" => (float)$request["qa_question_speed"],
                    "show_first" => ($request["qa_answer_first"]) ? "answer" : "question",
                    "exam_type" => "question_answer",
                    "random" => ($request["qa_random"]) ? 1 : 0,
                    "nonstudied" => ($request["qa_non_studied"]) ? 1 : 0
                );
                $return = array(
                    "status" => "success",
                    "location" => $request["conspect"] . "?" . http_build_query($query)
                );
                break;
            case "yn":
                if (!$request["yn_count_questions"] || !$request["yn_question_speed"]){
                    dropError("wrong params");
                }

                $query = array(
                    "count_questions" => (int)$request["yn_count_questions"],
                    "question_speed" => (float)$request["yn_question_speed"],
                    "exam_type" => "yes_no",
                    "random" => ($request["yn_random"]) ? 1 : 0,
                    "nonstudied" => ($request["yn_non_studied"]) ? 1 : 0
                );
                $return = array(
                    "status" => "success",
                    "location" => $request["conspect"] . "?" . http_build_query($query)
                );
                break;
            case "mch":
                if (!$request["mch_count_questions"] || !$request["mch_question_speed"]){
                    dropError("wrong params");
                }

                $query = array(
                    "count_questions" => (int)$request["mch_count_questions"],
                    "question_speed" => (float)$request["mch_question_speed"],
                    "exam_type" => "mchoice",
                    "random" => ($request["mch_random"]) ? 1 : 0,
                    "nonstudied" => ($request["mch_non_studied"]) ? 1 : 0
                );
                $return = array(
                    "status" => "success",
                    "location" => $request["conspect"] . "?" . http_build_query($query)
                );
                break;
            default:
                $return = array(
                    "status" => "error",
                    "message" => "wrong exam"
                );
                break;
        endswitch;

        break;
    case "save_exam":
        if (!$request["conspect_id"])
            dropError(Loc::getMessage("ERROR_TOAST", array("#FIELD#"=>"id конспекта")));
        if (!$USER->IsAuthorized())
            dropError(Loc::getMessage("ERROR_NO_AUTH"));

        switch ($request["exam_type"]):
            case "yes_no":
                if (!$request["answers"])
                    dropError(Loc::getMessage("ERROR_TOAST", array("#FIELD#"=>"ответы")));

                $arAnswers = $request["answers"];
                $answers = array();
                $correctAnswers = 0;

                $arConspect = Conspects::GetByID($request["conspect_id"]);
                $arCardsFilter = array(
                    "ID" => $arConspect["UF_CARDS"]
                );
                $arCards = Cards::GetAll($arCardsFilter);
                foreach ($arCards as $cardId => $arCard){
                    if (!isset($arAnswers[$cardId])) {
                        $answers[$cardId] = 0;
                        continue;
                    } else {
                        $arAnswer = $arAnswers[$cardId];
                        if ($arAnswer["answer_text"] == $arCard["UF_ANSWER"]){
                            if (isset($arAnswer["answer"])) {
                                if ($arAnswer["answer"] == "Y") {
                                    $answers[$cardId] = 1;
                                    $correctAnswers++;
                                } else {
                                    $answers[$cardId] = 0;
                                }
                            }
                        } else {
                            if (isset($arAnswer["answer"])) {
                                if ($arAnswer["answer"] == "Y") {
                                    $answers[$cardId] = 0;
                                } else {
                                    $answers[$cardId] = 1;
                                    $correctAnswers++;
                                }
                            }
                        }
                    }
                }
                $result = array(
                    "answers" => $answers,
                    "cards" => $request["exam_cards"]
                );
                $result = Exams::SaveResult($USER->GetID(), $request["conspect_id"], $result, EXAM_TYPES[$request["exam_type"]]);
                $return["exam_id"] = $result;
                $return["correct_count"] = $correctAnswers;
                break;
            case "mchoice":
                if (!$request["answers"])
                    dropError(Loc::getMessage("ERROR_TOAST", array("#FIELD#"=>"ответы")));

                $arAnswers = $request["answers"];
                $answers = array();
                $correctAnswers = 0;

                $arConspect = Conspects::GetByID($request["conspect_id"]);
                $arCardsFilter = array(
                    "ID" => $arConspect["UF_CARDS"]
                );
                $arCards = Cards::GetAll($arCardsFilter);
                foreach ($arCards as $cardId => $arCard){
                    if (!isset($arAnswers[$cardId])) {
                        $answers[$cardId] = array(
                            "correct" => 0,
                            "answer" => "Нет ответа"
                        );
                        continue;
                    } else {
                        $arAnswer = $arAnswers[$cardId];
                        if ($arAnswer["answer_text"] == $arCard["ID"]){
                            $answers[$cardId] = array(
                                "correct" => 1,
                                "answer" => $arAnswer["answer_text"]
                            );
                            $correctAnswers++;
                        } else {
                            $answers[$cardId] = array(
                                "correct" => 0,
                                "answer" => $arAnswer["answer_text"]
                            );
                        }
                    }
                }
                $result = array(
                    "answers" => $answers,
                    "cards" => $request["exam_cards"]
                );
                $result = Exams::SaveResult($USER->GetID(), $request["conspect_id"], $result, EXAM_TYPES[$request["exam_type"]]);
                $return["exam_id"] = $result;
                $return["correct_count"] = $correctAnswers;
                break;
            case "question_answer":
                if (!isset($request["count_answers"]))
                    dropError(Loc::getMessage("ERROR_TOAST", array("#FIELD#"=>"count_answers")));
                if (!$request["result"])
                    dropError(Loc::getMessage("ERROR_TOAST", array("#FIELD#"=>"результат")));

                $request["result"] = stripslashes($request["result"]);
                $request["result"] = Bitrix\Main\Web\Json::decode($request["result"]);
        
                $result = Exams::SaveResult($USER->GetID(), $request["conspect_id"], $request["result"], EXAM_TYPES[$request["exam_type"]]);
                break;
        endswitch;

        if (!$result) {
            $return = array(
                "status" => "error",
                "message" => "Ошибка при сохранении экзамена"
            );
        } else {
            $return["status"] = "success";
            $return["message"] = "Экзамен окончен";
        }

        break;
    case "save_conspect":
        if (!$request["user_id"])
            dropError(Loc::getMessage("ERROR_NO_AUTH"));
        if (!$request["conspect_id"])
            dropError(Loc::getMessage("ERROR_TOAST", array("#FIELD#"=>"id конспекта")));
        if (!$request["category_id"])
            dropError(Loc::getMessage("ERROR_TOAST", array("#FIELD#"=>"id категории")));
        //if ($request["user_id"] != $USER->GetID())
//            dropError(Loc::getMessage("ERROR_WRONG_USER"));

        $conspect = Conspects::SaveConspect($request["conspect_id"], $USER->GetID(), $request["category_id"]);

        if ($conspect) {
            if (is_array($conspect) && $conspect["status"] == "error") {
                $return = $conspect;
            } else {
                $return = array(
                    "status" => "success",
                    "message" => "Конспект успешно сохранен",
                    "location" => getLink("MY_CONSPECTS", $request["user_id"])
                );
            }
        } else {
            $return = array(
                "status" => "error",
                "message" => "Ошибка при сохранении конспекта"
            );
        }
        break;
    case "conspect_save":
        if (!$request["conspect_id"])
            dropError(Loc::getMessage("ERROR_TOAST", array("#FIELD#"=>"конспект")));
        if (!$request["user_id"])
            dropError(Loc::getMessage("ERROR_NO_AUTH"));

        ob_start();
        $APPLICATION->IncludeComponent(
            "custom:conspect_download",
            ".default",
            array(
                "CONSPECT_ID" => $request["conspect_id"],
                "USER_ID" => $USER->GetID(),
                "COMPONENT_TEMPLATE" => ".default",
                "CACHE_TYPE" => "A",
                "CACHE_TIME" => "36000"
            ),
            false
        );
        $html = ob_get_contents();
        ob_end_clean();
        $return = array(
            "status" => "html",
            "container" => $request["modal"],
            "html" => $html
        );
        break;
    case "crop_image":
        $imageID = CFile::SaveFile(
            CFile::MakeFileArray($request["image"]),
            "/user_files/"
        );

        $return = array();
        $arImage = array(
            "SRC" => $request["image"],
            "ID" => $imageID
        );
        $newImage = cropImage(
            $arImage,
            $request["crop_data"]["x"],
            $request["crop_data"]["y"],
            $request["crop_data"]["width"],
            $request["crop_data"]["height"]
        );

        if ($newImage !== false){
            $return = array(
                "status" => "success",
                "image" => $newImage,
                "message" => "Изображение успешно изменено"
            );
        } else {
//            AddMessage2File($newImage, $_SERVER["DOCUMENT_ROOT"] . "/testlog.txt");
            $return = array(
                "status" => "error",
                "message" => "Ошибка",
//                "message" => $newImage,
            );
        }

        break;
    case "new":
        global $USER;
        $userID = $USER->GetID();
        $arViewed = $request["viewed_conspects"];

        $arSelfConspects = Conspects::GetAll($userID);

        foreach ($arSelfConspects as $arConspect){
            if ($arConspect["UF_ORIGIN_USER"] && $arConspect["UF_ORIGIN_CONSPECT"])
                $arSelfConspectIDs[] = $arConspect["UF_ORIGIN_CONSPECT"];
        }

        $arViewed = array_merge($arViewed, $arSelfConspectIDs);

        $arNewFilter = array(
            "!ID" => $arViewed,
            "UF_SHOW_IN_LIBRARY" => 1,
            "UF_USER_ID" => $request["subscribes"],
            ">=UF_CREATE_TIME" => array(ConvertTimeStamp(time()-86400, "FULL")),
            "UF_ORIGIN_USER" => false,
            "UF_SHOP" => false
        );
        $arNew = array();
        $dbNew = LenalHelp::getHighLoadBlockList(CONSPECTS_HL, array(), $arNewFilter, array("ID", "UF_NAME"));
        while ($res = $dbNew->Fetch())
            $arNew[] = $res;

        $return = array(
            "count" => count($arNew),
            "cs" => $arNew
        );

        break;
    case "bug_notification":
        if (!$request["text"])
            dropError(Loc::getMessage("ERROR_TOAST", array("#FIELD#"=>"описание ошибки")));

        Loader::includeModule("form");

        $arFormResult = array(
            "form_textarea_1" => strip_tags($request["text"])
        );
        $res = CFormResult::Add(1, $arFormResult);
        if ($res) {
            if (CFormResult::Mail($res))
            {
                $return = array(
                    "status" => "success",
                    "message" => "Ваше сообщение успешно отправлено.",
                    "callbackFunction" => "closePopups"
                );
            }
            else // ошибка
            {
                global $strError;
                $return = array(
                    "status" => "error",
                    "message" => "Ошибка при отправке сообщения.",
                    "error" => $strError
                );
            }
        } else {
            $return = array(
                "status" => "error",
                "message" => "Ошибка при отправке сообщения."
            );
        }
        break;
    case "shop_buy":
        if (!$request["conspect_id"])
            dropError(Loc::getMessage("ERROR_TOAST", array("#FIELD#"=>"id конспекта")));
        if (!$request["shop_id"])
            dropError(Loc::getMessage("ERROR_TOAST", array("#FIELD#"=>"shop id конспекта")));

        Loader::includeModule("sale");

        $return = array();

        $basket = Sale\Basket::loadItemsForFUser(Sale\Fuser::getId(), Bitrix\Main\Context::getCurrent()->getSite());
        
        if ($item = $basket->getExistsItem(
            'catalog',
            $request["shop_id"],
            array(
                array(
                'NAME' => 'Id конспекта',
                'CODE' => 'CONSPECT_ID',
                'VALUE' => $request["conspect_id"],
                'SORT' => 100,
                )
            )
        )){
//            $item->setField('QUANTITY', $item->getQuantity() + $quantity);
//            $return["status"] = "success";
//            $return["message"] = "Вы уже добавили этот конспект в корзину";
//            break;
        } else {
            $item = $basket->createItem('catalog', $request["shop_id"]);
            $item->setFields(array(
                'QUANTITY' => 1,
                'CURRENCY' => Bitrix\Currency\CurrencyManager::getBaseCurrency(),
                'LID' => Bitrix\Main\Context::getCurrent()->getSite(),
                'PRODUCT_PROVIDER_CLASS' => 'CCatalogProductProvider',
            ));
        }

        $result = $basket->save();

        if (!$result->isSuccess()){
            $return["status"] = "error";
            $return["message"] = "Ошибка при добавлении в корзину";
            $return["error"] = $result->getErrorMessages();
        }

        $basketPropertyCollection = $item->getPropertyCollection();

        $basketPropertyCollection->setProperty(array(
            array(
                'NAME' => 'Id конспекта',
                'CODE' => 'CONSPECT_ID',
                'VALUE' => $request["conspect_id"],
                'SORT' => 100,
            ),
        ));
        $basketPropertyCollection->save();

        $result = $basket->save();

        if ($result->isSuccess()){
            ob_start();
            $APPLICATION->IncludeComponent(
                "bitrix:sale.basket.basket.line",
                "header_cart",
                Array(
                    "HIDE_ON_BASKET_PAGES" => "Y",
                    "PATH_TO_BASKET" => SITE_DIR."shop/cart/",
                    "PATH_TO_ORDER" => SITE_DIR."shop/order/make/",
                    "PATH_TO_PERSONAL" => SITE_DIR."personal/",
                    "PATH_TO_PROFILE" => SITE_DIR."personal/",
                    "PATH_TO_REGISTER" => SITE_DIR."login/",
                    "POSITION_FIXED" => "N",
                    "SHOW_AUTHOR" => "N",
                    "SHOW_EMPTY_VALUES" => "Y",
                    "SHOW_NUM_PRODUCTS" => "Y",
                    "SHOW_PERSONAL_LINK" => "Y",
                    "SHOW_PRODUCTS" => "N",
                    "SHOW_TOTAL_PRICE" => "Y"
                )
            );
            $header_basket = ob_get_contents();
            ob_end_clean();
            ob_start();
            ?>
            <a class="modal__btn-close icon icon-cross" href="javascript:void(0)"></a>
            <form class="cart-modal__body" action="">
                <span class="cart-modal__heading">Конспект добавлен в корзину</span>
                <div class="cart-modal__lastCard-wrap"><?
                    $APPLICATION->IncludeComponent(
                        "custom:conspect_card_shop",
                        "shop",
                        Array(
                            "CACHE_TIME" => 0,
                            "CACHE_TYPE" => "N",
                            "CARDS_HLBLOCK" => "4",
                            "CATEGORIES_HLBLOCK" => "3",
                            "CONSPECTS_HLIBLOCK" => "6",
                            "CONSPECT_ID" => $request["conspect_id"],
                            "SEF_MODE" =>  array(
                                "list" => "",
                                "detail" => "#CONSPECT_CODE#/",
                                "user" => "user/#USER_TITLE#/",
                                "cart" => "cart/",
                                "history" => "history/",
                            ),
                            "MASTER" => "N",
                            "SEF_FOLDER" => "shop/",
                        )
                    );
                ?></div>
                <a class="cart-modal__btn-toCart button" href="/shop/cart/">Перейти в корзину</a>
                <a class="cart-modal__btn-back button js-close_modal" href="/shop/">Продолжить покупки</a>
            </form>
            <?
            $popup = ob_get_contents();
            ob_end_clean();

            $return["status"] = "success";
            $return["message"] = "Конспект добавлен в корзину";
            $return["header_basket"] = $header_basket;
            $return["popup"] = $popup;
        } else {
            $return["status"] = "error";
            $return["message"] = "Ошибка при добавлении в корзину";
            $return["error"] = $result->getErrorMessages();
        }

        break;
    case "shop_buy_lots":
        if (!$request["ids"])
            dropError(Loc::getMessage("ERROR_TOAST", array("#FIELD#"=>"id конспекта")));
        if (!$request["trial"])
            dropError(Loc::getMessage("ERROR_TOAST", array("#FIELD#"=>"trial")));

        Loader::includeModule("sale");

        $return = array();

        $basket = Sale\Basket::loadItemsForFUser(Sale\Fuser::getId(), Bitrix\Main\Context::getCurrent()->getSite());


        $ids = Bitrix\Main\Web\Json::decode(stripslashes($request["ids"]));

        foreach ($ids as $arIDs) :

            $shopId = ($request["trial"] == "true") ? $arIDs["TRIAL_ID"] : $arIDs["BUY_ID"];

            if ($item = $basket->getExistsItem(
                'catalog',
                $shopId,
                array(
                    array(
                        'NAME' => 'Id конспекта',
                        'CODE' => 'CONSPECT_ID',
                        'VALUE' => $arIDs["ID"],
                        'SORT' => 100,
                    )
                )
            )){
    //            $item->setField('QUANTITY', $item->getQuantity() + $quantity);
    //            $return["status"] = "success";
    //            $return["message"] = "Вы уже добавили этот конспект в корзину";
    //            break;
            } else {
                $item = $basket->createItem('catalog', $shopId);
                $item->setFields(array(
                    'QUANTITY' => 1,
                    'CURRENCY' => Bitrix\Currency\CurrencyManager::getBaseCurrency(),
                    'LID' => Bitrix\Main\Context::getCurrent()->getSite(),
                    'PRODUCT_PROVIDER_CLASS' => 'CCatalogProductProvider',
                ));
            }

            $result = $basket->save();

            if (!$result->isSuccess()){
                $return["status"] = "error";
                $return["message"] = "Ошибка при добавлении в корзину";
                $return["error"] = $result->getErrorMessages();
            }

            $basketPropertyCollection = $item->getPropertyCollection();

            $basketPropertyCollection->setProperty(array(
                array(
                    'NAME' => 'Id конспекта',
                    'CODE' => 'CONSPECT_ID',
                    'VALUE' => $arIDs["ID"],
                    'SORT' => 100,
                ),
            ));
            $basketPropertyCollection->save();

            $result = $basket->save();

        endforeach;

        if ($result->isSuccess()){
            ob_start();
            $APPLICATION->IncludeComponent(
                "bitrix:sale.basket.basket.line",
                "header_cart",
                Array(
                    "HIDE_ON_BASKET_PAGES" => "Y",
                    "PATH_TO_BASKET" => SITE_DIR."shop/cart/",
                    "PATH_TO_ORDER" => SITE_DIR."shop/order/make/",
                    "PATH_TO_PERSONAL" => SITE_DIR."personal/",
                    "PATH_TO_PROFILE" => SITE_DIR."personal/",
                    "PATH_TO_REGISTER" => SITE_DIR."login/",
                    "POSITION_FIXED" => "N",
                    "SHOW_AUTHOR" => "N",
                    "SHOW_EMPTY_VALUES" => "Y",
                    "SHOW_NUM_PRODUCTS" => "Y",
                    "SHOW_PERSONAL_LINK" => "Y",
                    "SHOW_PRODUCTS" => "N",
                    "SHOW_TOTAL_PRICE" => "Y"
                )
            );
            $header_basket = ob_get_contents();
            ob_end_clean();
            ob_start();
            ?>
            <a class="modal__btn-close icon icon-cross" href="javascript:void(0)"></a>
            <form class="cart-modal__body" action="">
                <span class="cart-modal__heading">Конспекты добавлены в корзину</span>
                <div class="cart-modal__lastCard-wrap"></div>
                <a class="cart-modal__btn-toCart button" href="/shop/cart/">Перейти в корзину</a>
                <a class="cart-modal__btn-back button js-close_modal" href="/shop/">Продолжить покупки</a>
            </form>
            <?
            $popup = ob_get_contents();
            ob_end_clean();

            $return["status"] = "success";
            $return["message"] = "Конспект добавлен в корзину";
            $return["header_basket"] = $header_basket;
            $return["popup"] = $popup;
        } else {
            $return["status"] = "error";
            $return["message"] = "Ошибка при добавлении в корзину";
            $return["error"] = $result->getErrorMessages();
        }

        break;
    case "shop_delete":
        if (!$request["id"])
            dropError(Loc::getMessage("ERROR_TOAST", array("#FIELD#"=>"id")));

        Loader::includeModule("sale");

        $return = array();

        $basket = Sale\Basket::loadItemsForFUser(Sale\Fuser::getId(), Bitrix\Main\Context::getCurrent()->getSite());

        $basket->getItemById($request["id"])->delete();
        $basket->save();

        ob_start();
        $APPLICATION->IncludeComponent(
            "bitrix:sale.basket.basket.line",
            "header_cart",
            Array(
                "HIDE_ON_BASKET_PAGES" => "Y",
                "PATH_TO_BASKET" => SITE_DIR."shop/cart/",
                "PATH_TO_ORDER" => SITE_DIR."shop/order/make/",
                "PATH_TO_PERSONAL" => SITE_DIR."personal/",
                "PATH_TO_PROFILE" => SITE_DIR."personal/",
                "PATH_TO_REGISTER" => SITE_DIR."login/",
                "POSITION_FIXED" => "N",
                "SHOW_AUTHOR" => "N",
                "SHOW_EMPTY_VALUES" => "Y",
                "SHOW_NUM_PRODUCTS" => "Y",
                "SHOW_PERSONAL_LINK" => "Y",
                "SHOW_PRODUCTS" => "N",
                "SHOW_TOTAL_PRICE" => "Y"
            )
        );
        $header_basket = ob_get_contents();
        ob_end_clean();

        $return["status"] = "success";
        $return["message"] = "Товар удален из корзины";
        $return["header_basket"] = $header_basket;

        break;
    case "order_save":
        check_bitrix_sessid();

        Loader::includeModule("sale");

        $return = array();

        function getPropertyByCode($propertyCollection, $code)  {
            foreach ($propertyCollection as $property)
            {
                if($property->getField('CODE') == $code)
                    return $property;
            }
            return false;
        }

        $siteId = \Bitrix\Main\Context::getCurrent()->getSite();

        global $USER;
        $fio = $USER->GetFormattedName();
        $phone = $USER->GetParam("PERSONAL_PHONE");
        $email = $USER->GetEmail();

        $currencyCode = Option::get('sale', 'default_currency', 'USD');

        $basket = Sale\Basket::loadItemsForFUser(Sale\Fuser::getId(), Bitrix\Main\Context::getCurrent()->getSite());

        DiscountCouponsManager::init();
        $order = Sale\Order::create(Bitrix\Main\Context::getCurrent()->getSite(), $USER->GetID());
        $order->setPersonTypeId(1);

        $order->setBasket($basket);

        /*Shipment*/
        $shipmentCollection = $order->getShipmentCollection();
        $shipment = $shipmentCollection->createItem();
        $shipmentItemCollection = $shipment->getShipmentItemCollection();
        $shipment->setField('CURRENCY', $order->getCurrency());
        foreach ($order->getBasket() as $item)
        {
            $shipmentItem = $shipmentItemCollection->createItem($item);
            $shipmentItem->setQuantity($item->getQuantity());
        }

        $arDeliveryServiceAll = Delivery\Services\Manager::getRestrictedObjectsList($shipment);
        $shipmentCollection = $shipment->getCollection();

        if (!empty($arDeliveryServiceAll)) {
            reset($arDeliveryServiceAll);
            $deliveryObj = current($arDeliveryServiceAll);

            if ($deliveryObj->isProfile()) {
                $name = $deliveryObj->getNameWithParent();
            } else {
                $name = $deliveryObj->getName();
            }

            $shipment->setFields(array(
                'DELIVERY_ID' => $deliveryObj->getId(),
                'DELIVERY_NAME' => $name,
                'CURRENCY' => $order->getCurrency()
            ));

            $shipmentCollection->calculateDelivery();
        } else {
            $shipment->setFields(array(
                'DELIVERY_ID' => 1,
                'DELIVERY_NAME' => "Test",
                'CURRENCY' => $order->getCurrency()
            ));

            $shipmentCollection->calculateDelivery();
        }
        /**/

        /*Payment*/
        $arPaySystemServiceAll = [];
        $paySystemId = 1;
        $paymentCollection = $order->getPaymentCollection();

        $remainingSum = $order->getPrice() - $paymentCollection->getSum();
        if ($remainingSum > 0 || $order->getPrice() == 0)
        {
            $extPayment = $paymentCollection->createItem();
            $extPayment->setField('SUM', $remainingSum);
            $arPaySystemServices = PaySystem\Manager::getListWithRestrictions($extPayment);

            $arPaySystemServiceAll += $arPaySystemServices;

            if (array_key_exists($paySystemId, $arPaySystemServiceAll))
            {
                $arPaySystem = $arPaySystemServiceAll[$paySystemId];
            }
            else
            {
                reset($arPaySystemServiceAll);

                $arPaySystem = current($arPaySystemServiceAll);
            }

            if (!empty($arPaySystem))
            {
                $extPayment->setFields(array(
                    'PAY_SYSTEM_ID' => $arPaySystem["ID"],
                    'PAY_SYSTEM_NAME' => $arPaySystem["NAME"]
                ));
            }
            else
                $extPayment->delete();
        }
        /**/

        $order->doFinalAction(true);
        $propertyCollection = $order->getPropertyCollection();

        $emailProperty = getPropertyByCode($propertyCollection, 'EMAIL');
        $emailProperty->setValue($email);

        $phoneProperty = getPropertyByCode($propertyCollection, 'PHONE');
        $phoneProperty->setValue($phone);

        $order->setField('COMMENTS', 'Комментарии');

        $res = $order->save();

        if ($order->getId() > 0) {
            $return = array(
                "status" => "success",
                "message" => "Заказ успешно оформлен",
                "location" => "/shop/history/?order_id=" . $order->getId()
            );
        } else {
            $return = array(
                "status" => "error",
                "message" => "Ошибка при оформлении заказа",
                "error" => $res->getErrorMessages()
            );
        }

        break;
    case "conspect_publish":
        $return = $request;
        if (!$request["conspect_id"])
            dropError(Loc::getMessage("ERROR_TOAST", array("#FIELD#"=>"id конспекта")));
        if (!$request["user_id"])
            dropError(Loc::getMessage("ERROR_NO_AUTH"));
//        if ($request["user_id"] != $USER->GetID())
//            dropError(Loc::getMessage("ERROR_WRONG_USER"));
        check_bitrix_sessid();

        $arConspect = Conspects::GetByID($request["conspect_id"]);

        if ($arConspect["UF_USER_ID"] != $request["user_id"])
            dropError(Loc::getMessage("ERROR_WRONG_USER"));

        $arNewConspect = array(
            "UF_PUBLISH_STATUS" => "PUBLISH_REQUESTED",
            "UF_SHOP_PRICE" => $request["price"],
            "UF_ACTIVE" => false
        );

        $res = Conspects::Update($arConspect["ID"], $arNewConspect);

        if ($res){
            $return = array(
                "status" => "success",
                "message" => "Вам конспект отправлен на модерацию",
                "reload" => "Y"
            );
        } else {
            $return = array(
                "status" => "error",
                "message" => "Ошибка",
                "error" => $res
            );
        }

        break;
    case "conspect_final_publish":
        if (!$request["conspect_id"])
            dropError(Loc::getMessage("ERROR_TOAST", array("#FIELD#"=>"конспект")));

        $arConspect = Conspects::GetByID($request["conspect_id"]);

        if ($arConspect["UF_USER_ID"] == $request["user_id"]){
            if ($request["publish"] == "publish") {
                $res = Conspects::Update($request["conspect_id"], array("UF_ACTIVE" => 1, "UF_PUBLISH_STATUS" => "PUBLISH_ALLOWED"));
                if ($res){
                    $return = array(
                        "status" => "success",
                        "message" => "Конспект будет добавлен в магазин в ближайшее время",
                        "reload" => "Y",
                        "callbackFunction" => "closePopups"
                    );
                }
            } else {
                $res = Conspects::Update($request["conspect_id"], array("UF_PUBLISH_STATUS" => "PUBLISH_REQUESTED", "UF_NOTE" => $request["comment"]));
                if ($res){
                    $return = array(
                        "status" => "success",
                        "message" => "Конспект отправлен на повторную проверку.",
                        "reload" => "Y",
                        "callbackFunction" => "closePopups"
                    );
                }
            }

        } else {
            $return = array(
                "status" => "error",
                "message" => "wrong user"
            );
        }
        break;
    case "conspect_publish_popup":
        if (!$request["conspect_id"])
            dropError(Loc::getMessage("ERROR_TOAST", array("#FIELD#"=>"конспект")));

        ob_start();
        $APPLICATION->IncludeComponent(
            "custom:conspect_publish",
            ".default",
            array(
                "CONSPECT_ID" => $request["conspect_id"],
                "USER_ID" => $USER->GetID(),
                "COMPONENT_TEMPLATE" => ".default",
                "CACHE_TYPE" => "A",
                "CACHE_TIME" => "36000"
            ),
            false
        );
        $html = ob_get_contents();
        ob_end_clean();
        $return = array(
            "status" => "html",
            "container" => $request["modal"],
            "html" => $html
        );
        break;
    case "card_study":
        if (!$request["card_id"])
            dropError(Loc::getMessage("ERROR_TOAST", array("#FIELD#"=>"card_id")));

        $res = Cards::SetStudied($request["card_id"]);

        if (!$res["errors"]){
            $return = array(
                "status" => "success",
                "message" => ($res == 1) ? "Вопрос отмечен как выученый" : "Вопрос отмечен как не выученый"
            );
        } else {
            $return = array(
                "status" => "error",
                "message" => $res["errors"]
            );
        }
        break;
    case "shop_buy-select":
        ob_start();
        $AvailableConspects = new HL('sys_available_conspects');
        $arAvailable = $AvailableConspects->GetList(
            array(
                "filter" => array(
                    "UF_CONSPECT" => $request["conspect_id"],
                    "UF_USER_ID" => ($USER->GetID()) ? $USER->GetID() : 0
                )
            )
        )->Fetch();

        if (!!$arAvailable){
            $arConspects = Conspects::GetAll($USER->GetID(), array("UF_ORIGIN_CONSPECT"=>$arAvailable["UF_CONSPECT"]));
            $arConspect = array_shift($arConspects);
        }

        $APPLICATION->IncludeComponent(
            "custom:conspect_card_shop",
            "choice_popup",
            Array(
                "CACHE_TIME" => 0,
                "CACHE_TYPE" => "N",
                "CARDS_HLBLOCK" => "4",
                "CATEGORIES_HLBLOCK" => "3",
                "CONSPECTS_HLIBLOCK" => "6",
                "CONSPECT_ID" => $request["conspect_id"],
                "SEF_MODE" =>  array(
                    "list" => "",
                    "detail" => "#CONSPECT_CODE#/",
                    "user" => "user/#USER_TITLE#/",
                    "cart" => "cart/",
                    "history" => "history/",
                ),
                "MASTER" => "N",
                "SEF_FOLDER" => "shop/",
                "OWNED" => !!$arAvailable,
                "OWNED_CONSPECT_ID" => $arConspect["ID"]
            )
        );
        $popup = ob_get_contents();
        ob_end_clean();
        $return["status"] = "success";
        $return["popup"] = $popup;
        break;

    default :
        $return = array(
            "status" => "error",
            "message" => "Неверный action"
        );
        break;
}

$return["form_id"] = $request["form_id"];

echo Bitrix\Main\Web\Json::encode($return);