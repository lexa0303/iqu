<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Персональный раздел");
?>
<?$APPLICATION->IncludeComponent(
    "custom:personal",
    "",
    Array()
);?>
<?php
global $arUser;
if ($arUser["IS_AUTHOR"] == "Y") :?>
    <?$APPLICATION->IncludeComponent(
        "custom:personal_authors",
        "",
        Array()
    );?>
<?endif;?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>