<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 20.06.17
 * Time: 14:55
 */

include_once("admin_tools.php");

global $USER;


\Bitrix\Main\Loader::registerAutoLoadClasses(
    "iquadmin",
    array(
        "iquMain" => "classes/general/iquMain.php"
    )
);

global $IQU;
$IQU = new iquMain;