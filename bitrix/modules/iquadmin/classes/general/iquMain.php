<?php

/**
 * Created by PhpStorm.
 * User: alex
 * Date: 03.07.17
 * Time: 10:50
 */

/**
 * Helper for IQU admin preferences
 * Class iquMain
 */
class iquMain
{

    private $bCanCorrect = false;
    private $bCanDesign = false;
    private $bCanEdit = false;

    private $authorGroup = 9;
    private $correctorGroup = 10;
    private $designerGroup = 11;
    private $editorGroup = 12;

    function __construct()
    {
        /**
         * @var $USER CUser
         */
        global $USER;


        if ($USER->IsAuthorized()) {
            if ($USER->IsAdmin()) {
                $this->bCanCorrect = true;
                $this->bCanDesign = true;
                $this->bCanEdit = true;
            } else {
                $arUserGroups = $USER->GetUserGroupArray();

                if (in_array($this->correctorGroup, $arUserGroups)){
                    $this->bCanCorrect = true;
                }
                if (in_array($this->designerGroup, $arUserGroups)){
                    $this->bCanDesign = true;
                }
                if (in_array($this->editorGroup, $arUserGroups)){
                    $this->bCanEdit = true;
                }
            }
        }
    }

    public function canCorrect(){
        return !!$this->bCanCorrect;
    }

    public function canDesign(){
        return !!$this->bCanDesign;
    }

    public function canEdit(){
        return !!$this->bCanEdit;
    }
}