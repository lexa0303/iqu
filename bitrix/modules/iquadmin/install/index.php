<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 20.06.17
 * Time: 14:20
 */

Class iquadmin extends CModule {
    var $MODULE_ID = "iquadmin";
    var $PARTNER_NAME;
    var $PARTNER_URI;

    public function __construct() {

        $arModuleVersion = array();

        $path = str_replace('\\', '/', __FILE__);
        $path = substr($path, 0, strlen($path) - strlen('/index.php'));
        include($path.'/version.php');

        if (is_array($arModuleVersion) && array_key_exists('VERSION', $arModuleVersion)) {
            $this->MODULE_VERSION = $arModuleVersion['VERSION'];
            $this->MODULE_VERSION_DATE = $arModuleVersion['VERSION_DATE'];
        }

        $this->PARTNER_NAME = "Абрамов Алексей";
        $this->PARTNER_URI = "https://lenal.biz/";

        $this->MODULE_NAME = "Админ часть IQU";
        $this->MODULE_DESCRIPTION = "test";
    }

    function DoInstall(){
        CopyDirFiles($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/iquadmin/install/admin', $_SERVER['DOCUMENT_ROOT']."/bitrix/admin");
        RegisterModule($this->MODULE_ID);
        return true;
    }

    function DoUninstall(){
        DeleteDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/iquadmin/install/admin", $_SERVER["DOCUMENT_ROOT"]."/bitrix/admin");
        UnRegisterModule($this->MODULE_ID);
        return true;
    }
}