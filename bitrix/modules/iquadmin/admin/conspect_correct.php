<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 21.06.17
 * Time: 17:45
 */

use Bitrix\Main\Loader;
use Bitrix\Main\Page\Asset;

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_before.php");

global $USER_FIELD_MANAGER, $APPLICATION;

$APPLICATION->SetTitle("Коррекция конспектов");

Loader::includeModule("iquadmin");

/**
 * @var $IQU iquMain
 */
global $IQU;

if (!$IQU->canCorrect()) {
    $APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));
}

$oSort = new CAdminSorting(CONSPECTS_HL, "ID", "desc");
$lAdmin = new CAdminList(CONSPECTS_HL, $asSort);

$arHeaders = array(array(
    'id' => 'ID',
    'content' => 'ID',
    'sort' => 'ID',
    'default' => true
));

$USER_FIELD_MANAGER->AdminListAddHeaders("HLBLOCK_6", $arHeaders);

foreach ($arHeaders as &$arHeader) {
    $arHeader["default"] = true;
}

$lAdmin->AddHeaders($arHeaders);


$Conspects = new HL(CONSPECTS_HL);
$arConspectsFields = $Conspects->getMap();

$rsConspects = $Conspects->GetList(
    array(
        "filter" => array(
            "UF_PUBLISH_STATUS" => 15
        )
    )
);

$rsAdminConspects = new CAdminResult($rsConspects, CONSPECTS_HL);

while ($res = $rsAdminConspects->Fetch()) {
    $row = $lAdmin->AddRow($res["ID"], $res);
    $USER_FIELD_MANAGER->AddUserFields('HLBLOCK_6', $res, $row);

    $arActions = array();

    $arActions[] = array(
        "ICON" => "edit",
        "TEXT" => "Проверить",
        "ACTION" => $lAdmin->ActionRedirect("conspect_correct_detail.php?ID=".$res["ID"].'&lang='.LANGUAGE_ID),
        "DEFAULT" => true
    );

    $row->AddActions($arActions);

    $arConspects[] = $row;
}




/* ADD BUTTONS TO HEADING */
$lAdmin->AddAdminContextMenu(array(), false, true);


$lAdmin->CheckListMode();

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");
Asset::getInstance()->addJs("/bitrix/js/iquadmin/script.js");

$lAdmin->DisplayList();
