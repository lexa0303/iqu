<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 20.06.17
 * Time: 14:38
 */

use Bitrix\Main\Loader;

global $IQU;
if (!$IQU){
    Loader::includeModule("iquadmin");
    $IQU = new iquMain;
}

$arItems = array();

if ($IQU->canDesign()) {
    $arItems[] = array(
        "text" => "Дизайнер",
        "url" => "/bitrix/admin/conspect_design.php",
        "title" => "Дизайнер",
        "more_url" => array(
            "conspect_design_detail.php"
        ),
    );
}

if ($IQU->canCorrect()) {
    $arItems[] = array(
        "text" => "Корректор",
        "url" => "/bitrix/admin/conspect_correct.php",
        "title" => "Корректор",
        "more_url" => array(
            "conspect_correct_detail.php"
        ),
    );
}

if ($IQU->canEdit()) {
    $arItems[] = array(
        "text" => "Редактор",
        "url" => "/bitrix/admin/conspect_edit.php",
        "title" => "Редактор",
        "more_url" => array(
            "conspect_edit_detail.php"
        ),
    );
}

if ($IQU->canEdit()) {
    $arItems[] = array(
        "text" => "Публикация",
        "url" => "/bitrix/admin/conspect_publish.php",
        "title" => "Публикация",
        "more_url" => array(
            "conspect_publish_detail.php"
        ),
    );
}

if (!empty($arItems)) {
    $aMenu[] = array(
        "parent_menu" => "global_menu_content",
        "sort" => 1800,
        "text" => "IQU Admin",
        "title" => "IQU Admin",
        "icon" => "util_menu_icon",
        "page_icon" => "util_page_icon",
        "items_id" => "menu_util",
        "items" => $arItems
    );
}

return $aMenu;