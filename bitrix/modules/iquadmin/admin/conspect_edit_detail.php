<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 21.06.17
 * Time: 17:45
 */

use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Page\Asset;

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_before.php");
global $USER_FIELD_MANAGER, $APPLICATION, $DB;

Loader::includeModule("iquadmin");

/**
 * @var $IQU iquMain
 */
global $IQU;

if (!$IQU->canEdit()) {
    $APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));
}

$errors = array();

$Conspects = new HL(CONSPECTS_HL);
$arConspectsFields = $Conspects->getMap();

$arConspect = Conspects::GetByID($_REQUEST["ID"]);
if ($arConspect) {
    $arConspect["USER"]["AUTHOR_NAME"] = $arConspect["USER"]["NAME"] . " " . $arConspect["USER"]["LAST_NAME"];
}
if (!$arConspect || $arConspect["UF_PUBLISH_STATUS"] != 17) {
    $fatalErrors[] = "Конспект не найден";
}

/**/

if ($_SERVER["REQUEST_METHOD"] == "POST" &&
    $_REQUEST["update"] == "Y") {
    $Cards = new HL(CARDS_HL);
    $Conspects = new HL(CONSPECTS_HL);

    $arConspect = $Conspects->GetList(
        array(
            "filter" => array(
                "ID" => $_REQUEST["ID"]
            )
        )
    )->Fetch();

    if (is_array($_REQUEST["cards"])) {
        $rsCards = $Cards->GetList(
            array(
                "filter" => array(
                    "ID" => array_keys($_REQUEST["cards"])
                )
            )
        );
        while ($res = $rsCards->Fetch()) {
            $arCards[$res["ID"]] = $res;
        }
    }

    foreach ($_REQUEST["cards"] as $id => $arNewCard) {
        if (is_array($arCards[$id])) {
            if (!$arNewCard["UF_QUESTION_IMAGE"]) {
                $arNewCard["UF_QUESTION_IMAGE"] = null;
                unset($arNewCard["UF_QUESTION_IMAGE"]);
            }
            if (!$arNewCard["UF_ANSWER_IMAGE"]) {
                $arNewCard["UF_ANSWER_IMAGE"] = null;
                unset($arNewCard["UF_ANSWER_IMAGE"]);
            }
            $arUpdateCard = $Cards->PrepareUpdate($arNewCard);
            $DB->StartTransaction();
            $res = $Cards->Update($id, $arUpdateCard);
            if (!$res->isSuccess()) {
                $DB->Rollback();
                $message = $res->getErrorMessages();
                break;
            } else {
                $DB->Commit();
                $arUpdated[$id] = $arUpdateCard;
            }
        }
    }

    $arUpdateFields = array();
    foreach ($_REQUEST as $key => $field){
        if (array_key_exists($key, $arConspectsFields)){
            if ($arConspectsFields[$key]["USER_TYPE_ID"] == "double") {
                $arUpdateFields[$key] = (double)$field;
            } elseif ($arConspectsFields[$key]["USER_TYPE_ID"] == "number") {
                $arUpdateFields[$key] = (int)$field;
            } else {
                $arUpdateFields[$key] = $field;
            }
        }
    }
    $res = $Conspects->Update($arConspect["ID"], $arUpdateFields);

    if (isset($_REQUEST["apply"]) && $_REQUEST["apply"]) {
        LocalRedirect("/bitrix/admin/conspect_edit_detail.php?ID=" . $_REQUEST["ID"] . "&hlrow_edit_6_active_tab=" . $_REQUEST["hlrow_edit_6_active_tab"]);
    } elseif (isset($_REQUEST["save"]) && $_REQUEST["save"]) {
        $res = $Conspects->Update(
            $arConspect["ID"],
            array(
                "UF_SHOP" => 1,
                "UF_SHOP_PRICE" => ($arUpdateFields["UF_SHOP_PRICE"]) ? $arUpdateFields["UF_SHOP_PRICE"] : $arConspect["UF_SHOP_PRICE"]
            )
        );
        Conspects::Update(
            $arConspect["ID"],
            array(
                "UF_PUBLISH_STATUS" => "APPROVED",
            )
        );
        LocalRedirect("/bitrix/admin/conspect_edit.php?lang=" . LANGUAGE_ID);
    }
}

/**/

$APPLICATION->ShowCSS();

Asset::getInstance()->addCss("/local/templates/iq/fonts/icomoon/style.css");
Asset::getInstance()->addCss("/local/templates/iq/external/cropper/cropper.min.css");
Asset::getInstance()->addCss("/local/templates/iq/css/style.css");
Asset::getInstance()->addCss("/local/templates/iq/css/common.css");
Asset::getInstance()->addCss("/bitrix/css/iquadmin/style.css");

Asset::getInstance()->addJs("/bitrix/js/iquadmin/jquery-2.2.4.min.js");
Asset::getInstance()->addJs("/bitrix/js/iquadmin/cropper.min.js");
Asset::getInstance()->addJs("/bitrix/js/iquadmin/script.js");



$arAllowedFields = array(
    "UF_NAME",
    "UF_IMAGE"
);

$arConspect["CARDS"] = Cards::GetAll(array("ID" => $arConspect["UF_CARDS"]));

foreach ($arConspect as $key => $arField) {
    if (in_array($key, $arAllowedFields)) {
        $arViewConspect[$key] = $arField;
    }
}

$aMenu = array(
    array(
        "TEXT" => "Назад",
        "TITLE" => "Назад",
        "LINK" => "conspect_edit.php?lang=" . LANGUAGE_ID,
        "ICON" => "btn_list",
    )
);

$context = new CAdminContextMenu($aMenu);

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");

$APPLICATION->SetTitle($arConspect["UF_NAME"] . " - редакция конспекта");

$context->Show();
?>
<?php
if (!empty($fatalErrors)) :
    CAdminMessage::ShowMessage(join("\n", $fatalErrors));
else :

    if (!empty($errors)){
        CAdminMessage::ShowMessage(join("\n", $errors));
    }

    $aTabs = array(
        array(
            "DIV" => "conspect_info",
            "TAB" => "О конспекте",
            "ICON" => "ad_contract_edit",
            "TITLE" => "О конспекте"
        ),
        array(
            "DIV" => "cards",
            "TAB" => "Редактирование конспекта",
            "ICON" => "ad_contract_edit",
            "TITLE" => "Редактирование конспекта"
        )
    );

    $tabControl = new CAdminForm("hlrow_edit_6", $aTabs);
    $tabControl->SetShowSettings(false);

    ?>
    <script>
        var body = $("body");

        var globals = {
            ajax: '<?=AJAX;?>',
            ajaxFile: '<?=AJAX_FILE;?>',
            site_id: '<?=SITE_ID;?>',
            lang_id: '<?=LANGUAGE_ID;?>',
            default_image: '<?=DEFAULT_IMAGE;?>'
        };
    </script>
    <div class="modal" id="change-image-modal">
        <a class="modal__btn-close icon icon-cross js-close_modals" href="javascript:void(0)"></a>
        <form class="new-pass-modal__body ajax_submit">
            <span class="new-pass-modal__heading js-image_crop-heading">Выберите фото</span>
            <!-- Cropper start -->
            <div id="cropper-wrap" class="change-image-wrap">
                <img class="js-cropper-image" id="cropper-image" src="" alt="">
            </div>
            <div id="cropper-preview"></div>
            <div id="cropper-cropped"></div>
            <button id="cropper-reset" title="Сбросить">
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1"
                     viewBox="0 0 44 44" enable-background="new 0 0 44 44" width="24px" height="24px">
                    <path d="m22,0c-12.2,0-22,9.8-22,22s9.8,22 22,22 22-9.8 22-22-9.8-22-22-22zm3.3,33.5c-0.6,0.2-1.3-0.3-1.3-1v-2.1c0-0.4 0.2-0.8 0.6-0.9 3.1-1.1 5.4-4.1 5.4-7.5 0-4.4-3.6-8-8-8s-8,3.6-8,8c0,1.4 0.4,2.8 1.1,4 0.1,0.2 0.4,0.3 0.7,0.2 0.2-0.1 0.2-0.3 0.2-0.4v-0.8c0-0.6 0.4-1 1-1h2c0.6,0 1,0.4 1,1v8c0,0.6-0.4,1-1,1h-8c-0.6,0-1-0.4-1-1v-2c0-0.6 0.4-1 1-1h1c0.3,0 0.5-0.2 0.5-0.5 0-0.1 0-0.2-0.1-0.3-1.6-2-2.4-4.5-2.4-7.2 0-6.6 5.4-12 12-12s12,5.4 12,12c0,5.5-3.7,10.1-8.7,11.5z"/>
                </svg>
            </button>
            <label class="change-img-input-label">
                <input type="file"
                       accept="image/jpeg, image/png, image/jpg, image/gif"
                       name="image_cropped"
                       class="change-img-input"
                       id="image_cropped">
                <span class="change-img-input-span">Загрузить фото</span>
            </label>
            <div class="progress_bar hidden"></div>
            <!--        <button id="cropper-crop">Crop</button>-->
            <!-- Cropper end -->

            <button id="cropper-apply" class="button button--rounded new-pass-modal__btn" href="javascript:void(0)">
                Сохранить
            </button>
            <a class="new-pass-modal__btn--txt js-close_modals" href="javascript:void(0)">Отмена</a>
        </form>
    </div>
    <div class="modal-bg js-close_modals"></div>
    <? $tabControl->BeginEpilogContent(); ?>
        <?= bitrix_sessid_post() ?>
        <input type="hidden" name="ID" value="<?= htmlspecialcharsbx(!empty($arConspect) ? $arConspect['ID'] : '') ?>">
        <input type="hidden" name="lang" value="<?= LANGUAGE_ID ?>">
        <input type="hidden" name="update" value="Y">
    <? $tabControl->EndEpilogContent(); ?>

    <?
    $tabControl->Begin(
        array(
            "FORM_ACTION" => $APPLICATION->GetCurPage() . "?ID=" . IntVal($arConspect["ID"]) . "&lang=" . LANG
        )
    );
    ?>

    <? $tabControl->BeginNextFormTab(); ?>

    <? $tabControl->AddViewField("ID", "ID:", !empty($arConspect) ? $arConspect['ID'] : ''); ?>
    <? $tabControl->AddViewField("NAME", "Название:", $arConspect["UF_NAME"]); ?>
    <? $tabControl->AddViewField("AUTHOR", "Автор:", $arConspect["USER"]["AUTHOR_NAME"]); ?>
    <? $tabControl->AddViewField("NOTE", "Комментарий:", $arConspect["UF_NOTE"]); ?>

    <? $tabControl->BeginNextFormTab(); ?>
    <? $tabControl->AddEditField("UF_SHOP_PRICE", "Цена", true, array(), $arConspect["UF_SHOP_PRICE"]); ?>
    <? $tabControl->AddTextField("UF_NOTE", "Комментарий", $arConspect["UF_NOTE"], array("cols" => 50, "rows" => 5)); ?>
    <? $tabControl->BeginCustomField("UF_TAGS", ""); ?>
        <? $tagField = $arConspectsFields["UF_TAGS"];
            $tagField["EDIT_FORM_LABEL"] = "Тэги";
            $tagField["VALUE"] = $arConspect["UF_TAGS"];
            $tagField["USER_TYPE"] = array(
                "USER_TYPE_ID" => "string",
                "CLASS_NAME" => "CUserTypeString",
                "DESCRIPTION" => "Строка",
                "BASE_TYPE" => "String"
            );?>
        <?= $USER_FIELD_MANAGER->GetEditFormHTML(false, "UF_TAGS", $tagField);?>
    <? $tabControl->EndCustomField("UF_TAGS"); ?>
    <? foreach ($arConspect["CARDS"] as $key => $arCard) : ?>
        <? $tabControl->BeginCustomField($key, ""); ?>
            <div id="card-<?= $arCard["ID"]; ?>" class="new-card js-editable" data-id="<?= $arCard["ID"]; ?>">
                <div class="new-card__wrapper js-question_wrap">
                    <div class="new-card__title"><?= Loc::getMessage("QUESTION"); ?></div>
                    <div class="new-card__body">
                        <div class="new-card__img new-card__img-width">
                            <img id="question_image-<?= $key; ?>"
                                 class="js-question_image"
                                <? if (is_array($arCard["UF_QUESTION_IMAGE"])): ?>
                                    src="<?= $arCard["UF_QUESTION_IMAGE"]["SRC"]; ?>"
                                <? endif; ?>
                                 alt=""></div>
                        <textarea name="cards[<?= $arCard["ID"]; ?>][UF_QUESTION]" class="new-card__text js-question_text"
                                  contenteditable="true"><?= $arCard["UF_QUESTION"]; ?></textarea>
                        <div class="new-card__btn new-card__btn--add js-question_image_add <?= ($arCard["UF_QUESTION_IMAGE"]["ID"] > 0) ? "hidden" : ""; ?>">
                            <label id="question_image-label-<?= $key; ?>"
                                   for="question_image-input-<?= $key; ?>"
                                   class="new-card__btn-descr js-question_image-label"
                                   title="<?= Loc::getMessage("CREATE_CONSPECT_ADD_CARD_IMAGE"); ?>">
                                <span class="icon icon-add-photo"></span>
                            </label>
                            <input name="cards[<?= $arCard["ID"]; ?>][UF_QUESTION_IMAGE]"
                                   id="question_image-input-<?= $key; ?>"
                                   class="new-card__add-img js-question_image-input"
                                   type="text">
                        </div>
                    </div>
                </div>
                <script>
                    (function () {
                        var params = {
                            input_file: document.querySelector("#question_image-input-<?=$key;?>"),
                            input_label: document.querySelector("#question_image-label-<?=$key;?>"),
                            image: document.querySelector("#question_image-<?=$key;?>")
                        };

                        var question<?=$key;?>Cropper = new imageCropper(params);
                        question<?=$key;?>Cropper.Init();
                    })();
                </script>
                <div class="new-card__wrapper js-answer_wrap">
                    <div class="new-card__title"><?= Loc::getMessage("ANSWER"); ?></div>
                    <div class="new-card__body">
                        <div class="new-card__img new-card__img-width">
                            <img id="answer_image-<?= $key; ?>" class="js-answer_image"
                                <? if (is_array($arCard["UF_ANSWER_IMAGE"])): ?>
                                    src="<?= $arCard["UF_ANSWER_IMAGE"]["SRC"]; ?>"
                                <? endif; ?>
                                 alt="">
                        </div>
                        <textarea name="cards[<?= $arCard["ID"]; ?>][UF_ANSWER]"
                                  class="new-card__text js-answer_text"><?= $arCard["UF_ANSWER"]; ?></textarea>
                        <div class="new-card__btn new-card__btn--add js-answer_image_add <?= ($arCard["UF_ANSWER_IMAGE"]["ID"] > 0) ? "hidden" : ""; ?>">
                            <label id="answer_image-label-<?= $key; ?>" for="answer_image-input-<?= $key; ?>"
                                   class="new-card__btn-descr js-answer_image-label"
                                   title="<?= Loc::getMessage("CREATE_CONSPECT_ADD_CARD_IMAGE"); ?>">
                                <span class="icon icon-add-photo"></span>
                            </label>
                            <input name="cards[<?= $arCard["ID"]; ?>][UF_ANSWER_IMAGE]"
                                   id="answer_image-input-<?= $key; ?>"
                                   class="new-card__add-img js-answer_image-input"
                                   type="text">
                        </div>
                    </div>
                </div>
                <script>
                    (function () {
                        var params = {
                            input_file: document.querySelector("#answer_image-input-<?=$key;?>"),
                            input_label: document.querySelector("#answer_image-label-<?=$key;?>"),
                            image: document.querySelector("#answer_image-<?=$key;?>")
                        };

                        var answer<?=$key;?>Cropper = new imageCropper(params);
                        answer<?=$key;?>Cropper.Init();
                    })();
                </script>
            </div>
        <? $tabControl->EndCustomField($key); ?>
    <? endforeach; ?>

    <? $tabControl->Buttons(
        array(
            "back_url" => "correct.php?lang=" . LANGUAGE_ID
        )
    ); ?>

    <? $tabControl->Show(); ?>
<? endif; ?>