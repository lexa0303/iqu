<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 27.06.17
 * Time: 16:12
 */

if (!function_exists("_ShowStringPropertyField")) {
    function _ShowStringPropertyField($name, $property_fields, $values, $bInitDef = false, $bVarsFromForm = false)
    {
        global $bCopy;

        $rows = $property_fields["ROW_COUNT"];
        $cols = $property_fields["COL_COUNT"];

        $MULTIPLE_CNT = intval($property_fields["MULTIPLE_CNT"]);
        if ($MULTIPLE_CNT <= 0 || $MULTIPLE_CNT > 30)
            $MULTIPLE_CNT = 5;

        $bInitDef = $bInitDef && (strlen($property_fields["DEFAULT_VALUE"]) > 0);

        $cnt = ($property_fields["MULTIPLE"] == "Y" ? $MULTIPLE_CNT + ($bInitDef ? 1 : 0) : 1);

        $start = 0;
        $show = true;


        if ($property_fields["WITH_DESCRIPTION"] == "Y")
            $strAddDesc = "[VALUE]";
        else
            $strAddDesc = "";

        if (!is_array($values))
            $values = array();

        foreach ($values as $key => $val) {
            $show = false;
            if ($bCopy) {
                $key = "n" . $start;
                $start++;
            }

            echo "<tr><td>";

            $val_description = "";
            if (is_array($val) && array_key_exists("VALUE", $val)) {
                $val_description = $val["DESCRIPTION"];
                $val = $val["VALUE"];
            }

            if ($rows > 1)
                echo '<textarea name="' . $name . '[' . $key . ']' . $strAddDesc . '" cols="' . $cols . '" rows="' . $rows . '">' . htmlspecialcharsex($val) . '</textarea>';
            else
                echo '<input name="' . $name . '[' . $key . ']' . $strAddDesc . '" value="' . htmlspecialcharsex($val) . '" size="' . $cols . '" type="text">';

            if ($property_fields["WITH_DESCRIPTION"] == "Y")
                echo ' <span title="' . GetMessage("IBLOCK_AT_PROP_DESC") . '">' . GetMessage("IBLOCK_AT_PROP_DESC_1") . '<input name="' . $name . '[' . $key . '][DESCRIPTION]" value="' . htmlspecialcharsex($val_description) . '" size="18" type="text" id="' . $name . '[' . $key . '][DESCRIPTION]"></span>';

            echo "<br>";
            echo "</td></tr>";

            if ($property_fields["MULTIPLE"] != "Y") {
                $bVarsFromForm = true;
                break;
            }
        }

        if (!$bVarsFromForm || $show) {
            for ($i = 0; $i < $cnt; $i++) {
                echo "<tr><td>";
                if ($i == 0 && $bInitDef)
                    $val = $property_fields["DEFAULT_VALUE"];
                else
                    $val = "";

                if ($rows > 1)
                    echo '<textarea name="' . $name . '[n' . ($start + $i) . ']' . $strAddDesc . '" cols="' . $cols . '" rows="' . $rows . '">' . htmlspecialcharsex($val) . '</textarea>';
                else
                    echo '<input name="' . $name . '[n' . ($start + $i) . ']' . $strAddDesc . '" value="' . htmlspecialcharsex($val) . '" size="' . $cols . '" type="text">';

                if ($property_fields["WITH_DESCRIPTION"] == "Y")
                    echo ' <span title="' . GetMessage("IBLOCK_AT_PROP_DESC") . '">' . GetMessage("IBLOCK_AT_PROP_DESC_1") . '<input name="' . $name . '[n' . ($start + $i) . '][DESCRIPTION]" value="" size="18" type="text"></span>';

                echo "<br>";
                echo "</td></tr>";
            }
        }

        if ($property_fields["MULTIPLE"] == "Y") {
            echo '<tr><td><input type="button" value="' . GetMessage("IBLOCK_AT_PROP_ADD") . '" onClick="addNewRow(\'tb' . md5($name) . '\')"></td></tr>';
            echo "<script type=\"text/javascript\">BX.addCustomEvent('onAutoSaveRestore', function(ob, data) {for (var i in data){if (i.substring(0," . (strlen($name) + 1) . ")=='" . CUtil::JSEscape($name) . "['){addNewRow('tb" . md5($name) . "')}}})</script>";
        }

    }
}

if (!function_exists("_ShowGroupPropertyField")) {
    function _ShowGroupPropertyField($name, $property_fields, $values)
    {
        if (!is_array($values))
            $values = array();

        foreach ($values as $key => $value) {
            if (is_array($value) && array_key_exists("VALUE", $value))
                $values[$key] = $value["VALUE"];
        }

        $res = "";
        $bWas = false;
        $sections = CIBlockSection::GetList(
            array("left_margin" => "asc"),
            array("IBLOCK_ID" => $property_fields["LINK_IBLOCK_ID"]),
            false,
            array("ID", "DEPTH_LEVEL", "NAME")
        );
        while ($ar = $sections->GetNext()) {
            $res .= '<option value="' . $ar["ID"] . '"';
            if (in_array($ar["ID"], $values)) {
                $bWas = true;
                $res .= ' selected';
            }
            $res .= '>' . str_repeat(" . ", $ar["DEPTH_LEVEL"] - 1) . $ar["NAME"] . '</option>';
        }

        echo '<select name="' . $name . '[]" size="' . $property_fields["MULTIPLE_CNT"] . '" ' . ($property_fields["MULTIPLE"] == "Y" ? "multiple" : "") . '>';
        echo '<option value=""' . (!$bWas ? ' selected' : '') . '>' . GetMessage("IBLOCK_AT_NOT_SET") . '</option>';
        echo $res;
        echo '</select>';
    }
}

if (!function_exists("_ShowElementPropertyField")) {
    function _ShowElementPropertyField($name, $property_fields, $values, $bVarsFromForm = false)
    {
        global $bCopy;
        $index = 0;
        $show = true;

        $MULTIPLE_CNT = intval($property_fields["MULTIPLE_CNT"]);
        if ($MULTIPLE_CNT <= 0 || $MULTIPLE_CNT > 30)
            $MULTIPLE_CNT = 5;

        $cnt = ($property_fields["MULTIPLE"] == "Y" ? $MULTIPLE_CNT : 1);

        if (!is_array($values))
            $values = array();

        $fixIBlock = $property_fields["LINK_IBLOCK_ID"] > 0;

        echo '<table cellpadding="0" cellspacing="0" border="0" class="nopadding" width="100%" id="tb' . md5($name) . '">';
        foreach ($values as $key => $val) {
            $show = false;
            if ($bCopy) {
                $key = "n" . $index;
                $index++;
            }

            if (is_array($val) && array_key_exists("VALUE", $val))
                $val = $val["VALUE"];

            $db_res = CIBlockElement::GetByID($val);
            $ar_res = $db_res->GetNext();
            echo '<tr><td>' .
                '<input name="' . $name . '[' . $key . ']" id="' . $name . '[' . $key . ']" value="' . htmlspecialcharsex($val) . '" size="5" type="text">' .
                '<input type="button" value="..." onClick="jsUtils.OpenWindow(\'/bitrix/admin/iblock_element_search.php?lang=' . LANGUAGE_ID . '&amp;IBLOCK_ID=' . $property_fields["LINK_IBLOCK_ID"] . '&amp;n=' . $name . '&amp;k=' . $key . ($fixIBlock ? '&amp;iblockfix=y' : '') . '\', 900, 700);">' .
                '&nbsp;<span id="sp_' . md5($name) . '_' . $key . '" >' . $ar_res['NAME'] . '</span>' .
                '</td></tr>';

            if ($property_fields["MULTIPLE"] != "Y") {
                $bVarsFromForm = true;
                break;
            }
        }

        if (!$bVarsFromForm || $show) {
            for ($i = 0; $i < $cnt; $i++) {
                $val = "";
                $key = "n" . $index;
                $index++;

                echo '<tr><td>' .
                    '<input name="' . $name . '[' . $key . ']" id="' . $name . '[' . $key . ']" value="' . htmlspecialcharsex($val) . '" size="5" type="text">' .
                    '<input type="button" value="..." onClick="jsUtils.OpenWindow(\'/bitrix/admin/iblock_element_search.php?lang=' . LANGUAGE_ID . '&amp;IBLOCK_ID=' . $property_fields["LINK_IBLOCK_ID"] . '&amp;n=' . $name . '&amp;k=' . $key . ($fixIBlock ? '&amp;iblockfix=y' : '') . '\', 900, 700);">' .
                    '&nbsp;<span id="sp_' . md5($name) . '_' . $key . '"></span>' .
                    '</td></tr>';
            }
        }

        if ($property_fields["MULTIPLE"] == "Y") {
            echo '<tr><td>' .
                '<input type="button" value="' . GetMessage("IBLOCK_AT_PROP_ADD") . '..." onClick="jsUtils.OpenWindow(\'/bitrix/admin/iblock_element_search.php?lang=' . LANGUAGE_ID . '&amp;IBLOCK_ID=' . $property_fields["LINK_IBLOCK_ID"] . '&amp;n=' . $name . '&amp;m=y&amp;k=' . $key . ($fixIBlock ? '&amp;iblockfix=y' : '') . '\', 900, 700);">' .
                '<span id="sp_' . md5($name) . '_' . $key . '" ></span>' .
                '</td></tr>';
        }

        echo '</table>';
        echo '<script type="text/javascript">' . "\r\n";
        echo "var MV_" . md5($name) . " = " . $index . ";\r\n";
        echo "function InS" . md5($name) . "(id, name){ \r\n";
        echo "	oTbl=document.getElementById('tb" . md5($name) . "');\r\n";
        echo "	oRow=oTbl.insertRow(oTbl.rows.length-1); \r\n";
        echo "	oCell=oRow.insertCell(-1); \r\n";
        echo "	oCell.innerHTML=" .
            "'<input name=\"" . $name . "[n'+MV_" . md5($name) . "+']\" value=\"'+id+'\" id=\"" . $name . "[n'+MV_" . md5($name) . "+']\" size=\"5\" type=\"text\">'+\r\n" .
            "'<input type=\"button\" value=\"...\" '+\r\n" .
            "'onClick=\"jsUtils.OpenWindow(\'/bitrix/admin/iblock_element_search.php?lang=" . LANGUAGE_ID . "&amp;IBLOCK_ID=" . $property_fields["LINK_IBLOCK_ID"] . "&amp;n=" . $name . "&amp;k=n'+MV_" . md5($name) . "+'" . ($fixIBlock ? '&amp;iblockfix=y' : '') . "\', '+\r\n" .
            "' 900, 700);\">'+" .
            "'&nbsp;<span id=\"sp_" . md5($name) . "_'+MV_" . md5($name) . "+'\" >'+name+'</span>" .
            "';";
        echo 'MV_' . md5($name) . '++;';
        echo '}';
        echo "\r\n</script>";
    }
}

if (!function_exists("_ShowFilePropertyField")) {
    function _ShowFilePropertyField($name, $property_fields, $values, $max_file_size_show = 50000, $bVarsFromForm = false)
    {
        global $bCopy, $historyId;

        static $maxSize = array();
        if (empty($maxSize)) {
            $detailImageSize = (int)Main\Config\Option::get('iblock', 'detail_image_size');
            $maxSize = array(
                'W' => $detailImageSize,
                'H' => $detailImageSize
            );
            unset($detailImageSize);
        }

        CModule::IncludeModule('fileman');
        $bVarsFromForm = false;

        if (empty($values) || $bCopy || !is_array($values)) {
            $values = array(
                "n0" => 0,
            );
        }

        if ($property_fields["MULTIPLE"] == "N") {
            foreach ($values as $key => $val) {
                if (is_array($val))
                    $file_id = $val["VALUE"];
                else
                    $file_id = $val;

                if ($historyId > 0)
                    echo CFileInput::Show($name . "[" . $key . "]", $file_id, array(
                        "IMAGE" => "Y",
                        "PATH" => "Y",
                        "FILE_SIZE" => "Y",
                        "DIMENSIONS" => "Y",
                        "IMAGE_POPUP" => "Y",
                        "MAX_SIZE" => $maxSize,
                    ));
                else
                    echo CFileInput::Show($name . "[" . $key . "]", $file_id, array(
                        "IMAGE" => "Y",
                        "PATH" => "Y",
                        "FILE_SIZE" => "Y",
                        "DIMENSIONS" => "Y",
                        "IMAGE_POPUP" => "Y",
                        "MAX_SIZE" => $maxSize,
                    ), array(
                        'upload' => true,
                        'medialib' => true,
                        'file_dialog' => true,
                        'cloud' => true,
                        'del' => true,
                        'description' => $property_fields["WITH_DESCRIPTION"] == "Y",
                    ));
                break;
            }
        } else {
            $inputName = array();
            foreach ($values as $key => $val) {
                if (is_array($val))
                    $inputName[$name . "[" . $key . "]"] = $val["VALUE"];
                else
                    $inputName[$name . "[" . $key . "]"] = $val;
            }

            if (class_exists('\Bitrix\Main\UI\FileInput', true)) {
                echo \Bitrix\Main\UI\FileInput::createInstance((
                    array(
                        "name" => $name . "[n#IND#]",
                        "id" => $name . "[n#IND#]_" . mt_rand(1, 1000000),
                        "description" => $property_fields["WITH_DESCRIPTION"] == "Y",
                        "allowUpload" => "F",
                        "allowUploadExt" => $property_fields["FILE_TYPE"]
                    ) + ($historyId > 0 ? array(
                        "delete" => false,
                        "edit" => false
                    ) : array(
                        "upload" => true,
                        "medialib" => true,
                        "fileDialog" => true,
                        "cloud" => true
                    ))
                ))->show($inputName);
            } else if ($historyId > 0)
                echo CFileInput::ShowMultiple($inputName, $name . "[n#IND#]", array(
                    "IMAGE" => "Y",
                    "PATH" => "Y",
                    "FILE_SIZE" => "Y",
                    "DIMENSIONS" => "Y",
                    "IMAGE_POPUP" => "Y",
                    "MAX_SIZE" => $maxSize,
                ), false);
            else
                echo CFileInput::ShowMultiple($inputName, $name . "[n#IND#]", array(
                    "IMAGE" => "Y",
                    "PATH" => "Y",
                    "FILE_SIZE" => "Y",
                    "DIMENSIONS" => "Y",
                    "IMAGE_POPUP" => "Y",
                    "MAX_SIZE" => $maxSize,
                ), false, array(
                    'upload' => true,
                    'medialib' => true,
                    'file_dialog' => true,
                    'cloud' => true,
                    'del' => true,
                    'description' => $property_fields["WITH_DESCRIPTION"] == "Y",
                ));
        }
    }
}

if (!function_exists("_ShowListPropertyField")) {
    function _ShowListPropertyField($name, $property_fields, $values, $bInitDef = false, $def_text = false)
    {
        if (!is_array($values))
            $values = array();

        foreach ($values as $key => $value) {
            if (is_array($value) && array_key_exists("VALUE", $value))
                $values[$key] = $value["VALUE"];
        }

        $id = $property_fields["ID"];
        $multiple = $property_fields["MULTIPLE"];
        $res = "";
        if ($property_fields["LIST_TYPE"] == "C") //list property as checkboxes
        {
            $cnt = 0;
            $wSel = false;
            $prop_enums = CIBlockProperty::GetPropertyEnum($id);
            while ($ar_enum = $prop_enums->Fetch()) {
                $cnt++;
                if ($bInitDef)
                    $sel = ($ar_enum["DEF"] == "Y");
                else
                    $sel = in_array($ar_enum["ID"], $values);
                if ($sel)
                    $wSel = true;

                $uniq = md5(uniqid(rand(), true));
                if ($multiple == "Y") //multiple
                    $res .= '<input type="checkbox" name="' . $name . '[]" value="' . htmlspecialcharsbx($ar_enum["ID"]) . '"' . ($sel ? " checked" : "") . ' id="' . $uniq . '"><label for="' . $uniq . '">' . htmlspecialcharsex($ar_enum["VALUE"]) . '</label><br>';
                else //if(MULTIPLE=="Y")
                    $res .= '<input type="radio" name="' . $name . '[]" id="' . $uniq . '" value="' . htmlspecialcharsbx($ar_enum["ID"]) . '"' . ($sel ? " checked" : "") . '><label for="' . $uniq . '">' . htmlspecialcharsex($ar_enum["VALUE"]) . '</label><br>';

                if ($cnt == 1)
                    $res_tmp = '<input type="checkbox" name="' . $name . '[]" value="' . htmlspecialcharsbx($ar_enum["ID"]) . '"' . ($sel ? " checked" : "") . ' id="' . $uniq . '"><br>';
            }


            $uniq = md5(uniqid(rand(), true));

            if ($cnt == 1)
                $res = $res_tmp;
            elseif ($multiple != "Y")
                $res = '<input type="radio" name="' . $name . '[]" value=""' . (!$wSel ? " checked" : "") . ' id="' . $uniq . '"><label for="' . $uniq . '">' . htmlspecialcharsex(($def_text ? $def_text : GetMessage("IBLOCK_AT_PROP_NO"))) . '</label><br>' . $res;

            if ($multiple == "Y" || $cnt == 1)
                $res = '<input type="hidden" name="' . $name . '" value="">' . $res;
        } else //list property as list
        {
            $bNoValue = true;
            $prop_enums = CIBlockProperty::GetPropertyEnum($id);
            while ($ar_enum = $prop_enums->Fetch()) {
                if ($bInitDef)
                    $sel = ($ar_enum["DEF"] == "Y");
                else
                    $sel = in_array($ar_enum["ID"], $values);
                if ($sel)
                    $bNoValue = false;
                $res .= '<option value="' . htmlspecialcharsbx($ar_enum["ID"]) . '"' . ($sel ? " selected" : "") . '>' . htmlspecialcharsex($ar_enum["VALUE"]) . '</option>';
            }

            if ($property_fields["MULTIPLE"] == "Y" && IntVal($property_fields["ROW_COUNT"]) < 2)
                $property_fields["ROW_COUNT"] = 5;
            if ($property_fields["MULTIPLE"] == "Y")
                $property_fields["ROW_COUNT"]++;
            $res = '<select name="' . $name . '[]" size="' . $property_fields["ROW_COUNT"] . '" ' . ($property_fields["MULTIPLE"] == "Y" ? "multiple" : "") . '>' .
                '<option value=""' . ($bNoValue ? ' selected' : '') . '>' . htmlspecialcharsex(($def_text ? $def_text : GetMessage("IBLOCK_AT_PROP_NA"))) . '</option>' .
                $res .
                '</select>';
        }
        echo $res;
    }
}

if (!function_exists("_ShowUserPropertyField")) {
    function _ShowUserPropertyField($name, $property_fields, $values, $bInitDef = false, $bVarsFromForm = false, $max_file_size_show = 50000, $form_name = "form_element", $bCopy = false)
    {
        global $bCopy;
        $start = 0;

        if (!is_array($property_fields["~VALUE"]))
            $values = array();
        else
            $values = $property_fields["~VALUE"];

        unset($property_fields["VALUE"]);
        unset($property_fields["~VALUE"]);

        $html = '<table cellpadding="0" cellspacing="0" border="0" class="nopadding" width="100%" id="tb' . md5($name) . '">';
        $arUserType = CIBlockProperty::GetUserType($property_fields["USER_TYPE"]);
        $bMultiple = $property_fields["MULTIPLE"] == "Y" && array_key_exists("GetPropertyFieldHtmlMulty", $arUserType);
        $max_val = -1;

        if (($arUserType["PROPERTY_TYPE"] !== "F") || (!$bCopy)) {
            if ($bMultiple) {
                $html .= '<tr><td>';
                $html .= call_user_func_array($arUserType["GetPropertyFieldHtmlMulty"],
                    array(
                        $property_fields,
                        $values,
                        array(
                            "VALUE" => 'PROP[' . $property_fields["ID"] . ']',
                            "FORM_NAME" => $form_name,
                            "MODE" => "FORM_FILL",
                            "COPY" => $bCopy,
                        ),
                    ));
                $html .= '</td></tr>';
            } else {
                foreach ($values as $key => $val) {
                    if ($bCopy) {
                        $key = "n" . $start;
                        $start++;
                    }

                    if (!is_array($val) || !array_key_exists("VALUE", $val))
                        $val = array("VALUE" => $val, "DESCRIPTION" => "");

                    $html .= '<tr><td>';
                    if (array_key_exists("GetPropertyFieldHtml", $arUserType))
                        $html .= call_user_func_array($arUserType["GetPropertyFieldHtml"],
                            array(
                                $property_fields,
                                $val,
                                array(
                                    "VALUE" => 'PROP[' . $property_fields["ID"] . '][' . $key . '][VALUE]',
                                    "DESCRIPTION" => 'PROP[' . $property_fields["ID"] . '][' . $key . '][DESCRIPTION]',
                                    "FORM_NAME" => $form_name,
                                    "MODE" => "FORM_FILL",
                                    "COPY" => $bCopy,
                                ),
                            ));
                    else
                        $html .= '&nbsp;';
                    $html .= '</td></tr>';

                    if (substr($key, -1, 1) == 'n' && $max_val < intval(substr($key, 1)))
                        $max_val = intval(substr($key, 1));
                    if ($property_fields["MULTIPLE"] != "Y") {
                        $bVarsFromForm = true;
                        break;
                    }
                }
            }
        }

        if (
            (!$bVarsFromForm && !$bMultiple)
            || ($bVarsFromForm && !$bMultiple && count($values) == 0) //Was not displayed
        ) {
            $bDefaultValue = is_array($property_fields["DEFAULT_VALUE"]) || strlen($property_fields["DEFAULT_VALUE"]);

            if ($property_fields["MULTIPLE"] == "Y") {
                $cnt = (int)$property_fields["MULTIPLE_CNT"];
                if ($cnt <= 0 || $cnt > 30)
                    $cnt = 5;

                if ($bInitDef && $bDefaultValue)
                    $cnt++;
            } else {
                $cnt = 1;
            }

            for ($i = $max_val + 1; $i < $max_val + 1 + $cnt; $i++) {
                if ($i == 0 && $bInitDef && $bDefaultValue)
                    $val = array(
                        "VALUE" => $property_fields["DEFAULT_VALUE"],
                        "DESCRIPTION" => "",
                    );
                else
                    $val = array(
                        "VALUE" => "",
                        "DESCRIPTION" => "",
                    );

                $key = "n" . ($start + $i);

                $html .= '<tr><td>';
                if (array_key_exists("GetPropertyFieldHtml", $arUserType))
                    $html .= call_user_func_array($arUserType["GetPropertyFieldHtml"],
                        array(
                            $property_fields,
                            $val,
                            array(
                                "VALUE" => 'PROP[' . $property_fields["ID"] . '][' . $key . '][VALUE]',
                                "DESCRIPTION" => 'PROP[' . $property_fields["ID"] . '][' . $key . '][DESCRIPTION]',
                                "FORM_NAME" => $form_name,
                                "MODE" => "FORM_FILL",
                                "COPY" => $bCopy,
                            ),
                        ));
                else
                    $html .= '&nbsp;';
                $html .= '</td></tr>';
            }
            $max_val += $cnt;
        }
        if (
            $property_fields["MULTIPLE"] == "Y"
            && $arUserType["USER_TYPE"] !== "HTML"
            && $arUserType["USER_TYPE"] !== "employee"
            && !$bMultiple
        ) {
            $html .= '<tr><td><input type="button" value="' . GetMessage("IBLOCK_AT_PROP_ADD") . '" onClick="addNewRow(\'tb' . md5($name) . '\')"></td></tr>';
        }
        $html .= '</table>';
        echo $html;
    }
}

if (!function_exists("_ShowPropertyField")) {
    function _ShowPropertyField($name, $property_fields, $values, $bInitDef = false, $bVarsFromForm = false, $max_file_size_show = 50000, $form_name = "form_element", $bCopy = false)
    {
        $type = $property_fields["PROPERTY_TYPE"];
        if ($property_fields["USER_TYPE"] != "")
            _ShowUserPropertyField($name, $property_fields, $values, $bInitDef, $bVarsFromForm, $max_file_size_show, $form_name, $bCopy);
        elseif ($type == "L") //list property
            _ShowListPropertyField($name, $property_fields, $values, $bInitDef);
        elseif ($type == "F") //file property
            _ShowFilePropertyField($name, $property_fields, $values, $max_file_size_show, $bVarsFromForm);
        elseif ($type == "G") //section link
        {
            if (function_exists("_ShowGroupPropertyField_custom"))
                _ShowGroupPropertyField_custom($name, $property_fields, $values, $bVarsFromForm);
            else
                _ShowGroupPropertyField($name, $property_fields, $values, $bVarsFromForm);
        } elseif ($type == "E") //element link
            _ShowElementPropertyField($name, $property_fields, $values, $bVarsFromForm);
        else
            _ShowStringPropertyField($name, $property_fields, $values, $bInitDef, $bVarsFromForm);
    }
}

if (!function_exists("_ShowHiddenValue")) {
    function _ShowHiddenValue($name, $value)
    {
        $res = "";

        if (is_array($value)) {
            foreach ($value as $k => $v)
                $res .= _ShowHiddenValue($name . '[' . htmlspecialcharsbx($k) . ']', $v);
        } else {
            $res .= '<input type="hidden" name="' . $name . '" value="' . htmlspecialcharsbx($value) . '">' . "\n";
        }

        return $res;
    }
}