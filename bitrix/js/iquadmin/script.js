/**
 * Created by alex on 26.06.17.
 */

var dbclickEvent = new MouseEvent('dblclick', {
    'view': window,
    'bubbles': true,
    'cancelable': true
});

document.addEventListener("click", function (e) {
    var target = e.target;

    if (target.classList.contains("js-close_modals")){
        closeModals();
    }
});

var cropper_count = 0;
var imageCropper = function(params){
    if ((!params.image || !params.input_file || !params.input_label) && (params.cropCallback !== undefined && typeof params.cropCallback !== "function")){
        console.error("Wrong imageCropper params");
        return false;
    }
    this.params = params;
    this.input_file = params.input_file;
    this.input_label = params.input_label;
    this.image = params.image;
    this.show_preview = params.show_preview;
    this.modal = document.querySelector("#change-image-modal");
    this.popupParams = {
        modal_id: "change-image-modal",
        file: this.modal.querySelector("#image_cropped"),
        image: this.modal.querySelector(".js-cropper-image")
    };
    this.cropperParams = {
        image_id: "cropper-image",
        reset: document.querySelector("#cropper-reset"),
        apply: document.querySelector("#cropper-apply")
    };
    this.cropper_image = $("body").find("#" + this.cropperParams.image_id);
    this.cropper_number = cropper_count;
    cropper_count++;
};
imageCropper.prototype.Init = function(){
    this.AddHandlers();
};
imageCropper.prototype.InitCrop = function(){
    this.cropper_image.cropper({
        minContainerWidth: 300,
        minContainerHeight: 300,
        aspectRatio: 1 / 1,
        preview: "#cropper-preview",
        movable: false,
        zoomOnWheel: false,
        zoomOnTouch: false,
        scalable: false,
        viewMode: 1,
        zoomable: false,
        toggleDragModeOnDblclick: false,
        dragMode: 'move',
        crop: function(e) {
            // Output the result data for cropping image.
        }
    });
    if (this.show_preview === true){
        document.querySelector("#cropper-preview").style.display = "block";
    }
};
imageCropper.prototype.Check = function(){
    var self = this;
    return (self.modal.dataset.cropper_number && parseInt(self.modal.dataset.cropper_number) === parseInt(self.cropper_number));
};
imageCropper.prototype.ShowModal = function(){
    var self = this;
    self.Refresh();
    self.modal.dataset.cropper_number = self.cropper_number;
    callPopup(self.popupParams.modal_id);
};
imageCropper.prototype.AddHandlers = function(){
    var self = this;

    if (self.image) {
        self.input_label = document.querySelectorAll("label[for='" + self.input_file.id + "']");

        for (var label, i = 0, n = self.input_label.length; i < n; i++) {
            if (self.input_label.hasOwnProperty(i)) {
                label = self.input_label[i];
                label.addEventListener("click", function (e) {
                    e.preventDefault();
                    self.ShowModal();
                });
            }
        }

        // self.input_label.addEventListener("click", function(e){
        //     e.preventDefault();
        //     self.Refresh();
        //     callPopup(self.popupParams.modal_id);
        //     self.modal.dataset.cropper_number = self.cropper_number;
        // });
    }
    self.popupParams.file.addEventListener("change", function(e){
        if (!self.Check()){
            return false;
        }
        sendFile(this, function (result) {
            if (result.status === true) {
                self.popupParams.image.src = result.file;
                self.InitCrop();
                self.modal.querySelector("#cropper-reset").classList.remove("hidden");
            }
        })
    });
    self.cropperParams.reset.addEventListener('click', function(e){
        if (!self.Check()){
            return false;
        }
        e.preventDefault();
        self.cropper_image.cropper('crop');
    });
    self.cropperParams.apply.addEventListener('click', function(e){
        if (!self.Check()){
            return false;
        }
        e.preventDefault();
        self.Crop();
    });
};
imageCropper.prototype.Refresh = function(){
    this.modal.querySelector("#cropper-reset").classList.add("hidden");
    this.modal.querySelector(".progress_bar").classList.add("hidden");
    this.cropper_image.cropper('clear');
    this.cropper_image.cropper('destroy');
    this.cropper_image.attr("src", '');
    this.popupParams.file.value = "";
};
imageCropper.prototype.Crop = function(){
    var self = this;
    var cropped_data = self.cropper_image.cropper('getData', 'rounded');
    var data = new FormData();

    data.append("action", "crop_image");
    data.append("image", self.popupParams.image.src);
    toFormData(cropped_data, data, "crop_data");

    var ajaxOptions = {
        body: data,
        callback: function(res){
            var result = JSON.parse(res);
            if (result.status === "success") {
                if (self.image !== undefined) {
                    if (self.image.tagName === "IMG") {
                        self.image.src = result.image.image;
                    } else {
                        self.image.style.backgroundImage = 'url("' + result.image.image + '")';
                    }
                    closeModals();
                    self.input_file.value = result.image.image;
                    $(self.input_file).change();

                    if (self.params.personal === true) {
                        $("body").find("#personal_form").submit();
                    }
                }
            }

            if (self.params.cropCallback !== undefined && typeof self.params.cropCallback === "function") {
                self.params.cropCallback(result);
            }
        }
    };

    ajaxRequest(ajaxOptions);
};

function ajaxRequest(options){
    var url = options.url || globals.ajaxFile,
        method = (options.method === "GET") ? "GET" : 'POST',
        callback = options.callback || function(res){
                console.log(res);
            },
        data = options.data || {},
        xmlHttp = new  XMLHttpRequest();

    var body = '';

    xmlHttp.open(method,url);

    if (options.body) {
        body = options.body;
    } else {
        xmlHttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        var i = 0;

        for (var key in data) {
            if (data.hasOwnProperty(key)) {
                body += (i == 0) ? '' : '&';
                body += key + '=' + encodeURIComponent(data[key]);
                i++;
            }
        }
    }

    xmlHttp.send(body);

    xmlHttp.onreadystatechange = function(){
        if(xmlHttp.status === 200 && xmlHttp.readyState ===4){
            callback(xmlHttp.responseText);
        }
    };
}

function callPopup(modalName, disableBG){
    if (disableBG === undefined){
        disableBG = false;
    }
    body.find('.modal.active').hide();
    body.find('.examNav-burger').removeClass('is-active');
    body.addClass('open-modal-js');
    body.find('.modal-bg').fadeIn();
    body.find('#' + modalName).fadeIn().addClass('active');
    if (disableBG){
        body.find(".modal-bg").addClass("js-disabled");
    }

}

function sendFile(el, success) {

    var file = $(el),
        files = el.files;

    if (file.val().length > 0) {
        var data = new FormData();

        $.each( files, function( key, value ){
            data.append( key, value );
        });

        var parent = file.closest("form");
        var submit = parent.find("button");
        var progressBar = parent.find(".progress_bar");

        if (submit) {
            submit.disabled = true;
        }
        if (progressBar) {
            progressBar.css("backgroundColor", "red");
            progressBar.removeClass("hidden");
        }

        $.ajax({
            url: globals.ajax + "load_file.php",
            data: data,
            type: "post",
            cache: false,
            dataType: "json",
            processData: false,
            contentType: false,
            xhr: function() {
                var xhr = new window.XMLHttpRequest();
                // Upload progress
                xhr.upload.addEventListener("progress", function(evt){
                    if (evt.lengthComputable) {
                        var percentComplete = evt.loaded / evt.total;

                        if (progressBar) {
                            progressBar.css("width", ((percentComplete * 100) + "%"));
                        }

                        if (percentComplete == 1) {
                            if (progressBar) {
                                progressBar.css("backgroundColor", "green");
                            }
                        }
                        // Do something with upload progress
                    }
                }, false);

                return xhr;
            },
            success: function(result) {
                if (submit){
                    submit.attr("disabled", false);
                }
                if (result.file.length > 0){
                    success(result);
                }
            }
        });
    }
}

function toFormData(obj, form, namespace) {
    var fd = form || new FormData();
    var formKey;

    for(var property in obj) {
        if(obj.hasOwnProperty(property) && obj[property]) {
            if (namespace) {
                formKey = namespace + '[' + property + ']';
            } else {
                formKey = property;
            }

            // if the property is an object, but not a File, use recursivity.
            if (obj[property] instanceof Date) {
                fd.append(formKey, obj[property].toISOString());
            }
            else if (typeof obj[property] === 'object' && !(obj[property] instanceof File)) {
                toFormData(obj[property], fd, formKey);
            } else { // if it's a string or a File object
                fd.append(formKey, obj[property]);
            }
        }
    }

    return fd;
}

function closeModals(){
    body.removeClass('open-modal-js');

    body.find(".modal").fadeOut();
    body.find('.modal-bg').fadeOut();


}

function addNewRow(tableID, row_to_clone)
{
    var tbl = document.getElementById(tableID);
    var cnt = tbl.rows.length;
    if(row_to_clone == null)
        row_to_clone = -2;
    var sHTML = tbl.rows[cnt+row_to_clone].cells[0].innerHTML;
    var oRow = tbl.insertRow(cnt+row_to_clone+1);
    var oCell = oRow.insertCell(0);

    var s, e, n, p;
    p = 0;
    while(true)
    {
        s = sHTML.indexOf('[n',p);
        if(s<0)break;
        e = sHTML.indexOf(']',s);
        if(e<0)break;
        n = parseInt(sHTML.substr(s+2,e-s));
        sHTML = sHTML.substr(0, s)+'[n'+(++n)+']'+sHTML.substr(e+1);
        p=s+1;
    }
    p = 0;
    while(true)
    {
        s = sHTML.indexOf('__n',p);
        if(s<0)break;
        e = sHTML.indexOf('_',s+2);
        if(e<0)break;
        n = parseInt(sHTML.substr(s+3,e-s));
        sHTML = sHTML.substr(0, s)+'__n'+(++n)+'_'+sHTML.substr(e+1);
        p=e+1;
    }
    p = 0;
    while(true)
    {
        s = sHTML.indexOf('__N',p);
        if(s<0)break;
        e = sHTML.indexOf('__',s+2);
        if(e<0)break;
        n = parseInt(sHTML.substr(s+3,e-s));
        sHTML = sHTML.substr(0, s)+'__N'+(++n)+'__'+sHTML.substr(e+2);
        p=e+2;
    }
    p = 0;
    while(true)
    {
        s = sHTML.indexOf('xxn',p);
        if(s<0)break;
        e = sHTML.indexOf('xx',s+2);
        if(e<0)break;
        n = parseInt(sHTML.substr(s+3,e-s));
        sHTML = sHTML.substr(0, s)+'xxn'+(++n)+'xx'+sHTML.substr(e+2);
        p=e+2;
    }
    p = 0;
    while(true)
    {
        s = sHTML.indexOf('%5Bn',p);
        if(s<0)break;
        e = sHTML.indexOf('%5D',s+3);
        if(e<0)break;
        n = parseInt(sHTML.substr(s+4,e-s));
        sHTML = sHTML.substr(0, s)+'%5Bn'+(++n)+'%5D'+sHTML.substr(e+3);
        p=e+3;
    }
    oCell.innerHTML = sHTML;

    var patt = new RegExp ("<"+"script"+">[^\000]*?<"+"\/"+"script"+">", "ig");
    var code = sHTML.match(patt);
    if(code)
    {
        for(var i = 0; i < code.length; i++)
        {
            if(code[i] != '')
            {
                s = code[i].substring(8, code[i].length-9);
                jsUtils.EvalGlobal(s);
            }
        }
    }

    if (BX && BX.adminPanel)
    {
        BX.adminPanel.modifyFormElements(oRow);
        BX.onCustomEvent('onAdminTabsChange');
    }

    setTimeout(function() {
        var r = BX.findChildren(oCell, {tag: /^(input|select|textarea)$/i});
        if (r && r.length > 0)
        {
            for (var i=0,l=r.length;i<l;i++)
            {
                if (r[i].form && r[i].form.BXAUTOSAVE)
                    r[i].form.BXAUTOSAVE.RegisterInput(r[i]);
                else
                    break;
            }
        }
    }, 10);
}

function checkNext(){
    var next = document.querySelector("#sys_conspects .adm-list-table-row");
    if (next) {
        next.dispatchEvent(dbclickEvent);
    }
}