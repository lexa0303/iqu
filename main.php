<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 15.02.17
 * Time: 14:00
 */
?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Description");
$APPLICATION->SetPageProperty("keywords", "keywords");?>
<?
global $APPLICATION;
global $arUser;
?>
<? $APPLICATION->IncludeComponent(
	"custom:main", 
	".default", 
	array(
		"CACHE_TIME" => "30",
		"CACHE_TYPE" => "N",
		"IBLOCK_ID" => "",
		"IBLOCK_TYPE" => "news",
		"COMPONENT_TEMPLATE" => ".default",
		"SEF_MODE" => "Y",
		"SEF_FOLDER" => "/",
		"USER_TITLE" => $arUser["TITLE"],
		"CONSPECTS_HLBLOCK" => "6",
		"CATEGORIES_HLBLOCK" => "4",
		"CARDS_HLBLOCK" => "3",
		"CONSPECTS_HLIBLOCK" => "",
		"CATEGORY_CATEGORY_LIST_TITLE" => "Y",
		"CATEGORY_PAGEN_COUNT" => "5",
		"LIBRARY_PAGEN_COUNT" => "16",
		"CATEGORY_PAGEN_TEMPLATE" => "",
		"LIBRARY_PAGEN_TEMPLATE" => "infinity",
		"CATEGORY_SET_LIST_TITLE" => "Y",
		"MASTER_NEEDED" => array(
			0 => "conspect_edit",
			1 => "",
		),
		"NO_AUTH_PAGES" => array(
			0 => "exam",
			1 => "lesson",
			2 => "conspect_add",
			3 => "conspect_edit",
			4 => "exam_result",
			5 => "",
		),
		"SEF_URL_TEMPLATES" => array(
			"main_page" => "",
			"conspect_add" => "conspects/add/",
			"conspect_detail" => "conspects/#USER_TITLE#/#CONSPECT_ID#/",
			"conspect_edit" => "conspects/#USER_TITLE#/#CONSPECT_ID#/edit/",
			"conspect_list" => "conspects/#USER_TITLE#/",
			"category_detail" => "category/#USER_TITLE#/#CATEGORY_ID#/",
			"categories_list" => "categories/#USER_TITLE#/",
			"exam" => "conspects/#USER_TITLE#/#CONSPECT_ID#/exam/",
			"lesson" => "conspects/#USER_TITLE#/#CONSPECT_ID#/lesson/",
			"search" => "search/",
			"new" => "new/",
            "exam_result" => "conspects/#USER_TITLE#/#CONSPECT_ID#/exam/#RESULT_ID#/",
		)
	),
	false
); ?>
<?if ($USER->IsAuthorized()) : ?>
	<?/*$APPLICATION->IncludeComponent(
		"custom:notifications",
		"",
		Array(
			"NOTIFICATIONS_HLBLOCK" => "7",
			"USER_ID" => $USER->GetID()
		)
	);*/?>
<?endif;?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
