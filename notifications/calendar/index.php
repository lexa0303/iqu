<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 04.07.17
 * Time: 16:43
 */
?>
<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
global $USER;
if (!$USER->IsAuthorized()) {
    LocalRedirect(SITE_DIR);
}
$APPLICATION->SetTitle("Календарь напоминаний");
?>
<section class="calendar-page__section">
    <div class="container">
        <?$APPLICATION->IncludeComponent(
            "custom:notifications",
            "calendar_page",
            Array(
                "SHOW_ALL" => "Y"
            )
        );?>
    </div>
</section>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
