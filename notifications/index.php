<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
global $USER;
if (!$USER->IsAuthorized()) {
    LocalRedirect(SITE_DIR);
}
$APPLICATION->SetTitle("Напоминания");
?>
<section class="library__wrapper">
    <div class="container">
        <? $APPLICATION->IncludeComponent(
            "custom:notifications",
            "notification_page",
            Array()
        ); ?>
    </div>
</section>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>