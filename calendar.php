<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Calendar");
?>


<div class="container">
    <h1 class="section__heading">
        <span class="section__heading-txt">
            Календарь напоминаний
        </span>
    </h1>
    <!--    <div id='external-events'>-->
    <!--        <h4>Перетащите напоминание на желаемый день</h4>-->
    <!--        <div class='fc-event'>Рыбки</div>-->
    <!--        <div class='fc-event'>Английский</div>-->
    <!--        <div class='fc-event'>Апгрейд Русского</div>-->
    <!--        <div class='fc-event'>АТБ</div>-->
    <!--        <div class='fc-event'>Пелёнки</div>-->
    <!--        <div class='fc-event'>Рыбки</div>-->
    <!--        <div class='fc-event'>Английский</div>-->
    <!--        <div class='fc-event'>Апгрейд Русского</div>-->
    <!--        <div class='fc-event'>АТБ</div>-->
    <!--        <div class='fc-event'>Пелёнки</div>-->
    <!--        <div class='fc-event'>Рыбки</div>-->
    <!--        <div class='fc-event'>Английский</div>-->
    <!--        <div class='fc-event'>Апгрейд Русского</div>-->
    <!--        <div class='fc-event'>АТБ</div>-->
    <!--        <div class='fc-event'>Пелёнки</div>-->
    <!--        <div class='fc-event'>Рыбки</div>-->
    <!--        <div class='fc-event'>Английский</div>-->
    <!--        <div class='fc-event'>Апгрейд Русского</div>-->
    <!--        <div class='fc-event'>АТБ</div>-->
    <!--        <div class='fc-event'>Пелёнки</div>-->
    <!--    </div>-->
    <!--        <p>-->
    <!--            <input type='checkbox' id='drop-remove'/>-->
    <!--            <label for='drop-remove'>remove after drop</label>-->
    <!--        </p>-->

    <div class="calendar__aside">
        <h2>Список напоминаний:</h2>
        <ul class="calendar__notif-list">
            <li class="calendar__notif-item">
                <span class="calendar__notif-name">Котики</span>
                <button class="calendar__notif-delete" title="Удалить напоминание">&#10005;</button>
            </li>
            <li class="calendar__notif-item">
                <span class="calendar__notif-name">Английский Звери</span>
                <button class="calendar__notif-delete" title="Удалить напоминание">&#10005;</button>
            </li>
            <li class="calendar__notif-item">
                <span class="calendar__notif-name">Апгрейд Русского Языка</span>
                <button class="calendar__notif-delete" title="Удалить напоминание">&#10005;</button>
            </li>
            <li class="calendar__notif-item">
                <span class="calendar__notif-name">Котики</span>
                <button class="calendar__notif-delete" title="Удалить напоминание">&#10005;</button>
            </li>
            <li class="calendar__notif-item">
                <span class="calendar__notif-name">Английский Звери</span>
                <button class="calendar__notif-delete" title="Удалить напоминание">&#10005;</button>
            </li>
            <li class="calendar__notif-item">
                <span class="calendar__notif-name">Апгрейд Русского Языка</span>
                <button class="calendar__notif-delete" title="Удалить напоминание">&#10005;</button>
            </li>
        </ul>
    </div>

    <div id='calendar'></div>
    <div style='clear:both'></div>
</div>


<script>
    /* Calendar Notification START */
    $(document).ready(function () {


        /* initialize the external events
         -----------------------------------------------------------------*/

        $('body').find('#external-events .fc-event').each(function () {

            // store data so the calendar knows to render an event upon drop
            $(this).data('event', {
                title: $.trim($(this).text()), // use the element's text as the event title
                stick: true // maintain when user navigates (see docs on the renderEvent method)

            });

            // make the event draggable using jQuery UI
            $(this).draggable({
                zIndex: 999,
                revert: true,      // will cause the event to go back to its
                revertDuration: 0  //  original position after the drag
            });

        });


        /* initialize the calendar
         -----------------------------------------------------------------*/

        $('body').find('#calendar').fullCalendar({
            header: {
                left: 'prev, today',
                center: 'title',
                right: 'today, next'
                // right: 'month'
            },
            editable: true,
            droppable: true, // this allows things to be dropped onto the calendar
            eventDurationEditable: false,
            drop: function () {
                // is the "remove after drop" checkbox checked?
                if ($('#drop-remove').is(':checked')) {
                    // if so, remove the element from the "Draggable Events" list
                    $(this).remove();
                }
            },
            eventClick: function (calEvent, jsEvent, view) {

                alert('Event: ' + calEvent.title);
                alert('Coordinates: ' + jsEvent.pageX + ',' + jsEvent.pageY);
                alert('View: ' + view.name);

                // change the border color just for fun
                $(this).css('border-color', 'red');

            }
        });


    });
    /* Calendar Notification END */
</script>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>
